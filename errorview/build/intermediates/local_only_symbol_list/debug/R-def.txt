R_DEF: Internal format may change without notice
local
attr? ev_errorImage
attr? ev_retryButtonBackground
attr? ev_retryButtonText
attr? ev_retryButtonTextColor
attr? ev_showRetryButton
attr? ev_showSubtitle
attr? ev_showTitle
attr? ev_style
attr? ev_subtitle
attr? ev_subtitleAlignment
attr? ev_subtitleColor
attr? ev_title
attr? ev_titleColor
color errorView_accent
color error_view_retry_button_background
color error_view_retry_button_background_stroke
color error_view_text
color error_view_text_dark
color error_view_text_light
color error_view_title
drawable bad_gateway
drawable error_view_cloud
drawable error_view_retry_button_background
drawable network_error
drawable no_coupons
drawable no_product
font poppins_medium
font poppins_regular
font poppins_semi_bold
id center
id error_image
id error_retry
id error_subtitle
id error_title
id left
id right
layout error_view_layout
string error_could_not_load
string error_subtitle_failed_one_more_time
string error_view_oops
string error_view_retry
string no_internet
style ErrorView
styleable ErrorView ev_title ev_titleColor ev_subtitle ev_subtitleColor ev_errorImage ev_showTitle ev_showSubtitle ev_showRetryButton ev_retryButtonText ev_retryButtonBackground ev_retryButtonTextColor ev_subtitleAlignment
styleable ErrorViewStyle ev_style
