package com.purposecodes.emetwafar.fragment;

import static android.view.View.GONE;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.isValidEmail;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.isValidMobile;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.isValidPassword;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.isValidWeb;
import static com.purposecodes.emetwafar.utils.UtilsVariable.ARG_COMPANY_REGISTER;
import static com.purposecodes.emetwafar.utils.UtilsVariable.ARG_OTPMODEL;
import static com.purposecodes.emetwafar.utils.UtilsVariable.DEFAULT_COUNTRY_CODE;
import static com.purposecodes.emetwafar.utils.UtilsVariable.DEFAULT_CURRENCY;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.adapter.AddressSpinnerArrayAdapter;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.api.ApiFailureMessageListener;
import com.purposecodes.emetwafar.api.ApiSuccessListener;
import com.purposecodes.emetwafar.authentication.AccountVerificationActivity;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.model.City;
import com.purposecodes.emetwafar.model.CompanyRegister;
import com.purposecodes.emetwafar.model.State;
import com.purposecodes.emetwafar.model.Subscription;
import com.purposecodes.emetwafar.responsemodel.ErrorModel;
import com.purposecodes.emetwafar.responsemodel.OtpResponseModel;
import com.purposecodes.emetwafar.responsemodel.StateResponse;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class RegistrationFragment extends BaseFragment {

    private final String TAG = "RegistrationFragment";
    private final String OTHER_TAG = "other";
    private final String PLACE_HOLDER_TAG = "placeholder";

    private static final String ARG_POSITION = "param_position";
    private static final String ARG_SUBSCRIPTION = "param_subscription";
    private static final String ARG_TRIALMODE = "param_trial_mode";

    // Widgets
    @BindView(R.id.editText_registration_companyName)
    EditText etCompanyName;

    @BindView(R.id.editText_registration_addressLine_1)
    EditText etAddress1;

    @BindView(R.id.editText_registration_addressLine_2)
    EditText etAddress2;

    @BindView(R.id.editText_registration_poBox)
    EditText etPoBox;

    @BindView(R.id.editText_registration_companyPhoneNumber)
    EditText etCompanyPhone;

    @BindView(R.id.editText_registration_mobileNumber)
    EditText etMobileNumber;

    @BindView(R.id.editText_registration_webAddress)
    EditText etWeb;

    @BindView(R.id.editText_registration_email)
    EditText etEmail;

    @BindView(R.id.editText_registration_otherCity)
    EditText etOtherCity;

    @BindView(R.id.editText_registration_password)
    EditText etPassword;

    @BindView(R.id.editText_registration_confirmPassword)
    EditText etConfirmPassword;


    @BindView(R.id.spinner_register_subscription)
    AppCompatSpinner spinnerSubscription;

    @BindView(R.id.spinner_registration_selectState)
    AppCompatSpinner spinnerState;

    @BindView(R.id.spinner_registration_selectCity)
    AppCompatSpinner spinnerCity;

    @BindView(R.id.layout_register_otherCity)
    ViewGroup viewOtherCity;


    @BindView(R.id.layout_prgressBg)
    ViewGroup progressLayer;


    @BindView(R.id.button_reg_next)
    Button btnNext;

    @BindView(R.id.icon_registration_password)
    ToggleButton tbShowPassword;

    @BindView(R.id.icon_registration_confirmPassword)
    ToggleButton tbShowConfPassword;


    private ArrayList<Subscription> subscriptions;

    private CompanyRegister companyRegister;
    private int position;
    private String trialMode = "0";


    public static RegistrationFragment newInstance(ArrayList<Subscription> subscriptions, String _trialMode, int posi) {
        RegistrationFragment fragment = new RegistrationFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_TRIALMODE, _trialMode);
        args.putSerializable(ARG_POSITION, posi);
        args.putSerializable(ARG_SUBSCRIPTION, subscriptions);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            try {


                trialMode = getArguments().getString(ARG_TRIALMODE);
                position = getArguments().getInt(ARG_POSITION);
                subscriptions = (ArrayList<Subscription>) getArguments().getSerializable(ARG_SUBSCRIPTION);

            } catch (Exception e) {

            }
        }
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


   /*     etCompanyName.setText("metwafer");
        etAddress1.setText("metwafer Tower");
        etAddress2.setText("7 th floor,Sl Valley");

        etPoBox.setText("45986");
        etCompanyPhone.setText("02578965");
        etMobileNumber.setText("999991111");
        etWeb.setText("www.metwafer.com");
        etEmail.setText("metwafer2@yopmail.com");
        etPassword.setText("Qw@12345");
        etConfirmPassword.setText("Qw@12345");

        etCompanyName.setText("KS Mall");
        etAddress1.setText("MT Tower");
        etAddress2.setText("Opp: Al Sayyaara Petrol station");
        etPoBox.setText("78974346");
        etCompanyPhone.setText("02578965");
        etMobileNumber.setText("0895632");
        etWeb.setText("www.ks.com");
        etPassword.setText("Qw@12345");
        etConfirmPassword.setText("Qw@12345");*/

//{"status":true,"text":"Saved subscription data and reference Id","data":{"order_ref":"1605080685307","subscription_id":117}}
        // payment_type=telr&plan_id=1&order_ref=1605080685307

        // {transaction_no=030026077254, subscription_id=1, order_ref=1605080685307}
        //     token 6ab9e0978de53e4c6ca805d0e3b93c64ab19859c37e60ec387f3698ef4ed0aee


        setSubscription();
        tbShowPassword.setOnCheckedChangeListener((buttonView, isChecked) -> {
            etPassword.setTransformationMethod(isChecked ? HideReturnsTransformationMethod.getInstance() : PasswordTransformationMethod.getInstance());
            etPassword.setSelection(etPassword.getText().length());

        });


        tbShowConfPassword.setOnCheckedChangeListener((buttonView, isChecked) -> {
            etConfirmPassword.setTransformationMethod(isChecked ? HideReturnsTransformationMethod.getInstance() : PasswordTransformationMethod.getInstance());
            etConfirmPassword.setSelection(etConfirmPassword.getText().length());

        });

        getStateList();
    }


    private void getStateList() {

        if (AppController.get().isNetworkAvailable()) {
            showProgress();
            AppController.get().getStateAndCities();
        } else
            showToast(getResources().getString(R.string.no_internet));

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_registration;
    }


    private void validate() {
        hideSoftKeyboard();


        String name = etCompanyName.getText().toString().trim();
        String address1 = etAddress1.getText().toString().trim();
        String address2 = etAddress2.getText().toString().trim();


        State state = (State) spinnerState.getSelectedItem();
        City city = (City) spinnerCity.getSelectedItem();
        String poBox = etPoBox.getText().toString().trim();
        String companyPhone = etCompanyPhone.getText().toString().trim();
        String mobilePhone = etMobileNumber.getText().toString().trim();
        String webAddress = etWeb.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
        String newCity = etOtherCity.getText().toString().trim();
        String password = etPassword.getText().toString();
        String confirmPassword = etConfirmPassword.getText().toString();
        Subscription subscrub = (Subscription) spinnerSubscription.getSelectedItem();


        if (subscrub == null && !trialMode.equalsIgnoreCase("1")) {
            showToast("Please try any plan");
        } else if (TextUtils.isEmpty(name)) {
            showToast("User name cannot be blank.");
            etCompanyName.requestFocus();
        } else if (TextUtils.isEmpty(address1)) {
            showToast("Address Line 1 cannot be blank.");
            etAddress1.requestFocus();
        } else if (TextUtils.isEmpty(address2)) {
            showToast("Address Line 2 cannot be blank.");
            etAddress2.requestFocus();
        } else if ((address1 + address2).trim().length() < 10 || (address1 + address2).trim().length() > 500) {
            showToast("Address Should be between 10 to 500 Characters");
            etAddress1.requestFocus();
        } else if (state == null) {
            showToast("Please select state");
            getStateList();
        } else if (state.getId().equalsIgnoreCase(PLACE_HOLDER_TAG)) {
            showToast("Please select state");
        } else if (city == null || city.getId().equalsIgnoreCase(PLACE_HOLDER_TAG)) {
            showToast("Please select city");
        } else if (city.getId().equalsIgnoreCase(OTHER_TAG) && TextUtils.isEmpty(newCity)) {
            showToast("City cannot be blank.");
            etOtherCity.requestFocus();

        } else if (TextUtils.isEmpty(poBox)) {
            showToast("P.O Box cannot be blank.");
            etPoBox.requestFocus();

        } else if (!isValidMobile(companyPhone)) {
            showToast("Invalid Phone number ");
            etCompanyPhone.requestFocus();

        } else if (!isValidMobile(mobilePhone)) {
            showToast("Invalid Mobile number ");
            etMobileNumber.requestFocus();

        } else if (!isValidWeb(webAddress)) {
            showToast("Invalid web address");
            etWeb.requestFocus();
        } else if (!isValidEmail(email)) {
            showToast("Invalid email address");
            etEmail.requestFocus();

        } else if (!isValidPassword(password)) {
            showToast(getString(R.string.error_password));
            etPassword.requestFocus();

        } else if (!password.equalsIgnoreCase(confirmPassword)) {
            showToast("Confirm Password doesn't match.");
            etConfirmPassword.requestFocus();

        } else if (AppController.get().isNetworkAvailable()) {
            progressLayer.setVisibility(View.VISIBLE);
            btnNext.setEnabled(false);


            companyRegister = new CompanyRegister();
            companyRegister.setPlan(trialMode.equalsIgnoreCase("1") ? null : subscrub);
            companyRegister.setTrialMode(trialMode);

            companyRegister.setCompany(name);
            companyRegister.setAddress1(address1);
            companyRegister.setAddress2(address2);
            companyRegister.setCountry(DEFAULT_COUNTRY_CODE); //ksa
            companyRegister.setState(state);
            companyRegister.setCity(city);  //else other
            companyRegister.setZipCode(poBox);
            companyRegister.setPhone(companyPhone);
            companyRegister.setMobile(mobilePhone);
            companyRegister.setWebsite(webAddress);
            companyRegister.setEmail(email);
            companyRegister.setPassword(password);

            companyRegister.setRole("1");
            companyRegister.setFax("");

            companyRegister.setOtherCity(city.getId().equalsIgnoreCase(OTHER_TAG) ? newCity : "");


            AppController.get().emailVerification(email);


//            dummyRegister();


        } else {
            showToast(getResources().getString(R.string.no_internet));
        }
    }

    private void dummyRegister() {

////////////////dummy/////////////
        try {

            String s = "{\"status\":true,\"text\":\"Please check your email and enter OTP!\",\"data\":{\"email\":\"metwafer2@yopmail.com\",\"token\":\"c0c9adb27cfd59b713c1bfa4e1949cbb9510afe15216776046c8166155f950ec\"}}";


            OtpResponseModel otpResponseModel = new Gson().fromJson(s, OtpResponseModel.class);

            //off to otp
//    OTPFragment fragment = new OTPFragment().newInstance(otpResponseModel, companyRegister);
//    FragmentManager fm = getActivity().getSupportFragmentManager();
//    FragmentTransaction fragmentTransaction = fm.beginTransaction();
//    fragmentTransaction.add(R.id.register_container, fragment);
//    fragmentTransaction.commit();


//off to verification
            Intent intent = new Intent(getContext(), AccountVerificationActivity.class);
            intent.putExtra(ARG_OTPMODEL, otpResponseModel);
            intent.putExtra(ARG_COMPANY_REGISTER, companyRegister);
            getContext().startActivity(intent);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //////////////////////////////

    }

    @SuppressLint("NonConstantResourceId")
    @OnClick(R.id.button_reg_next)
    public void onClickNext() {


        validate();
    }


    @Subscribe
    public void onHttpGetStateAndCitiesSuccess(ApiSuccessListener<StateResponse> response) {

        if (response.type.equalsIgnoreCase("getStateAndCities")) {

            dismissProgress();
            StateResponse responseModel = response.response.body();
            if (responseModel != null && responseModel.getStatus()) {


                if (responseModel.getStates() != null && !responseModel.getStates().isEmpty())
                    setStates(responseModel.getStates());
                else
                    showToast("No States found!");
            }
        }
    }


    @Subscribe
    public void onOtpSendSuccess(ApiSuccessListener<OtpResponseModel> response) {

        if (response.type.equalsIgnoreCase("emailVerification")) {

            progressLayer.setVisibility(GONE);
            btnNext.setEnabled(true);


            OtpResponseModel otpResponseModel = response.response.body();

            assert otpResponseModel != null;
            if (otpResponseModel.getStatus() && otpResponseModel.getOtpDataData() != null) {


                showToast(otpResponseModel.getText());

                OTPFragment fragment = new OTPFragment().newInstance(otpResponseModel, companyRegister);
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.add(R.id.register_container, fragment);
                fragmentTransaction.commit();


            } else {
                showToast(otpResponseModel.getText());
            }
        }
    }

    @Subscribe
    public void onHttpFailure(ApiFailureListener response) {

        if (response.type.equalsIgnoreCase("emailVerification")) {

            showToast("There is a problem while registering");
            progressLayer.setVisibility(GONE);
            btnNext.setEnabled(true);
        } else if (response.type.equalsIgnoreCase("getStateAndCities")) {

            showToast(getResources().getString(R.string.error_try_again));
            dismissProgress();

        }
    }


    @Subscribe
    public void onHttpError(ApiFailureMessageListener<ErrorModel> response) {

        if (response.type.equalsIgnoreCase("emailVerification") || response.type.equalsIgnoreCase("getStateAndCities")) {
            dismissProgress();
            btnNext.setEnabled(true);

            try {

                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                showToast(errorModel.getText());
            } catch (Exception e) {
                e.printStackTrace();
                showToast("There is a problem while sending");

            }

        }
    }


    private void setSubscription() {
        spinnerSubscription.setVisibility(trialMode.equalsIgnoreCase("1") ? GONE : View.VISIBLE);
        if (trialMode.equalsIgnoreCase("1"))
            return;

        else if (subscriptions == null || subscriptions.isEmpty()) {
            showToast("Unable to load subscription ");
            return;
        }

        SubscriptionArrayAdapter adapter = new SubscriptionArrayAdapter(getActivity(), subscriptions);


        spinnerSubscription.setAdapter(adapter);

        if (subscriptions.size() > position)
            spinnerSubscription.setSelection(position);

        spinnerSubscription.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            //@Override
            public void onItemSelected(AdapterView<?> parent, View arg1, int arg2, long arg3) {
                // TODO Auto-generated method stub
                Subscription item = (Subscription) parent.getSelectedItem();

            }


            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });


//        ArrayAdapter<String> countryName = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, CountryCode.countryCodes);
//        ArrayAdapter<String> countryName = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, CountryCode.countryAreaCodes);
    }

    private void setStates(List<State> states) {

        State s = new State();
        s.setId(PLACE_HOLDER_TAG);
        s.setStateName("Select State");
        states.add(0, s);

//        spinnerPhoneCode.setDropDownWidth(100); //Pull-down width
//        spinnerPhoneCode.setDropDownHorizontalOffset(100); //Pull down lateral offset
//        spinnerPhoneCode.setDropDownVerticalOffset(100); //Drop down vertical offset


        AddressSpinnerArrayAdapter adp = new AddressSpinnerArrayAdapter(getActivity(), states, true);
//        ArrayAdapter<State> adp = new ArrayAdapter<State>(getActivity(), R.layout.item_spinner_code, states);
//        adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        spinnerState.setAdapter(adp);

        spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View arg1, int arg2, long arg3) {
                State item = (State) parent.getItemAtPosition(arg2);


                setCities(item.getCities());
            }


            public void onNothingSelected(AdapterView<?> arg0) {


            }
        });


    }

    private void setCities(List<City> cities) {


        if (cities == null)
            cities = new ArrayList<>();

        City c = new City();
        c.setId(PLACE_HOLDER_TAG);
        c.setName("Select City");
        cities.add(0, c);

        c = new City();
        c.setId(OTHER_TAG);
        c.setName(OTHER_TAG);
        cities.add(c);

        AddressSpinnerArrayAdapter adp = new AddressSpinnerArrayAdapter(getActivity(), cities, true);


        spinnerCity.setAdapter(adp);

        spinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View arg1, int arg2, long arg3) {
                // TODO Auto-generated method stub
                City item = (City) parent.getItemAtPosition(arg2);
                viewOtherCity.setVisibility(item.getId().equalsIgnoreCase(OTHER_TAG) ? View.VISIBLE : GONE);

                if (item.getId().equalsIgnoreCase(OTHER_TAG)) {
                    etOtherCity.getText().clear();
                    etOtherCity.requestFocus();
                }

            }


            public void onNothingSelected(AdapterView<?> arg0) {


            }
        });


    }


    public class SubscriptionArrayAdapter extends ArrayAdapter<String> {

        private final LayoutInflater mInflater;
        private final Context mContext;
        private final List<Subscription> items;


        public SubscriptionArrayAdapter(@NonNull Context context, @NonNull List objects) {
            super(context, 0, objects);

            mContext = context;
            mInflater = LayoutInflater.from(context);

            items = objects;
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView,
                                    @NonNull ViewGroup parent) {


            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.subscription_spinner_item, parent, false);
            }
            TextView tvDays = convertView.findViewById(R.id.textView_itemSubscription_day);
            TextView tvPrice = convertView.findViewById(R.id.textView_itemSubscription_price);

            Subscription subscription = items.get(position);
            tvDays.setText(subscription.getPlanName());

            tvPrice.setText(subscription.getCharge() + " " + DEFAULT_CURRENCY);


            return convertView;
        }

        @Override
        public @NonNull
        View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.warranty_view_item, parent, false);
            }
            TextView tvDays = convertView.findViewById(R.id.textView_itemWarrant_day);
            TextView tvPrice = convertView.findViewById(R.id.textView_itemWarrant_price);
            tvDays.setTextColor(Color.WHITE);

            Subscription subscription = items.get(position);

            tvDays.setText(subscription.getPlanName());
            tvPrice.setText(subscription.getCharge() + " " + DEFAULT_CURRENCY);
            return convertView;

        }


    }

}
