package com.purposecodes.emetwafar.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.otto.BusProvider;
import com.squareup.otto.Bus;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 */
public abstract class BaseFragment extends Fragment {

    // Otto handler
    private final Bus mBus = BusProvider.getInstance();


    @Nullable
    @BindView(R.id.layout_prgressBg)
    ViewGroup progressLayer;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutResource(), container, false);
        ButterKnife.bind(this, view);
        // Registering bus
        mBus.register(this);

//        ButterKnife.bind(getActivity());


        return view;
    }


    @Override
    public void onDestroyView() {
        mBus.unregister(this);
        super.onDestroyView();
    }


    protected abstract int getLayoutResource();


    protected void hideSoftKeyboard() {
        if (getActivity().getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    protected void hideSoftKeyboard(EditText editText) {
        if (editText != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        }
    }

    protected void showToast(String msg) {
      /*  if (Build.VERSION.SDK_INT >= 30) {

            View layout = LayoutInflater.from(getContext()).inflate(R.layout.widget_toast, null);

            TextView text = (TextView) layout.findViewById(R.id.text);

            text.setText(msg);

            Toast toast = new Toast(getContext());
            toast.setGravity(Gravity.BOTTOM, 0, 200);
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setView(layout);

            toast.addCallback(new Toast.Callback() {
                @Override
                public void onToastShown() {
                    super.onToastShown();
                }

                @Override
                public void onToastHidden() {
                    super.onToastHidden();
                }
            });
            toast.show();

        } else*/
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }


    protected void showProgress() {
        if (progressLayer != null) {
            progressLayer.setVisibility(View.VISIBLE);
        }
    }

    protected void dismissProgress() {

        if (progressLayer != null) {
            progressLayer.setVisibility(View.GONE);
        }
    }


}
