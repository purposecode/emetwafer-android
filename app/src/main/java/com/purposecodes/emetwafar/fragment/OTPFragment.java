package com.purposecodes.emetwafar.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.api.ApiFailureMessageListener;
import com.purposecodes.emetwafar.api.ApiSuccessListener;
import com.purposecodes.emetwafar.authentication.AccountVerificationActivity;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.model.CompanyRegister;
import com.purposecodes.emetwafar.responsemodel.ErrorModel;
import com.purposecodes.emetwafar.responsemodel.OtpResponseModel;
import com.squareup.otto.Subscribe;

import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;

import static com.purposecodes.emetwafar.utils.LogUtils.printLog;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link OTPFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OTPFragment extends BaseFragment {
    private final String TAG = "OTPFragment";
    private static final String ARG_OTPMODEL = "param_otp";
    private static final String ARG_COMPANY_REGISTER = "param_register";


    private OtpResponseModel otpResponseModel;
    private CompanyRegister companyRegister;

    public OTPFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.textView_otp_content)
    TextView tvContent;

    @BindView(R.id.editText_otp_1)
    EditText etOtp1;
    @BindView(R.id.editText_otp_2)
    EditText etOtp2;
    @BindView(R.id.editText_otp_3)
    EditText etOtp3;
    @BindView(R.id.editText_otp_4)
    EditText etOtp4;
    @BindView(R.id.button_otp_verify)
    Button btnVerify;

    @BindView(R.id.button_otp_resend)
    Button btnResend;


    public static OTPFragment newInstance(OtpResponseModel otpResponseModel, CompanyRegister companyRegister) {
        OTPFragment fragment = new OTPFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_OTPMODEL, otpResponseModel);
        args.putSerializable(ARG_COMPANY_REGISTER, companyRegister);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            if (getArguments() != null) {
                otpResponseModel = (OtpResponseModel) getArguments().getSerializable(ARG_OTPMODEL);
                companyRegister = (CompanyRegister) getArguments().getSerializable(ARG_COMPANY_REGISTER);



            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }



    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_o_t_p;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        try {
            tvContent.setText(getText(R.string.to_your_verify_your_email_we_ve_send_a_one_time_password_otp_to)+" "+companyRegister.getEmail());

        }catch (Exception e){
            e.printStackTrace();
        }
        init();
    }

    private void init() {
        StringBuilder sb = new StringBuilder();

        etOtp1.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (sb.length() == 0 & etOtp1.length() == 1) {
                    sb.append(s);
                    etOtp1.clearFocus();
                    etOtp2.requestFocus();
                    etOtp2.setCursorVisible(true);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (sb.length() == 1) {
                    sb.deleteCharAt(0);
                }
            }

            public void afterTextChanged(Editable s) {
                if (sb.length() == 0) {
                    etOtp1.requestFocus();
                }

            }
        });

        etOtp2.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (sb.length() == 1 & etOtp2.length() == 1) {
                    sb.append(s);
                    etOtp2.clearFocus();
                    etOtp3.requestFocus();
                    etOtp3.setCursorVisible(true);

                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (sb.length() == 2) {
                    sb.deleteCharAt(1);
                }
            }

            public void afterTextChanged(Editable s) {
                if (sb.length() == 1) {
                    etOtp2.requestFocus();
                }

            }
        });

        etOtp3.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (sb.length() == 2 & etOtp3.length() == 1) {
                    sb.append(s);
                    etOtp3.clearFocus();
                    etOtp4.requestFocus();
                    etOtp4.setCursorVisible(true);

                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (sb.length() == 3) {
                    sb.deleteCharAt(2);

                }
            }

            public void afterTextChanged(Editable s) {
                if (sb.length() == 2) {
                    etOtp3.requestFocus();
                }

            }
        });

        etOtp4.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (sb.length() == 3 & etOtp4.length() == 1) {
                    sb.append(s);
                    etOtp4.clearFocus();
                    hideSoftKeyboard();

                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (sb.length() == 4) {
                    sb.deleteCharAt(3);
                }
            }

            public void afterTextChanged(Editable s) {
                if (sb.length() == 3) {
                    etOtp4.requestFocus();
                }

            }
        });
    }


    @OnClick({R.id.button_otp_verify,R.id.button_otp_resend})
    public void onClickNext(View view) {

        switch (view.getId()){
            case R.id.button_otp_verify:
                validate();
                break;
            case R.id.button_otp_resend:
                resent();
                break;
        }

    }

    private void validate() {
        hideSoftKeyboard();


        String otp1 = etOtp1.getText().toString().trim();
        String otp2 = etOtp2.getText().toString().trim();
        String otp3 = etOtp3.getText().toString().trim();
        String otp4 = etOtp4.getText().toString().trim();


        if (TextUtils.isEmpty(otp1)) {
            showToast("OTP cannot be blank.");
            etOtp1.requestFocus();
        } else if (TextUtils.isEmpty(otp2)) {
            showToast("OTP cannot be blank.");
            etOtp2.requestFocus();
        } else if (TextUtils.isEmpty(otp3)) {
            showToast("OTP cannot be blank.");
            etOtp3.requestFocus();
        } else if (TextUtils.isEmpty(otp4)) {
            showToast("OTP cannot be blank.");
            etOtp4.requestFocus();


        } else if (AppController.get().isNetworkAvailable()) {
           showProgress();
            btnVerify.setEnabled(false);

            StringBuilder sb = new StringBuilder();
            sb.append(otp1).append(otp2).append(otp3).append(otp4);

            HashMap<String, String> options = new HashMap<>();
            options.put("otp", sb.toString());
            options.put("tokenId", otpResponseModel.getOtpDataData().getToken());



            AppController.get().verifyOtp(options);

        } else {
            showToast(getResources().getString(R.string.no_internet));
        }
    }

    private void resent() {
        hideSoftKeyboard();

        clearField();
        if (AppController.get().isNetworkAvailable()) {
            showProgress();
            btnResend.setEnabled(false);
            AppController.get().emailVerification(companyRegister.getEmail());
        } else {
            showToast(getResources().getString(R.string.no_internet));
        }
    }


    @Subscribe
    public void onVerificationSuccess(ApiSuccessListener<JsonObject> response) {

        if (response.type.equalsIgnoreCase("verifyOtp")) {

            dismissProgress();
            btnVerify.setEnabled(true);
            btnResend.setEnabled(true);


            JsonObject responseObject = response.response.body();

            assert responseObject != null;

            if (responseObject.get("status").getAsBoolean()) {

                showToast("Verification successfully");
                Intent intent=new Intent(getContext(), AccountVerificationActivity.class);
                intent.putExtra(ARG_OTPMODEL,otpResponseModel);
                intent.putExtra(ARG_COMPANY_REGISTER,companyRegister);
                requireContext().startActivity(intent);
               requireActivity().finish();


            } else {
                showToast(responseObject.get("text").getAsString());
                clearField();
            }
        }
    }



    @Subscribe
    public void onOtpSendSuccess(ApiSuccessListener<OtpResponseModel> response) {

        printLog(TAG, "onOtpSendSuccess  : " + response.response.toString());
        if (response.type.equalsIgnoreCase("emailVerification")) {

            dismissProgress();
            btnVerify.setEnabled(true);
            btnResend.setEnabled(true);

            OtpResponseModel otpResponseModel = response.response.body();

            assert otpResponseModel != null;
            if (otpResponseModel.getStatus()) {

                showToast("Please check your email and enter OTP!");
                clearField();
            } else {
                showToast(otpResponseModel.getText());
            }
        }
    }


    @Subscribe
    public void onVerificationFailure(ApiFailureListener response) {
        if (response.type.equalsIgnoreCase("verifyOtp")||response.type.equalsIgnoreCase("emailVerification")) {

            showToast(getResources().getString(R.string.error_try_again));
            dismissProgress();
            btnVerify.setEnabled(true);
            btnResend.setEnabled(true);
        }
    }


    @Subscribe
    public void onVerificationError(ApiFailureMessageListener<ErrorModel> response) {

        if (response.type.equalsIgnoreCase("verifyOtp")||response.type.equalsIgnoreCase("emailVerification")) {
          dismissProgress();
            btnVerify.setEnabled(true);
            btnResend.setEnabled(true);
            clearField();
            try {

                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                showToast(errorModel.getText());
            } catch (Exception e) {
                e.printStackTrace();
                showToast("There is a problem while sending");

            }
        }
    }



    private void clearField(){

        etOtp1.getText().clear();
        etOtp2.getText().clear();
        etOtp3.getText().clear();
        etOtp4.getText().clear();


        etOtp1.requestFocus();
        etOtp1.setCursorVisible(true);
    }
}
