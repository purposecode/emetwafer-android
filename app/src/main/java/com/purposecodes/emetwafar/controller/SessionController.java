package com.purposecodes.emetwafar.controller;

import static android.content.Context.MODE_PRIVATE;
import static com.purposecodes.emetwafar.utils.UtilsVariable.PREF_CART_COUNT;
import static com.purposecodes.emetwafar.utils.UtilsVariable.PREF_CART_ID;
import static com.purposecodes.emetwafar.utils.UtilsVariable.PREF_CHECK_EXPIRY;
import static com.purposecodes.emetwafar.utils.UtilsVariable.PREF_IS_REGISTERED;
import static com.purposecodes.emetwafar.utils.UtilsVariable.PREF_LOGIN_TYPE;
import static com.purposecodes.emetwafar.utils.UtilsVariable.PREF_MAINTENANCE_MODE;
import static com.purposecodes.emetwafar.utils.UtilsVariable.PREF_NOTIFICATION_COUNT;
import static com.purposecodes.emetwafar.utils.UtilsVariable.PREF_REFRESH_TOKEN;
import static com.purposecodes.emetwafar.utils.UtilsVariable.PREF_TRIAL_MODE;
import static com.purposecodes.emetwafar.utils.UtilsVariable.PREF_UPDATE_EXPIRY;
import static com.purposecodes.emetwafar.utils.UtilsVariable.PREF_USER_INFO;
import static com.purposecodes.emetwafar.utils.UtilsVariable.PREF_WISH_COUNT;
import static com.purposecodes.emetwafar.utils.UtilsVariable.SHARED_PREF;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.purposecodes.emetwafar.model.UserData;

public class SessionController {

    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor;
    private final Context context;


    // Constructor
    public SessionController(Context context) {
        this.context = context;
        preferences = this.context.getSharedPreferences(SHARED_PREF, MODE_PRIVATE);
        editor = preferences.edit();


    }


    public static void saveUser(UserData userData, String mRefreshToken, String mLoginType) {

        editor.putString(PREF_USER_INFO, new Gson().toJson(userData));
        editor.putBoolean(PREF_IS_REGISTERED, true);
        editor.putString(PREF_REFRESH_TOKEN, mRefreshToken);
        editor.putString(PREF_LOGIN_TYPE, mLoginType);
        editor.apply();
        editor.commit();

        AppController.get().setUser(userData);

    }

    public static void updateUser(UserData userData) {

        editor.putString(PREF_USER_INFO, new Gson().toJson(userData));
        editor.apply();
        editor.commit();

        AppController.get().setUser(userData);

    }

    public static UserData getUser() {

        String userInfo = preferences.getString(PREF_USER_INFO, "");
        UserData userData = null;
        if (!TextUtils.isEmpty(userInfo)) {
            userData = new Gson().fromJson(userInfo, UserData.class);
        }


        return userData;
    }

    /**
     * Create login session
     */

    public static void setCartId(String cartId) {
        // Storing name in pref
        editor.putString(PREF_CART_ID, cartId);
        editor.apply();
        // commit changes
        editor.commit();
    }

    public static String getCartId() {
        return preferences.getString(PREF_CART_ID, "");// return

    }


    public static int getCartQuantity() {
        return preferences.getInt(PREF_CART_COUNT, 0);// return
    }

    public static void setCartQuantity(int cartQuantity) {
        // Storing name in pref
        editor.putInt(PREF_CART_COUNT, cartQuantity);
        editor.apply();
        // commit changes
        editor.commit();
    }

    public static int getWishQuantity() {
        return preferences.getInt(PREF_WISH_COUNT, 0);// return
    }

    public static void setWishQuantity(int wishQuantity) {
        // Storing name in pref
        editor.putInt(PREF_WISH_COUNT, wishQuantity);
        editor.apply();
        // commit changes
        editor.commit();

        AppController.get().inValidateDataChanged();
    }

    public static int getNotificationCount() {
        return preferences.getInt(PREF_NOTIFICATION_COUNT, 0);// return
    }

    public static void setNotificationCount(int count) {
        // Storing name in pref
        editor.putInt(PREF_NOTIFICATION_COUNT, count);
        editor.apply();
        // commit changes
        editor.commit();

        AppController.get().inValidateDataChanged();
    }


    public static String getCheckExpiryLastDate() {
        return preferences.getString(PREF_CHECK_EXPIRY, "");// return
    }

    public static void setCheckExpiryLastDate(String date) {
        // Storing name in pref
        editor.putString(PREF_CHECK_EXPIRY, date);
        editor.apply();
        // commit changes
        editor.commit();

        AppController.get().inValidateDataChanged();
    }


    public static String getCheckUpdateLastDate() {
        return preferences.getString(PREF_UPDATE_EXPIRY, "");// return
    }

    public static void setCheckUpdateLastDate(String date) {
        // Storing name in pref
        editor.putString(PREF_UPDATE_EXPIRY, date);
        editor.apply();
        // commit changes
        editor.commit();

    }


    public static boolean getMaintenanceMode() {
        return preferences.getBoolean(PREF_MAINTENANCE_MODE, false);// return
    }

    public static void setMaintenanceMode(boolean isMaintenance) {
        // Storing name in pref
        editor.putBoolean(PREF_MAINTENANCE_MODE, isMaintenance);
        editor.apply();
        // commit changes
        editor.commit();

    }


    public static void setTrialMode(boolean isTrialmode) {
        // Storing name in pref
        editor.putBoolean(PREF_TRIAL_MODE, isTrialmode);
        editor.apply();
        // commit changes
        editor.commit();


    }


    public static boolean getTrialMode() {
        return preferences.getBoolean(PREF_TRIAL_MODE, false);// return
    }
    public static boolean isUserLoggedIn() {
        UserData userData = getUser();
        return userData != null;
    }


    public static void logoutSession() {
        //delete firebase token
        FirebaseMessaging.getInstance().deleteToken();
        AppController.get().setUser(null);
        editor.clear().apply();
        editor.commit();
    }




}
