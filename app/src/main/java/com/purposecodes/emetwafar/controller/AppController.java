package com.purposecodes.emetwafar.controller;


import static com.purposecodes.emetwafar.fcm.MyFirebaseMessagingService.notificationTopicSubscription;
import static com.purposecodes.emetwafar.utils.LogUtils.printLog;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.getVersionWiseKey;
import static com.purposecodes.emetwafar.utils.UtilsVariable.PREF_BASE_URL;
import static com.purposecodes.emetwafar.utils.UtilsVariable.PREF_USER_INFO;
import static com.purposecodes.emetwafar.utils.UtilsVariable.SHARED_PREF;
import static com.purposecodes.emetwafar.utils.UtilsVariable.SHARED_PREF_URL;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;

import androidx.multidex.MultiDex;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.purposecodes.emetwafar.api.ApiCallBack;
import com.purposecodes.emetwafar.api.ApiClient;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.authentication.LoginActivity;
import com.purposecodes.emetwafar.listener.ConnectivityReceiverListener;
import com.purposecodes.emetwafar.model.OrderPayment;
import com.purposecodes.emetwafar.model.PlanData;
import com.purposecodes.emetwafar.model.UserData;
import com.purposecodes.emetwafar.otto.BusProvider;
import com.purposecodes.emetwafar.receiver.ConnectivityReceiver;
import com.purposecodes.emetwafar.responsemodel.AddressResponse;
import com.purposecodes.emetwafar.responsemodel.CartListResponse;
import com.purposecodes.emetwafar.responsemodel.CategoryResponse;
import com.purposecodes.emetwafar.responsemodel.CheckoutResponse;
import com.purposecodes.emetwafar.responsemodel.CouponListResponse;
import com.purposecodes.emetwafar.responsemodel.NotificationResponse;
import com.purposecodes.emetwafar.responsemodel.OrderDetailsResponse;
import com.purposecodes.emetwafar.responsemodel.OrdersResponse;
import com.purposecodes.emetwafar.responsemodel.OtpResponseModel;
import com.purposecodes.emetwafar.responsemodel.ProductDetailsResponse;
import com.purposecodes.emetwafar.responsemodel.ProductResponse;
import com.purposecodes.emetwafar.responsemodel.ProfileResponse;
import com.purposecodes.emetwafar.responsemodel.RegisterResponse;
import com.purposecodes.emetwafar.responsemodel.SaveOrderResponse;
import com.purposecodes.emetwafar.responsemodel.StateResponse;
import com.purposecodes.emetwafar.responsemodel.SubscriptionHistoryResponse;
import com.purposecodes.emetwafar.responsemodel.SubscriptionsResponse;
import com.purposecodes.emetwafar.responsemodel.WishListResponse;
import com.squareup.otto.Bus;
import com.telr.mobile.sdk.TelrApplication;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class AppController extends TelrApplication {

    private final String TAG = "AppController";
    // A handler to ApiClient class
    private ApiClient mApiClient;

    // A handler to this application class
    private static AppController appController;

    // A handler to this session class
    private static SessionController sessionController;

    // A handler to Bus provider
    private Bus mBus;


    // Logged user details
    private UserData mUser;


    private OrderPayment orderPayment;
    private PlanData planData;


    // Base url to all api calls
    private String mBaseUrl;

    // Flag to check the base url is changed
    private boolean isBaseUrlChanged = false;




    @Override
    public void onCreate() {
        super.onCreate();
        sessionController = new SessionController(this);

        FirebaseApp.initializeApp(this);

        appController = this;
        mApiClient = ApiClient.getClient();
        mBus = BusProvider.getInstance();
        mBus.register(this);
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);


        setUser(SessionController.getUser());

//        SharedPreferences pref = getSharedPreferences(SHARED_PREF, MODE_PRIVATE);
//        mBaseUrl = pref.getString(PREF_BASE_URL, "");
        mBaseUrl = getBaseUrlToSharedPref();


        // Register network monitor
        registerNetworkMonitor();

        initializeBaseUrl();

        notificationTopicSubscription();


    }




    public OrderPayment getCheckoutData() {
        return orderPayment;
    }

    public void setCheckoutData(OrderPayment _orderPayment) {
        this.orderPayment = _orderPayment;
    }

    public PlanData getPlanData() {
        return planData;
    }

    public void setPlanData(PlanData _planData) {
        this.planData = _planData;
    }

    private void initializeBaseUrl() {


        // Write a message to the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();

//        DatabaseReference myRef = database.getReference("e_metwafer").child("base_url");
        DatabaseReference myRef = database.getReference("e_metwafer").child("version_urls").child(getVersionWiseKey());

        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                try {

//                    Map<String,Object> map= dataSnapshot.getValue(String.class);

                    String value = dataSnapshot.getValue(String.class);
                    printLog("AppController", "Value is onDataChange: " + getVersionWiseKey() + " : " + value);

                    if (!TextUtils.isEmpty(value))
                        AppController.get().setUrl(value);


                } catch (DatabaseException e) {

                    printLog(TAG, "Value is DatabaseException: " + e.getLocalizedMessage());
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                printLog(TAG, "Value is: Failed to read value." + error.toException());
            }
        });

    }


    public static SessionController getSessionController() {
        return sessionController;
    }


    public static AppController get() {
        return appController;
    }


    /**
     * Initializing Internet availability checker. Which is used to show the user
     * if he becomes online or offline.
     */
    private void registerNetworkMonitor() {
        InternetAvailabilityChecker.init(this);
    }


    public boolean isUserLoggedIn_() {
        SharedPreferences pref = getSharedPreferences(SHARED_PREF, MODE_PRIVATE);
        String userInfo = pref.getString(PREF_USER_INFO, "");
        UserData userData = null;

        if (!TextUtils.isEmpty(userInfo)) {
            userData = new Gson().fromJson(userInfo, UserData.class);
        }

        return userData != null;
    }


    public UserData getUser() {

        return mUser;
    }


    // Saving user data to singleton class
    public void setUser(UserData mUser) {
        this.mUser = mUser;
    }


    public String getUrl() {
        return mBaseUrl;
    }


    /**
     * Setting base url from firestore as retrofit base url.
     *
     * @param baseUrl - Server url
     */
    public void setUrl(String baseUrl) {
        this.mBaseUrl = baseUrl;
        mApiClient.setBaseUrl(mBaseUrl);

        setBaseUrlToSharedPref(mBaseUrl);
        setBaseUrlChanged(true);
    }

    public void setBaseUrlToSharedPref(String baseUrl) {
        SharedPreferences pref = getSharedPreferences(SHARED_PREF_URL, MODE_PRIVATE);
//        SharedPreferences pref = getSharedPreferences(SHARED_PREF, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(PREF_BASE_URL, baseUrl);
        editor.apply();
    }

    public  String getBaseUrlToSharedPref() {
        SharedPreferences pref = getSharedPreferences(SHARED_PREF_URL, MODE_PRIVATE);
        return pref.getString(PREF_BASE_URL, "");// return
    }

    public boolean isBaseUrlChanged() {
        return isBaseUrlChanged;
    }

    public void setBaseUrlChanged(boolean baseUrlChanged) {
        isBaseUrlChanged = baseUrlChanged;
    }

    /**
     * checking Internet availability.
     */
    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    /**
     * Logging the user out from the app
     */
    public void logoutUser() {

        AppController.get().setUser(null);
        SessionController.logoutSession();

        // Clearing all notification if any notification is present
        NotificationManager nMgr = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        if (nMgr != null)
            nMgr.cancelAll();

        Intent loginIntent = new Intent(this, LoginActivity.class);

        loginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(loginIntent);
    }





    /**
     * check plan expiry new user
     */
    public void checkexpiry() {
        ApiCallBack<JsonObject> callBack = new ApiCallBack<>(mBus, "checkexpiry", 0);
        mApiClient.checkexpiry(callBack);
    }





    /**
     * Registering new user
     *
     * @param email
     */
    public void emailVerification(String email) {

        ApiCallBack<OtpResponseModel> callBack = new ApiCallBack<>(mBus, "emailVerification", 0);
        mApiClient.emailVerification(email, callBack);
    }


    /**
     * Registering new user
     *
     * @param options contains otp and token
     */
    public void verifyOtp(HashMap<String, String> options) {
        ApiCallBack<JsonObject> callBack = new ApiCallBack<>(mBus, "verifyOtp", 0);
        mApiClient.verifyOtp(options, callBack);
    }




    /**
     * Api call to complete an order
     *
     * @param options order params
     * @param images  images
     */
    public void companyRegister(HashMap<String, String> options, ArrayList<String> images) {
        ApiCallBack<JsonObject> callBack = new ApiCallBack<>(mBus, "companyRegister", 0);
        mApiClient.companyRegister(options, images, callBack);
    }

    public void mediaUpload(Map<String, RequestBody> options, MultipartBody.Part part, int position) {
        ApiCallBack<JsonObject> callBack = new ApiCallBack<>(mBus, "mediaUpload", position);
        mApiClient.mediaUpload(options, part, callBack);
    }

    public void profileDpUpload(Map<String, RequestBody> options, MultipartBody.Part part) {
        ApiCallBack<JsonObject> callBack = new ApiCallBack<>(mBus, "profileDpUpload", 0);
        mApiClient.profileDpUpload(options, part, callBack);
    }

    /**
     * Api call to user login
     *
     * @param options containing login credentials
     */

    public void loginUser(Map<String, String> options) {

        ApiCallBack<JsonObject> callBack = new ApiCallBack<>(mBus, "loginUser", 0);
        mApiClient.loginUser(options, callBack);
    }

    public void changePassword(HashMap<String, String> options) {
        ApiCallBack<JsonObject> callBack = new ApiCallBack<>(mBus, "changePassword", 0);
        mApiClient.changePassword(options, callBack);
    }



    public void logout() {
        ApiCallBack<JsonObject> callBack = new ApiCallBack<>(mBus, "logout", 0);
        mApiClient.logoutUser( callBack);

    }


    /**
     * Api call to send password to email
     *
     * @param options - email to be get password
     */
    public void forgotPassword(HashMap<String, String> options) {
        ApiCallBack<RegisterResponse> callBack = new ApiCallBack<>(mBus, "forgotPassword", 0);
        mApiClient.forgotPassword(options, callBack);
    }


    public void sendUserToken(Map<String, String> options, String token_id) {
        ApiCallBack<JsonObject> callBack = new ApiCallBack<>(mBus, "sendUserToken", 0);

        mApiClient.sendUserToken(options, token_id, callBack);
    }


    public void getProfile() {
        ApiCallBack<ProfileResponse> callBack = new ApiCallBack<>(mBus, "getProfile", 0);
        mApiClient.getProfile(callBack);
    }


    public void updateProfile(Map<String, String> options) {
        ApiCallBack<ProfileResponse> callBack = new ApiCallBack<>(mBus, "updateProfile", 0);
        mApiClient.updateProfile(options, callBack);
    }

    /**
     * Api to get all  categories
     */
    public void getCategories() {
        ApiCallBack<CategoryResponse> callBack = new ApiCallBack<>(mBus, "getCategories", 0);
        mApiClient.getCategories(callBack);
    }

    public void getStateAndCities() {
        ApiCallBack<StateResponse> callBack = new ApiCallBack<>(mBus, "getStateAndCities", 0);
        mApiClient.getStateAndCities(callBack);
    }

    /**
     * Api to get   products
     */
    public void getProducts(Map<String, String> options, String ottoTag) {
        ApiCallBack<ProductResponse> callBack = new ApiCallBack<>(mBus, "getProducts/" + ottoTag, 0);
        mApiClient.getProducts(options, callBack);
    }

    /**
     * Api to get product detail
     */
    public void getProductDetail(String id) {
        ApiCallBack<ProductDetailsResponse> callBack = new ApiCallBack<>(mBus, "getProductDetail", 0);
        mApiClient.getProductDetail(id, callBack);
    }


    public void addToWish(Map<String, String> options, String ottoTag, int position) {
        ApiCallBack<JsonObject> callBack = new ApiCallBack<>(mBus, "addToWish_" + ottoTag, position);
        mApiClient.addToWish(options, callBack);
    }

    public void removeFromWish(String id, String ottoTag, int position) {
        ApiCallBack<JsonObject> callBack = new ApiCallBack<>(mBus, "removeFromWish_" + ottoTag, position);
        mApiClient.removeFromWish(id, callBack);
    }

    /**
     * Api to get  wish list
     */
    public void getWishList() {
        ApiCallBack<WishListResponse> callBack = new ApiCallBack<>(mBus, "getWishList", 0);
        mApiClient.getWishList(callBack);
    }

    public void getSubscriptionList() {
        ApiCallBack<SubscriptionsResponse> callBack = new ApiCallBack<>(mBus, "getSubscriptionList", 0);
        mApiClient.getSubscriptionList(callBack);
    }

    public void getSubscriptionHistory(Map<String, String> options) {
        ApiCallBack<SubscriptionHistoryResponse> callBack = new ApiCallBack<>(mBus, "getSubscriptionHistory", 0);
        mApiClient.getSubscriptionHistory(options, callBack);
    }

    public void getCouponList() {
        ApiCallBack<CouponListResponse> callBack = new ApiCallBack<>(mBus, "getCouponList", 0);
        mApiClient.getCouponList(callBack);
    }

    public void removeCoupon(String id, int position) {
        ApiCallBack<JsonObject> callBack = new ApiCallBack<>(mBus, "removeCoupon", position);
        mApiClient.removeCoupon(id, callBack);
    }

    public void clearAllCoupon() {
        ApiCallBack<JsonObject> callBack = new ApiCallBack<>(mBus, "clearAllCoupon", 0);
        mApiClient.removeCoupon("", callBack);
    }

    /**
     * Api to create cart id
     */
    public void generateCartId() {
        ApiCallBack<JsonObject> callBack = new ApiCallBack<>(mBus, "generateCartId", 0);
        mApiClient.generateCartId(callBack);
    }

    /**
     * Api to get cartList
     */
    public void getMyCart(String id) {
        ApiCallBack<CartListResponse> callBack = new ApiCallBack<>(mBus, "getMyCart", 0);
        mApiClient.getMyCart(id, callBack);
    }

    public void addToCart(Map<String, String> options, int position) {
        ApiCallBack<JsonObject> callBack = new ApiCallBack<>(mBus, "addToCart", position);
        mApiClient.addToCart(options, callBack);
    }

    public void updateCart(Map<String, String> options, int position) {
        ApiCallBack<JsonObject> callBack = new ApiCallBack<>(mBus, "updateCart", position);
        mApiClient.updateCart(options, callBack);
    }

    public void buyNow(Map<String, String> options) {
        ApiCallBack<CheckoutResponse> callBack = new ApiCallBack<>(mBus, "buyNow", 0);
        mApiClient.buyNow(options, callBack);
    }


    public void removeFromMyCart(String cart_id, String product_id, int position) {
        ApiCallBack<JsonObject> callBack = new ApiCallBack<>(mBus, "removeFromMyCart", position);
        mApiClient.removeFromMyCart(cart_id, product_id, callBack);
    }

    public void clearMyCart(String cart_id) {
        ApiCallBack<JsonObject> callBack = new ApiCallBack<>(mBus, "clearMyCart", 0);
        mApiClient.clearMyCart(cart_id, callBack);
    }

    public void checkout(Map<String, String> options) {
        ApiCallBack<CheckoutResponse> callBack = new ApiCallBack<>(mBus, "checkout", 0);
        mApiClient.checkout(options, callBack);
    }

    public void updateCheckout(Map<String, String> options) {
        ApiCallBack<CheckoutResponse> callBack = new ApiCallBack<>(mBus, "updateCheckout", 0);
        mApiClient.updateCheckout(options, callBack);
    }


    public void applyCoupon(Map<String, String> options) {
        ApiCallBack<JsonObject> callBack = new ApiCallBack<>(mBus, "applyCoupon", 0);
        mApiClient.applyCoupon(options, callBack);
    }

    public void removeSelectedCoupon(String cartId) {
        ApiCallBack<CheckoutResponse> callBack = new ApiCallBack<>(mBus, "removeSelectedCoupon", 0);
        mApiClient.removeSelectedCoupon(cartId, callBack);
    }

    public void saveOrder(Map<String, String> options) {
        ApiCallBack<SaveOrderResponse> callBack = new ApiCallBack<>(mBus, "saveOrder", 0);
        mApiClient.saveOrder(options, callBack);
    }

    public void initiatePayment(Map<String, String> options) {
        ApiCallBack<JsonObject> callBack = new ApiCallBack<>(mBus, "initiatePayment", 0);
        mApiClient.initiateOrderPayment(options, callBack);
    }

    public void refundRequest(Map<String, String> options, int position) {
        ApiCallBack<JsonObject> callBack = new ApiCallBack<>(mBus, "refundRequest", position);
        mApiClient.refundRequest(options, callBack);
    }

    public void cancelOrder(Map<String, String> options, int position) {
        ApiCallBack<JsonObject> callBack = new ApiCallBack<>(mBus, "cancelOrder", position);
        mApiClient.cancelOrder(options, callBack);
    }

    public void orderNeedHelp(Map<String, String> options) {
        ApiCallBack<JsonObject> callBack = new ApiCallBack<>(mBus, "orderNeedHelp", 0);
        mApiClient.orderNeedHelp(options, callBack);
    }

    /**
     * Api to get  orders
     */
    public void getOrders(Map<String, String> options) {
        ApiCallBack<OrdersResponse> callBack = new ApiCallBack<>(mBus, "getOrders", 0);
        mApiClient.getOrders(options, callBack);
    }

    /**
     * Api to get order detail
     */
    public void getOrderDetail(String id) {
        ApiCallBack<OrderDetailsResponse> callBack = new ApiCallBack<>(mBus, "getOrderDetail", 0);
        mApiClient.getOrderDetail(id, callBack);
    }

    public void saveAddress(Map<String, String> options) {
        ApiCallBack<JsonObject> callBack = new ApiCallBack<>(mBus, "saveAddress", 0);
        mApiClient.saveAddress(options, callBack);
    }

    public void editAddress(Map<String, String> options) {
        ApiCallBack<JsonObject> callBack = new ApiCallBack<>(mBus, "editAddress", 0);
        mApiClient.editAddress(options, callBack);
    }

    public void updateBillingAddress(Map<String, String> options) {
        ApiCallBack<JsonObject> callBack = new ApiCallBack<>(mBus, "updateBillingAddress", 0);
        mApiClient.updateBillingAddress(options, callBack);
    }

    /**
     * Api to get all  addresses
     */
    public void getAddresses(int position) {
        ApiCallBack<AddressResponse> callBack = new ApiCallBack<>(mBus, "getAddresses", position);
        mApiClient.getAddresses(callBack);
    }

    public void getAddresses() {
        ApiCallBack<AddressResponse> callBack = new ApiCallBack<>(mBus, "getAddresses", 0);
        mApiClient.getAddresses(callBack);
    }


    public void removeAddress(String address_Id, int position) {
        ApiCallBack<JsonObject> callBack = new ApiCallBack<>(mBus, "removeAddress", position);
        mApiClient.removeAddress(address_Id, callBack);
    }


    public void updateSubscriptionStatus(Map<String, String> options, String tokenId) {
        ApiCallBack<JsonObject> callBack = new ApiCallBack<>(mBus, "updateSubscriptionStatus", 0);
        mApiClient.updateSubscriptionStatus(options, tokenId, callBack);
    }

    /**
     * Api to get all  notification
     */
    public void getNotifications() {
        ApiCallBack<NotificationResponse> callBack = new ApiCallBack<>(mBus, "getNotifications", 0);
        mApiClient.getNotifications(callBack);
    }



    public void updateNotifications(Map<String, String> options) {
        ApiCallBack<JsonObject> callBack = new ApiCallBack<>(mBus, "updateNotifications", 0);
        mApiClient.updateNotifications(options, callBack);
    }

    /***
    Cart,wish,trailmode data changed
     * */
    public void inValidateDataChanged() {
        mBus.post(new ApiFailureListener("inValidateWishBadge", 0));

    }
    public void inValidateTrialChanged() {
        mBus.post(new ApiFailureListener("inValidateTrialMode", 0));

    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
//    NetWork Connection Check

    public void setConnectivityListener(ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }

}
