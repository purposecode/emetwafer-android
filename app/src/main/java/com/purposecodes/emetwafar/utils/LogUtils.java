package com.purposecodes.emetwafar.utils;

import android.util.Log;

import com.purposecodes.emetwafar.BuildConfig;


/**
 * Created by purposecodes on 9-7-2020.
 */
public  class LogUtils {
    public static void printLog(String tag,String message){

        if (BuildConfig.DEBUG)
        Log.d(tag,message);

    }

    public static void printLogLongString(String tag,String message){

        int maxLogSize = message.length();
        if (BuildConfig.DEBUG)
        for(int i = 0; i <= message.length() / maxLogSize; i++) {
            int start = i * maxLogSize;
            int end = (i+1) * maxLogSize;
            end = end > message.length() ? message.length() : end;
            printLog(tag,i+" :   "+ message.substring(start, end));
        }



    }
}
