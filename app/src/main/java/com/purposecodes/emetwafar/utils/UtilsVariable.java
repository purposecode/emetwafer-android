package com.purposecodes.emetwafar.utils;



import static com.purposecodes.emetwafar.BuildConfig.DEBUG;

import com.purposecodes.emetwafar.R;

public class UtilsVariable {

    public static final String DEFAULT_COUNTRY_NAME = "Saudi Arabia";
    public static final String DEFAULT_COUNTRY_CODE = "191";
    public static final String DEFAULT_CURRENCY = "SAR";


    //parms
    public static final String ARG_OTPMODEL = "param_otp";
    public static final String ARG_COMPANY_REGISTER = "param_register";


    public static final String SHARED_PREF = DEBUG? "sharedPref_test":"sharedPref";
    public static final String SHARED_PREF_URL ="sharedPref_url";

    public static final String PREF_USER_INFO = "userInfo";
    public static final String PREF_IS_REGISTERED = "isRegistered";
    public static final String PREF_REFRESH_TOKEN = "refreshToken";
    public static final String PREF_LOGIN_TYPE = "loginType";
    public static final String PREF_BASE_URL = "baseUrl";

    public static final String PREF_CART_ID = "cart_id";
    public static final String PREF_CART_COUNT = "cart_count";
    public static final String PREF_WISH_COUNT = "wish_count";
    public static final String PREF_CHECK_EXPIRY = "expiry_date";
    public static final String PREF_UPDATE_EXPIRY = "update_build_expiry";
    public static final String PREF_NOTIFICATION_COUNT = "notification_count";
    public static final String PREF_MAINTENANCE_MODE = "is_maintenance_mode";
    public static final String PREF_TRIAL_MODE = "is_trial_mode";




    public enum FileType {
        IMAGE,
        DOC
    }

    public enum CouponViewType {
        APPLY, FAILED, SUCCUSS
    }




    //    order status
    public static final String CONFIRMED = "CONFIRMED";
    public static final String RETURNED = "RETURNED";
    public static final String DELIVERED = "DELIVERED";
    public static final String CANCELLED = "CANCELLED";
    public static final String NOT_PLACED = "NOT PLACED";

    //    payment status
    public static final String REFUND_CONFIRMED = "REFUND CONFIRMED";
    public static final String REFUND_REQUESTED = "REFUND REQUESTED";
    public static final String NOT_PAID = "NOT PAID";
    public static final String PAID = "PAID";

    //    shipping status
    public static final String SHIPPED = "SHIPPED";
    public static final String NOT_SHIPPED = "NOT SHIPPED";

    /**
     * LAYOUT VISIBILITIES
     */

    public static final int VIEW_PROGRESSBAR = 1;
    public static final int VIEW_RECYCLERVIEW = 2;
    public static final int VIEW_ERRORVIEW = 3;
    public static final int VIEW_DATA = 4;
    public static final int VIEW_SWIPEREFRESH = 5;


    //error view images
    public static final int WRONG_RESPONSE = R.drawable.bad_gateway;
    public static final int NO_NETWORK = R.drawable.network_error;
    public static final int NO_DATA = R.drawable.no_product;
    public static final int NO_COUPON = R.drawable.no_coupons;


    // Details section
    //    ActivityType
    public static final String CALLING_ACTIVITY_KEY = "calling_activity";

    public static final int REQUEST_PRODUCT_FILTER = 102;
    public static final int REQUEST_ADD_NEW_ADDRESS = 103;
    public static final int REQUEST_ADD_EDIT_ADDRESS = 105;
    public static final int REQUEST_ORDER_CHECKOUT = 106;
    public static final int REQUEST_EDIT_BILLING_ADDRESS = 108;
    public static final int REQUEST_ORDER_DETAIL = 109;
    public static final int REQUEST_SUBSCRIPTION = 110;
    public static final int REQUEST_PRODUCT_DETAILS = 111;


    public static final String TYPE_TAG_CATEGORY = "category_tag";

    public static final String TAG_PRODUCT_ID = "product_id_tag";
    public static final String TAG_ORDER = "order_tag";
    public static final String TAG_ORDER_ID = "order_id_tag";
    public static final String TAG_IS_UPDATE = "is_update_tag";
    public static final String TAG_CHECKOUT = "checkout_tag";
    public static final String TAG_BUY_NOW_ID = "buy_now_tag";
    public static final String TAG_BILLING_ADDRESS = "billing_address_tag";
    public static final String TAG_PDF_URL = "pdf_tag";

    public static final String TAG_DISCOUNT = "discount_tag";
    public static final String TAG_OFFERS = "offers_tag";
    public static final String TAG_PRICE_MIN = "min_price_tag";
    public static final String TAG_PRICE_MAX = "max_price_tag";

    public static final String TAG_SAVED_ADDRESS = "saved_address_tag";
    public static final String TAG_POSITION = "position_tag";
    public static final String TAG_SUBSCRIPTIONS = "subscription_tag";
    public static final String TAG_TRIAL_MODE = "trial_mode_tag";
    public static final String TAG_IS_WISHED = "wished_tag";
    public static final String TAG_REMAINDER_DAY = "remainder_day_tag";


    public static final String TAG_MESSAGE = "message_tag";


    public static final String TYPE_ORDER_PAYMENT = "orderPaymentStatus";


    public static final String DEVICE_OS_KEY = "device_os";
    public static final String DEVICE_VERSION_KEY = "version_info";
    public static final String DEVICE_OS = "android";

    // Notification Type
    public static final String TYPE_IMAGE_PUSH = "image_push";

    public static final String TYPE_UPDATE = "appupdate";
    public static final String TYPE_LINK = "link_message";
    public static final String TYPE_ORDER = "order";
    public static final String TYPE_PRODUCT = "product";
    public static final String TYPE_SUBSCRIPTION_REMAINDER = "renew";
    public static final String TYPE_SUBSCRIPTION_EXPIRY = "subscription_expiry";
    public static final String TYPE_SUBSCRIPTION = "subscription";
    public static final String TYPE_MESSAGE = "message";


    public static final String OTHER_TAG = "other";
    public static final String PLACE_HOLDER_TAG = "placeholder";




    // Firebase topics
    public static final String DEBUG_TOPIC_VERSION_UPDATE = "version_update_demo"; //test
    public static final String RELEASE_TOPIC_VERSION_UPDATE = "version_update";  //live

     public static final String DEBUG_TOPIC_MESSAGE = "message_demo"; //test
     public static final String RELEASE_TOPIC_MESSAGE = "message"; //live

    // Terms and conditions on register page text

//   I have read and accepted terms and conditions of the agreement.
public static final String AGREE_TEXT = "<body> I  have read and accepted <a href=\"https://metwafer.com/terms/\">terms</a>  and <a href=\"" + "https://metwafer.com/privacy/" +  "\">privacy policy</a> </body>";



}
