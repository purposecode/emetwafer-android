package com.purposecodes.emetwafar.utils;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by PURPOSE CODE on 9/16/2016.
 */
public class NotificationID {

    private final static AtomicInteger c = new AtomicInteger(0);
    public static int getID() {
        return c.incrementAndGet();
    }
}
