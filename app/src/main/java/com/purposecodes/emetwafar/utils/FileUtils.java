package com.purposecodes.emetwafar.utils;

import static com.purposecodes.emetwafar.utils.LogUtils.printLog;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;

import androidx.annotation.NonNull;

import com.purposecodes.emetwafar.controller.AppController;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

public class FileUtils {

     private static final String TAG = "FileUtils";


    // Media Type
    public static final String IMAGE = "Image";
    public static final String PDF = "Pdf";

    public static String PARENT_DIRECTORY = "emetwafar";
    public static String IMAGE_DIRECTORY = "Photos";
    public static String PDF_DIRECTORY = "Pdf";
    public static String DOWNLOAD_DIRECTORY = "Downloads";
    public static String NO_MEDIA_FILE = ".nomedia";


    public static String directoryGenerator(String fileType) {



        File file;
        switch (fileType) {


                case IMAGE:
//                file =new File(AppController.get().getCacheDir().getPath()+ File.separator + PARENT_DIRECTORY + File.separator + IMAGE_DIRECTORY);
//                 file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getPath() +File.separator + PARENT_DIRECTORY + File.separator + IMAGE_DIRECTORY);


                    if (Build.VERSION.SDK_INT >= 30)
                        file = new File(Objects.requireNonNull(AppController.get().getExternalFilesDir("")).getPath() + File.separator + PARENT_DIRECTORY + File.separator + IMAGE_DIRECTORY);
                    else
                        file = new File(Environment.getExternalStorageDirectory(), File.separator + PARENT_DIRECTORY + File.separator + IMAGE_DIRECTORY);

                    break;
            case PDF: //public file

                if (Build.VERSION.SDK_INT >= 30)
                    file = new File(Objects.requireNonNull(AppController.get().getExternalFilesDir("")).getPath() +File.separator + PARENT_DIRECTORY );
                else
                    file = new File(Environment.getExternalStorageDirectory(), File.separator + PARENT_DIRECTORY + File.separator + PDF_DIRECTORY);
                break;


            default:
                file = new File(Objects.requireNonNull(AppController.get().getExternalFilesDir("")).getPath()+ File.separator + PARENT_DIRECTORY + File.separator + DOWNLOAD_DIRECTORY);

        }

        if (!file.isDirectory() && !file.exists()) {
            file.mkdirs();
        }

        return file.getAbsolutePath();
    }





    public static String fileDirectoryGenerator(String fileType) {
        /*
        String parentFilePath = directoryGenerator(fileType);

        if (!TextUtils.isEmpty(parentFilePath)) {

            File file = new File(parentFilePath);

            if (!file.exists()) {

                file.mkdirs();
                File noMediaFile = new File(file.getAbsolutePath(), NO_MEDIA_FILE);
                try {
                    noMediaFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            printLog(TAG, "fileDirectoryGenerator getAbsolutePath : " + file.getAbsolutePath());

            return file.getAbsolutePath();
        }
        return "";



*/



        String parentFilePath = directoryGenerator(fileType);

        if (!TextUtils.isEmpty(parentFilePath)) {

            File directory = new File(parentFilePath );

            if (!directory.isDirectory() && !directory.exists())
                directory.mkdirs();

            if (fileType.equalsIgnoreCase(IMAGE)) {

                File noMediaFile = new File(directory.getAbsolutePath(), NO_MEDIA_FILE);

                if (!noMediaFile.exists())
                    try {
                        noMediaFile.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                        printLog("FileUtils", "fileDirectoryGenerator IOException : " + e.getMessage());
                    }
            }

            return directory.getAbsolutePath();
        }
        return "";














    }



    public static String fileNameGenerator(String fileType) {

        Date date = new Date();
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.getDefault()).format(date.getTime());

        switch (fileType) {

            case PDF:
                return "DOC-" + timeStamp + ".pdf";
//            case CAPTURE:
            case IMAGE:
                return "IMG-" + timeStamp + ".jpg";

            default:
                return "";
        }
    }



    public static String getNewFilePath(String fileType) {
        return  fileDirectoryGenerator(fileType) + File.separator  +fileNameGenerator(fileType);
    }


    public static String getFileNameFromPath(String filePath) {
        String fileName = filePath.substring(filePath.lastIndexOf(File.separator) + 1);
        return fileName.substring(0, fileName.indexOf("."));
    }

    public static String getFileNameWithExtension(String filePath) {

        return filePath.substring(filePath.lastIndexOf(File.separator) + 1);
    }








    /**
     * Resizing the image independently of the screen density of the device. Maintains image aspect
     * ratio without loosing quality.
     *
     * New width and height are respectively 1224 and 918
     *
     * @param filePath : Source Image File Path
     * @return : Scaled Bitmap
     */
    public static Bitmap resizeImage(String filePath) {

        Bitmap scaledBitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();

        // By setting this field as true, the actual bitmap pixels are not loaded in the memory.
        // Just the bounds are loaded. If
        // you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

        // max Height and width values of the compressed image is taken as 1224x918

        float maxHeight = 1224.0f;
        float maxWidth = 918.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

        // width and height values are set maintaining the aspect ratio of the image
        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            }
            else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            }
            else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;
            }
        }

        // setting inSampleSize value allows to load a scaled down version of the original image
        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

        // inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

        try {
            // load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        }
        catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);

            float ratioX = actualWidth / (float) options.outWidth;
            float ratioY = actualHeight / (float) options.outHeight;
            float middleX = actualWidth / 2.0f;
            float middleY = actualHeight / 2.0f;

            Matrix scaleMatrix = new Matrix();
            scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

            Canvas canvas = new Canvas(scaledBitmap);
            canvas.setMatrix(scaleMatrix);
            canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

            // check the rotation of the image and display it properly
            ExifInterface exif;
            try {
                exif = new ExifInterface(filePath);

                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
                Matrix matrix = new Matrix();

                if (orientation == 6) {
                    matrix.postRotate(90);
                }
                else if (orientation == 3) {
                    matrix.postRotate(180);
                }
                else if (orientation == 8) {
                    matrix.postRotate(270);
                }

                // canvas.drawColor(0, PorterDuff.Mode.CLEAR);
                bmp.recycle();
                scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,true);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        return scaledBitmap;
    }


    /**
     * Calculating new sample size
     * @param options options containing details of image
     * @param reqWidth new Width
     * @param reqHeight new Height
     * @return new sampling size
     */
    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height/ (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;

        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }
    /**
     * Saving the bitmap to specified location
     *
     * @param src : Source bitmap
     * @return : The saved file Path
     */
    @NonNull
    public static String saveBitmapToExternalStorage(Bitmap src) {

        FileOutputStream out;
        String filename = FileUtils.fileDirectoryGenerator(IMAGE);
        filename = filename + File.separator + FileUtils.fileNameGenerator(IMAGE);

        try {
            out = new FileOutputStream(filename);

            //  write the compressed bitmap at the destination specified by filename.
            src.compress(Bitmap.CompressFormat.JPEG, 90, out);
            src.recycle();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;
    }

    public static String getPathFromURI(Context context, Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }


    public static double getFileSizeMegaBytes(File file) {
        return (double) file.length() / (1024 * 1024);
    }



    //MASHMALLOW

    public  static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }




    public static String getFileMimeType(File file){
        String fileExtension = getFileExtension(file);
        return MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension);
    }

    private static String getFileExtension(File file){
        Uri uri = Uri.fromFile(file);
        return MimeTypeMap.getFileExtensionFromUrl(uri.toString());
    }
}
