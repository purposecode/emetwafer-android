package com.purposecodes.emetwafar.utils;

import static com.purposecodes.emetwafar.utils.UtilsVariable.DEFAULT_CURRENCY;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Patterns;
import android.webkit.URLUtil;

import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.controller.AppController;

import java.net.URL;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UtilsFunctions {


    public static String getUrlFromText(String text) {
        String url = null;

        Pattern p = Patterns.WEB_URL;
        Matcher m = p.matcher(text);
        boolean result = m.find();

        if (result) {
            url = m.group(0);
        }

        return url;
    }




    public static String capitalizeString(String str) {
        String retStr = str;
        try { // We can face index out of bound exception if the string is null
            retStr = str.substring(0, 1).toUpperCase() + str.substring(1);
        }catch (Exception e){
            return retStr;
        }
        return retStr;
    }


    public static boolean isAppRunning(Context context) {
        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        String packageName = context.getPackageName();

        if (activityManager != null) {
            final List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();

            if (procInfos != null) {
                for (final ActivityManager.RunningAppProcessInfo processInfo : procInfos) {
                    if (processInfo.processName.equals(packageName)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public static String getAppVersion() {
        String appVersion = "";
        try {
            PackageInfo pInfo = AppController.get().getPackageManager().getPackageInfo(AppController.get().getPackageName(), 0);
            appVersion = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return appVersion;
    }

    public static boolean isAppInstalled(Context context, String packageName) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean isUsingLatestVersion(String newVersion) {
        return getAppVersion().equals(newVersion);
    }

    //convert version wise url key frm realtime db
    public static String getVersionWiseKey() {
      /*  String cVersion = "";

       try {
            PackageInfo pInfo = AppController.get().getPackageManager().getPackageInfo(AppController.get().getPackageName(), 0);
            cVersion = String.valueOf(pInfo.versionName).replace(".","_");

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
*/
        return getAppVersion().replace(".","_");
    }

    /**
     * Password must contain 1 digit, 1 lower case letter, 1 uppercase letter and
     * 8 character length.
     *
     * @param password user entered password
     * @return true if password matching
     */
    public static boolean isValidPassword(String password) {
        Pattern pattern;
        Matcher matcher;

                final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,}$";  //old
//        final String PASSWORD_PATTERN = "^(?=.*[#$^+=!*()@%&])(?=\\S+$).{6,}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();


    }

    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isValidWeb(CharSequence target) {
        return !TextUtils.isEmpty(target)
                && Patterns.WEB_URL.matcher(target).matches();
    }

    public static boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    public static boolean checkURL(CharSequence input) {
        if (TextUtils.isEmpty(input)) {
            return false;
        }
        Pattern URL_PATTERN = Patterns.WEB_URL;
        boolean isURL = URL_PATTERN.matcher(input).matches();
        if (!isURL) {
            String urlString = input + "";
            if (URLUtil.isNetworkUrl(urlString)) {
                try {
                    new URL(urlString);
                    isURL =
                            true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return isURL;
    }


    public static SpannableStringBuilder convertPriceText(String _price) {

        String firstWord = _price;

// Use a SpannableStringBuilder so that both the text and the spans are mutable
        SpannableStringBuilder ssb = new SpannableStringBuilder(firstWord);
        try {
            String secondWord = DEFAULT_CURRENCY;


// Create a span that will make the text red
            ForegroundColorSpan redForegroundColorSpan = new ForegroundColorSpan(AppController.get().getResources().getColor(R.color.redColor));


            ssb.setSpan(redForegroundColorSpan, 0, ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
// Add a blank space
            ssb.append(" ");

            ssb.append(secondWord);

        } catch (Exception e) {
            e.fillInStackTrace();
        }

// Set the TextView text and denote that it is Editable
// since it's a SpannableStringBuilder
        return ssb;

    }

    //    double value to amount format
    public static String getAmount(Double amount) {
        String str = "";
        try {
            DecimalFormat formatter = new DecimalFormat("##,##,##,###.00#");
            str = formatter.format(amount);
        } catch (IllegalArgumentException e) {
            e.fillInStackTrace();
        }
        return str;
    }

    public static SpannableStringBuilder convertWarrantyPriceFormatText(String text,String _price) {

        String firstWord = text;

// Use a SpannableStringBuilder so that both the text and the spans are mutable
        SpannableStringBuilder ssb = new SpannableStringBuilder(firstWord);
        try {
            String secondWord = _price;

            // Add a blank space
            ssb.append(" ");

            ssb.append(secondWord);

// Create a span that will make the text red
            ForegroundColorSpan redForegroundColorSpan = new ForegroundColorSpan(AppController.get().getResources().getColor(R.color.redColor));
            ssb.setSpan(redForegroundColorSpan, ssb.length()-secondWord.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            ssb.append(" ");
            ssb.append(DEFAULT_CURRENCY);

        } catch (Exception e) {
            e.fillInStackTrace();
        }

// Set the TextView text and denote that it is Editable
// since it's a SpannableStringBuilder
        return ssb;

    }




    public static  String getCurrentTime() {
        //getting current date and time using Date class
        DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
//        Date dateobj = new Date();
//        System.out.println(df.format(dateobj));

        /*getting current date time using calendar class
         * An Alternative of above*/
        Calendar calobj = Calendar.getInstance();
        return df.format(calobj.getTime());
    }



    public static String getTimeStamp() {

        //method 1
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        return String.valueOf(timestamp.getTime());

    }
    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int dpToPx(float dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int getPixels(int dps, Context context) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dps * scale + 0.5f);
    }


}
