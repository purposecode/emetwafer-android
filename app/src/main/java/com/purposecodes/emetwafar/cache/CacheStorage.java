package com.purposecodes.emetwafar.cache;

import android.content.Context;
import android.content.SharedPreferences;

import com.purposecodes.emetwafar.controller.AppController;

import static android.content.Context.MODE_PRIVATE;
import static com.purposecodes.emetwafar.utils.UtilsVariable.SHARED_PREF;

public class CacheStorage {

    /**
     *  cache keys variables
     */

    public static final String PRODUCTS_CACHE_KEY = "key_cache_products";
    public static final String CATEGORY_CACHE_KEY = "key_cache_category";
    public static final String WISHLIST_CACHE_KEY = "key_cache_wish";





    // A handler to this application class
    private static CacheStorage cacheStorage;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Context context;


    // Constructor
    private CacheStorage(Context context){
        this.context = context;
        preferences = this.context.getSharedPreferences(SHARED_PREF, MODE_PRIVATE);
        editor = preferences.edit();


    }

    public static synchronized CacheStorage getInstance() {
        if (cacheStorage==null)
            cacheStorage=new CacheStorage(AppController.get());
        return cacheStorage;
    }

    public void writeCache(String key,String data){
        editor.putString(key , data);
        editor.apply();
    }

    public String readCache(String key){
        return preferences.getString(key, "");
    }
}
