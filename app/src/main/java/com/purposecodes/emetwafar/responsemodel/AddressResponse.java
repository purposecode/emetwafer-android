package com.purposecodes.emetwafar.responsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.purposecodes.emetwafar.model.SavedAddress;

import java.util.ArrayList;

public class AddressResponse {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("text")
    @Expose
    private String text;

    @SerializedName("data")
    @Expose
    private ArrayList<SavedAddress> savedAddressList = null;


    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ArrayList<SavedAddress> getSavedAddressList() {
        return savedAddressList;
    }

    public void setSavedAddressList(ArrayList<SavedAddress> savedAddressList) {
        this.savedAddressList = savedAddressList;
    }
}
