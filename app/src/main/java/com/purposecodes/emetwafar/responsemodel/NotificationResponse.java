package com.purposecodes.emetwafar.responsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.purposecodes.emetwafar.model.NotificationItem;

import java.util.List;

public class NotificationResponse {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("text")
    @Expose
    private String text;


    @SerializedName("data")
    @Expose
    private NotificationData notificationData;



    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public NotificationData getNotificationData() {
        return notificationData;
    }

    public void setNotificationData(NotificationData notificationData) {
        this.notificationData = notificationData;
    }

    public class NotificationData{

        @SerializedName("notification_count")
        @Expose
        private Integer count;

        @SerializedName("notification")
        @Expose
        private List<NotificationItem> notificationList=null;

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }

        public List<NotificationItem> getNotificationList() {
            return notificationList;
        }

        public void setNotificationList(List<NotificationItem> notificationList) {
            this.notificationList = notificationList;
        }
    }
}
