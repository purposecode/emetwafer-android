package com.purposecodes.emetwafar.responsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterResponse {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("text")
    @Expose
    private String text;

    /**
     *
     * @return
     * The code
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The code
     */
    public void setBoolean(Boolean status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The text
     */
    public String getText() {
        return text;
    }

    /**
     *
     * @param text
     * The text
     */
    public void setText(String text) {
        this.text = text;
    }
}
