package com.purposecodes.emetwafar.responsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OtpResponseModel implements Serializable {
    @SerializedName("status")
    @Expose
    private Boolean status;

    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("data")
    @Expose
    private OtpData otpData;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public OtpData getOtpDataData() {
        return otpData;
    }

    public void setOtpDataData(OtpData otpData) {
        this.otpData = otpData;
    }


    public class OtpData implements Serializable {

        @SerializedName("otp")
        @Expose
        private Integer otp;
        @SerializedName("token")
        @Expose
        private String token;

        public Integer getOtp() {
            return otp;
        }

        public void setOtp(Integer otp) {
            this.otp = otp;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }
    }
}
