package com.purposecodes.emetwafar.responsemodel;

import androidx.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.purposecodes.emetwafar.model.ProductSpec;
import com.purposecodes.emetwafar.model.SliderImage;
import com.purposecodes.emetwafar.model.Warranty;

import java.util.ArrayList;
import java.util.List;

public class ProductDetailsResponse {


    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("text")
    @Expose
    private String text;

    @SerializedName("data")
    @Expose
    private ProductDetail productDetail = null;


    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ProductDetail getProductDetail() {
        return productDetail;
    }

    public void setProductDetail(ProductDetail productDetail) {
        this.productDetail = productDetail;
    }

    public static class ProductDetail {

        @SerializedName("product_id")
        @Expose
        private String productId;
        @SerializedName("product_code")
        @Expose
        private String productCode;
        @SerializedName("product_name")
        @Expose
        private String productName;
        @SerializedName("category_id")
        @Expose
        private String categoryId;
        @SerializedName("category_name")
        @Expose
        private String categoryName;
        @SerializedName("product_price")
        @Expose
        private String productPrice;
        @SerializedName("product_unit")
        @Expose
        private String productUnit;
        @SerializedName("product_offer")
        @Expose
        private String productOffer;
        @SerializedName("offer_price")
        @Expose
        private String offerPrice;
        @SerializedName("discount")
        @Expose
        private String discount;
        @SerializedName("product_desc")
        @Expose
        private String productDesc;
        @SerializedName("tech_spec_url")
        @Expose
        private String techSpecUrl;
        @SerializedName("is_in_wishlist")
        @Expose
        private Integer isInWishlist;
        @SerializedName("is_in_cart")
        @Expose
        private Integer isInCart;
        @SerializedName("available_quantity")
        @Expose
        private String availableQuantity;

        @Nullable
        @SerializedName("is_available")
        @Expose
        private Integer isAvailable ;

        @SerializedName("image_url")
        @Expose
        private String imageUrl;
        @SerializedName("supplier")
        @Expose
        private String supplier;
        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("commodity")
        @Expose
        private String commodity;
        @SerializedName("sub_commodity")
        @Expose
        private String subCommodity;
        @SerializedName("age")
        @Expose
        private String age;
        @SerializedName("discipline")
        @Expose
        private String discipline;

        @SerializedName("warranty")
        @Expose
        private String normalWarranty;


        @SerializedName("images")
        @Expose
        private List<SliderImage> sliderImages = null;


        @Nullable
        @SerializedName("tech_specs")
        @Expose
        private List<ProductSpec> productSpecs = null;


        @Nullable
        @SerializedName("warranty_specs")
        @Expose
        private ArrayList<Warranty> warranties = null;



        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getProductCode() {
            return productCode;
        }

        public void setProductCode(String productCode) {
            this.productCode = productCode;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(String categoryId) {
            this.categoryId = categoryId;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public String getProductPrice() {
            return productPrice;
        }

        public void setProductPrice(String productPrice) {
            this.productPrice = productPrice;
        }

        public String getProductUnit() {
            return productUnit;
        }

        public void setProductUnit(String productUnit) {
            this.productUnit = productUnit;
        }

        public String getProductOffer() {
            return productOffer;
        }

        public void setProductOffer(String productOffer) {
            this.productOffer = productOffer;
        }

        public String getOfferPrice() {
            return offerPrice;
        }

        public void setOfferPrice(String offerPrice) {
            this.offerPrice = offerPrice;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getProductDesc() {
            return productDesc;
        }

        public void setProductDesc(String productDesc) {
            this.productDesc = productDesc;
        }

        public String getTechSpecUrl() {
            return techSpecUrl;
        }

        public void setTechSpecUrl(String techSpecUrl) {
            this.techSpecUrl = techSpecUrl;
        }

        public Integer getIsInWishlist() {
            return isInWishlist;
        }

        public void setIsInWishlist(Integer isInWishlist) {
            this.isInWishlist = isInWishlist;
        }

        public Integer getIsInCart() {
            return isInCart;
        }

        public void setIsInCart(Integer isInCart) {
            this.isInCart = isInCart;
        }

        public String getAvailableQuantity() {
            return availableQuantity;
        }

        public void setAvailableQuantity(String availableQuantity) {
            this.availableQuantity = availableQuantity;
        }

        @Nullable
        public Integer getIsAvailable() {
            return isAvailable;
        }



        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public String getSupplier() {
            return supplier;
        }

        public void setSupplier(String supplier) {
            this.supplier = supplier;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getCommodity() {
            return commodity;
        }

        public void setCommodity(String commodity) {
            this.commodity = commodity;
        }

        public String getSubCommodity() {
            return subCommodity;
        }

        public void setSubCommodity(String subCommodity) {
            this.subCommodity = subCommodity;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getDiscipline() {
            return discipline;
        }

        public void setDiscipline(String discipline) {
            this.discipline = discipline;
        }

        public String getNormalWarranty() {
            return normalWarranty;
        }

        public void setNormalWarranty(String normalWarranty) {
            this.normalWarranty = normalWarranty;
        }

        public List<SliderImage> getSliderImages() {
            return sliderImages;
        }

        public void setSliderImages(List<SliderImage> sliderImages) {
            this.sliderImages = sliderImages;
        }

        public List<ProductSpec> getProductSpecs() {
            return productSpecs;
        }

        public void setProductSpecs(List<ProductSpec> productSpecs) {
            this.productSpecs = productSpecs;
        }


        @Nullable
        public ArrayList<Warranty> getWarranties() {
            return warranties;
        }

        public void setWarranties(@Nullable ArrayList<Warranty> warranties) {
            this.warranties = warranties;
        }
    }


}
