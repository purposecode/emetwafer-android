package com.purposecodes.emetwafar.responsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.purposecodes.emetwafar.model.Subscription;

import java.util.List;

public class SubscriptionsResponse {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("text")
    @Expose
    private String text;

    @SerializedName("data")
    @Expose
    private SubscriptionResponseData subscriptionResponseData;


    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public SubscriptionResponseData getSubscriptionResponseData() {
        return subscriptionResponseData;
    }

    public void setSubscriptionResponseData(SubscriptionResponseData subscriptionResponseData) {
        this.subscriptionResponseData = subscriptionResponseData;
    }

    public static class SubscriptionResponseData {

        @SerializedName("android_trial_mode")
        @Expose
        private String trialMode;

        @SerializedName("plans")
        @Expose
        private List<Subscription> subscriptionList = null;

        public String getTrialMode() {
            return trialMode;
        }

        public void setTrialMode(String trialMode) {
            this.trialMode = trialMode;
        }

        public List<Subscription> getSubscriptionList() {
            return subscriptionList;
        }

        public void setSubscriptionList(List<Subscription> subscriptionList) {
            this.subscriptionList = subscriptionList;
        }
    }
}
