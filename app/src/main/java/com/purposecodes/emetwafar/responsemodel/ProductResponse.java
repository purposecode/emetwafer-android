package com.purposecodes.emetwafar.responsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.purposecodes.emetwafar.model.Product;

import java.util.List;

public class ProductResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("text")
    @Expose
    private String text;

    @SerializedName("data")
    @Expose
    private ResponseResult responseResult;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ResponseResult getResponseResult() {
        return responseResult;
    }

    public void setResponseResult(ResponseResult responseResult) {
        this.responseResult = responseResult;
    }

    public class ResponseResult{

        @SerializedName("pageCount")
        @Expose
        private Integer pageCount;
        @SerializedName("currentPage")
        @Expose
        private String currentPage;
        @SerializedName("totalCount")
        @Expose
        private Integer totalCount;
        @SerializedName("products")
        @Expose
        private List<Product> products = null;


        public Integer getPageCount() {
            return pageCount;
        }

        public void setPageCount(Integer pageCount) {
            this.pageCount = pageCount;
        }

        public String getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(String currentPage) {
            this.currentPage = currentPage;
        }

        public Integer getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(Integer totalCount) {
            this.totalCount = totalCount;
        }

        public List<Product> getProducts() {
            return products;
        }

        public void setProducts(List<Product> products) {
            this.products = products;
        }
    }
}
