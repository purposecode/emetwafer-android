package com.purposecodes.emetwafar.responsemodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.purposecodes.emetwafar.model.SubscriptionHistoryItem;

import java.util.List;

public class SubscriptionHistoryResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("text")
    @Expose
    private String text;

    @SerializedName("data")
    @Expose
    private List<SubscriptionHistoryItem> items = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<SubscriptionHistoryItem> getItems() {
        return items;
    }

    public void setItems(List<SubscriptionHistoryItem> items) {
        this.items = items;
    }
}
