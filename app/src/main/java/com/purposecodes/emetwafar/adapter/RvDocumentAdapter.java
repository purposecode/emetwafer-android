package com.purposecodes.emetwafar.adapter;

import static com.purposecodes.emetwafar.utils.LogUtils.printLog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.model.FileModal;
import com.purposecodes.emetwafar.utils.UtilsVariable;

import java.io.File;
import java.util.ArrayList;

public class RvDocumentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private final String TAG = "RvDocumentAdapter";
    // Context
    private final Context mContext;

    // List

    private final ArrayList<FileModal> mList;

    // Image remove listener
    private final OnRemoveListener mListener;

    public RvDocumentAdapter(Context context, ArrayList<FileModal> list, OnRemoveListener listener) {
        mContext = context;
        mList = list;
        mListener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_verification_doc, parent, false);
        return new RvDocumentHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, @SuppressLint("RecyclerView") int position) {
        FileModal fileModal = mList.get(position);

        printLog(TAG, "showSelectedImage onBindViewHolder : " + fileModal);

        RvDocumentHolder holder = (RvDocumentHolder) viewHolder;

        if (fileModal.getType().equals(UtilsVariable.FileType.IMAGE))
            Glide.with(mContext)
                    .load(new File(fileModal.getPath()))
//                    .placeholder(R.drawable.ic_upload_placeholder)
//                    .error(R.drawable.ic_upload_placeholder)
                    .apply(new RequestOptions()
                            .centerCrop()
                            .placeholder(R.drawable.ic_upload_placeholder)
                            .error(R.drawable.ic_upload_placeholder))
                    .into(holder.mImageView);
        else
            Glide.with(mContext)
                    .load(R.drawable.ic_doc_placeholder)
                    .apply(new RequestOptions()
                            .fitCenter())
                    .into(holder.mImageView);


        holder.ibClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               showConfirmDialog(position);
            }
        });

    }

    private void showConfirmDialog(final int position) {
        new AlertDialog.Builder(mContext)
                .setTitle("Remove")
                .setMessage("Are sure want to remove this File")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (mListener != null)
                            mListener.onRemove(position);
                        dialogInterface.dismiss();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create()
                .show();
    }
    @Override
    public int getItemCount() {
        return mList.size();
    }

    static class RvDocumentHolder extends RecyclerView.ViewHolder {

        private final ImageView mImageView;
        private final ImageButton ibClose;

        RvDocumentHolder(View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.imageView_itemValidationDoc);
            ibClose = itemView.findViewById(R.id.imageButton_itemValidationDoc_close);
        }
    }

    public interface OnRemoveListener {
        void onRemove(int position);
    }
}
