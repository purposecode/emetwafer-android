package com.purposecodes.emetwafar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.purposecodes.emetwafar.R;

import java.util.List;

public class AddressSpinnerArrayAdapter extends ArrayAdapter<String> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final List items;
    private final boolean isDivider;


    public AddressSpinnerArrayAdapter(@NonNull Context context, @NonNull List objects, @NonNull boolean _isDivider) {
        super(context, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        items = objects;
        isDivider = _isDivider;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {


        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_spinner_code, parent, false);
        }
        TextView _title = convertView.findViewById(R.id.textView_item);


        String _item = items.get(position).toString();

        _title.setText(_item);

        if (position == 0) {
            // Set the hint text color gray
            _title.setTextColor(parent.getContext().getResources().getColor(R.color.grayText));
        } else {
            _title.setTextColor(parent.getContext().getResources().getColor(R.color.primaryText));
        }

        return convertView;
    }

    @Override
    public @NonNull
    View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.spinner_address_view_item, parent, false);

        }
        TextView _title = convertView.findViewById(R.id.textView_itemSpinnerView);
        View _divider = convertView.findViewById(R.id.separator_itemSpinnerView);

        _divider.setVisibility(isDivider ? View.VISIBLE : View.GONE);

        String _item = items.get(position).toString();

        _title.setText(_item);

        if (position == 0) {
            // Set the hint text color gray
            _title.setTextColor(parent.getContext().getResources().getColor(R.color.grayText));
        } else {
            _title.setTextColor(parent.getContext().getResources().getColor(R.color.primaryText));
        }
        return convertView;

    }


}