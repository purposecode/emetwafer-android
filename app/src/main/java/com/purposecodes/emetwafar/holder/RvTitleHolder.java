package com.purposecodes.emetwafar.holder;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.purposecodes.emetwafar.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RvTitleHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.textView_itemTitle)
    public TextView tvTitle;



    public RvTitleHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
