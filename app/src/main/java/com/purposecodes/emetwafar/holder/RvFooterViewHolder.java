package com.purposecodes.emetwafar.holder;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.purposecodes.emetwafar.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RvFooterViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.loadText)
    public TextView loadText;
    @BindView(R.id.loadProgress)
    public ProgressBar loadProgress;


    public RvFooterViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
