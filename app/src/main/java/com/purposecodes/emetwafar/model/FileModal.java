package com.purposecodes.emetwafar.model;

import com.purposecodes.emetwafar.utils.UtilsVariable;

public class FileModal {

    private String path;

    private UtilsVariable.FileType type;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public UtilsVariable.FileType getType() {
        return type;
    }

    public void setType(UtilsVariable.FileType type) {
        this.type = type;
    }
}
