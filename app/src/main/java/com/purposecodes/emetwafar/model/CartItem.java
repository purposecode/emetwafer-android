package com.purposecodes.emetwafar.model;

import androidx.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CartItem implements Serializable {

    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("product_code")
    @Expose
    private String productCode;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("product_price")
    @Expose
    private String productPrice;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("product_total")
    @Expose
    private String productTotal;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("product_offer")
    @Expose
    private String productOffer;
    @SerializedName("is_wishlist")
    @Expose
    private Integer isWishlist;
    @SerializedName("offer_price")
    @Expose
    private String offerPrice;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("supplier")
    @Expose
    private String supplier;
    @SerializedName("country")
    @Expose
    private String country;

    @Nullable
    @SerializedName("is_available")
    @Expose
    private Integer isAvailable ;

    @SerializedName("available_quantity")
    @Expose
    private String availableQuantity;

    @SerializedName("warranty_period")
    @Expose
    private String warrantyPeriod;
    @SerializedName("warranty_price")
    @Expose
    private String warrantyPrice;
    @SerializedName("warranty_total")
    @Expose
    private String warrantyTotal;


    @SerializedName("delivery_address")
    @Expose
    private SavedAddress  deliveryAddress;


    public String getProductId() {
        return productId;
    }

    public String getProductCode() {
        return productCode;
    }

    public String getProductName() {
        return productName;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public String getQuantity() {
        return quantity;
    }

    public String getProductTotal() {
        return productTotal;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getProductOffer() {
        return productOffer;
    }

    public Integer getIsWishlist() {
        return isWishlist;
    }

    public String getOfferPrice() {
        return offerPrice;
    }

    public String getDiscount() {
        return discount;
    }

    public String getSupplier() {
        return supplier;
    }

    public String getCountry() {
        return country;
    }

    @Nullable
    public Integer getIsAvailable() {
        return isAvailable;
    }

    public String getAvailableQuantity() {
        return availableQuantity;
    }

    public String getWarrantyPeriod() {
        return warrantyPeriod;
    }

    public String getWarrantyPrice() {
        return warrantyPrice;
    }

    public void setWarrantyTotal(String warrantyTotal) {
        this.warrantyTotal = warrantyTotal;
    }

    public String getWarrantyTotal() {
        return warrantyTotal;
    }

    public SavedAddress getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }




    public void setDiscount(String discount) {
        this.discount = discount;
    }



    public void setAvailableQuantity(String availableQuantity) {
        this.availableQuantity = availableQuantity;
    }


}
