package com.purposecodes.emetwafar.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CheckoutData implements Serializable {
    @SerializedName("checkout_id")
    @Expose
    private String checkoutId;
    @SerializedName("cart_id")
    @Expose
    private String cartId;
    @SerializedName("subtotal")
    @Expose
    private String subtotal;
    @SerializedName("coupon_code")
    @Expose
    private String couponCode;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("discount_percent")
    @Expose
    private String discountPercent;
    @SerializedName("vat")
    @Expose
    private String vat;
    @SerializedName("verify_cost")
    @Expose
    private String verifyCost;
    @SerializedName("warranty_total")
    @Expose
    private String warrantyAmount;
    @SerializedName("grand_total")
    @Expose
    private String grandTotal;

    @SerializedName("billing_address")
    @Expose
    private BillingAddress billingAddress;

    @SerializedName("items")
    @Expose
    private List<CartItem> cartItems = null;

    public String getCheckoutId() {
        return checkoutId;
    }

    public void setCheckoutId(String checkoutId) {
        this.checkoutId = checkoutId;
    }

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(String discountPercent) {
        this.discountPercent = discountPercent;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getVerifyCost() {
        return verifyCost;
    }

    public void setVerifyCost(String verifyCost) {
        this.verifyCost = verifyCost;
    }

    public String getWarrantyAmount() {
        return warrantyAmount;
    }

    public void setWarrantyAmount(String warrantyAmount) {
        this.warrantyAmount = warrantyAmount;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public BillingAddress getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(BillingAddress billingAddress) {
        this.billingAddress = billingAddress;
    }

    public List<CartItem> getCartItems() {
        return cartItems;
    }

    public void setCartItems(List<CartItem> cartItems) {
        this.cartItems = cartItems;
    }
}
