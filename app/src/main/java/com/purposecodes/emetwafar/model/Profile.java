package com.purposecodes.emetwafar.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Profile implements Serializable {

    @SerializedName("android_trial_mode")
    @Expose
    private String trialMode;

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("company_id")
    @Expose
    private String companyId;
    @SerializedName("role")
    @Expose
    private String role;
    @SerializedName("twofa")
    @Expose
    private String twofa;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("profileimg")
    @Expose
    private String profileimg;
    @SerializedName("company_dir")
    @Expose
    private String companyDir;
    @SerializedName("reg_date")
    @Expose
    private String regDate;
    @SerializedName("tokenId")
    @Expose
    private String tokenId;
    @SerializedName("expires")
    @Expose
    private String expires;
    @SerializedName("reg_status")
    @Expose
    private String regStatus;
    @SerializedName("verify_status")
    @Expose
    private String verifyStatus;
    @SerializedName("logged_status")
    @Expose
    private String loggedStatus;
    @SerializedName("modified_date")
    @Expose
    private String modifiedDate;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("company_about")
    @Expose
    private String companyAbout;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("address2")
    @Expose
    private String address2;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("zipcode")
    @Expose
    private String zipcode;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("fax")
    @Expose
    private String fax;
    @SerializedName("year_of_establish")
    @Expose
    private String yearOfEstablish;
    @SerializedName("no_of_employee")
    @Expose
    private String noOfEmployee;
    @SerializedName("reg_no")
    @Expose
    private String regNo;
    @SerializedName("interested_in")
    @Expose
    private String interestedIn;
    @SerializedName("business_activity")
    @Expose
    private String businessActivity;
    @SerializedName("turnover")
    @Expose
    private String turnover;
    @SerializedName("vat_no")
    @Expose
    private String vatNo;
    @SerializedName("profit")
    @Expose
    private String profit;
    @SerializedName("operation_type")
    @Expose
    private String operationType;
    @SerializedName("other_op")
    @Expose
    private String otherOp;
    @SerializedName("is_admin")
    @Expose
    private String isAdmin;
    @SerializedName("subscription")
    @Expose
    private CurrentPlan plan;


    public String getTrialMode() {
        return trialMode;
    }

    public void setTrialMode(String trialMode) {
        this.trialMode = trialMode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getTwofa() {
        return twofa;
    }

    public void setTwofa(String twofa) {
        this.twofa = twofa;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProfileimg() {
        return profileimg;
    }

    public void setProfileimg(String profileimg) {
        this.profileimg = profileimg;
    }

    public String getCompanyDir() {
        return companyDir;
    }

    public void setCompanyDir(String companyDir) {
        this.companyDir = companyDir;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getExpires() {
        return expires;
    }

    public void setExpires(String expires) {
        this.expires = expires;
    }

    public String getRegStatus() {
        return regStatus;
    }

    public void setRegStatus(String regStatus) {
        this.regStatus = regStatus;
    }

    public String getVerifyStatus() {
        return verifyStatus;
    }

    public void setVerifyStatus(String verifyStatus) {
        this.verifyStatus = verifyStatus;
    }

    public String getLoggedStatus() {
        return loggedStatus;
    }

    public void setLoggedStatus(String loggedStatus) {
        this.loggedStatus = loggedStatus;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCompanyAbout() {
        return companyAbout;
    }

    public void setCompanyAbout(String companyAbout) {
        this.companyAbout = companyAbout;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getYearOfEstablish() {
        return yearOfEstablish;
    }

    public void setYearOfEstablish(String yearOfEstablish) {
        this.yearOfEstablish = yearOfEstablish;
    }

    public String getNoOfEmployee() {
        return noOfEmployee;
    }

    public void setNoOfEmployee(String noOfEmployee) {
        this.noOfEmployee = noOfEmployee;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getInterestedIn() {
        return interestedIn;
    }

    public void setInterestedIn(String interestedIn) {
        this.interestedIn = interestedIn;
    }

    public String getBusinessActivity() {
        return businessActivity;
    }

    public void setBusinessActivity(String businessActivity) {
        this.businessActivity = businessActivity;
    }

    public String getTurnover() {
        return turnover;
    }

    public void setTurnover(String turnover) {
        this.turnover = turnover;
    }

    public String getVatNo() {
        return vatNo;
    }

    public void setVatNo(String vatNo) {
        this.vatNo = vatNo;
    }

    public String getProfit() {
        return profit;
    }

    public void setProfit(String profit) {
        this.profit = profit;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public String getOtherOp() {
        return otherOp;
    }

    public void setOtherOp(String otherOp) {
        this.otherOp = otherOp;
    }

    public String getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(String isAdmin) {
        this.isAdmin = isAdmin;
    }

    public CurrentPlan getPlan() {
        return plan;
    }

    public void setPlan(CurrentPlan plan) {
        this.plan = plan;
    }
}
