package com.purposecodes.emetwafar.model;

import androidx.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Order implements Serializable {


    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("order_no")
    @Expose
    private String orderNo;
    @SerializedName("transaction_no")
    @Expose
    private String transactionNo;

    @Nullable
    @SerializedName("awb_no")
    @Expose
    private String trackingNo;

    @SerializedName("refund_tranef")
    @Expose
    private String refundTransRef;
    @SerializedName("order_quantity")
    @Expose
    private String orderQuantity;
    @SerializedName("order_date")
    @Expose
    private String orderDate;
    @SerializedName("payment_status")
    @Expose
    private String paymentStatus;
    @SerializedName("order_status")
    @Expose
    private String orderStatus;

    @Nullable
    @SerializedName("shipping_status")
    @Expose
    private String shippingStatus;

    @Nullable
    @SerializedName("subtotal")
    @Expose
    private String subtotal;

    @Nullable
    @SerializedName("vat")
    @Expose
    private String vat;

    @Nullable
    @SerializedName("verify_cost")
    @Expose
    private String verifyCost;

    @SerializedName("warranty_total")
    @Expose
    private String warrantyTotal;
    @SerializedName("discount_applied")
    @Expose
    private String discountApplied;
    @SerializedName("order_total")
    @Expose
    private String orderTotal;

    @Nullable
    @SerializedName("invoice")
    @Expose
    private Invoice invoice;

    @Nullable
    @SerializedName("billing_address")
    @Expose
    private BillingAddress billingAddress;


    @Nullable
    @SerializedName("products")
    @Expose
    private ArrayList<CartItem> cartItems = null;

    @Nullable
    @SerializedName("tracking")
    @Expose
    private Tracking trackingObject;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public String getTrackingNo() {
        return trackingNo;
    }

    public void setTrackingNo(String trackingNo) {
        this.trackingNo = trackingNo;
    }

    public String getRefundTransRef() {
        return refundTransRef;
    }

    public void setRefundTransRef(String refundTransRef) {
        this.refundTransRef = refundTransRef;
    }

    public String getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(String orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getShippingStatus() {
        return shippingStatus;
    }

    public void setShippingStatus(String shippingStatus) {
        this.shippingStatus = shippingStatus;
    }

    @Nullable
    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(@Nullable String subtotal) {
        this.subtotal = subtotal;
    }

    @Nullable
    public String getVat() {
        return vat;
    }

    public void setVat(@Nullable String vat) {
        this.vat = vat;
    }

    @Nullable
    public String getVerifyCost() {
        return verifyCost;
    }

    public void setVerifyCost(@Nullable String verifyCost) {
        this.verifyCost = verifyCost;
    }

    public String getWarrantyTotal() {
        return warrantyTotal;
    }

    public void setWarrantyTotal(String warrantyTotal) {
        this.warrantyTotal = warrantyTotal;
    }

    public String getDiscountApplied() {
        return discountApplied;
    }

    public void setDiscountApplied(String discountApplied) {
        this.discountApplied = discountApplied;
    }

    public String getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(String orderTotal) {
        this.orderTotal = orderTotal;
    }

    @Nullable
    public Invoice getInvoice() {
        return invoice;
    }


    @Nullable
    public BillingAddress getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(@Nullable BillingAddress billingAddress) {
        this.billingAddress = billingAddress;
    }

    public ArrayList<CartItem> getCartItems() {
        return cartItems;
    }

    public void setCartItems(ArrayList<CartItem> cartItems) {
        this.cartItems = cartItems;
    }

    public Tracking getTrackingObject() {
        return trackingObject;
    }

    public void setTrackingObject(@Nullable Tracking trackingObject) {
        this.trackingObject = trackingObject;
    }

    public class Tracking implements Serializable {
        @Nullable
        @SerializedName("status")
        @Expose
        private Integer status;

        @Nullable
        @SerializedName("messages")
        @Expose
        private final ArrayList<TimeLineMessage> messages = null;

        @Nullable
        public Integer getStatus() {
            return status;
        }

        @Nullable
        public List<TimeLineMessage> getMessages() {
            return messages;
        }

    }


    public class Invoice implements Serializable {
        @Nullable
        @SerializedName("invoice_no")
        @Expose
        private String invoiceNo;
        @Nullable
        @SerializedName("invoice_date")
        @Expose
        private String invoiceDate;

        @Nullable
        @SerializedName("invoice_url")
        @Expose
        private String invoiceUrl;

        @Nullable
        public String getInvoiceNo() {
            return invoiceNo;
        }

        @Nullable
        public String getInvoiceDate() {
            return invoiceDate;
        }

        @Nullable
        public String getInvoiceUrl() {
            return invoiceUrl;
        }
    }
}
