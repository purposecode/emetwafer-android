package com.purposecodes.emetwafar.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CurrentPlan implements Serializable {


    @SerializedName("subscription_id")
    @Expose
    private String subscriptionId;
    @SerializedName("plan_id")
    @Expose
    private String planId;
    @SerializedName("plan_name")
    @Expose
    private String planName;
    @SerializedName("plan_period")
    @Expose
    private String planPeriod;
    @SerializedName("plan_charge")
    @Expose
    private String charge;
    @SerializedName("vat")
    @Expose
    private String vat;
    @SerializedName("grand_total")
    @Expose
    private String grandTotal;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("expiry_date")
    @Expose
    private String expiryDate;
    @SerializedName("balance_days")
    @Expose
    private String balanceDays;

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getPlanPeriod() {
        return planPeriod;
    }

    public void setPlanPeriod(String planPeriod) {
        this.planPeriod = planPeriod;
    }

    public String getCharge() {
        return charge;
    }

    public void setCharge(String charge) {
        this.charge = charge;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getBalanceDays() {
        return balanceDays;
    }

    public void setBalanceDays(String balanceDays) {
        this.balanceDays = balanceDays;
    }
}
