package com.purposecodes.emetwafar.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductSpec {




    @SerializedName("tech_id")
    @Expose
    private String techId;
//    @SerializedName("product_id")
//    @Expose
//    private String productId;
    @SerializedName("technical_spec_name")
    @Expose
    private String technicalSpecName;
    @SerializedName("technical_spec_value")
    @Expose
    private String technicalSpecValue;
    @SerializedName("product_id")
    @Expose
    private String product_id;

    @SerializedName("status")
    @Expose
    private String status;

    public String getTechId() {
        return techId;
    }

    public void setTechId(String techId) {
        this.techId = techId;
    }

//    public String getProductId() {
//        return productId;
//    }

//    public void setProductId(String productId) {
//        this.productId = productId;
//    }

    public String getTechnicalSpecName() {
        return technicalSpecName;
    }

    public void setTechnicalSpecName(String technicalSpecName) {
        this.technicalSpecName = technicalSpecName;
    }

    public String getTechnicalSpecValue() {
        return technicalSpecValue;
    }

    public void setTechnicalSpecValue(String technicalSpecValue) {
        this.technicalSpecValue = technicalSpecValue;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
