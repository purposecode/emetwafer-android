package com.purposecodes.emetwafar.model;

import java.io.Serializable;

public class MessageItem implements Serializable {

   public String title, body, image_url;

    public MessageItem(String title, String body, String image_url) {
        this.title = title;
        this.body = body;
        this.image_url = image_url;
    }
}
