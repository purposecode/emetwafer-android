package com.purposecodes.emetwafar.model;

import java.io.Serializable;

public class PlanData implements Serializable {

    private final String subscriptionId;
    private final String paymentReference;
    private final String tokenId;
    private final int callingIntent;

    public PlanData(String subscriptionId, String paymentReference, String tokenId, int callingIntent) {
        this.subscriptionId = subscriptionId;
        this.paymentReference = paymentReference;
        this.tokenId = tokenId;
        this.callingIntent = callingIntent;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public String getPaymentReference() {
        return paymentReference;
    }

    public String getTokenId() {
        return tokenId;
    }

    public int getCallingIntent() {
        return callingIntent;
    }
}
