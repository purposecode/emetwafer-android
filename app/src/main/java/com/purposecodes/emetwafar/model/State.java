package com.purposecodes.emetwafar.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class State  implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("state")
    @Expose
    private String stateName;
    @SerializedName("city")
    @Expose
    private List<City> cities = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    @NonNull
    @Override
    public String toString() {
        return getStateName();
    }
}
