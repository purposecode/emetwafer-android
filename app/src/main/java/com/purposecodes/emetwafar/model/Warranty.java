package com.purposecodes.emetwafar.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Warranty {


    @SerializedName("warranty_id")
    @Expose
    private String warrantyId;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("warranty_period")
    @Expose
    private String warrantyPeriod;
    @SerializedName("warranty_price")
    @Expose
    private String warrantyPrice;
    @SerializedName("status")
    @Expose
    private String status;


    public String getWarrantyId() {
        return warrantyId;
    }

    public void setWarrantyId(String warrantyId) {
        this.warrantyId = warrantyId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getWarrantyPeriod() {
        return warrantyPeriod;
    }

    public void setWarrantyPeriod(String warrantyPeriod) {
        this.warrantyPeriod = warrantyPeriod;
    }

    public String getWarrantyPrice() {
        return warrantyPrice;
    }

    public void setWarrantyPrice(String warrantyPrice) {
        this.warrantyPrice = warrantyPrice;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @NonNull
    @Override
    public String toString() {
        return getWarrantyPeriod();
    }
}
