package com.purposecodes.emetwafar.model;

import androidx.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TimeLineMessage implements Serializable {
    @Nullable
    @SerializedName("message")
    @Expose
    private String message;

    @Nullable
    public String getMessage() {
        return message;
    }

}
