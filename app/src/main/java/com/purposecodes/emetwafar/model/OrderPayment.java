package com.purposecodes.emetwafar.model;

import java.util.Map;

public class OrderPayment {

    private Map<String, String> options;
   private int callingActivity;

    public Map<String, String> getOptions() {
        return options;
    }

    public void setOptions(Map<String, String> options) {
        this.options = options;
    }

    public int getCallingActivity() {
        return callingActivity;
    }

    public void setCallingActivity(int callingActivity) {
        this.callingActivity = callingActivity;
    }
}
