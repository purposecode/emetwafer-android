package com.purposecodes.emetwafar.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Subscription implements Serializable {


    @SerializedName("plan_id")
    @Expose
    private String planId;

    @SerializedName("plan_name")
    @Expose
    private String planName;

    @SerializedName("charge")
    @Expose
    private String charge;

    @SerializedName("vat")
    @Expose
    private double vat;

    @SerializedName("grand_total")
    @Expose
    private String grandTotal;

    @SerializedName("discount")
    @Expose
    private String discount;

    @SerializedName("plan_period")
    @Expose
    private String planPeriod;

    @SerializedName("features")
    @Expose
    private List<SubscriptionFeature> features = null;

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getCharge() {
        return charge;
    }

    public void setCharge(String charge) {
        this.charge = charge;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public double getVat() {
        return vat;
    }

    public void setVat(double vat) {
        this.vat = vat;
    }

    public String getPlanPeriod() {
        return planPeriod;
    }

    public void setPlanPeriod(String planPeriod) {
        this.planPeriod = planPeriod;
    }

    public List<SubscriptionFeature> getFeatures() {
        return features;
    }

    public void setFeatures(List<SubscriptionFeature> features) {
        this.features = features;
    }

    public class SubscriptionFeature implements Serializable {
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("feature")
        @Expose
        private String feature;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFeature() {
            return feature;
        }

        public void setFeature(String feature) {
            this.feature = feature;
        }
    }

    @NonNull
    @Override
    public String toString() {
        return getPlanName();
    }
}
