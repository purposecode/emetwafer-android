package com.purposecodes.emetwafar.model;

import androidx.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserData {


    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("company_id")
    @Expose
    private String companyId;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("profileurl")
    @Expose
    private String profileurl;
    @SerializedName("reg_date")
    @Expose
    private String regDate;
    @SerializedName("token_id")
    @Expose
    private String tokenId;
    @SerializedName("verify_status")
    @Expose
    private String verifyStatus;
    @SerializedName("logged_status")
    @Expose
    private String loggedStatus;

    @SerializedName("cart_total")
    @Expose
    private Integer cartTotal;
    @SerializedName("wish_total")
    @Expose
    private Integer wishTotal;

    @SerializedName("is_expired")
    @Expose
    private Integer isExpired;

    @Nullable
    @SerializedName("is_subscribed")
    @Expose
    private Integer isSubscribed;

    @SerializedName("payment_status")
    @Expose
    private String paymentStatus;



    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfileurl() {
        return profileurl;
    }

    public void setProfileurl(String profileurl) {
        this.profileurl = profileurl;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getVerifyStatus() {
        return verifyStatus;
    }

    public void setVerifyStatus(String verifyStatus) {
        this.verifyStatus = verifyStatus;
    }

    public String getLoggedStatus() {
        return loggedStatus;
    }

    public void setLoggedStatus(String loggedStatus) {
        this.loggedStatus = loggedStatus;
    }

    public Integer getCartTotal() {
        return cartTotal;
    }

    public void setCartTotal(Integer cartTotal) {
        this.cartTotal = cartTotal;
    }

    public Integer getWishTotal() {
        return wishTotal;
    }

    public void setWishTotal(Integer wishTotal) {
        this.wishTotal = wishTotal;
    }

    public Integer getIsExpired() {
        return isExpired;
    }

    public Integer getIsSubscribed() {
        return isSubscribed;
    }

    public void setIsSubscribed(Integer isSubscribed) {
        this.isSubscribed = isSubscribed;
    }

    public void setIsExpired(Integer isExpired) {
        this.isExpired = isExpired;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }
}
