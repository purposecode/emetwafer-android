package com.purposecodes.emetwafar.view;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public abstract class EndlessScrollListener extends RecyclerView.OnScrollListener {

    // LayoutManager used in recyclerView
    private final LinearLayoutManager mLayoutManager;

    // The minimum number of items to have below your current scroll position
    // before loading more.
    private int mVisibleThreshold = 3;

    // The total number of items in the data set after the last load
    private int mPreviousTotal = 0;

    // True if we are still waiting for the last set of data to load.
    private boolean mLoading = true;

    // The first visible items position, total visible items count and the total
    // items in the layout manager.
    private int mFirstVisibleItem, mVisibleItemCount, mTotalItemCount;

    // The current offset index of data you have loaded
    private int mCurrentPage = 1;

    public EndlessScrollListener(LinearLayoutManager layoutManager) {
        mLayoutManager = layoutManager;
    }

    public EndlessScrollListener(LinearLayoutManager layoutManager, int visibleThreshold) {
        mLayoutManager = layoutManager;
        mVisibleThreshold = visibleThreshold;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        mVisibleItemCount = recyclerView.getChildCount();
        mTotalItemCount = mLayoutManager.getItemCount();
        mFirstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();

        // If the total item count is zero and the previous isn't, assume the
        // list is invalidated and should be reset back to initial state
        if (mTotalItemCount < mPreviousTotal) {
            mCurrentPage = 1;
            mPreviousTotal = mTotalItemCount;
            mLoading = (mTotalItemCount == 0);
        }

        // If it's still loading, we check to see if the data set count has
        // changed, if so we conclude it has finished loading and update the current page
        // number and total item count.
        if (mLoading) {
            if (mTotalItemCount > mPreviousTotal) {
                mLoading = false;
                mPreviousTotal = mTotalItemCount;
            }
        }

        // If it isn't currently loading, we check to see if we have breached
        // the visibleThreshold and need to reload more data.
        // If we do need to reload some more data, we execute onLoadMore to fetch the data.
        if (!mLoading && ((mFirstVisibleItem + mVisibleItemCount + mVisibleThreshold) >= mTotalItemCount)) {

            // End has been reached
            // Do something
            mCurrentPage++;

            onLoadMore(mCurrentPage);
            mLoading = true;
        }
    }

    public void setLoading(boolean loading) {
        mLoading = loading;
    }

    public abstract void onLoadMore(int currentPage);
}
