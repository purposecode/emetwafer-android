package com.purposecodes.emetwafar.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.otto.BusProvider;
import com.squareup.otto.Bus;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity implements InternetConnectivityListener {


    private ActionBar mActionBar;
    // Widgets
    @SuppressLint("NonConstantResourceId")
    @Nullable
    @BindView(R.id.layout_prgressBg)
    ViewGroup progressLayer;

    // Monitor network changes
    private InternetAvailabilityChecker mInternetChecker;

    // No need to show toast when opening the app.
    private boolean mShowNetworkChange;

    // Otto handler
    private final Bus mBus = BusProvider.getInstance();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());

        ButterKnife.bind(this);

        // Configuring toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        if (toolbar != null) {

            setSupportActionBar(toolbar);
            mActionBar = getSupportActionBar();

            if (mActionBar != null) {
                mActionBar.setDisplayHomeAsUpEnabled(false);
                mActionBar.setTitle(null);
            }
        }

        // Monitoring network changes
        monitorNetworkChanges();


    }

    protected abstract int getLayoutResource();


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void showProgress() {
        if (progressLayer != null) {
            progressLayer.setVisibility(View.VISIBLE);
        }
    }

    protected void dismissProgress() {

        if (progressLayer != null) {
            progressLayer.setVisibility(View.GONE);
        }
    }


    protected void showToast(String msg) {

        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    protected void monitorNetworkChanges() {
        mShowNetworkChange = !AppController.get().isNetworkAvailable();

        mInternetChecker = InternetAvailabilityChecker.getInstance();
        mInternetChecker.addInternetConnectivityListener(this);
    }

    protected void setActionBarTitle(String title) {
        if (mActionBar != null) {
            mActionBar.setTitle(title);
        }
    }

    @Override
    protected void onDestroy() {
        progressLayer = null;
        mActionBar = null;
        mInternetChecker.removeInternetConnectivityChangeListener(this);
        super.onDestroy();
    }


    @Override
    protected void onStop() {
        mBus.unregister(this);

        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBus.register(this);

    }

    protected void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    protected void hideSoftKeyboard(EditText editText) {
        if (editText != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        }
    }


    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        if (!mShowNetworkChange && !isConnected) {
            mShowNetworkChange = true;
        }

        //temprory hide internet checker
/*
        if (mShowNetworkChange) {
            String message;
            int color;
            if (isConnected) {
                message = "Back online";
                color = Color.parseColor("#00A868");
            } else {
                message = "No connection";
                color = Color.BLACK;
            }

            Snackbar snackbar = Snackbar
                    .make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG);

            Snackbar.SnackbarLayout sbView = (Snackbar.SnackbarLayout) snackbar.getView();
            sbView.getLayoutParams().height = dpToPx(20);
            sbView.setBackgroundColor(color);

            TextView textView = sbView.findViewById(R.id.snackbar_text);
            textView.setVisibility(View.INVISIBLE);
            TextView tv = new TextView(this);
            tv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            tv.setTextColor(Color.WHITE);
            tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
            tv.setGravity(Gravity.CENTER);
            tv.setText(message);

            sbView.addView(tv);
            snackbar.show();
        }
*/
    }


}
