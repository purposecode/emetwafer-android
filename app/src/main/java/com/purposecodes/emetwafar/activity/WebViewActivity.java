package com.purposecodes.emetwafar.activity;

import static com.purposecodes.emetwafar.utils.FileUtils.PDF;
import static com.purposecodes.emetwafar.utils.FileUtils.fileDirectoryGenerator;
import static com.purposecodes.emetwafar.utils.FileUtils.getFileNameWithExtension;
import static com.purposecodes.emetwafar.utils.LogUtils.printLog;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_PDF_URL;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;

import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.utils.PermissionUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Objects;

public class WebViewActivity extends BaseActivity {

    private final String TAG = "WebViewActivity";
    private String PDF_URL;


    private static final int PERMISSION_REQUEST_WRITE_STORAGE = 112;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        PDF_URL = getIntent().getStringExtra(TAG_PDF_URL);

        WebView myWebView = findViewById(R.id.webview);

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading Data...");
        progressDialog.setCancelable(false);

        myWebView.requestFocus();
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.getSettings().setSupportZoom(true);

        myWebView.loadUrl("https://docs.google.com/gview?embedded=true&url=" + PDF_URL);

        myWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
            }
        });
        myWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress < 100) {
                    progressDialog.show();
                }
                if (progress == 100) {
                    progressDialog.dismiss();
                }
            }
        });


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_web_view;
    }

    // Menu item
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_download, menu);
        return true;
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {

            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_download:
                checkWritePermission();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void checkWritePermission() {
        if (PermissionUtils.hasPermission(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE})) {

            downloadFile();
        } else {
            PermissionUtils.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_WRITE_STORAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {


        if (requestCode == PERMISSION_REQUEST_WRITE_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                downloadFile();
            } else {
                showToast("Please enable all permissions from app settings.");
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }


    }

    private void downloadFile() {
        //working
        new DownloadFromURL().execute(PDF_URL);
    }

    @SuppressLint("StaticFieldLeak")
    class DownloadFromURL extends AsyncTask<String, String, String> {
        private ProgressDialog progressDialog;
        public static final int progressType = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(WebViewActivity.this);
            progressDialog.setMessage("File is Downloading. Please wait...");
            progressDialog.setIndeterminate(false);
            progressDialog.setMax(100);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... fileUrl) {
            int count;
            try {
                URL url = new URL(fileUrl[0]);
                URLConnection urlConnection = url.openConnection();
                urlConnection.connect();
                // show progress bar 0-100%
                int fileLength = urlConnection.getContentLength();
                InputStream inputStream = new BufferedInputStream(url.openStream(), 8192);


                String path = fileDirectoryGenerator(PDF) + File.separator + getFileNameWithExtension(fileUrl[0]);

                OutputStream outputStream = new FileOutputStream(path);

                byte[] data = new byte[1024];
                long total = 0;
                while ((count = inputStream.read(data)) != -1) {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / fileLength));
                    outputStream.write(data, 0, count);
                }
                // flushing output
                outputStream.flush();
                // closing streams
                outputStream.close();
                inputStream.close();

                return path;
            } catch (Exception e) {
                printLog("Error: ", e.getMessage());
            }
            return "";
        }

        // progress bar Updating

        protected void onProgressUpdate(String... progress) {
            // progress percentage
            progressDialog.setProgress(Integer.parseInt(progress[0]));
        }


        @Override
        protected void onPostExecute(String fileUrl) {
            if (progressDialog != null)
                progressDialog.dismiss();

            if (!TextUtils.isEmpty(fileUrl)) {
                showToast("Download Successfully Completed");
                openPDF(fileUrl);
            } else
                showToast("Download Failed!");

        }
    }

    public void openPDF(String filePath) {

        File file = new File(filePath);

//Open it up

        if (file.exists()) {
            try {

//            Uri path = Uri.fromFile(file);
                Uri path = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", file);

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(path, "application/pdf");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(intent);

            } catch (Exception e) {
                showToast("No Application available to view pdf");
            }
        } else
            showToast("File not found!");

    }

}
