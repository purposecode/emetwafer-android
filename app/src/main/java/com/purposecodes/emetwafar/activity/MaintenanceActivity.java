package com.purposecodes.emetwafar.activity;

import static com.purposecodes.emetwafar.controller.SessionController.setMaintenanceMode;
import static com.purposecodes.emetwafar.utils.LogUtils.printLog;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.authentication.LoginActivity;
import com.purposecodes.emetwafar.controller.AppController;

public class MaintenanceActivity extends AppCompatActivity {

    private final String TAG = "MaintenanceActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maintenance);

        initializeMaintenanceMode();
    }


    private void initializeMaintenanceMode() {

        // Write a message to the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("e_metwafer").child("maintenance_mode");

        myRef.keepSynced(true);

        // Read from the database

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                try {


                    String value = dataSnapshot.getValue(String.class);
//                    printLog(TAG, "initializeMaintenanceMode Value is onDataChange: " + value);

                    if (value != null && !TextUtils.isEmpty(value)) {
                        setMaintenanceMode(value.equalsIgnoreCase("1"));

                        if (!value.equalsIgnoreCase("1")) {
                            Intent openIntent = new Intent(MaintenanceActivity.this, SplashActivity.class);

                            if (AppController.get().getUser() == null)
                                openIntent = new Intent(MaintenanceActivity.this, LoginActivity.class);

                            openIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(openIntent);
                            finish();

                        }
                    }

                } catch (DatabaseException e) {

                    printLog(TAG, "initializeMaintenanceMode DatabaseException: " + e.getLocalizedMessage());
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                printLog(TAG, "initializeMaintenanceMode Value is: Failed to read value." + error.toException());
            }
        });


    }
}
