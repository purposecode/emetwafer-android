package com.purposecodes.emetwafar.activity;


import static com.purposecodes.emetwafar.controller.SessionController.getCheckUpdateLastDate;
import static com.purposecodes.emetwafar.controller.SessionController.getMaintenanceMode;
import static com.purposecodes.emetwafar.controller.SessionController.setCheckUpdateLastDate;
import static com.purposecodes.emetwafar.controller.SessionController.setMaintenanceMode;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_EXPIRED;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_EXPIRE_FILED;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_PUSH_MESSAGE;
import static com.purposecodes.emetwafar.utils.LogUtils.printLog;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.getVersionWiseKey;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.isUsingLatestVersion;
import static com.purposecodes.emetwafar.utils.UtilsVariable.CALLING_ACTIVITY_KEY;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_MESSAGE;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.api.ApiFailureMessageListener;
import com.purposecodes.emetwafar.api.ApiSuccessListener;
import com.purposecodes.emetwafar.authentication.LoginActivity;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.controller.SessionController;
import com.purposecodes.emetwafar.features.home.HomeActivity;
import com.purposecodes.emetwafar.features.subscription.PlanExpiredActivity;
import com.purposecodes.emetwafar.model.MessageItem;
import com.purposecodes.emetwafar.otto.BusProvider;
import com.purposecodes.emetwafar.responsemodel.ErrorModel;
import com.squareup.otto.Subscribe;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import me.leolin.shortcutbadger.ShortcutBadger;

@SuppressLint("CustomSplashScreen")
public class SplashActivity extends AppCompatActivity {
    int callingActivity = -1;
    private final String TAG = "SplashActivity";


    // Get Firebase Remote Config instance.
    FirebaseRemoteConfig firebaseRemoteConfig;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BusProvider.getInstance().register(this);

// Get Firebase Remote Config instance.
        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();

        try {
            callingActivity = getIntent().getIntExtra(CALLING_ACTIVITY_KEY, -1);
            if (callingActivity == ACTIVITY_PUSH_MESSAGE) {
                showMessageDialog();
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        init();

    }


    private void init() {
        if (!getMaintenanceMode()) { //get stored data
            initializeMaintenanceMode();
            CheckAppUpdate();
        } else
            maintenanceModeActivate();
        try {
            ShortcutBadger.removeCount(SplashActivity.this); //for 1.1.4+
        } catch (Exception e) {
            e.printStackTrace();
            printLog(TAG, "onCreate exception :  " + e.getLocalizedMessage());

        }

    }

    private void CheckAppUpdate() {


        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(180)
                .build();
        firebaseRemoteConfig.setConfigSettingsAsync(configSettings);

        firebaseRemoteConfig.setDefaultsAsync(R.xml.remote_config_defaults);

        firebaseRemoteConfig.fetchAndActivate()
                .addOnCompleteListener(this, new OnCompleteListener<Boolean>() {
                    @Override
                    public void onComplete(@NonNull Task<Boolean> task) {
                       /* if (task.isSuccessful()) {
                            boolean updated = task.getResult();
                            printLog(TAG, "Config params updated  : " + updated);

                            Toast.makeText(SplashActivity.this, "Fetch and activate succeeded",  Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(SplashActivity.this, "Fetch failed",  Toast.LENGTH_SHORT).show();
                        }*/


                        if (!isUsingLatestVersion(firebaseRemoteConfig.getString("latest_app_version")) && firebaseRemoteConfig.getBoolean("force_update"))
                            showUpdateAlert(firebaseRemoteConfig.getString("title"), firebaseRemoteConfig.getString("message"), firebaseRemoteConfig.getBoolean("force_update"));
                        else if (!isUsingLatestVersion(firebaseRemoteConfig.getString("latest_app_version")) && isEligibleUpdateCheck())
                            showUpdateAlert(firebaseRemoteConfig.getString("title"), firebaseRemoteConfig.getString("message"), firebaseRemoteConfig.getBoolean("force_update"));
                        else
                            loggedUserFunction();

                    }
                });


    }


    private boolean isEligibleCheck() {

        try {
            String date = SessionController.getCheckExpiryLastDate(); //19/10/2020 4:17:05

            if (TextUtils.isEmpty(date))
                return true;

            long diff = compareDate(getCurrentTime()).getTime() - compareDate(date).getTime();

            // Get msec from each, and subtract.

//            long diffSeconds = diff / 1000;
//            long diffMinutes = diff / (60 * 1000);
            long diffHours = diff / (60 * 60 * 1000);
//            long diffDay = diff / (24 * (60 * 60 * 1000));

            return diffHours >= 24;


        } catch (ParseException e) {
            printLog(TAG, "getCurrentTime Exception  :    " + e.getMessage());
        }


        return true;
    }

    private boolean isEligibleUpdateCheck() {

        try {

            String date = getCheckUpdateLastDate();

            if (TextUtils.isEmpty(date))
                return true;

            long diff = compareDate(getCurrentTime()).getTime() - compareDate(date).getTime();

            // Get msec from each, and subtract.

            long diffHours = diff / (60 * 60 * 1000);

            return diffHours >= 24;

        } catch (ParseException e) {
            printLog(TAG, "isEligibleUpdateCheck Exception  :    " + e.getMessage());
        }
        return true;
    }

    private void initializeBaseUrl() {


        // Write a message to the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();

        DatabaseReference myRef = database.getReference("e_metwafer").child("version_urls").child(getVersionWiseKey());
        myRef.keepSynced(true);
        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                try {


                    String value = dataSnapshot.getValue(String.class);
//                    printLog(TAG, "initializeBaseUrl Value is onDataChange: " + getVersionWiseKey() + " : " + value);

                    if (!TextUtils.isEmpty(value))
                        AppController.get().setUrl(value);


                    if (AppController.get().isNetworkAvailable() && isEligibleCheck()) {
                        AppController.get().checkexpiry();

                    } else if (!AppController.get().isNetworkAvailable()) {
                        Intent intent = new Intent(SplashActivity.this, NoInternetActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        gotoActivity(intent);
                        showToast(getString(R.string.no_internet));
                    } else
                        launchActivity();


                } catch (DatabaseException e) {

                    printLog(TAG, "initializeBaseUrl Value is DatabaseException: " + e.getLocalizedMessage());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                // Failed to read value
                printLog(TAG, "initializeBaseUrl Value is: Failed to read value." + error.toException());
            }
        });

    }


    @Subscribe
    public void onHttpSuccess(ApiSuccessListener<JsonObject> response) {

        if (response.type.equalsIgnoreCase("checkexpiry")) {

            try {
                JsonObject jsonObject = response.response.body();
                assert jsonObject != null;
                if (jsonObject.get("status").getAsBoolean()) {

                    JsonObject dataObject = jsonObject.get("data").getAsJsonObject();

                    if ((dataObject.get("is_subscribed") != null) && dataObject.get("is_subscribed").getAsInt() == 0) {
                        Intent intent = new Intent(this, PlanExpiredActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra(CALLING_ACTIVITY_KEY, ACTIVITY_EXPIRE_FILED);
                        gotoActivity(intent);
                    } else if ((dataObject.get("is_expired") != null) && dataObject.get("is_expired").getAsInt() == 0) {  // 0 - expired ,1 - not expired
                        Intent intent = new Intent(SplashActivity.this, PlanExpiredActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra(CALLING_ACTIVITY_KEY, ACTIVITY_EXPIRED);
                        gotoActivity(intent);
                    } else {
                        launchActivity();
                        SessionController.setCheckExpiryLastDate(getCurrentTime());

                    }

                } else {

                    showToast(jsonObject.get("text").getAsString());
                }
            } catch (Exception e) {
                showToast(getResources().getString(R.string.error_try_again));
                printLog(TAG, "  Exception : " + e.getMessage());
            }


        }
    }


    @Subscribe
    public void onHttpError(ApiFailureMessageListener<ErrorModel> response) {
        if (response.type.equalsIgnoreCase("checkexpiry")) {
            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                showToast(errorModel.getText());


            } catch (Exception e) {
                e.printStackTrace();
                showToast(getResources().getString(R.string.error_try_again));

            }
        }

    }


    @Subscribe
    public void onHttpFailure(ApiFailureListener response) {

        if (response.type.equalsIgnoreCase("checkexpiry")) {
            showToast(getResources().getString(R.string.error_try_again));

        }
    }

    private void launchActivityHandler() {

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                launchActivity();
            }
        }, 1000);
    }

    private void launchActivity() {

        Intent intent;
        if (AppController.get().getUser() != null)
            intent = new Intent(SplashActivity.this, HomeActivity.class);
        else
            intent = new Intent(SplashActivity.this, LoginActivity.class);

        gotoActivity(intent);


    }


    private void showUpdateAlert(String title, String message, boolean isForceUpdate) {
        try {


            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setCancelable(false);
            builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    openMarket();
                }
            });

            if (!isForceUpdate) {
                builder.setNegativeButton("Later", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        loggedUserFunction();
                        setCheckUpdateLastDate(getCurrentTime());
                    }
                });
            } else {
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SplashActivity.this.finish();
                    }
                });
            }

            builder.create().show();
        } catch (Exception e) {
            printLog(TAG, " showUpdateAlert Exception : " + e.getMessage());
        }
    }


    private void openMarket() {
        final String appPackageName = getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }

        SplashActivity.this.finish();
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        BusProvider.getInstance().unregister(this);
        super.onDestroy();
    }

    public String getCurrentTime() {
        //getting current date and time using Date class
        DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");


        /*getting current date time using calendar class
         * An Alternative of above*/
        Calendar calobj = Calendar.getInstance();

        return df.format(calobj.getTime());

    }

    private Date compareDate(String strDate) throws ParseException {

        Date date1 = new SimpleDateFormat("ddd/MM/yy HH:mm:ss").parse(strDate);
        return date1;
    }



    private void initializeMaintenanceMode() {

        // Write a message to the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("e_metwafer").child("maintenance_mode");

        myRef.keepSynced(true);

        // Read from the database

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                try {

                    String value = dataSnapshot.getValue(String.class);

                    if (value != null && !TextUtils.isEmpty(value)) {
                        setMaintenanceMode(value.equalsIgnoreCase("1"));

                        if (value.equalsIgnoreCase("1")) {
                            maintenanceModeActivate();
                        }
                    }

                } catch (DatabaseException e) {
                    printLog(TAG, "initializeMaintenanceMode DatabaseException: " + e.getLocalizedMessage());
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                printLog(TAG, "initializeMaintenanceMode Value is: Failed to read value." + error.toException());
            }
        });


    }




    private void loggedUserFunction() {



        if (AppController.get().getUser() != null) {
            if (AppController.get().isNetworkAvailable()) {
                initializeBaseUrl();

            } else {
                Intent intent = new Intent(SplashActivity.this, NoInternetActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                gotoActivity(intent);
            }
        } else
            launchActivityHandler();


    }


    private void maintenanceModeActivate() {

        Intent intent = new Intent(SplashActivity.this, MaintenanceActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        gotoActivity(intent);

    }



    private void gotoActivity(Intent intent) {

        startActivity(intent);
        finish();


    }


    private void showMessageDialog() {


        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_notification_details);

        if (dialog.getWindow() != null) {
            dialog.getWindow().setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }

        dialog.setCancelable(false);


        final ImageView ivImage = dialog.findViewById(R.id.imageView_notificationDetails);
        final TextView tvTitle = dialog.findViewById(R.id.textView_notificationDetails_title);
        final TextView tvBody = dialog.findViewById(R.id.textView_notificationDetails_body);
        final ImageButton ibClose = dialog.findViewById(R.id.imageButton_notificationDetails_close);
        final ProgressBar progressBar = dialog.findViewById(R.id.progress_notificationDetails);


        String title, body, image_url;

        try {

            MessageItem msg = (MessageItem) getIntent().getSerializableExtra(TAG_MESSAGE);


            title = msg.title;
            body = msg.body;
            image_url = msg.image_url;

            tvTitle.setText(title != null ? title : "");
            tvBody.setText(body != null ? body : "");


            if (image_url != null && !TextUtils.isEmpty(image_url)) {


                progressBar.setVisibility(View.VISIBLE);
                ivImage.setVisibility(View.VISIBLE);


                Glide.with(this)
                        .load(msg.image_url)
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                progressBar.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                progressBar.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(ivImage);


            }

        } catch (Exception e) {

            printLog("Base Activity", "Exception  :  " + e.getMessage());

        }


        ibClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                init();
            }
        });

        dialog.show();
    }

}
