package com.purposecodes.emetwafar.activity;

import static com.purposecodes.emetwafar.utils.UtilsVariable.NO_NETWORK;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;

import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.listener.ConnectivityReceiverListener;
import com.purposecodes.emetwafar.receiver.ConnectivityReceiver;
import com.sk.icode.errorview.ErrorView;

import butterknife.BindView;

public class NoInternetActivity extends BaseActivity implements ConnectivityReceiverListener {

    private final String TAG="NoInternetActivity";
    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.errorView)
    ErrorView errorView;

    private ConnectivityReceiver networkReceiver = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        networkReceiver=new ConnectivityReceiver();
        errorView.setVisibility(View.VISIBLE);
        errorView.setConfig(ErrorView.Config.create()
                .title(getString(R.string.no_internet))
                .subtitle("Make sure that Wi-Fi or mobile data is tuned on,then try again.")
                .retryVisible(true)
                .image(NO_NETWORK)
                .build());


        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {
                if (AppController.get().isNetworkAvailable() ) {
                    Intent intent = new Intent(NoInternetActivity.this, SplashActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }

            }
        });


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_errorview;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // register connection status listener


        AppController.get().setConnectivityListener(this);
        registerReceiver(networkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

    }


    protected void onPause() {
        super.onPause();
        unregisterReceiver(networkReceiver);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {


        if (isConnected ) {
            Intent intent = new Intent(NoInternetActivity.this, SplashActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }
}