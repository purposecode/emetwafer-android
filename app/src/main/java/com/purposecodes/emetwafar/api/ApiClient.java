package com.purposecodes.emetwafar.api;

import static com.purposecodes.emetwafar.api.UnsafeOkHttpClient.getSSLSocketFactory;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.getAppVersion;

import android.os.Build;
import android.text.TextUtils;

import com.google.gson.JsonObject;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.model.UserData;
import com.purposecodes.emetwafar.responsemodel.AddressResponse;
import com.purposecodes.emetwafar.responsemodel.CartListResponse;
import com.purposecodes.emetwafar.responsemodel.CategoryResponse;
import com.purposecodes.emetwafar.responsemodel.CheckoutResponse;
import com.purposecodes.emetwafar.responsemodel.CouponListResponse;
import com.purposecodes.emetwafar.responsemodel.NotificationResponse;
import com.purposecodes.emetwafar.responsemodel.OrderDetailsResponse;
import com.purposecodes.emetwafar.responsemodel.OrdersResponse;
import com.purposecodes.emetwafar.responsemodel.OtpResponseModel;
import com.purposecodes.emetwafar.responsemodel.ProductDetailsResponse;
import com.purposecodes.emetwafar.responsemodel.ProductResponse;
import com.purposecodes.emetwafar.responsemodel.ProfileResponse;
import com.purposecodes.emetwafar.responsemodel.RegisterResponse;
import com.purposecodes.emetwafar.responsemodel.SaveOrderResponse;
import com.purposecodes.emetwafar.responsemodel.StateResponse;
import com.purposecodes.emetwafar.responsemodel.SubscriptionHistoryResponse;
import com.purposecodes.emetwafar.responsemodel.SubscriptionsResponse;
import com.purposecodes.emetwafar.responsemodel.WishListResponse;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by PURPOSE CODE on 9/7/2020.
 */
public class ApiClient {


    private static final String BASE_URL = "http://purposecodes.com/emetwafar/api/";  // Test

//private static final String BASE_URL = "https://metwafer.com/trial/api/" ;  // trial
//    private static final String BASE_URL = "https://metwafer.com/api/" ;  // Live

    private static ApiClient mApiClient;
    private static Retrofit mRetrofit;
    private static OkHttpClient client;


    public static ApiClient getClient() {

        if (mApiClient == null) {
            mApiClient = new ApiClient();
        }

        return mApiClient;
    }

    private ApiClient() {

//                HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        client = new OkHttpClient.Builder()
                .readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .connectTimeout(1, TimeUnit.MINUTES)
//                .addInterceptor(interceptor)
                .addNetworkInterceptor(chain -> {
                    Request request = chain.request();
                    UserData user = AppController.get().getUser();
                    Request.Builder builder = request.newBuilder();
                    builder.header("Device-Type", "android");
                    builder.header("Version", getAppVersion());
                    if (user != null && !TextUtils.isEmpty(user.getTokenId()))
                        builder.header("Authorization", user.getTokenId());

                    request = builder.build();
                    return chain.proceed(request);
                }).build();

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.N) {
            try {

                client = new OkHttpClient.Builder().sslSocketFactory(getSSLSocketFactory())
                        .readTimeout(1, TimeUnit.MINUTES)
                        .writeTimeout(1, TimeUnit.MINUTES)
                        .connectTimeout(1, TimeUnit.MINUTES)
//                        .addInterceptor(interceptor)
                        .addNetworkInterceptor(chain -> {
                            Request request = chain.request();
                            UserData user = AppController.get().getUser();

                            Request.Builder builder = request.newBuilder();
                            builder.header("Device-Type", "android");
                            builder.header("Version", getAppVersion());
                            if (user != null && !TextUtils.isEmpty(user.getTokenId()))
                                builder.header("Authorization", user.getTokenId());

                            request = builder.build();
                            return chain.proceed(request);
                        })

                        .build();

            } catch (KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException | KeyManagementException e) {
                e.printStackTrace();

            }



        }


            mRetrofit = new Retrofit.Builder()
                    .baseUrl((AppController.get().getBaseUrlToSharedPref()!=null&&!TextUtils.isEmpty(AppController.get().getBaseUrlToSharedPref()))?AppController.get().getBaseUrlToSharedPref():BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();



    }


    public void setBaseUrl(String baseUrl) {

        try {

            mRetrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }catch (Exception e){


            mRetrofit = new Retrofit.Builder()
                    .baseUrl((AppController.get().getBaseUrlToSharedPref()!=null&&!TextUtils.isEmpty(AppController.get().getBaseUrlToSharedPref()))?AppController.get().getBaseUrlToSharedPref():BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
    }


    public void checkexpiry(ApiCallBack<JsonObject> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<JsonObject> call = interfaces.checkexpiry();
        callback.setRequestCall(call);
        call.enqueue(callback);
    }




    public void emailVerification(String email, ApiCallBack<OtpResponseModel> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<OtpResponseModel> call = interfaces.emailVerification(email);
        callback.setRequestCall(call);
        call.enqueue(callback);
    }

    public void verifyOtp(HashMap<String, String> options, ApiCallBack<JsonObject> callback) {

        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<JsonObject> call = interfaces.verifyOtp(options);
        call.enqueue(callback);
    }



    public void mediaUpload(Map<String, RequestBody> options, MultipartBody.Part partFile, ApiCallBack<JsonObject> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<JsonObject> call = interfaces.mediaUpload(options, partFile);
        callback.setRequestCall(call);
        call.enqueue(callback);
    }

    public void profileDpUpload(Map<String, RequestBody> options, MultipartBody.Part partFile, ApiCallBack<JsonObject> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<JsonObject> call = interfaces.profileDpUpload(options, partFile);
        callback.setRequestCall(call);
        call.enqueue(callback);
    }

    public void companyRegister(HashMap<String, String> options, ArrayList<String> images, ApiCallBack<JsonObject> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<JsonObject> call = interfaces.companyRegister(options, images);
        call.enqueue(callback);
    }



    public void loginUser(Map<String, String> options, ApiCallBack<JsonObject> callback) {

        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<JsonObject> call = interfaces.loginUser(options);
        callback.setRequestCall(call);
        call.enqueue(callback);
    }


    public void forgotPassword(HashMap<String, String> options, ApiCallBack<RegisterResponse> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<RegisterResponse> call = interfaces.forgotPassword(options);
        callback.setRequestCall(call);
        call.enqueue(callback);
    }


    public void changePassword(HashMap<String, String> options, ApiCallBack<JsonObject> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<JsonObject> call = interfaces.changePassword(options);
        call.enqueue(callback);
    }


    public void logoutUser( ApiCallBack<JsonObject> callback) {

        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<JsonObject> call = interfaces.logout();
        callback.setRequestCall(call);
        call.enqueue(callback);
    }




    public void sendUserToken(Map<String, String> options, String token_id, ApiCallBack<JsonObject> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<JsonObject> call = interfaces.sendUserToken(options, token_id);
        callback.setRequestCall(call);
        call.enqueue(callback);
    }

    public void getProfile(ApiCallBack<ProfileResponse> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<ProfileResponse> call = interfaces.getProfile();
        callback.setRequestCall(call);
        call.enqueue(callback);
    }

    public void updateProfile(Map<String, String> options, ApiCallBack<ProfileResponse> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<ProfileResponse> call = interfaces.updateProfile(options);
        callback.setRequestCall(call);
        call.enqueue(callback);
    }


    public void getCategories(ApiCallBack<CategoryResponse> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<CategoryResponse> call = interfaces.getCategories();
        callback.setRequestCall(call);
        call.enqueue(callback);
    }

    public void getNotifications(ApiCallBack<NotificationResponse> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<NotificationResponse> call = interfaces.getNotifications();
        callback.setRequestCall(call);
        call.enqueue(callback);
    }


    public void updateNotifications(Map<String, String> options, ApiCallBack<JsonObject> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<JsonObject> call = interfaces.updateNotifications(options);
        call.enqueue(callback);
    }


    public void getStateAndCities(ApiCallBack<StateResponse> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<StateResponse> call = interfaces.getStateAndCities();
        callback.setRequestCall(call);
        call.enqueue(callback);
    }


    public void getProducts(Map<String, String> options, ApiCallBack<ProductResponse> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<ProductResponse> call = interfaces.getProducts(options);
        callback.setRequestCall(call);
        call.enqueue(callback);
    }


    public void getProductDetail(String id, ApiCallBack<ProductDetailsResponse> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<ProductDetailsResponse> call = interfaces.getProductDetail(id);
        callback.setRequestCall(call);
        call.enqueue(callback);
    }


    public void getWishList(ApiCallBack<WishListResponse> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<WishListResponse> call = interfaces.getWishList();
        callback.setRequestCall(call);
        call.enqueue(callback);
    }

    public void getSubscriptionList(ApiCallBack<SubscriptionsResponse> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<SubscriptionsResponse> call = interfaces.getSubscriptionList();
        callback.setRequestCall(call);
        call.enqueue(callback);
    }

    public void getSubscriptionHistory(Map<String, String> options, ApiCallBack<SubscriptionHistoryResponse> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<SubscriptionHistoryResponse> call = interfaces.getSubscriptionHistory(options);
        callback.setRequestCall(call);
        call.enqueue(callback);
    }

    public void getCouponList(ApiCallBack<CouponListResponse> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<CouponListResponse> call = interfaces.getCouponList();
        callback.setRequestCall(call);
        call.enqueue(callback);
    }


    public void addToWish(Map<String, String> options, ApiCallBack<JsonObject> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<JsonObject> call = interfaces.addToWish(options);
        callback.setRequestCall(call);
        call.enqueue(callback);
    }


    public void removeFromWish(String id, ApiCallBack<JsonObject> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<JsonObject> call = interfaces.removeFromWish(id);
        call.enqueue(callback);
    }

    public void removeCoupon(String id, ApiCallBack<JsonObject> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<JsonObject> call = interfaces.removeCoupon(id);
        call.enqueue(callback);
    }

    public void cancelOrder(Map<String, String> options, ApiCallBack<JsonObject> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<JsonObject> call = interfaces.cancelOrder(options);
        call.enqueue(callback);
    }

    public void orderNeedHelp(Map<String, String> options, ApiCallBack<JsonObject> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<JsonObject> call = interfaces.orderNeedHelp(options);
        call.enqueue(callback);
    }


    public void generateCartId(ApiCallBack<JsonObject> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<JsonObject> call = interfaces.generateCartId();
        callback.setRequestCall(call);
        call.enqueue(callback);
    }


    public void addToCart(Map<String, String> options, ApiCallBack<JsonObject> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<JsonObject> call = interfaces.addToCart(options);
        call.enqueue(callback);
    }

    public void updateCart(Map<String, String> options, ApiCallBack<JsonObject> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<JsonObject> call = interfaces.updateCart(options);
        call.enqueue(callback);
    }

    public void buyNow(Map<String, String> options, ApiCallBack<CheckoutResponse> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<CheckoutResponse> call = interfaces.buyNow(options);
        call.enqueue(callback);
    }


    public void getMyCart(String id, ApiCallBack<CartListResponse> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<CartListResponse> call = interfaces.getMyCart(id);
        callback.setRequestCall(call);
        call.enqueue(callback);
    }

    public void removeFromMyCart(String cart_id, String product_id, ApiCallBack<JsonObject> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<JsonObject> call = interfaces.removeFromMyCart(cart_id, product_id);
        callback.setRequestCall(call);
        call.enqueue(callback);
    }


    public void removeSelectedCoupon(String cartId, ApiCallBack<CheckoutResponse> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<CheckoutResponse> call = interfaces.removeSelectedCoupon(cartId);
        callback.setRequestCall(call);
        call.enqueue(callback);
    }

    public void clearMyCart(String cart_id, ApiCallBack<JsonObject> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<JsonObject> call = interfaces.clearMyCart(cart_id);
        callback.setRequestCall(call);
        call.enqueue(callback);
    }

    public void checkout(Map<String, String> options, ApiCallBack<CheckoutResponse> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<CheckoutResponse> call = interfaces.checkout(options);
        call.enqueue(callback);
    }

    public void applyCoupon(Map<String, String> options, ApiCallBack<JsonObject> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<JsonObject> call = interfaces.applyCoupon(options);
        call.enqueue(callback);
    }

    public void updateCheckout(Map<String, String> options, ApiCallBack<CheckoutResponse> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<CheckoutResponse> call = interfaces.updateCheckout(options);
        call.enqueue(callback);
    }

    public void saveOrder(Map<String, String> options, ApiCallBack<SaveOrderResponse> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<SaveOrderResponse> call = interfaces.saveOrder(options);
        call.enqueue(callback);
    }

    public void initiateOrderPayment(Map<String, String> options, ApiCallBack<JsonObject> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<JsonObject> call = interfaces.initiateOrderPayment(options);
        call.enqueue(callback);
    }

    public void refundRequest(Map<String, String> options, ApiCallBack<JsonObject> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<JsonObject> call = interfaces.refundRequest(options);
        call.enqueue(callback);
    }

    public void getOrders(Map<String, String> options, ApiCallBack<OrdersResponse> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<OrdersResponse> call = interfaces.getOrders(options);
        callback.setRequestCall(call);
        call.enqueue(callback);
    }


    public void getOrderDetail(String id, ApiCallBack<OrderDetailsResponse> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<OrderDetailsResponse> call = interfaces.getOrderDetail(id);
        callback.setRequestCall(call);
        call.enqueue(callback);
    }

    public void removeAddress(String addressId, ApiCallBack<JsonObject> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<JsonObject> call = interfaces.removeAddress(addressId);
        callback.setRequestCall(call);
        call.enqueue(callback);
    }

    public void saveAddress(Map<String, String> options, ApiCallBack<JsonObject> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<JsonObject> call = interfaces.saveAddress(options);
        call.enqueue(callback);
    }

    public void editAddress(Map<String, String> options, ApiCallBack<JsonObject> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<JsonObject> call = interfaces.editAddress(options);
        call.enqueue(callback);
    }

    public void updateBillingAddress(Map<String, String> options, ApiCallBack<JsonObject> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<JsonObject> call = interfaces.updateBillingAddress(options);
        call.enqueue(callback);
    }

    public void getAddresses(ApiCallBack<AddressResponse> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<AddressResponse> call = interfaces.getAddresses();
        callback.setRequestCall(call);
        call.enqueue(callback);
    }

    public void updateSubscriptionStatus(Map<String, String> options, String token_id, ApiCallBack<JsonObject> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<JsonObject> call = interfaces.updateSubscriptionStatus(options, token_id);
        callback.setRequestCall(call);
        call.enqueue(callback);
    }


    public void profileUpdate(Map<String, String> options, ApiCallBack<JsonObject> callback) {
        ApiEndpointInterface interfaces = mRetrofit.create(ApiEndpointInterface.class);
        Call<JsonObject> call = interfaces.profileUpdate(options);
        call.enqueue(callback);
    }


}