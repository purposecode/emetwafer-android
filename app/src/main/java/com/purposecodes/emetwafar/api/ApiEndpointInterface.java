package com.purposecodes.emetwafar.api;

import com.google.gson.JsonObject;
import com.purposecodes.emetwafar.responsemodel.AddressResponse;
import com.purposecodes.emetwafar.responsemodel.CartListResponse;
import com.purposecodes.emetwafar.responsemodel.CategoryResponse;
import com.purposecodes.emetwafar.responsemodel.CheckoutResponse;
import com.purposecodes.emetwafar.responsemodel.CouponListResponse;
import com.purposecodes.emetwafar.responsemodel.NotificationResponse;
import com.purposecodes.emetwafar.responsemodel.OrderDetailsResponse;
import com.purposecodes.emetwafar.responsemodel.OrdersResponse;
import com.purposecodes.emetwafar.responsemodel.OtpResponseModel;
import com.purposecodes.emetwafar.responsemodel.ProductDetailsResponse;
import com.purposecodes.emetwafar.responsemodel.ProductResponse;
import com.purposecodes.emetwafar.responsemodel.ProfileResponse;
import com.purposecodes.emetwafar.responsemodel.RegisterResponse;
import com.purposecodes.emetwafar.responsemodel.SaveOrderResponse;
import com.purposecodes.emetwafar.responsemodel.StateResponse;
import com.purposecodes.emetwafar.responsemodel.SubscriptionHistoryResponse;
import com.purposecodes.emetwafar.responsemodel.SubscriptionsResponse;
import com.purposecodes.emetwafar.responsemodel.WishListResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by PURPOSE CODE on 9/7/2020.
 */
public interface ApiEndpointInterface {


    @GET("checkexpiry")
    Call<JsonObject> checkexpiry();

    @FormUrlEncoded
    @POST("email_verify")
    Call<OtpResponseModel> emailVerification(@Field("email") String email);



    @FormUrlEncoded
    @POST("verify_otp")
    Call<JsonObject> verifyOtp(@FieldMap HashMap<String, String> options);


    @GET("statecity")
    Call<StateResponse> getStateAndCities();

    @Multipart
    @POST("account_verify")
    Call<JsonObject> mediaUpload(@PartMap Map<String, RequestBody> options, @Part MultipartBody.Part file);


    @FormUrlEncoded
    @POST("company_register")
    Call<JsonObject> companyRegister(@FieldMap HashMap<String, String> options, @Field("verify_doc[]") ArrayList<String> friends);




    @FormUrlEncoded
    @POST("login")
    Call<JsonObject> loginUser(@FieldMap Map<String, String> options);

    //    @FormUrlEncoded
    @POST("logout")
    Call<JsonObject> logout();


    @FormUrlEncoded
    @POST("savedevice")
    Call<JsonObject> sendUserToken(@FieldMap Map<String, String> options, @Header("Authorization") String token_id);


    @FormUrlEncoded
    @POST("forgotpassword")
    Call<RegisterResponse> forgotPassword(@FieldMap HashMap<String, String> options);


    @FormUrlEncoded
    @POST("changepassword")
    Call<JsonObject> changePassword(@FieldMap HashMap<String, String> options);


    @GET("profile")
    Call<ProfileResponse> getProfile();

    @FormUrlEncoded
    @PUT("profile")
    Call<ProfileResponse> updateProfile(@FieldMap Map<String, String> options);


    @Multipart
    @POST("mediaupload")
    Call<JsonObject> profileDpUpload(@PartMap Map<String, RequestBody> options, @Part MultipartBody.Part file);


    @GET("category")
    Call<CategoryResponse> getCategories();


    @GET("notification")
    Call<NotificationResponse> getNotifications();



    @FormUrlEncoded
    @PUT("notification")
    Call<JsonObject> updateNotifications(@FieldMap Map<String, String> options);

    @DELETE("notification")
    Call<CheckoutResponse> deleteNotifications();


    @GET("products/")
    Call<ProductResponse> getProducts(@QueryMap Map<String, String> options);

    @GET("products/{product_id}/")
    Call<ProductDetailsResponse> getProductDetail(@Path("product_id") String _id);


    @FormUrlEncoded
    @POST("wishlist/")
    Call<JsonObject> addToWish(@FieldMap Map<String, String> options);


    @DELETE("wishlist/{product_id}")
    Call<JsonObject> removeFromWish(@Path("product_id") String _id);

    @DELETE("coupons/{coupon_id}")
    Call<JsonObject> removeCoupon(@Path("coupon_id") String _id);


    @GET("wishlist/")
    Call<WishListResponse> getWishList();


    @GET("plans")
    Call<SubscriptionsResponse> getSubscriptionList();

    @GET("subscriptions")
    Call<SubscriptionHistoryResponse> getSubscriptionHistory(@QueryMap Map<String, String> options);


    @GET("coupons/")
    Call<CouponListResponse> getCouponList();


    @GET("guest-cart")
    Call<JsonObject> generateCartId();

    @FormUrlEncoded
    @POST("addtocart")
    Call<JsonObject> addToCart(@FieldMap Map<String, String> options);

    @FormUrlEncoded
    @POST("updatecart")
    Call<JsonObject> updateCart(@FieldMap Map<String, String> options);

    @FormUrlEncoded
    @POST("buynow")
    Call<CheckoutResponse> buyNow(@FieldMap Map<String, String> options);

    @FormUrlEncoded
    @POST("checkout")
    Call<CheckoutResponse> checkout(@FieldMap Map<String, String> options);

    @FormUrlEncoded
    @POST("checkout")
    Call<JsonObject> applyCoupon(@FieldMap Map<String, String> options);

    @FormUrlEncoded
    @PUT("checkout")
    Call<CheckoutResponse> updateCheckout(@FieldMap Map<String, String> options);


    @DELETE("checkout?")
    Call<CheckoutResponse> removeSelectedCoupon(@Query("cart_id") String cartId);


    @FormUrlEncoded
    @POST("saveorder")
    Call<SaveOrderResponse> saveOrder(@FieldMap Map<String, String> options);

    @FormUrlEncoded
    @POST("initiatepayment")
    Call<JsonObject> initiateOrderPayment(@FieldMap Map<String, String> options);

    @FormUrlEncoded
    @POST("refundrequest")
    Call<JsonObject> refundRequest(@FieldMap Map<String, String> options);

    @FormUrlEncoded
    @POST("cancelorder")
    Call<JsonObject> cancelOrder(@FieldMap Map<String, String> options);


    @FormUrlEncoded
    @POST("needhelp")
    Call<JsonObject> orderNeedHelp(@FieldMap Map<String, String> options);


    @GET("orders/")
    Call<OrdersResponse> getOrders(@QueryMap Map<String, String> options);


    @GET("orders/{order_id}/")
    Call<OrderDetailsResponse> getOrderDetail(@Path("order_id") String _id);


    @GET("cart/{cart_id}/")
    Call<CartListResponse> getMyCart(@Path("cart_id") String _id);

    @DELETE("cart/{cart_id}/{product_id}")
    Call<JsonObject> removeFromMyCart(@Path("cart_id") String cart_id, @Path("product_id") String product_id);


    @DELETE("cart/{cart_id}/")
    Call<JsonObject> clearMyCart(@Path("cart_id") String _id);


    @FormUrlEncoded
    @POST("saveaddress/")
    Call<JsonObject> saveAddress(@FieldMap Map<String, String> options);


    @FormUrlEncoded
    @PUT("saveaddress/")
    Call<JsonObject> editAddress(@FieldMap Map<String, String> options);

    @FormUrlEncoded
    @PUT("updatebilling/")
    Call<JsonObject> updateBillingAddress(@FieldMap Map<String, String> options);

    @GET("saveaddress/")
    Call<AddressResponse> getAddresses();

    @DELETE("saveaddress/{address_id}")
    Call<JsonObject> removeAddress(@Path("address_id") String _id);


    @FormUrlEncoded
    @POST("updatemyplan")
    Call<JsonObject> updateSubscriptionStatus(@FieldMap Map<String, String> options, @Header("Authorization") String token_id);


    @FormUrlEncoded
    @POST("api.php?action=updateprofile")
    Call<JsonObject> profileUpdate(@FieldMap Map<String, String> options);


}
