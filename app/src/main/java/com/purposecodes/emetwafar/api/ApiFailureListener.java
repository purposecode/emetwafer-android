package com.purposecodes.emetwafar.api;

/**
 * Created by PURPOSE CODE on 9/7/2020.
 */
public class ApiFailureListener {

    public String type;
    public int position;

    public ApiFailureListener(String type, int position) {
        this.type = type;
        this.position = position;
    }
}
