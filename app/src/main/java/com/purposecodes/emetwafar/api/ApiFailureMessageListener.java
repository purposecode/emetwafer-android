package com.purposecodes.emetwafar.api;

import retrofit2.Response;

public class ApiFailureMessageListener<T>  {
    public String type;
    public Response<T> response;
    public int position;

    public ApiFailureMessageListener(String type,Response<T> response, int position) {
        this.type = type;
        this.response = response;
        this.position = position;
    }


}
