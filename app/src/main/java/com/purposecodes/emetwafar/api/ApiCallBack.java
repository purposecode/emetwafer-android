package com.purposecodes.emetwafar.api;

import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_EXPIRED;
import static com.purposecodes.emetwafar.utils.UtilsVariable.CALLING_ACTIVITY_KEY;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.purposecodes.emetwafar.activity.MaintenanceActivity;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.features.subscription.PlanExpiredActivity;
import com.squareup.otto.Bus;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by PURPOSE CODE on 9/7/2016.
 */
public class ApiCallBack<T> implements Callback<T> {


    private final Bus bus;
    private final String type;
    private final int position;
    private static final int TOTAL_RETRIES = 3;
    private Call<T> call;
    private int retryCount = 0;

    public void setRequestCall(Call<T> call) {
        this.call = call;
    }

    public ApiCallBack(Bus bus, String type, int position) {
        this.bus = bus;
        this.type = type;
        this.position = position;
    }

    @Override
    public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {


        switch (response.code()) {

            case HttpURLConnection.HTTP_UNAUTHORIZED:   // User token expired / Unauthorized   401
            case HttpURLConnection.HTTP_FORBIDDEN: {
                // User token expired  403


                if (AppController.get().getUser() != null) {
                    AppController.get().logoutUser();

                    try {
                        assert response.errorBody() != null;
                        String s = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(s);
                        Toast.makeText(AppController.get(), jsonObject.getString("text"), Toast.LENGTH_LONG).show();
                    } catch (JSONException | IOException | NullPointerException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        bus.post(new ApiFailureMessageListener<>(type, response, position));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            break;
            case HttpURLConnection.HTTP_GONE: {
                // User subscsription expiry expired  410
                if (AppController.get().getUser() != null) {


                    try {
                        assert response.errorBody() != null;
                        String s = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(s);
                        Toast.makeText(AppController.get(), jsonObject.getString("text"), Toast.LENGTH_LONG).show();

                        Context context = AppController.get();

                        Intent expiryIntent = new Intent(context, PlanExpiredActivity.class);
                        expiryIntent.putExtra(CALLING_ACTIVITY_KEY, ACTIVITY_EXPIRED);
                        expiryIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(expiryIntent);

                    } catch (JSONException | IOException | NullPointerException e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        bus.post(new ApiFailureMessageListener<>(type, response, position));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            break;
            case HttpURLConnection.HTTP_UNAVAILABLE:   // Currently the app is in maintenance mode  503
                try {
                    assert response.errorBody() != null;
                    String s = response.errorBody().string();
                    JSONObject jsonObject = new JSONObject(s);
                    Toast.makeText(AppController.get(), jsonObject.getString("text"), Toast.LENGTH_LONG).show();

                    Context context = AppController.get();
                    Intent loginIntent = new Intent(context, MaintenanceActivity.class);
                    loginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(loginIntent);
                } catch (JSONException | IOException | NullPointerException e) {
                    e.printStackTrace();
                }
                break;
//            case HttpURLConnection.HTTP_NOT_FOUND:   // Currently tno data  404
//
//                try {
//                    bus.post(new ApiFailureMessageListener<>(type, response,position));
//
//                }
//                catch ( NullPointerException e) {
//                    e.printStackTrace();
//                    bus.post(new ApiFailureListener(type, position));
//                }
//                break;

            default:

                if (response.isSuccessful()) {
                    bus.post(new ApiSuccessListener<>(response, type, position));
                } else {
                    bus.post(new ApiFailureMessageListener<>(type, response, position));
                }

        }


    }


    @Override
    public void onFailure(Call<T> call, Throwable t) {
        t.printStackTrace();

        if (retryCount++ < TOTAL_RETRIES) {
            retry();
        } else {
            bus.post(new ApiFailureListener(type, position));
        }

    }

    private void retry() {
        try {
            call.clone().enqueue(this);
        } catch (Exception e) {
            bus.post(new ApiFailureListener(type, position));
        }
    }
}
