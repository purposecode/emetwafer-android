package com.purposecodes.emetwafar.api;

import retrofit2.Response;

/**
 * Created by PURPOSE CODE on 9/7/2020.
 */
public class ApiSuccessListener<T> {

    public Response<T> response;
    public String type;
    public int position;

    public ApiSuccessListener (Response<T> response, String type, int position) {
        this.response = response;
        this.type = type;
        this.position = position;
    }
}
