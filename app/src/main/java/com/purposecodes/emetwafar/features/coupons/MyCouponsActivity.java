package com.purposecodes.emetwafar.features.coupons;

import static com.purposecodes.emetwafar.utils.LogUtils.printLog;
import static com.purposecodes.emetwafar.utils.UtilsVariable.NO_COUPON;
import static com.purposecodes.emetwafar.utils.UtilsVariable.NO_NETWORK;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_ERRORVIEW;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_PROGRESSBAR;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_RECYCLERVIEW;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_SWIPEREFRESH;
import static com.purposecodes.emetwafar.utils.UtilsVariable.WRONG_RESPONSE;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.api.ApiFailureMessageListener;
import com.purposecodes.emetwafar.api.ApiSuccessListener;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.model.CouponItem;
import com.purposecodes.emetwafar.responsemodel.CouponListResponse;
import com.purposecodes.emetwafar.responsemodel.ErrorModel;
import com.purposecodes.emetwafar.view.EndlessScrollListener;
import com.sk.icode.errorview.ErrorView;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class MyCouponsActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener, RvMyCouponsAdapter.onRemoveCouponListener {

    private final String TAG = "MyCouponsActivity";

    @BindView(R.id.errorView)
    ErrorView errorView;


    @BindView(R.id.recyclerView_layout_list)
    RecyclerView recyclerView;


    @BindView(R.id.swipeRefreshLayout_layout_list)
    SwipeRefreshLayout swipeRefreshLayout;

    // RecyclerView date set
    private ArrayList<CouponItem> couponList;

    private RvMyCouponsAdapter couponsAdapter;

    // Scroll Listener for pagination
    private EndlessScrollListener mScrollListener;

    // Current page
    private int mCurrentPage = 0;
    private final int mTotalPages = -1;

    private final String mPageCount = "15";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        swipeRefreshLayout.setOnRefreshListener(this);

        setUpRecyclerView();

        loadData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_list_activity;
    }


    private void setUpRecyclerView() {

        couponList = new ArrayList<>();
        couponsAdapter = new RvMyCouponsAdapter(this, couponList, this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(couponsAdapter);
        recyclerView.setNestedScrollingEnabled(false);
        mScrollListener = new EndlessScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int currentPage) {
                if (mCurrentPage < mTotalPages) {
                    showRecyclerViewLoading(true);
                    getCouponList(mCurrentPage + 1);
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        };

    }

    private void loadData() {


        updateViews(VIEW_SWIPEREFRESH);

        mCurrentPage = 0;
        couponsAdapter.notifyDataSetChanged();

        if (AppController.get().isNetworkAvailable()) {
            getCouponList(mCurrentPage + 1);  // First page
        } else if (couponList.isEmpty()) {
            setErrorView(getString(R.string.no_internet), "", false, NO_NETWORK);
        }


    }


    private void showRecyclerViewLoading(final boolean showLoading) {
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                couponsAdapter.setLoading(showLoading);
            }
        });
    }

    private void getCouponList(int pageNo) {

        AppController.get().getCouponList();
    }

    //set ErrorView
    private void setErrorView(final String title, final String subTitle, boolean isRetry, int drawable) {

        updateViews(VIEW_ERRORVIEW);

        errorView.setConfig(ErrorView.Config.create()
                .title(title)
                .subtitle(subTitle)
                .retryVisible(isRetry)
                .image(drawable)
                .build());

        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {
                loadData();
            }
        });
    }

    private void updateViews(int viewCode) {
        switch (viewCode) {
            case VIEW_PROGRESSBAR:
                showProgress();

                errorView.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                break;
            case VIEW_RECYCLERVIEW:
                dismissProgress();
                recyclerView.setVisibility(View.VISIBLE);
                errorView.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                break;

            case VIEW_ERRORVIEW:
                dismissProgress();
                recyclerView.setVisibility(View.GONE);
                errorView.setVisibility(View.VISIBLE);
                swipeRefreshLayout.setRefreshing(false);
                break;
            case VIEW_SWIPEREFRESH:
                dismissProgress();
                swipeRefreshLayout.setRefreshing(true);
                break;
        }
    }


    @SuppressLint("NotifyDataSetChanged")
    @Subscribe
    public void onHttpGetWishListSuccess(ApiSuccessListener<CouponListResponse> response) {


        if (response.type.equalsIgnoreCase("getCouponList")) {
            CouponListResponse responseModel = response.response.body();
            if (responseModel != null && responseModel.getStatus()) {



                couponList.clear();
                List<CouponItem> list = responseModel.getCouponItems();
                if (list != null && list.size() > 0) {
                    couponList.addAll(list);
                }

                couponsAdapter.notifyDataSetChanged();
                updateViews(VIEW_RECYCLERVIEW);


            } else if (mCurrentPage == 0) {
                couponList.clear();
                couponsAdapter.notifyDataSetChanged();
                assert responseModel != null;
                setErrorView(responseModel.getText(), "", false, NO_COUPON);
            } else if (couponList.isEmpty() && responseModel.getText() != null && TextUtils.isEmpty(responseModel.getText()))
                setErrorView(responseModel.getText(), "", false, NO_COUPON);
            else if (couponList.isEmpty())
                setErrorView("No Data found!", "", false, NO_COUPON);


            showRecyclerViewLoading(false);


        }
    }

    @SuppressLint("NotifyDataSetChanged")
    @Subscribe
    public void onHttpRemoveSuccess(ApiSuccessListener<JsonObject> response) {

        if (response.type.equalsIgnoreCase("removeCoupon")) {
            updateViews(VIEW_RECYCLERVIEW);

            try {
                JsonObject jsonObject = response.response.body();
                assert jsonObject != null;
                if (jsonObject.get("status").getAsBoolean()) {

                    couponList.remove(response.position);
                    couponsAdapter.notifyDataSetChanged();
                    showToast(jsonObject.get("text").getAsString());
                    if (couponList.isEmpty())
                        setErrorView("Your coupons empty!", "", true, NO_COUPON);

                } else {
                    showToast(jsonObject.get("text").getAsString());
                }
            } catch (Exception e) {
                printLog(TAG, " onHttpRemoveCartSuccess Exception : " + e.getMessage());
            }
        } else if (response.type.equalsIgnoreCase("clearAllCoupon")) {
            updateViews(VIEW_RECYCLERVIEW);

            try {
                JsonObject jsonObject = response.response.body();
                assert jsonObject != null;
                if (jsonObject.get("status").getAsBoolean()) {

                    couponList.clear();
                    couponsAdapter.notifyDataSetChanged();
                    setErrorView("Your coupon empty!", "", false, NO_COUPON);

                } else {
                    showToast(jsonObject.get("text").getAsString());
                }
            } catch (Exception e) {
                printLog(TAG, " onHttpRemoveSuccess Exception : " + e.getMessage());
            }
        }
    }


    @Subscribe
    public void onHttpError(ApiFailureMessageListener<ErrorModel> response) {
        //

        if (response.type.equalsIgnoreCase("getCouponList")) {
            updateViews(VIEW_RECYCLERVIEW);
            showRecyclerViewLoading(false);
            mScrollListener.setLoading(false);

            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                setErrorView(errorModel.getText(), "", false, NO_COUPON);

            } catch (Exception e) {
                e.printStackTrace();
                setErrorView(getResources().getString(R.string.error_try_again), getString(R.string.error_subtitle_failed_one_more_time), true, WRONG_RESPONSE);

            }


        } else if (response.type.equalsIgnoreCase("removeCoupon") || response.type.equalsIgnoreCase("clearAllCoupon")) {
            updateViews(VIEW_RECYCLERVIEW);
            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                showToast(errorModel.getText());
            } catch (Exception e) {
                e.printStackTrace();
                showToast(getResources().getString(R.string.error_try_again));

            }

        }
    }


    @Subscribe
    public void onHttpFailure(ApiFailureListener response) {

        if (response.type.equalsIgnoreCase("getCouponList")) {

            setErrorView(getResources().getString(R.string.error_try_again), getString(R.string.error_subtitle_failed_one_more_time), true, WRONG_RESPONSE);

            showRecyclerViewLoading(false);
            mScrollListener.setLoading(false);

        } else if (response.type.equalsIgnoreCase("removeCoupon") || response.type.equalsIgnoreCase("clearAllCoupon")) {
            showToast(getResources().getString(R.string.error_try_again));
            updateViews(VIEW_RECYCLERVIEW);
        }


    }


    @Override
    public void onRefresh() {

        loadData();
    }




    @Override
    public void onCouponRemoveClicked(CouponItem couponItem, int position) {


        AlertDialog.Builder builder = new AlertDialog.Builder(MyCouponsActivity.this);
//Are you sure you want to remove?
        builder.setMessage("Are you sure?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if (AppController.get().isNetworkAvailable()) {
                            updateViews(VIEW_PROGRESSBAR);
                            AppController.get().removeCoupon(couponItem.getCouponId(), position);
                        } else
                            showToast(getString(R.string.no_internet));
                    }
                })
                .setNegativeButton("No", (dialog, id) -> dialog.cancel()).show();


    }
}
