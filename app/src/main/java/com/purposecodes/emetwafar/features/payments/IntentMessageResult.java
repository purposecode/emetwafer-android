package com.purposecodes.emetwafar.features.payments;

import com.google.gson.JsonObject;



public class IntentMessageResult {
    public String type;
    public JsonObject response;

    public IntentMessageResult(String type, JsonObject response) {
        this.type = type;
        this.response = response;
    }

    public String getType() {
        return type;
    }

    public JsonObject getResponse() {
        return response;
    }
}
