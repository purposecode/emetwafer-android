package com.purposecodes.emetwafar.features.coupons;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.holder.RvBlankViewHolder;
import com.purposecodes.emetwafar.holder.RvFooterViewHolder;
import com.purposecodes.emetwafar.model.CouponItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RvMyCouponsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final String TAG = "RvMyCouponsHolder";
    // Context
    private final Context mContext;

    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;
    private static final int TYPE_BLANK = 3;

    // Checking the its loading for new items
    private boolean mLoading = false;
    // List
    private final ArrayList<CouponItem> mList;


    private final onRemoveCouponListener listListener;

    public RvMyCouponsAdapter(Context mContext, ArrayList<CouponItem> mList, onRemoveCouponListener _listListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.listListener = _listListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {



        View view;
        switch (viewType) {

            case TYPE_FOOTER:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_loading, parent, false);
                return new RvFooterViewHolder(view);

            case TYPE_ITEM:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_my_coupon, parent, false);
                return new RvMyCouponsHolder(view);

            default:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_blank, parent, false);
                return new RvBlankViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        int viewType = getItemViewType(position);
        switch (viewType) {

            case TYPE_FOOTER:
                bindFooterView((RvFooterViewHolder) viewHolder);
                break;
            case TYPE_ITEM:
                bindItemView((RvMyCouponsHolder) viewHolder, position);
                break;
        }


    }


    private void bindItemView(RvMyCouponsHolder holder, final int position) {

        CouponItem couponItem = mList.get(position);


        holder.tvCode.setText(couponItem.getCouponCode());
        holder.tvDate.setText(couponItem.getExpiryDate());

        holder.btnRemove.setOnClickListener(v -> {
            if (listListener != null)
                listListener.onCouponRemoveClicked(couponItem, position);
        });

        holder.btnCopy.setOnClickListener(v -> {
            ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("e_metwafar_code", couponItem.getCouponCode());
            clipboard.setPrimaryClip(clip);
            Toast.makeText(mContext, "Coupon copied", Toast.LENGTH_SHORT).show();
        });


    }


    private void bindFooterView(RvFooterViewHolder holder) {
        if (mLoading) {
            holder.loadText.setText(R.string.loading);
            holder.loadText.setVisibility(View.VISIBLE);
            holder.loadProgress.setVisibility(View.VISIBLE);
        } else {
            holder.loadText.setText("");
            holder.loadText.setVisibility(View.GONE);
            holder.loadProgress.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {

        int count = 0;
        if (!mList.isEmpty()) {
            count = mList.size() + 1;
        }
        return count;
    }


    @Override
    public int getItemViewType(int position) {

        try {


            if (position == getItemCount() - 1) {
                return TYPE_FOOTER;
            } else {

                return TYPE_ITEM;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            return TYPE_BLANK;
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setLoading(boolean loading) {
        mLoading = loading;
        notifyDataSetChanged();
    }


    static class RvMyCouponsHolder extends RecyclerView.ViewHolder {


        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.textView_itemMyCoupon_no)
        TextView tvCode;

        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.textView_itemMyCoupon_date)
        TextView tvDate;


        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.button_itemMyCoupon_copy)
        MaterialButton btnCopy;

        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.button_itemMyCoupon_remove)
        MaterialButton btnRemove;


        RvMyCouponsHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    public interface onRemoveCouponListener {
        void onCouponRemoveClicked(CouponItem couponItem, int position);

    }
}
