package com.purposecodes.emetwafar.features.productview;

import static android.app.Activity.RESULT_OK;
import static com.purposecodes.emetwafar.cache.CacheStorage.PRODUCTS_CACHE_KEY;
import static com.purposecodes.emetwafar.controller.SessionController.setWishQuantity;
import static com.purposecodes.emetwafar.utils.LogUtils.printLog;
import static com.purposecodes.emetwafar.utils.UtilsVariable.NO_DATA;
import static com.purposecodes.emetwafar.utils.UtilsVariable.NO_NETWORK;
import static com.purposecodes.emetwafar.utils.UtilsVariable.REQUEST_PRODUCT_DETAILS;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_IS_WISHED;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_POSITION;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_PRODUCT_ID;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_ERRORVIEW;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_PROGRESSBAR;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_RECYCLERVIEW;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_SWIPEREFRESH;
import static com.purposecodes.emetwafar.utils.UtilsVariable.WRONG_RESPONSE;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.api.ApiFailureMessageListener;
import com.purposecodes.emetwafar.api.ApiSuccessListener;
import com.purposecodes.emetwafar.cache.CacheStorage;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.fragment.BaseFragment;
import com.purposecodes.emetwafar.listener.OnProductClickListener;
import com.purposecodes.emetwafar.model.Product;
import com.purposecodes.emetwafar.responsemodel.ErrorModel;
import com.purposecodes.emetwafar.responsemodel.ProductResponse;
import com.purposecodes.emetwafar.view.EndlessScrollListener;
import com.sk.icode.errorview.ErrorView;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;


public class ProductListFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, OnProductClickListener {


    private final String TAG = "ProductListFragment";


    @BindView(R.id.errorView)
    ErrorView errorView;


    @BindView(R.id.recyclerView_layout_list)
    RecyclerView recyclerView;


    @BindView(R.id.swipeRefreshLayout_layout_list)
    SwipeRefreshLayout swipeRefreshLayout;

    // RecyclerView date set
    private ArrayList<Product> productList;

    private RvProductAdapter productAdapter;

    // Scroll Listener for pagination
    private EndlessScrollListener mScrollListener;

    // Current page
    private int mCurrentPage = 0, mTotalPages = -1;

    private final String mPageCount = "14";

    String responseTag = "all_products";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_list;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        swipeRefreshLayout.setOnRefreshListener(this);

        productList = new ArrayList<>();
        productAdapter = new RvProductAdapter(getContext(), productList, this);


        setUpRecyclerView();

        loadData();

    }


    private void setUpRecyclerView() {


        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(productAdapter);
//        recyclerView.setNestedScrollingEnabled(false);
        mScrollListener = new EndlessScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int currentPage) {
                if (mCurrentPage < mTotalPages) {
                    showRecyclerViewLoading(true);
                    getProducts(mCurrentPage + 1);
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        };

        recyclerView.addOnScrollListener(mScrollListener);


    }

    private void loadData() {

        productList.clear();
        updateViews(VIEW_SWIPEREFRESH);

        mCurrentPage = 0;
        productList.addAll(readCache());
        productAdapter.notifyDataSetChanged();

        if (AppController.get().isNetworkAvailable())
            getProducts(mCurrentPage + 1);  // First page
        else if (productList.isEmpty())
            setErrorView(getString(R.string.no_internet), "", false, NO_NETWORK);

        else
            swipeRefreshLayout.setRefreshing(false);


    }


    private void showRecyclerViewLoading(final boolean showLoading) {
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                productAdapter.setLoading(showLoading);
            }
        });
    }

    private void getProducts(int pageNo) {

        if (AppController.get().isNetworkAvailable()) {
            Map<String, String> options = new HashMap<>();
            options.put("count", mPageCount);
            options.put("page_no", String.valueOf(pageNo));


            AppController.get().getProducts(options, responseTag);
        } else
            showToast(getString(R.string.no_internet));


    }

    private void postWishList(Product product, int position) {

        if (AppController.get().isNetworkAvailable()) {
            Map<String, String> options = new HashMap<>();
            options.put("product_id", product.getProductId());
            AppController.get().addToWish(options, responseTag, position);
        } else {
            showToast(getString(R.string.no_internet));
            productAdapter.notifyDataSetChanged();
        }
    }

    //set ErrorView

    private void setErrorView(final String title, final String subTitle, boolean isRetry, int drawable) {

        updateViews(VIEW_ERRORVIEW);

        errorView.setConfig(ErrorView.Config.create()
                .title(title)
                .subtitle(subTitle)
                .retryVisible(isRetry)
                .image(drawable)
                .build());


        errorView.setOnRetryListener(() -> loadData());
    }

    private void updateViews(int viewCode) {
        switch (viewCode) {

            case VIEW_SWIPEREFRESH:
                dismissProgress();
                swipeRefreshLayout.setRefreshing(true);
                errorView.setVisibility(View.GONE);
                break;
            case VIEW_PROGRESSBAR:
                showProgress();

                errorView.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                break;
            case VIEW_RECYCLERVIEW:
                dismissProgress();
                recyclerView.setVisibility(View.VISIBLE);
                errorView.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                break;

            case VIEW_ERRORVIEW:
                dismissProgress();
                recyclerView.setVisibility(View.GONE);
                errorView.setVisibility(View.VISIBLE);
                swipeRefreshLayout.setRefreshing(false);
                break;

        }
    }


    @Subscribe
    public void onHttpGetProductListSuccess(ApiSuccessListener<ProductResponse> response) {
        if (response.type.equalsIgnoreCase("getProducts/" + responseTag)) {
            ProductResponse responseModel = response.response.body();
            if (responseModel != null && responseModel.getStatus()) {

                mCurrentPage = Integer.parseInt(responseModel.getResponseResult().getCurrentPage());
                mTotalPages = responseModel.getResponseResult().getTotalCount();

                if (mCurrentPage == 1) {
                    productList.clear();
                }
                List<Product> list = responseModel.getResponseResult().getProducts();
                if (list != null && list.size() > 0) {
                    productList.addAll(list);
                }
                productAdapter.notifyDataSetChanged();
                updateViews(VIEW_RECYCLERVIEW);

            } else if (mCurrentPage == 0) {
                productList.clear();
                productAdapter.notifyDataSetChanged();
                setErrorView(responseModel.getText(), "", false, NO_DATA);
            } else if (productList.isEmpty() && responseModel.getText() != null && TextUtils.isEmpty(responseModel.getText()))
                setErrorView(responseModel.getText(), "", false, NO_DATA);
            else if (productList.isEmpty())
                setErrorView("No Products found!", "", false, NO_DATA);
            showRecyclerViewLoading(false);


            String data = new Gson().toJson(productList);
            CacheStorage.getInstance().writeCache(PRODUCTS_CACHE_KEY, data);
        }
    }


    @Subscribe
    public void onHttpAddWishSuccess(ApiSuccessListener<JsonObject> response) {

        if (response.type.equalsIgnoreCase("addToWish_" + responseTag)) {

            try {
                JsonObject jsonObject = response.response.body();
                assert jsonObject != null;
                if (jsonObject.get("status").getAsBoolean()) {
                    JsonObject productObject = jsonObject.get("data").getAsJsonObject().get("product").getAsJsonObject();
                    int wishQuantity = jsonObject.get("data").getAsJsonObject().get("wish_total").getAsInt();

                    if (productList.size() > response.position) {
                        Product p = new Gson().fromJson(productObject, Product.class);
                        productList.set(response.position, p);
                        productAdapter.notifyDataSetChanged();
                    }
                    setWishQuantity(wishQuantity);
                    showToast(jsonObject.get("text").getAsString());
                } else {
                    showToast(jsonObject.get("text").getAsString());
                }
            } catch (Exception e) {
                printLog(TAG, " onHttpAddWishSuccess Exception : " + e.getMessage());

            }
        }
    }


    @Subscribe
    public void onHttpError(ApiFailureMessageListener<ErrorModel> response) {

        if (response.type.equalsIgnoreCase("getProducts/" + responseTag)) {

            showRecyclerViewLoading(false);
            mScrollListener.setLoading(false);


            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);

                if (mCurrentPage != 0) {
//                    showToast(errorModel.getText());
                    updateViews(VIEW_RECYCLERVIEW);
                } else {
                    setErrorView(errorModel.getText(), "", false, NO_DATA);
                }
            } catch (Exception e) {
                e.printStackTrace();
                setErrorView(getResources().getString(R.string.error_try_again), getString(R.string.error_subtitle_failed_one_more_time), true, WRONG_RESPONSE);

            }


        } else if (response.type.equalsIgnoreCase("addToWish" + responseTag)) {

            updateViews(VIEW_RECYCLERVIEW);

            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                showToast(errorModel.getText());
                productAdapter.notifyDataSetChanged();

            } catch (Exception e) {

                showToast(getResources().getString(R.string.error_try_again));
            }

        }


    }


    @Subscribe
    public void onHttpFailure(ApiFailureListener response) {

        if (response.type.equalsIgnoreCase("getProducts/" + responseTag)) {

            if (mCurrentPage != 0) {
                showToast(getResources().getString(R.string.error_try_again));
                updateViews(VIEW_RECYCLERVIEW);
            } else {
                setErrorView(getResources().getString(R.string.error_try_again), getString(R.string.error_subtitle_failed_one_more_time), true, WRONG_RESPONSE);
            }
            showRecyclerViewLoading(false);
            mScrollListener.setLoading(false);

        } else if (response.type.equalsIgnoreCase("addToWish" + responseTag)) {
            showToast(getResources().getString(R.string.error_try_again));
            updateViews(VIEW_RECYCLERVIEW);
            productAdapter.notifyDataSetChanged();
        }
    }


    @Override
    public void onRefresh() {
        loadData();
    }


    @Override
    public void onProductClicked(Product product, int position) {

        Intent intent = new Intent(getActivity(), ProductDetailActivity.class);
        intent.putExtra(TAG_PRODUCT_ID, product.getProductId());
        intent.putExtra(TAG_POSITION, position);
        startActivityForResult(intent, REQUEST_PRODUCT_DETAILS);
    }

    @Override
    public void onAddToWishClicked(Product product, int position) {
        postWishList(product, position);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (resultCode == RESULT_OK) {
                if (requestCode == REQUEST_PRODUCT_DETAILS && data != null) {
                    //intent.putExtra(TAG_IS_WISHED,  productDetail.getIsInWishlist());
                    //        intent.putExtra(TAG_POSITION, SELECTED_POSITION);
                    int position = data.getIntExtra(TAG_POSITION, -1);
                    int isWished = data.getIntExtra(TAG_IS_WISHED, -1);

                    if (position != -1 && isWished != -1) {
                        Product p = productList.get(position);
                        p.setIsWishlist(isWished);
                        productList.set(position, p);
//                        productAdapter.notifyDataSetChanged();
                        productAdapter.notifyItemChanged(position);


                    }
                }
            }
        } catch (IndexOutOfBoundsException e) {
            e.getLocalizedMessage();
        }
    }


    private List<Product> readCache() {
        List<Product> list = new ArrayList<>();
        try {
            Gson gson = new Gson();
            list = Arrays.asList(gson.fromJson(CacheStorage.getInstance().readCache(PRODUCTS_CACHE_KEY), Product[].class));
        } catch (NullPointerException e) {
            list = new ArrayList<>();
        }
        return list;
    }
}


