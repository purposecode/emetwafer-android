package com.purposecodes.emetwafar.features.myaccount;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.purposecodes.emetwafar.controller.SessionController.getCartQuantity;
import static com.purposecodes.emetwafar.controller.SessionController.getTrialMode;
import static com.purposecodes.emetwafar.controller.SessionController.setTrialMode;
import static com.purposecodes.emetwafar.controller.SessionController.updateUser;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_UPDATE_PLAN;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.capitalizeString;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.convertPriceText;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.isValidPassword;
import static com.purposecodes.emetwafar.utils.UtilsVariable.CALLING_ACTIVITY_KEY;
import static com.purposecodes.emetwafar.utils.UtilsVariable.NO_DATA;
import static com.purposecodes.emetwafar.utils.UtilsVariable.NO_NETWORK;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_DATA;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_ERRORVIEW;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_PROGRESSBAR;
import static com.purposecodes.emetwafar.utils.UtilsVariable.WRONG_RESPONSE;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.api.ApiFailureMessageListener;
import com.purposecodes.emetwafar.api.ApiSuccessListener;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.features.address.SavedAddressActivity;
import com.purposecodes.emetwafar.features.home.HomeActivity;
import com.purposecodes.emetwafar.features.order.MyOrdersActivity;
import com.purposecodes.emetwafar.features.subscription.SubscriptionListActivity;
import com.purposecodes.emetwafar.fragment.BaseFragment;
import com.purposecodes.emetwafar.model.CurrentPlan;
import com.purposecodes.emetwafar.model.Profile;
import com.purposecodes.emetwafar.model.UserData;
import com.purposecodes.emetwafar.responsemodel.ErrorModel;
import com.purposecodes.emetwafar.responsemodel.ProfileResponse;
import com.sk.icode.errorview.ErrorView;
import com.squareup.otto.Subscribe;

import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class MyAccountFragment extends BaseFragment {

    private final String TAG = "MyAccountFragment";


    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.imageView_myAccount_profile)
    CircleImageView ivProfile;

    @BindView(R.id.imageView_myAccount_verify)
    ImageView ivVerify;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_myAccount_name)
    TextView tvName;


    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_myAccount_location)
    TextView tvLocation;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.title_myAccount_companyOrderCount)
    TextView tvOrderCount;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.title_myAccount_companyReviewCount)
    TextView tvReviewCount;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_myAccount_savedAddress)
    TextView tvSavedAddress;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_myAccount_planName)
    TextView tvPlanName;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_myAccount_planAmount)
    TextView tvPlanAmount;
    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_myAccount_planDays)
    TextView tvPlanDay;
    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_myAccount_planExpiredDate)
    TextView tvPlanExpiredDate;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_myAccount_pendingDays)
    TextView tvPendingDays;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.cardView_myAccount_planView)
    CardView planView;


    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.view_myAccount)
    ViewGroup dataView;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.errorView)
    ErrorView errorView;

    private UserData userData;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        userData = AppController.get().getUser();


        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getMyProfile();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_my_account;
    }


    @Subscribe
    public void onHttpGetProfileSuccess(ApiSuccessListener<ProfileResponse> response) {

        Profile profile = null;
        if (response.type.equalsIgnoreCase("getProfile")) {

            ProfileResponse responseModel = response.response.body();
            if (responseModel != null && responseModel.getStatus()) {

                profile = responseModel.getProfile();
                setProfileView(profile);

            } else if (responseModel.getText() != null && TextUtils.isEmpty(responseModel.getText()))
                setErrorView(responseModel.getText(), "", false, NO_DATA);
            else
                setErrorView(getResources().getString(R.string.error_try_again), getString(R.string.error_subtitle_failed_one_more_time), true, NO_DATA);

        } else if (response.type.equalsIgnoreCase("updateProfile")) {

            ProfileResponse responseModel = response.response.body();
            if (responseModel != null && responseModel.getStatus()) {

                profile = responseModel.getProfile();
                setProfileView(profile);

                HomeActivity.getInstance().setProfile(); //checkout page

            } else if (responseModel.getText() != null && TextUtils.isEmpty(responseModel.getText()))
                showToast(responseModel.getText());
            else
                showToast(getResources().getString(R.string.error_try_again));

        }
    }

    @Subscribe
    public void onHttpSuccess(ApiSuccessListener<JsonObject> response) {

        if (response.type.equalsIgnoreCase("logout")) {
            dismissProgress();
            JsonObject jsonObject = response.response.body();
            assert jsonObject != null;
            if (jsonObject.get("status").getAsBoolean()) {
                AppController.get().logoutUser();
            } else
                showToast(jsonObject.get("text").getAsString());
        } else if (response.type.equalsIgnoreCase("changePassword")) {
            updateViews(VIEW_DATA);

            JsonObject jsonObject = response.response.body();
            if (jsonObject != null) {
                showToast(jsonObject.get("text").getAsString());
            } else
                showToast(getResources().getString(R.string.error_try_again));
        }
    }


    @Subscribe
    public void onHttpError(ApiFailureMessageListener<ErrorModel> response) {
        if (response.type.equalsIgnoreCase("getProfile")) {
            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                setErrorView(errorModel.getText(), "", false, NO_DATA);

            } catch (Exception e) {
                e.printStackTrace();
                setErrorView(getResources().getString(R.string.error_try_again), getString(R.string.error_subtitle_failed_one_more_time), true, NO_DATA);

            }
        } else if (response.type.equalsIgnoreCase("logout") || response.type.equalsIgnoreCase("changePassword")) {
            updateViews(VIEW_DATA);
            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                showToast(errorModel.getText());
            } catch (Exception e) {
                e.printStackTrace();
                showToast(getString(R.string.error_try_again));

            }
        }
    }


    @Subscribe
    public void onHttpFailure(ApiFailureListener response) {

        if (response.type.equalsIgnoreCase("getProfile"))
            setErrorView(getResources().getString(R.string.error_try_again), getString(R.string.error_subtitle_failed_one_more_time), true, WRONG_RESPONSE);
        else if (response.type.equalsIgnoreCase("logout") || response.type.equalsIgnoreCase("changePassword")) {
            updateViews(VIEW_DATA);
            showToast(getResources().getString(R.string.error_try_again));
        }


    }

    //set ErrorView
    private void setErrorView(final String title, final String subTitle, boolean isRetry, int drawable) {

        updateViews(VIEW_ERRORVIEW);

        errorView.setConfig(ErrorView.Config.create()
                .title(title)
                .subtitle(subTitle)
                .retryVisible(isRetry)
                .image(drawable)
                .build());


        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {
                getMyProfile();
            }
        });
    }

    private void getMyProfile() {

        if (AppController.get().isNetworkAvailable()) {
            AppController.get().getProfile();
            updateViews(VIEW_PROGRESSBAR);
        } else
            setErrorView(getString(R.string.no_internet), "", false, NO_NETWORK);
    }


    @SuppressLint("SetTextI18n")
    private void setProfileView(Profile profile) {
        try {

            updateViews(VIEW_DATA);
            Glide.with(this)
                    .load(profile.getProfileimg())
                    .apply(new RequestOptions()
                            .centerCrop()
                            .placeholder(R.drawable.placeholder_user)
                            .error(R.drawable.placeholder_user))
                    .into(ivProfile);


            tvName.setText(capitalizeString(profile.getCompany()));
            ivVerify.setVisibility(GONE);

            StringBuilder builder = new StringBuilder();


            if (profile.getAddress() != null && !TextUtils.isEmpty(profile.getAddress()))
                builder.append(profile.getAddress());

            if (profile.getAddress2() != null && !TextUtils.isEmpty(profile.getAddress2()))
                builder.append(", ").append(profile.getAddress2()).append("\n");

            if (profile.getCity() != null && !TextUtils.isEmpty(profile.getCity()))
                builder.append(profile.getCity());


            if (profile.getState() != null && !TextUtils.isEmpty(profile.getState()))
                builder.append(", ").append(profile.getState());

            if (profile.getCountry() != null && !TextUtils.isEmpty(profile.getCountry()))
                builder.append(", ").append(profile.getCountry());


            SpannableStringBuilder iconBuilder = new SpannableStringBuilder("  ").append(builder);
            iconBuilder.setSpan(new ImageSpan(getContext(), R.drawable.ic_location_text_my_account), 0, 1, 0);

            tvLocation.setText(iconBuilder);
            tvOrderCount.setText(String.valueOf(getCartQuantity()));
            tvReviewCount.setText("NAPI");
            tvSavedAddress.setText(builder);


            CurrentPlan plan = profile.getPlan();

            if (getTrialMode()!=(profile.getTrialMode() != null && profile.getTrialMode().equalsIgnoreCase("1"))) { //check saved mode
                setTrialMode(profile.getTrialMode() != null && profile.getTrialMode().equalsIgnoreCase("1"));
                AppController.get().inValidateTrialChanged();
            }
            if (plan != null) {
                planView.setVisibility((profile.getTrialMode() != null && profile.getTrialMode().equalsIgnoreCase("1")) ? GONE : VISIBLE);


                tvPlanName.setText(plan.getPlanName());
                tvPlanAmount.setText(convertPriceText(plan.getGrandTotal()));
                tvPlanDay.setText("For " + plan.getPlanPeriod() + " Days");
                tvPlanExpiredDate.setText(plan.getExpiryDate());
                tvPendingDays.setText(plan.getBalanceDays() + " Day(s)");
            }

            userData.setCompany(profile.getCompany());
            userData.setVerifyStatus(profile.getVerifyStatus());
            userData.setEmail(profile.getEmail());
            userData.setProfileurl(profile.getProfileimg());

            updateUser(userData);


        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void updateViews(int viewCode) {

        switch (viewCode) {

            case VIEW_PROGRESSBAR:
                showProgress();

                errorView.setVisibility(GONE);
                dataView.setVisibility(GONE);
                break;
            case VIEW_DATA:
                dismissProgress();
                dataView.setVisibility(VISIBLE);
                errorView.setVisibility(GONE);

                break;

            case VIEW_ERRORVIEW:
                dismissProgress();
                dataView.setVisibility(GONE);
                errorView.setVisibility(VISIBLE);
                break;

        }
    }

    @SuppressLint("NonConstantResourceId")
    @OnClick({R.id.button_myAccount_companyOrder, R.id.button_myAccount_viewMore, R.id.button_myAccount_planUpdate, R.id.button_myAccount_changePassword, R.id.button_myAccount_logout})
    public void onClickButton(View view) {
        switch (view.getId()) {
            case R.id.button_myAccount_companyOrder:
                startActivity(new Intent(getActivity(), MyOrdersActivity.class));

                break;
            case R.id.button_myAccount_viewMore:
                startActivity(new Intent(getActivity(), SavedAddressActivity.class));

                break;
            case R.id.button_myAccount_planUpdate:
                Intent subscriptionIntent = new Intent(getActivity(), SubscriptionListActivity.class);
                subscriptionIntent.putExtra(CALLING_ACTIVITY_KEY, ACTIVITY_UPDATE_PLAN);
                startActivity(subscriptionIntent);
                break;
            case R.id.button_myAccount_changePassword:
                showChangePasswordDialog();
                break;
            case R.id.button_myAccount_logout:
                showLogoutAlert();
                break;
        }

    }


    private void showLogoutAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
        builder.setMessage("Are you sure you want to Log Out?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                UserData _user = AppController.get().getUser();

                if (_user != null) {

                    if (AppController.get().isNetworkAvailable()) {

                        updateViews(VIEW_PROGRESSBAR);
                        AppController.get().logout();
                    } else
                        showToast(getString(R.string.no_internet));

                } else
                    AppController.get().logoutUser();

            }
        });
        builder.setNegativeButton("No", (dialog, which) -> dialog.dismiss());
        builder.create().show();
    }

    private void showChangePasswordDialog() {


        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_change_password);

        if (dialog.getWindow() != null) {
            dialog.getWindow().setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }

        dialog.setCancelable(true);
        final EditText currentEdit = dialog.findViewById(R.id.currentPassword);

        final EditText newEdit = dialog.findViewById(R.id.newPassword);
        final EditText confirmEdit = dialog.findViewById(R.id.confirmNewPassword);
        final ImageButton ibClose = dialog.findViewById(R.id.imageButton_changePassword_close);
        Button sendButton = dialog.findViewById(R.id.button_changePassword_submit);


        sendButton.setOnClickListener(view -> {
            String currentPass = currentEdit.getText().toString();
            String newPass = newEdit.getText().toString();
            String confirmNewPass = confirmEdit.getText().toString();


            if (TextUtils.isEmpty(currentPass)) {
                showToast("Current password cannot be empty");
            } else if (!isValidPassword(newPass)) {
                showToast("New password must contain minimum 8 character length, one upper case, one lower case and one digit");
                newEdit.requestFocus();
            } else if (!newPass.equals(confirmNewPass)) {
                showToast("Password doesn't match");
                confirmEdit.requestFocus();

            } else {

                hideSoftKeyboard();
                if (currentEdit.hasFocus()) {
                    hideSoftKeyboard(currentEdit);
                } else if (newEdit.hasFocus()) {
                    hideSoftKeyboard(newEdit);
                } else {
                    hideSoftKeyboard(confirmEdit);
                }


                updateViews(VIEW_PROGRESSBAR);

                HashMap<String, String> options = new HashMap<>();
                options.put("user_id", userData.getUserId());
                options.put("old_pass", currentPass);
                options.put("new_pass", newPass);


                AppController.get().changePassword(options);
                dialog.dismiss();
            }
        });

        ibClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


}
