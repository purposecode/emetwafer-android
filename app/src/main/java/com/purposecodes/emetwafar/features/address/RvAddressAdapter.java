package com.purposecodes.emetwafar.features.address;

import static android.os.Build.VERSION_CODES.LOLLIPOP;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.dpToPx;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.getPixels;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.model.SavedAddress;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RvAddressAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final String TAG = "RvAddressAdapter";
    // Context
    private final Context mContext;


    private static final int POPUP_WIDTH = 120;

    // List
    private final ArrayList<SavedAddress> mList;


    private final OnAddressClickListener listener;

    public RvAddressAdapter(Context mContext, ArrayList<SavedAddress> mList, OnAddressClickListener _listener) {
        this.mContext = mContext;
        this.mList = mList;
        this.listener = _listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_saved_address, parent, false);
        return new RvSavedAddressHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        bindItemView((RvSavedAddressHolder) viewHolder, position);
    }


    @SuppressLint("SetTextI18n")
    private void bindItemView(RvSavedAddressHolder holder, final int position) {
        try {
            SavedAddress savedAddress = mList.get(position);


            holder.tvName.setText(savedAddress.getName());


            StringBuilder builder = new StringBuilder();

            if (savedAddress.getAddress1() != null && !TextUtils.isEmpty(savedAddress.getAddress1()))
                builder.append(savedAddress.getAddress1());

            if (savedAddress.getAddress2() != null && !TextUtils.isEmpty(savedAddress.getAddress2()))
                builder.append(", ").append(savedAddress.getAddress2()).append("\n");

            if (savedAddress.getCity() != null && !TextUtils.isEmpty(savedAddress.getCity()))
                builder.append(savedAddress.getCity());


            if (savedAddress.getLandmark() != null && !TextUtils.isEmpty(savedAddress.getLandmark()))
                builder.append(", ").append(savedAddress.getLandmark());

            if (savedAddress.getState() != null && !TextUtils.isEmpty(savedAddress.getState()))
                builder.append(", ").append(savedAddress.getState());

            if (savedAddress.getCountry() != null && !TextUtils.isEmpty(savedAddress.getCountry()))
                builder.append(", ").append(savedAddress.getCountry());

            if (savedAddress.getMobile() != null && !TextUtils.isEmpty(savedAddress.getMobile()))
                builder.append(", ").append(savedAddress.getMobile());

            if (savedAddress.getAlternatePhone() != null && !TextUtils.isEmpty(savedAddress.getAlternatePhone()))
                builder.append(", ").append(savedAddress.getAlternatePhone());



            holder.tvAddress.setText(builder);


            holder.ibMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    PopupWindow popupWindow = createPopupWindow(holder.itemView.getContext(), savedAddress, position);
                    int[] loc_int = new int[2];
                    v.getLocationOnScreen(loc_int);
                    int left = v.getRight() - dpToPx(POPUP_WIDTH);
                    int top = loc_int[1];
                    popupWindow.showAtLocation(v, Gravity.TOP | Gravity.END, 70, top + 40);

                }
            });

        } catch (Exception e) {
            e.fillInStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        int count = 0;

        return mList != null ? mList.size() : 0;


    }


    private void delete(int position) { //removes the row

        mList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mList.size());
    }

    private void addItem(SavedAddress item) {
        mList.add(item);
        notifyItemInserted(mList.size() - 1);
        notifyDataSetChanged();
    }

    public void updateItem(SavedAddress item, int pos) {
        mList.set(pos, item);
        notifyItemChanged(pos);

    }


    private PopupWindow createPopupWindow(final Context context, SavedAddress savedAddress, final int position) {

        // initialize a pop up window type
        final PopupWindow popupWindow = new PopupWindow(context);
        popupWindow.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.curved_white_rect));
        popupWindow.setAnimationStyle(R.style.popup_window_animation_phone);
        // the drop down list is a list view
        ListView mPopupListView = new ListView(context);

// Check if we're running on Android 5.0 or higher
        if (Build.VERSION.SDK_INT >= LOLLIPOP) {
            // Call some material design APIs here
            popupWindow.setElevation(10);
        }


        final List<String> items = new ArrayList<>();

        items.add("Edit");
        items.add("Remove");


        ArrayAdapter<String> adapter = new ArrayAdapter<>(context, R.layout.item_spinner_code, items);

        // set our adapter and pass our pop up window contents
        mPopupListView.setAdapter(adapter);

        // set the item click listener
        mPopupListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView textView = (TextView) view;
                String text = textView.getText().toString();

                if (text.equalsIgnoreCase("Edit")) {
                    if (listener != null)
                        listener.onEditClicked(savedAddress, position);

                } else if (text.equalsIgnoreCase("Remove")) {
                    if (listener != null)
                        listener.onDeleteClicked(savedAddress, position);
                }

                popupWindow.dismiss();
            }
        });

        // some other visual settings
        popupWindow.setFocusable(true);
        popupWindow.setWidth(getPixels(POPUP_WIDTH, context));
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        // set the list view as pop up window content
        popupWindow.setContentView(mPopupListView);


        return popupWindow;
    }


    static class RvSavedAddressHolder extends RecyclerView.ViewHolder {

        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.textView_itemSavedAddress_name)
        TextView tvName;

        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.textView_itemSavedAddress_address)
        TextView tvAddress;

        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.imageButton_itemSavedAddress_more)
        ImageButton ibMore;


        RvSavedAddressHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    public interface OnAddressClickListener {
        void onEditClicked(SavedAddress savedAddress, int position);

        void onDeleteClicked(SavedAddress savedAddress, int position);
    }
}
