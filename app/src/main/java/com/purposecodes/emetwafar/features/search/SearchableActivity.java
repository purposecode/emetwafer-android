package com.purposecodes.emetwafar.features.search;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import androidx.appcompat.widget.Toolbar;

import com.google.gson.Gson;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.api.ApiFailureMessageListener;
import com.purposecodes.emetwafar.api.ApiSuccessListener;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.features.productview.ProductDetailActivity;
import com.purposecodes.emetwafar.model.Product;
import com.purposecodes.emetwafar.responsemodel.ErrorModel;
import com.purposecodes.emetwafar.responsemodel.ProductResponse;
import com.sk.icode.errorview.ErrorView;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;

import static com.purposecodes.emetwafar.utils.LogUtils.printLog;
import static com.purposecodes.emetwafar.utils.UtilsVariable.NO_DATA;
import static com.purposecodes.emetwafar.utils.UtilsVariable.NO_NETWORK;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_PRODUCT_ID;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_DATA;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_ERRORVIEW;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_PROGRESSBAR;
import static com.purposecodes.emetwafar.utils.UtilsVariable.WRONG_RESPONSE;

public class SearchableActivity extends BaseActivity implements TextWatcher {

    private final String TAG = "SearchableActivity";


    private ArrayList<Product> productList;

    private ArrayAdapter<Product> searchListAdapter;


    @BindView(R.id.errorView)
    ErrorView errorView;

    @BindView(R.id.listView_search)
    ListView productListView;

    // Widgets
    @BindView(R.id.editText_searchProduct)
    EditText etSearch;

    // Timer to track user input
    private Timer mTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_searchable);
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        setActionBar();
        initProductList();


        etSearch.addTextChangedListener(this);
    }


    private void setTextWatcher() {
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mTimer != null) {
                    mTimer.cancel();
                }
            }

            @Override
            public void afterTextChanged(final Editable s) {
                mTimer = new Timer();

                if (!TextUtils.isEmpty(s.toString())) {
                    getSearchProducts(s.toString());
                } else {

                    productList.clear();
                    searchListAdapter.notifyDataSetChanged();

                }

                /*
                mTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if (!TextUtils.isEmpty(s.toString())) {
                            getSearchProducts(s.toString());
                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    productList.clear();
                                    searchListAdapter.notifyDataSetChanged();
                                }
                            });
                        }
                    }
                }, 600);
                */
            }
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_searchable;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {//                SearchableActivity.this.finish();
            onBackPressed();
//                SearchableActivity.this.overridePendingTransition(R.anim.stay_in, R.anim.bottom_out);
//                ActivityUtils.hideSoftKeyboard(this);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Set action bar
     * 1. properties
     * 2. title with custom font
     */
    private void setActionBar() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(false);

    }

    /**
     * Initialize product list
     */
    private void initProductList() {

        productList = new ArrayList<>();
        searchListAdapter = new ArrayAdapter<Product>(this, android.R.layout.simple_list_item_1, productList);

        productListView.setAdapter(searchListAdapter);


        // set up click listener
        productListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

//                printLog(TAG, "productListView : " + position + "  size : " + productList.size());
                if (position >= 0 && position <= productList.size()) {
                    handelListItemClick(searchListAdapter.getItem(position));
                }
            }
        });
    }

    /**
     * Navigate to share activity form here
     *
     * @param product
     */
    private void handelListItemClick(Product product) {
        // close search view if its visible



        // pass selected user and sensor to share activity
        Intent intent = new Intent(this, ProductDetailActivity.class);
        intent.putExtra(TAG_PRODUCT_ID, product.getProductId());
        this.startActivity(intent);
//        this.overridePendingTransition(R.anim.right_in, R.anim.stay_in);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        hideSoftKeyboard();
        printLog(TAG, " onBackPressed  : ");

//        this.overridePendingTransition(R.anim.stay_in, R.anim.bottom_out);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hideSoftKeyboard();
    }


    private void getSearchProducts(String search) {
        updateViews(VIEW_DATA);
        productList.clear();
        searchListAdapter.notifyDataSetChanged();

        Map<String, String> options = new HashMap<>();
        options.put("keyword", search);

        if (!AppController.get().isNetworkAvailable())
            setErrorView(getString(R.string.no_internet), "", false, NO_NETWORK);
        else if (!TextUtils.isEmpty(search)) {
            updateViews(VIEW_PROGRESSBAR);
            AppController.get().getProducts(options, "search");
        }


    }


    @Subscribe
    public void onHttpGetProductListSuccess(ApiSuccessListener<ProductResponse> response) {

        if (response.type.equalsIgnoreCase("getProducts/search")) {
            updateViews(VIEW_DATA);
            ProductResponse responseModel = response.response.body();
            if (responseModel != null && responseModel.getStatus()) {

                productList.clear();

                List<Product> list = responseModel.getResponseResult().getProducts();
                if (list != null && list.size() > 0) {
                    productList.addAll(list);
                }

                printLog(TAG, "onHttpGetProductListSuccess " + response.response);
                searchListAdapter.notifyDataSetChanged();


//                if (productList.isEmpty() && responseModel.getText() != null && TextUtils.isEmpty(responseModel.getText()))
//                    setErrorView(responseModel.getText(), "", false);
//                else
                if (productList.isEmpty())
                    setErrorView("Sorry,No Result found!", "", false, NO_DATA);
//                else
//                    updateViews(VIEW_DATA);

            }
        }
    }

    @Subscribe
    public void onHttpError(ApiFailureMessageListener<ErrorModel> response) {

        if (response.type.equalsIgnoreCase("getProducts/search")) {

            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);


                setErrorView(errorModel.getText(), "", false, NO_DATA);

            } catch (Exception e) {
                e.printStackTrace();


            }


        }
    }


    @Subscribe
    public void onHttpFailure(ApiFailureListener response) {

        if (response.type.equalsIgnoreCase("getProducts/search"))
            setErrorView(getResources().getString(R.string.error_try_again), getString(R.string.error_subtitle_failed_one_more_time), true, WRONG_RESPONSE);

    }

    //set ErrorView
    private void setErrorView(final String title, final String subTitle, boolean isRetry, int drawable) {

        updateViews(VIEW_ERRORVIEW);

        errorView.setConfig(ErrorView.Config.create()
                .title(title)
                .subtitle(subTitle)
                .retryVisible(isRetry)
                .image(drawable)
                .build());


        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {
                if (etSearch != null)
                    getSearchProducts(etSearch.getText().toString());

            }
        });
    }

    private void updateViews(int viewCode) {

        switch (viewCode) {


            case VIEW_PROGRESSBAR:
                showProgress();

                errorView.setVisibility(View.GONE);
//                productListView.setVisibility(View.GONE);
                break;
            case VIEW_DATA:
                dismissProgress();
                productListView.setVisibility(View.VISIBLE);
                errorView.setVisibility(View.GONE);

                break;

            case VIEW_ERRORVIEW:
                dismissProgress();
                productListView.setVisibility(View.GONE);
                errorView.setVisibility(View.VISIBLE);
                break;

        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (mTimer != null) {
            mTimer.cancel();
        }


    }

    @Override
    public void afterTextChanged(Editable s) {
        mTimer = new Timer();

        if (!TextUtils.isEmpty(s.toString())) {
            getSearchProducts(s.toString());
        } else {

//            productList.clear();
//            searchListAdapter.notifyDataSetChanged();
//
//        }

            mTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    if (!TextUtils.isEmpty(s.toString())) {
                        getSearchProducts(s.toString());
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                productList.clear();
                                searchListAdapter.notifyDataSetChanged();

                            }
                        });
                    }
                }
            }, 600);

        }
    }
}
