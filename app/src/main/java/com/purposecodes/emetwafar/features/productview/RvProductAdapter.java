package com.purposecodes.emetwafar.features.productview;

import android.content.Context;
import android.graphics.Typeface;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.holder.RvBlankViewHolder;
import com.purposecodes.emetwafar.holder.RvFooterViewHolder;
import com.purposecodes.emetwafar.listener.OnProductClickListener;
import com.purposecodes.emetwafar.model.Product;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.purposecodes.emetwafar.utils.UtilsVariable.DEFAULT_CURRENCY;

public class RvProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final String TAG = "RvProductAdapter";
    // Context
    private final Context mContext;

    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;
    private static final int TYPE_BLANK = 3;

    // Checking the its loading for new items
    private boolean mLoading = false;
    // List
    private final ArrayList<Product> mList;

    private final OnProductClickListener listener;

    public RvProductAdapter(Context mContext, ArrayList<Product> mList, OnProductClickListener mListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.listener = mListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;
        switch (viewType) {

            case TYPE_FOOTER:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_loading, parent, false);
                return new RvFooterViewHolder(view);

            case TYPE_ITEM:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_product, parent, false);
                return new RvProductHolder(view);

            default:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_blank, parent, false);
                return new RvBlankViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        int viewType = getItemViewType(position);
        switch (viewType) {

            case TYPE_FOOTER:
                bindFooterView((RvFooterViewHolder) viewHolder);
                break;
            case TYPE_ITEM:
                bindItemView((RvProductHolder) viewHolder, position);
                break;
        }


    }


    private void bindItemView(RvProductHolder holder, final int position) {
        try {
            Product p = mList.get(position);

            Glide.with(mContext)
                    .load(p.getImageUrl())
                    .apply(new RequestOptions()
                            .centerCrop()
                            .placeholder(R.drawable.ic_upload_placeholder)
                            .error(R.drawable.ic_upload_placeholder))
                    .into(holder.ivProduct);

            holder.tvName.setText(p.getProductName());

            holder.tvCategory.setText(p.getCategoryName());


            holder.tvCode.setText("Code : " + p.getProductCode());


            holder.tvSellerId.setText(convertSellerIdrText(p.getSupplier()));


            if (!TextUtils.isEmpty(p.getOfferPrice()) && !p.getOfferPrice().equalsIgnoreCase("0")) {
                holder.tvAmount.setText(convertPriceText(p.getOfferPrice()));
                holder.tvOffer.setText(convertOfferText(p.getProductPrice(), p.getDiscount()));
                holder.tvOffer.setVisibility(View.VISIBLE);
            } else {
                holder.tvAmount.setText(convertPriceText(p.getProductPrice()));
                holder.tvOffer.setVisibility(View.GONE);
            }


            holder.tvLocation.setText(p.getCountry());

            holder.ibWish.setSelected(p.getIsWishlist() == 1);

//            printLog(TAG," bindItemView name :  "+p.getProductName()+", isWished  : "+(p.getIsWishlist() == 1));
//
            holder.ibWish.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    holder.ibWish.setSelected(!holder.ibWish.isSelected());
                    if (listener != null)
                        listener.onAddToWishClicked(p, position);
                }
            });


            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        listener.onProductClicked(p, position);
                }
            });

        } catch (Exception e) {
            e.fillInStackTrace();
        }
    }


    private SpannableStringBuilder convertPriceText(String _price) {

        String firstWord = "Price :";

// Use a SpannableStringBuilder so that both the text and the spans are mutable
        SpannableStringBuilder ssb = new SpannableStringBuilder(firstWord);
        try {
            String secondWord = _price;


// Create a span that will make the text red
            ForegroundColorSpan redForegroundColorSpan = new ForegroundColorSpan(mContext.getResources().getColor(R.color.redColor));


            ssb.append(" ");


            ssb.append(secondWord);
            // Apply the color span
            ssb.setSpan(redForegroundColorSpan, ssb.length() - secondWord.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ssb.setSpan(new StyleSpan(Typeface.BOLD), ssb.length() - secondWord.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            ssb.append(" ");
            ssb.append(DEFAULT_CURRENCY);

            ssb.setSpan(new StyleSpan(Typeface.BOLD), ssb.length() - DEFAULT_CURRENCY.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        } catch (Exception e) {
            e.fillInStackTrace();
        }

// Set the TextView text and denote that it is Editable
// since it's a SpannableStringBuilder
        return ssb;

    }

    private SpannableStringBuilder convertOfferText(String withoutOffer, String _offer) {

        String firstWord = withoutOffer + " " + DEFAULT_CURRENCY;

        // Use a SpannableStringBuilder so that both the text and the spans are mutable
        SpannableStringBuilder ssb = new SpannableStringBuilder(firstWord);
        try {
            String secondWord = _offer + "% Offer";

//        Typeface myTypeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/poppins_semi_bold.ttf");
            Typeface myTypeface = Typeface.create(ResourcesCompat.getFont(mContext, R.font.poppins_semi_bold), Typeface.BOLD);

// Create a span that will make the text red
            ForegroundColorSpan greenForegroundColorSpan = new ForegroundColorSpan(mContext.getResources().getColor(R.color.greenColor));


            // Create a span that will strikethrough the text
            StrikethroughSpan strikethroughSpan = new StrikethroughSpan();


// Apply the color span
            ssb.setSpan(
                    strikethroughSpan,            // the span to add
                    0,                                 // the start of the span (inclusive)
                    ssb.length(),                      // the end of the span (exclusive)
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE); // behavior when text is later inserted into the SpannableStringBuilder
            // SPAN_EXCLUSIVE_EXCLUSIVE means to not extend the span when additional
            // text is added in later
            ssb.setSpan(myTypeface, 0, ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
// Add a blank space
            ssb.append(" ");


// Add the secondWord and apply the strikethrough span to only the second word
            ssb.append(secondWord);
            ssb.setSpan(greenForegroundColorSpan, ssb.length() - secondWord.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

// Set the TextView text and denote that it is Editable
// since it's a SpannableStringBuilder
        } catch (Exception e) {
            e.fillInStackTrace();
        }
        return ssb;

    }

    private SpannableStringBuilder convertSellerIdrText(String _sellerId) throws NullPointerException {

        String firstWord = "Seller ID :";
        // Use a SpannableStringBuilder so that both the text and the spans are mutable
        SpannableStringBuilder ssb = new SpannableStringBuilder(firstWord);
        try {

//        Typeface myTypeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/poppins_semi_bold.ttf");

            String secondWord = _sellerId;

            Typeface myTypeface = Typeface.create(ResourcesCompat.getFont(mContext, R.font.poppins_semi_bold), Typeface.BOLD);

// Create a span that will make the text red
            ForegroundColorSpan blackForegroundColorSpan = new ForegroundColorSpan(mContext.getResources().getColor(android.R.color.black));


            // Apply the color span
//            ssb.setSpan(myTypeface, 0, ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
// Add a blank space
            ssb.append(" ");


// Add the secondWord and apply the strikethrough span to only the second word
            ssb.append(secondWord);
            ssb.setSpan(blackForegroundColorSpan, ssb.length() - secondWord.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ssb.setSpan(myTypeface, ssb.length() - secondWord.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

// Set the TextView text and denote that it is Editable
// since it's a SpannableStringBuilder
        } catch (Exception e) {
            e.fillInStackTrace();
        }
        return ssb;

    }

    private void bindFooterView(RvFooterViewHolder holder) {
        if (mLoading) {
            holder.loadText.setText(R.string.loading);
            holder.loadText.setVisibility(View.VISIBLE);
            holder.loadProgress.setVisibility(View.VISIBLE);
        } else {
            holder.loadText.setText("");
            holder.loadText.setVisibility(View.GONE);
            holder.loadProgress.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {

        int count = 0;
        if (!mList.isEmpty()) {
            count = mList.size() + 1;
        }
        return count;

    }

    @Override
    public int getItemViewType(int position) {

        try {
            if (position == getItemCount() - 1) {
                return TYPE_FOOTER;
            } else {

                return TYPE_ITEM;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            return TYPE_BLANK;
        }
    }

    public void setLoading(boolean loading) {
        mLoading = loading;
        notifyDataSetChanged();
    }


/*

    private void showMenu(View view) {


        //Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(mContext, view);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.menu_actions, popup.getMenu());


        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {


                    case R.id.action_change_password:



                        break;


                }

                return true;
            }
        });
        popup.show(); //showing popup menu
        //closing the setOnClickListener method

        //End PopUp Menu

    }

*/

    static class RvProductHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageView_itemProduct_imageView)
        ImageView ivProduct;

        @BindView(R.id.textView_itemProduct_category)
        TextView tvCategory;

        @BindView(R.id.textView_itemProduct_name)
        TextView tvName;

        @BindView(R.id.textView_itemProduct_code)
        TextView tvCode;

        @BindView(R.id.textView_itemProduct_sellerId)
        TextView tvSellerId;

        @BindView(R.id.textView_itemProduct_amount)
        TextView tvAmount;

        @BindView(R.id.textView_itemProduct_offer)
        TextView tvOffer;

        @BindView(R.id.textView_itemProduct_location)
        TextView tvLocation;

        @BindView(R.id.imageButton_itemProduct_wish)
        ImageButton ibWish;


        RvProductHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }


}
