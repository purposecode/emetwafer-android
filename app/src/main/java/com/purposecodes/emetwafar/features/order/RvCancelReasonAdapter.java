package com.purposecodes.emetwafar.features.order;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.purposecodes.emetwafar.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RvCancelReasonAdapter extends RecyclerView.Adapter<RvCancelReasonAdapter.RvCancelReasonHolder> {
    private final String TAG = "RvCancelReasonAdapter";
    // Context
//    private Context mContext;


    // List
    private final ArrayList<String> mList;

    private OnReasonClickListener listener;
    private int SELECTED_POSITION = -1;

    //    public RvCancelReasonAdapter(Context mContext, ArrayList<String> mList, OnReasonClickListener _listener) {
    public RvCancelReasonAdapter(ArrayList<String> mList) {

        this.mList = mList;
//        this.listener = _listener;
    }


    @NonNull
    @Override
    public RvCancelReasonHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_check_box, parent, false);
        return new RvCancelReasonHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RvCancelReasonHolder holder, @SuppressLint("RecyclerView") int position) {
        String offer = mList.get(position);
        holder.cbReason.setText(offer);

        holder.cbReason.setChecked(SELECTED_POSITION == position);
     /*   holder.cbReason.setOnCheckedChangeListener((buttonView, isChecked) -> {
            SELECTED_POSITION=position;
            notifyDataSetChanged();
            if (listener!=null)
                listener.onReasonSelected(offer,position);
        });*/

        holder.cbReason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SELECTED_POSITION = position;
                notifyDataSetChanged();
                if (listener != null)
                    listener.onReasonSelected(offer, position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return (mList != null ? mList.size() : 0);

    }

    public int getSelectedSelection() {
        return SELECTED_POSITION;
    }


    static class RvCancelReasonHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.checkedTextView_item)
        CheckBox cbReason;

        RvCancelReasonHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    public interface OnReasonClickListener {
        void onReasonSelected(String payment, int position);

    }
}
