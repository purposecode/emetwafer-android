package com.purposecodes.emetwafar.features.order;

import android.content.Intent;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.api.ApiFailureMessageListener;
import com.purposecodes.emetwafar.api.ApiSuccessListener;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.model.Order;
import com.purposecodes.emetwafar.responsemodel.ErrorModel;
import com.purposecodes.emetwafar.responsemodel.OrdersResponse;
import com.purposecodes.emetwafar.view.EndlessScrollListener;
import com.sk.icode.errorview.ErrorView;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

import static com.purposecodes.emetwafar.utils.LogUtils.printLog;
import static com.purposecodes.emetwafar.utils.UtilsVariable.NO_DATA;
import static com.purposecodes.emetwafar.utils.UtilsVariable.NO_NETWORK;
import static com.purposecodes.emetwafar.utils.UtilsVariable.REQUEST_ORDER_DETAIL;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_IS_UPDATE;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_ORDER;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_ORDER_ID;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_POSITION;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_ERRORVIEW;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_PROGRESSBAR;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_RECYCLERVIEW;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_SWIPEREFRESH;
import static com.purposecodes.emetwafar.utils.UtilsVariable.WRONG_RESPONSE;

public class MyOrdersActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener, RvOrderAdapter.OnOrderClickListener {


    private final String TAG = "MyOrdersActivity";

    @BindView(R.id.errorView)
    ErrorView errorView;


    @BindView(R.id.recyclerView_layout_list)
    RecyclerView recyclerView;


    @BindView(R.id.swipeRefreshLayout_layout_list)
    SwipeRefreshLayout swipeRefreshLayout;


    // RecyclerView date set
    private ArrayList<Order> orderList;
    private RvOrderAdapter orderAdapter;

    // Scroll Listener for pagination
    private EndlessScrollListener mScrollListener;

    // Current page
    private int mCurrentPage = 0, mTotalPages = -1;

    private final String mPageCount = "15";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_order_history);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        swipeRefreshLayout.setOnRefreshListener(this);
        setUpRecyclerView();

        loadData();
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.layout_list_activity;
    }


    private void setUpRecyclerView() {

        orderList = new ArrayList<>();

        orderAdapter = new RvOrderAdapter(this, orderList, this, true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(orderAdapter);
        recyclerView.setNestedScrollingEnabled(false);
        mScrollListener = new EndlessScrollListener(layoutManager, 7) {
            @Override
            public void onLoadMore(int currentPage) {
                if (mCurrentPage < mTotalPages) {
                    showRecyclerViewLoading(true);
                    getMyOrders(mCurrentPage + 1);
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        };

        recyclerView.addOnScrollListener(mScrollListener);
    }

    private void loadData() {

        mCurrentPage = 0;
        orderAdapter.notifyDataSetChanged();


        if (AppController.get().isNetworkAvailable()) {
            updateViews(VIEW_SWIPEREFRESH);
            getMyOrders(mCurrentPage + 1);  // First page
        } else {
            setErrorView(getString(R.string.no_internet), "", false, NO_NETWORK);
        }
//        else
//            showToast(getResources().getString(R.string.no_internet));


    }


    private void showRecyclerViewLoading(final boolean showLoading) {
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                orderAdapter.setLoading(showLoading);
            }
        });
    }


    private void getMyOrders(int pageNo) {

        if (AppController.get().isNetworkAvailable()) {

            Map<String, String> options = new HashMap<>();
            options.put("count", mPageCount);
//            options.put("type", "1");  //1 - pending  2- return order 3 -completed
            options.put("page_no", String.valueOf(pageNo));
//            options.put("order_no", String.valueOf(pageNo)); //order no search   [{"key":"order_no","value":"18","equals":true,"description":"order no search","enabled":true}]

            AppController.get().getOrders(options);
        } else
            showToast(getResources().getString(R.string.no_internet));

    }


    //set ErrorView
    private void setErrorView(final String title, final String subTitle, boolean isRetry, int drawable) {

        updateViews(VIEW_ERRORVIEW);
        errorView.setConfig(ErrorView.Config.create()
                .title(title)
                .subtitle(subTitle)
                .retryVisible(isRetry)
                .image(drawable)
                .build());


        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {
                loadData();
            }
        });
    }


    private void updateViews(int viewCode) {

        switch (viewCode) {
            case VIEW_PROGRESSBAR:
                showProgress();

                errorView.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                break;
            case VIEW_RECYCLERVIEW:
                dismissProgress();
                recyclerView.setVisibility(View.VISIBLE);
                errorView.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                break;

            case VIEW_ERRORVIEW:
                dismissProgress();
                recyclerView.setVisibility(View.GONE);
                errorView.setVisibility(View.VISIBLE);
                swipeRefreshLayout.setRefreshing(false);
                break;
            case VIEW_SWIPEREFRESH:
                dismissProgress();
                swipeRefreshLayout.setRefreshing(true);
                break;
        }
    }


    @Subscribe
    public void onHttpGetOrdersSuccess(ApiSuccessListener<OrdersResponse> response) {

        if (response.type.equalsIgnoreCase("getOrders")) {

            dismissProgress();
            OrdersResponse responseModel = response.response.body();
            if (responseModel != null && responseModel.getStatus()) {


                mCurrentPage = Integer.parseInt(responseModel.getResponseResult().getCurrentPage());
                mTotalPages = responseModel.getResponseResult().getTotalCount();

                if (mCurrentPage == 1) {
                    orderList.clear();
                }

                List<Order> list = responseModel.getResponseResult().getOrders();
                if (list != null && list.size() > 0) {
                    orderList.addAll(list);
                }

                orderAdapter.notifyDataSetChanged();
                updateViews(VIEW_RECYCLERVIEW);

            } else if (mCurrentPage == 0) {
                orderList.clear();
                orderAdapter.notifyDataSetChanged();
                setErrorView(responseModel.getText(), "", false, NO_DATA);
            } else if (orderList.isEmpty() && responseModel.getText() != null && TextUtils.isEmpty(responseModel.getText()))

                setErrorView(responseModel.getText(), "", false, NO_DATA);
            else if (orderList.isEmpty())
                setErrorView("No Order found!", "", false, NO_DATA);

            showRecyclerViewLoading(false);


        }
    }

    @Subscribe
    public void onHttpRemoveOrderSuccess(ApiSuccessListener<JsonObject> response) {

        if (response.type.equalsIgnoreCase("cancelOrder")) {
            updateViews(VIEW_RECYCLERVIEW);

            try {
                JsonObject jsonObject = response.response.body();
                assert jsonObject != null;
                if (jsonObject.get("status").getAsBoolean()) {


                    Order order = orderList.get(response.position);

                    order.setOrderStatus("CANCELLED");
                    orderList.set(response.position, order);
                    orderAdapter.notifyDataSetChanged();
                    orderAdapter.notifyDataSetChanged();

                    showToast(jsonObject.get("text").getAsString());
                    if (orderList.isEmpty())
                        loadData();

                } else {
                    showToast(jsonObject.get("text").getAsString());
                }
            } catch (Exception e) {
                printLog(TAG, " onHttpAddWishSuccess Exception : " + e.getMessage());
            }
        }
    }


    @Subscribe
    public void onHttpError(ApiFailureMessageListener<ErrorModel> response) {

        if (response.type.equalsIgnoreCase("getOrders") || response.type.equalsIgnoreCase("cancelOrder")) {

            showRecyclerViewLoading(false);
            mScrollListener.setLoading(false);
            updateViews(VIEW_RECYCLERVIEW);
            String msg = "";
            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                msg = errorModel.getText();
            } catch (Exception e) {
                e.printStackTrace();
                msg = getResources().getString(R.string.error_try_again);
            }
            if (!orderList.isEmpty())
                showToast(msg);
            else
                setErrorView(msg, "", false, NO_DATA);

        }
    }


    @Subscribe
    public void onHttpFailure(ApiFailureListener response) {

        if (response.type.equalsIgnoreCase("getOrders") || response.type.equalsIgnoreCase("cancelOrder")) {

            if (!orderList.isEmpty()) {
                showToast(getResources().getString(R.string.error_try_again));
                updateViews(VIEW_RECYCLERVIEW);
            } else {
                setErrorView(getResources().getString(R.string.error_try_again), getString(R.string.error_subtitle_failed_one_more_time), true, WRONG_RESPONSE);
            }
            showRecyclerViewLoading(false);
            mScrollListener.setLoading(false);

        }
    }

    @Override
    public void onRefresh() {
        loadData();
    }


    @Override
    public void onClickOrderCancel(Order order, int position) {
        showBottomAddressDialog(order, position);
    }

    @Override
    public void onClickViewAll(Order order, int position) {
        Intent intent = new Intent(MyOrdersActivity.this, OrderDetailsActivity.class);
        intent.putExtra(TAG_ORDER_ID, order.getOrderId());
        intent.putExtra(TAG_POSITION, position);
        startActivityForResult(intent, REQUEST_ORDER_DETAIL);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {
                if (requestCode == REQUEST_ORDER_DETAIL && data != null) {
                    try {

                        Order order = (Order) data.getSerializableExtra(TAG_ORDER);
                        int pos = data.getIntExtra(TAG_POSITION, -1);
                        boolean isUpdate = data.getBooleanExtra(TAG_IS_UPDATE, false);

                        if (isUpdate && pos != -1) {
                            orderList.set(pos, order);
                            orderAdapter.notifyDataSetChanged();
                        }

                    } catch (Exception e) {
                        loadData();
                    }
                }
            }
        } catch (IndexOutOfBoundsException e) {
            e.getLocalizedMessage();
        }
    }


    private void showBottomAddressDialog(Order order, int position) {

        View view = getLayoutInflater().inflate(R.layout.bottom_dialog_order_cancel, null);

        RecyclerView rvReason = view.findViewById(R.id.recyclerView_orderCancelDialog_reason);
        TextView tvComment = view.findViewById(R.id.editText_orderCancelDialog_comment);
        TextView tvHint = view.findViewById(R.id.textView_orderCancelDialog_deliveryHint);
        Button btnAddCancel = view.findViewById(R.id.button_orderCancelDialog_cancel);
        Button btnAddSubmit = view.findViewById(R.id.button_orderCancelDialog_submit);


        BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(view);
        dialog.show();


        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(getString(R.string.order_cancel_info));
//        builder.append(getString(R.string.order_cancel_info)).append(" ").append(" ", new ImageSpan(this, R.drawable.ic_info_white), 0);
        builder.append("  ");
        builder.setSpan(new ImageSpan(this, R.drawable.ic_info_white), builder.length() - 1, builder.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);


//        builder.append("My string. I ").append(" ");
//        builder.setSpan(new ImageSpan(this, R.drawable.ic_info_white),
//                builder.length() - 1, builder.length(), 0);
//        builder.append(" Cree by Dexode");

        tvHint.setText(builder);


//        String msg="Hello world   ";
//        SpannableStringBuilder ssb = new SpannableStringBuilder(msg);
//        ssb.setSpan(new ImageSpan(this, R.drawable.ic_info_white_), msg.length()-1, msg.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
//        msg="Not interested      ";
//        ssb.append(msg);
//        ssb.setSpan(new ImageSpan(this, R.drawable.ic_info_white), msg.length()-1, msg.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
//        tvHint.setText(ssb);

        ArrayList<String> reasonList = new ArrayList<>();
        reasonList.add("Not interested");
        reasonList.add("Don't Like product quality");
        reasonList.add("Changed my mind");
        reasonList.add("Other");

        RvCancelReasonAdapter cancelReasonAdapter = new RvCancelReasonAdapter(reasonList);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvReason.setHasFixedSize(true);

        rvReason.setLayoutManager(layoutManager);
        rvReason.setAdapter(cancelReasonAdapter);
//        rvBottomAddress.setNestedScrollingEnabled(true);
        cancelReasonAdapter.notifyDataSetChanged();

        btnAddSubmit.setOnClickListener(v -> {

            String strComment = tvComment.getText().toString().trim();
            if (cancelReasonAdapter.getSelectedSelection() < 0)
                showToast("Please select a reason");
            else if (TextUtils.isEmpty(strComment))
                showToast("Comment cannot be blank.");

            else if (AppController.get().isNetworkAvailable()) {
                updateViews(VIEW_PROGRESSBAR);
                Map<String, String> options = new HashMap<>();
                options.put("order_id", order.getOrderId());
                options.put("cancel_reason", reasonList.get(cancelReasonAdapter.getSelectedSelection()));
                options.put("cancel_comments", strComment);
                AppController.get().cancelOrder(options, position);

                dialog.dismiss();
            } else {
                showToast(getString(R.string.no_internet));
            }

        });

        btnAddCancel.setOnClickListener(v -> {

            dialog.dismiss();
        });


    }

}
