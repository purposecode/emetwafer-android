package com.purposecodes.emetwafar.features.filter;

import static com.purposecodes.emetwafar.utils.LogUtils.printLog;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_DISCOUNT;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_OFFERS;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_PRICE_MAX;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_PRICE_MIN;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarFinalValueListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class FilterActivity extends BaseActivity implements RvFilterOfferAdapter.OnFilterOfferClickListener, RvFilterDiscountAdapter.OnFilterDiscountClickListener {


    private final String TAG = "FilterActivity";

    // Widgets
    @SuppressLint("NonConstantResourceId")
    @Nullable
    @BindView(R.id.rangeSeekbar_filter_priceRange)
    CrystalRangeSeekbar rangeSeekbar;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_filter_minPrice)
    TextView tvMinPrice;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_filter_maxPrice)
    TextView tvMaxPrice;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.recyclerView_filter_offer)
    RecyclerView rvOffers;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.recyclerView_filter_discounts)
    RecyclerView rvDiscounts;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.button_filter_apply)
    Button btnApply;

    private ArrayList<String> selectedOffers;
    private String selectedDiscount = "";
    private String minPrice = "";
    private String maxPrice = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_filter);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        try {
            selectedDiscount = getIntent().getStringExtra(TAG_DISCOUNT);
            minPrice = getIntent().getStringExtra(TAG_PRICE_MIN);
            maxPrice = getIntent().getStringExtra(TAG_PRICE_MAX);
        } catch (Exception e) {
            e.printStackTrace();
        }


        selectedOffers = new ArrayList<>();
        setUpRangeBar();
        setUpOfferRecyclerView();
        setUpDiscountRecyclerView();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_filter;
    }

    @OnClick(R.id.button_filter_apply)
    public void onApply(View view) {


        Intent intent = new Intent();

        intent.putExtra(TAG_DISCOUNT, selectedDiscount);
        intent.putExtra(TAG_OFFERS, selectedOffers);
        intent.putExtra(TAG_PRICE_MIN, String.valueOf(rangeSeekbar.getSelectedMinValue()));
        intent.putExtra(TAG_PRICE_MAX, String.valueOf(rangeSeekbar.getSelectedMaxValue()));
        setResult(RESULT_OK, intent);
        onBackPressed();

    }


    private void setUpRangeBar() {

        rangeSeekbar.setMinValue(0f);
        rangeSeekbar.setMaxValue(100000f);

        float min = TextUtils.isEmpty(minPrice) ? 0 : Float.valueOf(minPrice);
        float max = TextUtils.isEmpty(maxPrice) ?  100000f: Float.valueOf(maxPrice);


// set listener
        rangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                tvMinPrice.setText(minValue + " SAR");
//                tvMaxPrice.setText(String.valueOf(maxValue) + " above");
                tvMaxPrice.setText(maxValue + " SAR");
            }
        });

// set final value listener
        rangeSeekbar.setOnRangeSeekbarFinalValueListener(new OnRangeSeekbarFinalValueListener() {
            @Override
            public void finalValue(Number minValue, Number maxValue) {
                printLog("CRS=>", minValue + " : " + maxValue);
            }
        });
        rangeSeekbar.setMinStartValue(min).setMaxStartValue(max).apply();
    }

    private void setUpOfferRecyclerView() {

        ArrayList<String> offerList = new ArrayList<>();
        offerList.add("Buy more,Save more");
        offerList.add("No Cost EMI");
        offerList.add("Special Price");
        RvFilterOfferAdapter offerAdapter = new RvFilterOfferAdapter(this, offerList, this);

        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        rvOffers.setHasFixedSize(true);

        rvOffers.setLayoutManager(layoutManager);
        rvOffers.setAdapter(offerAdapter);
        rvOffers.setNestedScrollingEnabled(false);

    }

    private void setUpDiscountRecyclerView() {

        ArrayList<String> discountList = new ArrayList<>();
        discountList.add("5");
        discountList.add("10");
        discountList.add("15");
        discountList.add("20");
        discountList.add("25");
        discountList.add("30");

        RvFilterDiscountAdapter offerAdapter = new RvFilterDiscountAdapter(this, discountList,selectedDiscount, this);

//        int mNoOfColumns = calculateNoOfColumns(this,150);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);

        rvDiscounts.setHasFixedSize(false);

        rvDiscounts.setLayoutManager(layoutManager);
        rvDiscounts.setAdapter(offerAdapter);
        rvDiscounts.setNestedScrollingEnabled(false);

    }


    public static int calculateNoOfColumns(Context context, float columnWidthDp) { // For example columnWidthdp=180
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float screenWidthDp = displayMetrics.widthPixels / displayMetrics.density;
        int noOfColumns = (int) (screenWidthDp / columnWidthDp + 0.5); // +0.5 for correct rounding to int.
        return noOfColumns;
    }

    @Override
    public void onOfferChecked(String offer, int position) {
        selectedOffers.add(offer);
    }

    @Override
    public void onOfferUnChecked(String offer, int position) {
        selectedOffers.remove(offer);
    }


    // Menu item
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_clear, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //            case android.R.id.home:
        //                return true;
        if (id == R.id.action_clear) {
            clearAll();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void clearAll() {
        rangeSeekbar.setMinStartValue(0f).setMaxStartValue(100000f).apply();


        selectedOffers.clear();
        selectedDiscount = "";
        setUpOfferRecyclerView();
        setUpDiscountRecyclerView();

    }

    @Override
    public void onDiscountSelected(String offer, int position) {
        selectedDiscount = offer;

    }


}
