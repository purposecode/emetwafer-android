package com.purposecodes.emetwafar.features.category;

import static com.purposecodes.emetwafar.utils.LogUtils.printLog;
import static com.purposecodes.emetwafar.utils.UtilsVariable.NO_DATA;
import static com.purposecodes.emetwafar.utils.UtilsVariable.NO_NETWORK;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TYPE_TAG_CATEGORY;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_ERRORVIEW;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_RECYCLERVIEW;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_SWIPEREFRESH;
import static com.purposecodes.emetwafar.utils.UtilsVariable.WRONG_RESPONSE;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.api.ApiFailureMessageListener;
import com.purposecodes.emetwafar.api.ApiSuccessListener;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.features.productview.ProductFilterActivity;
import com.purposecodes.emetwafar.listener.OnCategoryListener;
import com.purposecodes.emetwafar.model.Category;
import com.purposecodes.emetwafar.responsemodel.CategoryResponse;
import com.purposecodes.emetwafar.responsemodel.ErrorModel;
import com.purposecodes.emetwafar.view.EndlessScrollListener;
import com.sk.icode.errorview.ErrorView;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;

public class AllCategoryActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener, OnCategoryListener {

    private final String TAG = "AllCategoryActivity";

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.errorView)
    ErrorView errorView;


    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.recyclerView_layout_list)
    RecyclerView recyclerView;


    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.swipeRefreshLayout_layout_list)
    SwipeRefreshLayout swipeRefreshLayout;


    // RecyclerView date set
    private ArrayList<Category> categoryList;

    private RvCategoryAdapter categoryAdapter;

    // Scroll Listener for pagination
    private EndlessScrollListener mScrollListener;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        swipeRefreshLayout.setOnRefreshListener(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        categoryList = new ArrayList<>();

        try {
        categoryList.addAll( (ArrayList<Category>)getIntent().getSerializableExtra(TYPE_TAG_CATEGORY));

        }catch (NullPointerException e){
            printLog(TAG,"intent data Exception : "+e.getMessage());
        }
        setUpRecyclerView();

        if (categoryList.isEmpty())
        loadData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_list_activity;
    }


    private void setUpRecyclerView() {

        categoryAdapter = new RvCategoryAdapter(this, categoryList,this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(categoryAdapter);
        recyclerView.setNestedScrollingEnabled(false);

    }

    @SuppressLint("NotifyDataSetChanged")
    private void loadData() {

        categoryAdapter.notifyDataSetChanged();

        if (AppController.get().isNetworkAvailable()) {
            getCategories();  // First page
        } else if (categoryList.isEmpty()) {
            setErrorView(getString(R.string.no_internet), "", false,NO_NETWORK);
        }
        else {
            swipeRefreshLayout.setRefreshing(false);
            showToast(getResources().getString(R.string.no_internet));
        }

    }


    private void showRecyclerViewLoading(final boolean showLoading) {
        recyclerView.post(() -> categoryAdapter.setLoading(showLoading));
    }


    private void getCategories() {

        if (AppController.get().isNetworkAvailable()) {

            updateViews(VIEW_SWIPEREFRESH);
            AppController.get().getCategories();
        } else
            showToast(getResources().getString(R.string.no_internet));

    }


    //set ErrorView
    private void setErrorView(final String title, final String subTitle, boolean isRetry, int drawable) {

        updateViews(VIEW_ERRORVIEW);

        errorView.setConfig(ErrorView.Config.create()
                .title(title)
                .subtitle(subTitle)
                .retryVisible(isRetry)
                .image(drawable)
                .build());

        errorView.setOnRetryListener(this::getCategories);
    }

    private void updateViews(int viewCode) {

        switch (viewCode) {


            case VIEW_RECYCLERVIEW:
                dismissProgress();
                recyclerView.setVisibility(View.VISIBLE);
                errorView.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);

                break;

            case VIEW_ERRORVIEW:
                dismissProgress();
                recyclerView.setVisibility(View.GONE);
                errorView.setVisibility(View.VISIBLE);
                swipeRefreshLayout.setRefreshing(false);
                break;

            case VIEW_SWIPEREFRESH:
                dismissProgress();
                swipeRefreshLayout.setRefreshing(true);
                break;
        }
    }


    @SuppressLint("NotifyDataSetChanged")
    @Subscribe
    public void onHttpGetCategoriesSuccess(ApiSuccessListener<CategoryResponse> response) {

        if (response.type.equalsIgnoreCase("getCategories")) {

            dismissProgress();
            CategoryResponse responseModel = response.response.body();
            if (responseModel != null && responseModel.getStatus()) {

                printLog(TAG, "onHttpGetCategoriesSuccess  : " + new Gson().toJson(responseModel));

                    categoryList.clear();

                List<Category> list = responseModel.getCategories();
                if (list != null && list.size() > 0) {
                    categoryList.addAll(list);
                }


                categoryAdapter.notifyDataSetChanged();

                updateViews(VIEW_RECYCLERVIEW);



            } else if (categoryList.isEmpty() && Objects.requireNonNull(responseModel).getText() != null && TextUtils.isEmpty(responseModel.getText()))

                setErrorView(responseModel.getText(), "", false,NO_DATA);
            else if (categoryList.isEmpty())
                setErrorView("No Category found!", "", false,NO_DATA);


            showRecyclerViewLoading(false);


        }
    }


    @Subscribe
    public void onHttpError(ApiFailureMessageListener<ErrorModel> response) {

        if (response.type.equalsIgnoreCase("getCategories")) {

            showRecyclerViewLoading(false);
            mScrollListener.setLoading(false);

            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                setErrorView(errorModel.getText(), "", false,WRONG_RESPONSE);

            } catch (Exception e) {
                e.printStackTrace();
                setErrorView(getResources().getString(R.string.error_try_again), getString(R.string.error_subtitle_failed_one_more_time), true,WRONG_RESPONSE);

            }
        }
    }


    @Subscribe
    public void onHttpFailure(ApiFailureListener response) {

        if (response.type.equalsIgnoreCase("getCategories")) {
            setErrorView(getResources().getString(R.string.error_try_again), getString(R.string.error_subtitle_failed_one_more_time), true,WRONG_RESPONSE);
            showRecyclerViewLoading(false);
            mScrollListener.setLoading(false);

        }
    }


    @Override
    public void onRefresh() {
        loadData();
    }

    @Override
    public void onCategoryClicked(Category category, int position) {

        Intent intent=new Intent(AllCategoryActivity.this, ProductFilterActivity.class);
        intent.putExtra("category_tag",category);
        startActivity(intent);

    }
}
