package com.purposecodes.emetwafar.features.checkout;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity;
import com.telr.mobile.sdk.activty.WebviewActivity;
import com.telr.mobile.sdk.entity.response.status.StatusResponse;

import butterknife.BindView;
import butterknife.OnClick;

public class OrderFailedActivity extends BaseActivity  {

    private final String TAG = "OrderFailedActivity";


    @BindView(R.id.button_failedTransaction_retry)
    Button btnRetry;

    @BindView(R.id.textView_failedTransaction_title)
    TextView tvTitle;

    @BindView(R.id.textView_failedTransaction_description)
    TextView tvDescription;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_failed_transaction);

//                tvTitle.setText(getTitleText());


//            printLog(TAG,"getCartId :"+getCartId());
//            if (orders != null)
//                setUpRecyclerView(orders);



    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_failed_transaction;
    }




    @OnClick(R.id.button_failedTransaction_retry)
    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    private SpannableStringBuilder getTitleText() {
        // Use a SpannableStringBuilder so that both the text and the spans are mutable
        SpannableStringBuilder ssb = new SpannableStringBuilder();
        String tStr = "";
        try {


//            ssb.append("Your order has been placed,");
//            ssb.append("\n");
            tStr = "Your payment is declined";
            ssb.append(tStr);
            ssb.setSpan(new StyleSpan(Typeface.BOLD), ssb.length() - tStr.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        } catch (Exception e) {
            e.fillInStackTrace();
        }
        return ssb;

    }

/*    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        Object object = intent.getParcelableExtra(WebviewActivity.PAYMENT_RESPONSE);

    }*/


    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        Object object = intent.getParcelableExtra(WebviewActivity.PAYMENT_RESPONSE);


        if (object != null)
            tvDescription.setVisibility(View.VISIBLE);
        else
            return;

        if (object instanceof StatusResponse) {
            StatusResponse status = (StatusResponse) object;
            tvDescription.setText(tvDescription.getText() + " : " + status.getTrace() + " \n" + status.getAuth().getMessage());

            if (status.getAuth() != null) {
                status.getAuth().getStatus();   // Authorisation status. A indicates an authorised transaction. H also indicates an authorised transaction, but where the transaction has been placed on hold. Any other value indicates that the request could not be processed.
                status.getAuth().getAvs();      /* Result of the AVS check:
                                            Y = AVS matched OK
                                            P = Partial match (for example, post-code only)
                                            N = AVS not matched
                                            X = AVS not checked
                                            E = Error, unable to check AVS */
                status.getAuth().getCode();     // If the transaction was authorised, this contains the authorisation code from the card issuer. Otherwise it contains a code indicating why the transaction could not be processed.
                status.getAuth().getMessage();  // The authorisation or processing error message.
                status.getAuth().getCa_valid();
                status.getAuth().getCardcode(); // Code to indicate the card type used in the transaction. See the code list at the end of the document for a list of card codes.
                //  status.getAuth().getCardlast4();// The last 4 digits of the card number used in the transaction. This is supplied for all payment types (including the Hosted Payment Page method) except for PayPal.
                status.getAuth().getCvv();      /* Result of the CVV check:
                                           Y = CVV matched OK
                                           N = CVV not matched
                                           X = CVV not checked
                                           E = Error, unable to check CVV */
                status.getAuth().getTranref(); //The payment gateway transaction reference allocated to this r/equest.
                status.getAuth().getAvs();     /* Result of the AVS check:
                                           Y = AVS matched OK
                                           P = Partial match (for example, post-code only)
                                           N = AVS not matched
                                           X = AVS not checked
                                           E = Error, unable to check AVS */
            }
        } else if (object instanceof String) {
            String errorMessage = (String) object;
            tvDescription.setText(tvDescription.getText() + " : " + errorMessage);
        }
    }

//    @OnClick(R.id.button_failedTransaction_retry)
//    public void paymentRetry() {
//
//    }
}
