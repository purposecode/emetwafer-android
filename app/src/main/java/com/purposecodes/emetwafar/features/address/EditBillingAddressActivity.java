package com.purposecodes.emetwafar.features.address;

import static com.purposecodes.emetwafar.utils.LogUtils.printLog;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.isValidEmail;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.isValidMobile;
import static com.purposecodes.emetwafar.utils.UtilsVariable.DEFAULT_COUNTRY_CODE;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_BILLING_ADDRESS;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity;
import com.purposecodes.emetwafar.adapter.AddressSpinnerArrayAdapter;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.api.ApiFailureMessageListener;
import com.purposecodes.emetwafar.api.ApiSuccessListener;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.model.BillingAddress;
import com.purposecodes.emetwafar.model.City;
import com.purposecodes.emetwafar.model.State;
import com.purposecodes.emetwafar.responsemodel.ErrorModel;
import com.purposecodes.emetwafar.responsemodel.StateResponse;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class EditBillingAddressActivity extends BaseActivity {

    private final String TAG = "EditBillingAddressActivity";
    private final String OTHER_TAG = "other";
    private final String PLACE_HOLDER_TAG = "placeholder";

    @BindView(R.id.editText_billingAddress_name)
    EditText etName;

    @BindView(R.id.editText_billingAddress_addressLine_1)
    EditText etAddress1;

    @BindView(R.id.editText_billingAddress_addressLine_2)
    EditText etAddress2;

    @BindView(R.id.editText_billingAddress_poBox)
    EditText etPoBox;

    @BindView(R.id.editText_billingAddress_email)
    EditText etEmail;

    @BindView(R.id.editText_billingAddress_otherCity)
    EditText etNewCity;

    @BindView(R.id.editText_billingAddress_mobileNumber)
    EditText etMobile;

    @BindView(R.id.button_billingAddress_update)
    Button btnSave;

    @BindView(R.id.spinner_billingAddress_selectState)
    AppCompatSpinner spinnerState;

    @BindView(R.id.spinner_billingAddress_selectCity)
    AppCompatSpinner spinnerCity;

    private BillingAddress billingAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        billingAddress = (BillingAddress) getIntent().getSerializableExtra(TAG_BILLING_ADDRESS);

        assert billingAddress != null;
        setEditableData();

        getStateList();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_edit_billing_address;
    }

    private void setEditableData() {

        etName.setText(billingAddress.getCompany());
        etAddress1.setText(billingAddress.getAddress());
        etAddress2.setText(billingAddress.getAddress2());
        etPoBox.setText(billingAddress.getZipcode());
        etMobile.setText(billingAddress.getMobile());
        etEmail.setText(billingAddress.getEmail());


    }


    private void getStateList() {

        if (AppController.get().isNetworkAvailable()) {
            showProgress();
            AppController.get().getStateAndCities();
        } else
            showToast(getResources().getString(R.string.no_internet));

    }

    private void validate() {
        hideSoftKeyboard();

        String name = etName.getText().toString().trim();
        String address1 = etAddress1.getText().toString().trim();
        String address2 = etAddress2.getText().toString().trim();

        State state = (State) spinnerState.getSelectedItem();
        City city = (City) spinnerCity.getSelectedItem();
        String poBox = etPoBox.getText().toString().trim();
        String mail = etEmail.getText().toString().trim();
        String mobilePhone = etMobile.getText().toString().trim();
        String newCity = etNewCity.getText().toString().trim();


        if (TextUtils.isEmpty(name)) {
            showToast("Name cannot be blank.");
            etName.requestFocus();
        } else if (TextUtils.isEmpty(address1)) {
            showToast("Address Line 1 cannot be blank.");
            etAddress1.requestFocus();
        } else if (TextUtils.isEmpty(address2)) {
            showToast("Address Line 2 cannot be blank.");
            etAddress2.requestFocus();
        } else if ((address1+address2).trim().length()<10||(address1+address2).trim().length()>500) {
            showToast("Address Should be between 10 to 500 Characters");
            etAddress1.requestFocus();
        } else if (state == null) {
            showToast("Please select state");
            getStateList();

        } else if (state.getId().equalsIgnoreCase(PLACE_HOLDER_TAG)) {
            showToast("Please select state");
        } else if (city == null || city.getId().equalsIgnoreCase(PLACE_HOLDER_TAG)) {
            showToast("Please select city");

        } else if (city.getId().equalsIgnoreCase(OTHER_TAG) && TextUtils.isEmpty(newCity)) {
            showToast("City cannot be blank.");
            etNewCity.requestFocus();
        } else if (TextUtils.isEmpty(poBox)) {
            showToast("Postal code cannot be blank.");
            etPoBox.requestFocus();

        } else if (!isValidEmail(mail)) {
            showToast("Invalid mail ");
            etEmail.requestFocus();

        } else if (!isValidMobile(mobilePhone)) {
            showToast("Invalid Mobile number ");
            etMobile.requestFocus();

        } else if (AppController.get().isNetworkAvailable()) {

            showProgress();
            HashMap<String, String> options = new HashMap<>();
            options.put("company", name);
            options.put("address1", address1);
            options.put("address2", address2);
            options.put("email", mail);
            options.put("state", state.getId());
            options.put("city", city.getId());
            options.put("country", DEFAULT_COUNTRY_CODE);
            options.put("zipcode", poBox);
            options.put("mobile", mobilePhone);
            options.put("bill_id", billingAddress.getBill_Id());
            options.put("new_city", city.getId().equalsIgnoreCase(OTHER_TAG) ? newCity : "");

            AppController.get().updateBillingAddress(options);

        } else {
            showToast(getResources().getString(R.string.no_internet));
        }
    }

    @OnClick(R.id.button_billingAddress_update)
    public void onClickSave() {
        validate();
    }


    @Subscribe
    public void onHttpGetStateAndCitiesSuccess(ApiSuccessListener<StateResponse> response) {

        if (response.type.equalsIgnoreCase("getStateAndCities")) {

            dismissProgress();
            StateResponse responseModel = response.response.body();
            if (responseModel != null && responseModel.getStatus()) {
                if (responseModel.getStates() != null && !responseModel.getStates().isEmpty())
                    setStates(responseModel.getStates());
                else
                    showToast("No States found!");
            }
        }
    }

    @Subscribe
    public void onHttpAddressSuccess(ApiSuccessListener<JsonObject> response) {

        if (response.type.equalsIgnoreCase("updateBillingAddress")) {

            dismissProgress();
            try {
                JsonObject jsonObject = response.response.body();
                assert jsonObject != null;
                if (jsonObject.get("status").getAsBoolean()) {

                    JsonObject addressObject = jsonObject.get("data").getAsJsonObject();

                    BillingAddress address = new Gson().fromJson(addressObject, BillingAddress.class);
                    Intent intent = new Intent();
                    intent.putExtra(TAG_BILLING_ADDRESS, address);
                    setResult(RESULT_OK, intent);
                    onBackPressed();

                    showToast(jsonObject.get("text").getAsString());
                } else {
                    showToast(jsonObject.get("text").getAsString());
                }
            } catch (Exception e) {
                showToast(e.getLocalizedMessage());
                printLog(TAG, " onHttpAddAddressSuccess edit Exception : " + e.getMessage());
            }
        }
    }


    @Subscribe
    public void onHttpError(ApiFailureMessageListener<ErrorModel> response) {
        if (response.type.equalsIgnoreCase("updateBillingAddress") || response.type.equalsIgnoreCase("getStateAndCities")) {
            dismissProgress();
            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                showToast(errorModel.getText());
            } catch (Exception e) {
                showToast(getResources().getString(R.string.error_try_again));
            }
        }
    }


    @Subscribe
    public void onHttpFailure(ApiFailureListener response) {

        if (response.type.equalsIgnoreCase("updateBillingAddress") || response.type.equalsIgnoreCase("getStateAndCities")) {
            dismissProgress();
            showToast(getResources().getString(R.string.error_try_again));
        }
    }


    private void setStates(List<State> states) {

        State s = new State();
        s.setId(PLACE_HOLDER_TAG);
        s.setStateName("Select State");
        states.add(0, s);

        AddressSpinnerArrayAdapter adp = new AddressSpinnerArrayAdapter(this, states, false);

        spinnerState.setAdapter(adp);
        spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View arg1, int arg2, long arg3) {
                State item = (State) parent.getItemAtPosition(arg2);
                setCities(item.getCities());
            } public void onNothingSelected(AdapterView<?> arg0) {


            }
        });

        if (billingAddress != null && billingAddress.getState() != null) {
            for (int i = 0; i < states.size(); i++) {
                if (billingAddress.getState().equalsIgnoreCase(states.get(i).getStateName())) {
                    spinnerState.setSelection(i, true);
                    break;
                }
            }

        }

    }

    private void setCities(List<City> cities) {

        if (cities == null)
            cities = new ArrayList<>();

        City c = new City();
        c.setId(PLACE_HOLDER_TAG);
        c.setName("Select City");
        cities.add(0, c);

        c = new City();
        c.setId(OTHER_TAG);
        c.setName(OTHER_TAG);
        cities.add(c);


        AddressSpinnerArrayAdapter adp = new AddressSpinnerArrayAdapter(this, cities, false);
        spinnerCity.setAdapter(adp);
        spinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View arg1, int arg2, long arg3) {

                City item = (City) parent.getItemAtPosition(arg2);
                etNewCity.setVisibility(item.getId().equalsIgnoreCase(OTHER_TAG) ? View.VISIBLE : View.GONE);

                if (item.getId().equalsIgnoreCase(OTHER_TAG)) {
                    etNewCity.getText().clear();
                    etNewCity.requestFocus();
                }

            }

            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });


        if (billingAddress != null && billingAddress.getCity() != null) {
            for (int i = 0; i < cities.size(); i++) {
                if (billingAddress.getCity().equalsIgnoreCase(cities.get(i).getName())) {
                    spinnerCity.setSelection(i, true);
                    break;
                }
            }

        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        hideSoftKeyboard();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }


}
