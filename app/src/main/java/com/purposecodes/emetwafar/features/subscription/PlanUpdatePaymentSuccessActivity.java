package com.purposecodes.emetwafar.features.subscription;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.api.ApiFailureMessageListener;
import com.purposecodes.emetwafar.api.ApiSuccessListener;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.model.PlanData;
import com.purposecodes.emetwafar.responsemodel.ErrorModel;
import com.squareup.otto.Subscribe;
import com.telr.mobile.sdk.activty.WebviewActivity;
import com.telr.mobile.sdk.entity.response.status.StatusResponse;

import java.util.HashMap;

import butterknife.BindView;

import static com.purposecodes.emetwafar.utils.LogUtils.printLog;

public class PlanUpdatePaymentSuccessActivity extends BaseActivity {

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.view_paymentSuccess)
    View viewError;


    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_paymentSuccess_title)
    TextView tvTitle;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_paymentSuccess_description)
    TextView tvDescription;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.button_paymentSuccess_retry)
    Button btnRetry;


    private final String TAG = "SubscriptionPaymentSuccessActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_order_payment_success);
        showProgress();


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_order_payment_success;
    }


    private void setErrorView(final String title) {

        viewError.setVisibility(View.VISIBLE);
        dismissProgress();
        tvTitle.setText(title);
//        tvDescription.setText(getResources().getString(R.string.error_try_again));
        tvDescription.setText("Subscription not Completed");


    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        StatusResponse status = intent.getParcelableExtra(WebviewActivity.PAYMENT_RESPONSE);
        if (status == null)
            return;

//        printLog(TAG, " success data : "  + new Gson().toJson(status));
//        if (AppController.get().getPaymentOrders()!=null)
//            printLog(TAG," success size: "+ AppController.get().getPaymentOrders().size());
// {"auth":{"avs":"X","ca_valid":"1","cardcode":"VC","cardlast4":"1111","code":"925810","cvv":"Y","message":"Authorised","status":"A","tranref":"040025617463"},"trace":"4000/6089/5f8e9ccc"}


        if (status.getAuth() != null) {
            status.getAuth().getStatus();   // Authorisation status. A indicates an authorised transaction. H also indicates an authorised transaction, but where the transaction has been placed on hold. Any other value indicates that the request could not be processed.
            status.getAuth().getAvs();      /* Result of the AVS check:
                                            Y = AVS matched OK
                                            P = Partial match (for example, post-code only)
                                            N = AVS not matched
                                            X = AVS not checked
                                            E = Error, unable to check AVS */
            status.getAuth().getCode();     // If the transaction was authorised, this contains the authorisation code from the card issuer. Otherwise it contains a code indicating why the transaction could not be processed.
            status.getAuth().getMessage();  // The authorisation or processing error message.
            status.getAuth().getCa_valid();
            status.getAuth().getCardcode(); // Code to indicate the card type used in the transaction. See the code list at the end of the document for a list of card codes.
            status.getAuth().getCardlast4();// The last 4 digits of the card number used in the transaction. This is supplied for all payment types (including the Hosted Payment Page method) except for PayPal.
            status.getAuth().getCvv();      /* Result of the CVV check:
                                           Y = CVV matched OK
                                           N = CVV not matched
                                           X = CVV not checked
                                           E = Error, unable to check CVV */
            status.getAuth().getTranref(); //The payment gateway transaction reference allocated to this request.
            printLog(TAG, "hany : " + status.getAuth().getTranref());
            status.getAuth().getCardfirst6(); // The first 6 digits of the card number used in the transaction, only for version 2 is submitted in Tran -> Version


            planUpdate(status.getAuth().getTranref());

            btnRetry.setOnClickListener(v -> planUpdate(status.getAuth().getTranref()));


        }
    }


    private void planUpdate(String transRef) {

        PlanData planData = AppController.get().getPlanData();

        if (planData != null) {
            showProgress();
            HashMap<String, String> options = new HashMap<>();

            options.put("subscription_id", planData.getSubscriptionId());
            options.put("transaction_no", transRef);
            options.put("order_ref", planData.getPaymentReference());


            printLog(TAG, "uploadCompanyRegister planUpdate  : " + options + " \n token " + planData.getTokenId());

            AppController.get().updateSubscriptionStatus(options, planData.getTokenId());
        }
    }


    @Subscribe
    public void onHttpSuccess(ApiSuccessListener<JsonObject> response) {
        if (response.type.equalsIgnoreCase("updateSubscriptionStatus")) {
            try {

                dismissProgress();
                JsonObject jsonObject = response.response.body();

                if (jsonObject != null) {

                    Toast.makeText(getApplicationContext(), jsonObject.get("text").getAsString(), Toast.LENGTH_LONG).show();

                    if (jsonObject.get("status").getAsBoolean()) {


//                        JsonObject dataObject = jsonObject.get("data").getAsJsonObject();

                        startActivity(new Intent(PlanUpdatePaymentSuccessActivity.this, SubscriptionSuccessActivity.class));
                        finish();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }


    @Subscribe
    public void onHttpError(ApiFailureMessageListener<ErrorModel> response) {

        if (response.type.equalsIgnoreCase("updateSubscriptionStatus")) {
            dismissProgress();
            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                setErrorView(errorModel.getText());

            } catch (Exception e) {
                e.printStackTrace();
                setErrorView(getString(R.string.error_try_again));

            }
        }
    }

    @Subscribe
    public void onHttpFailure(ApiFailureListener response) {

        if (response.type.equalsIgnoreCase("updateSubscriptionStatus")) {
            dismissProgress();
            setErrorView(getResources().getString(R.string.error_try_again));
        }
    }

}
