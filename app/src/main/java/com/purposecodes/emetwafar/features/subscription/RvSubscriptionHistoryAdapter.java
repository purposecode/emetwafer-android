package com.purposecodes.emetwafar.features.subscription;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.holder.RvBlankViewHolder;
import com.purposecodes.emetwafar.holder.RvFooterViewHolder;
import com.purposecodes.emetwafar.model.SubscriptionHistoryItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.convertPriceText;

public class RvSubscriptionHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final String TAG = "RvSubscriptionHistoryAdapter";
    // Context
    private final Context mContext;

    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;
    private static final int TYPE_BLANK = 3;

    // Checking the its loading for new items
    private boolean mLoading = false;
    // List
    private final ArrayList<SubscriptionHistoryItem> mList;


    public RvSubscriptionHistoryAdapter(Context mContext, ArrayList<SubscriptionHistoryItem> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;
        switch (viewType) {

            case TYPE_FOOTER:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_loading, parent, false);
                return new RvFooterViewHolder(view);

            case TYPE_ITEM:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_subscription_history, parent, false);
                return new RvSubscriptionHistoryHolder(view);

            default:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_blank, parent, false);
                return new RvBlankViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        int viewType = getItemViewType(position);
        switch (viewType) {

            case TYPE_FOOTER:
                bindFooterView((RvFooterViewHolder) viewHolder);
                break;
            case TYPE_ITEM:
                bindItemView((RvSubscriptionHistoryHolder) viewHolder, position);
                break;
        }


    }


    private void bindItemView(RvSubscriptionHistoryHolder holder, final int position) {

        try {


            SubscriptionHistoryItem item = mList.get(position);


            String status = item.getStatus();
            holder.cvBackground.setCardBackgroundColor(status.equalsIgnoreCase("active") ? mContext.getResources().getColor(R.color.whiteColor) : mContext.getResources().getColor(R.color.expiredBg));

            holder.viewDivider.setBackgroundColor(status.equalsIgnoreCase("active")? mContext.getResources().getColor(R.color.dimWhite) : mContext.getResources().getColor(R.color.whiteColor50));

            holder.tvTitleExpireDate.setText(status.equalsIgnoreCase("active")?"Expiry Date":"Expired Date");

            String upperString = status.substring(0, 1).toUpperCase() + status.substring(1).toLowerCase();

            holder.tvTitle.setText(item.getPlanName());
            holder.tvStatus.setText(upperString);
            holder.tvPlanDays.setText("For " + item.getPlanPeriod() + " Days");
            holder.tvStartDate.setText(item.getStartDate());
            holder.tvBalanceDays.setText(item.getBalanceDays() + " Days");
            holder.tvExpireDate.setText(item.getExpiryDate());
            holder.tvPlanCharge.setText(convertPriceText(item.getPlanCharge()));
            holder.tvVat.setText(convertPriceText(item.getVat()));
            holder.tvGrandTotal.setText(convertPriceText(item.getGrandTotal()));

            holder.viewBalanceDays.setVisibility(status.equals("active") ? VISIBLE : GONE);

            if (status.equals("active"))
                holder.tvStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_green_tick, 0, 0, 0);
            else
                holder.tvStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_plan_inactive, 0, 0, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void bindFooterView(RvFooterViewHolder holder) {
        if (mLoading) {
            holder.loadText.setText(R.string.loading);
            holder.loadText.setVisibility(VISIBLE);
            holder.loadProgress.setVisibility(VISIBLE);
        } else {
            holder.loadText.setText("");
            holder.loadText.setVisibility(View.GONE);
            holder.loadProgress.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() + 1 : 0;
    }

    @Override
    public int getItemViewType(int position) {

        try {
            if (position == getItemCount() - 1) {
                return TYPE_FOOTER;
            } else {

                return TYPE_ITEM;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            return TYPE_BLANK;
        }
    }

    public void setLoading(boolean loading) {
        mLoading = loading;
        notifyDataSetChanged();
    }


    static class RvSubscriptionHistoryHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.textView_itemSubscription_title)
        TextView tvTitle;

        @BindView(R.id.textView_itemSubscription_status)
        TextView tvStatus;

        @BindView(R.id.textView_itemSubscription_planPeriod)
        TextView tvPlanDays;

        @BindView(R.id.textView_itemSubscription_startDate)
        TextView tvStartDate;

        @BindView(R.id.textView_itemSubscription_balanceDays)
        TextView tvBalanceDays;

        @BindView(R.id.textView_itemSubscription_expireDate)
        TextView tvExpireDate;

        @BindView(R.id.title_itemSubscription_expireDate)
        TextView tvTitleExpireDate;

        @BindView(R.id.textView_itemSubscription_planCharge)
        TextView tvPlanCharge;
        @BindView(R.id.textView_itemSubscription_vat)
        TextView tvVat;
        @BindView(R.id.textView_itemSubscription_grandTotal)
        TextView tvGrandTotal;
        @BindView(R.id.view_itemSubscription_balanceDays)
        View viewBalanceDays;
        @BindView(R.id.view_itemSubscriptionHistory_divider)
        View viewDivider;
        @BindView(R.id.cardView_itemSubscriptionHistory)
        CardView cvBackground;


        RvSubscriptionHistoryHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }


}
