package com.purposecodes.emetwafar.features.checkout;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.model.SavedAddress;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RvBottomAddressAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final String TAG = "RvBottomAddressAdapter";
    // Context
    private final Context mContext;


    private String SELECTED_ADDRESS_ID = "";
    // List
    private final ArrayList<SavedAddress> mList;


    private final OnAddressSelectListener listener;

    public RvBottomAddressAdapter(Context mContext, String adId, ArrayList<SavedAddress> mList, OnAddressSelectListener _listener) {
        this.mContext = mContext;
        this.mList = mList;
        this.SELECTED_ADDRESS_ID = adId;
        this.listener = _listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_checkoutsaved_address, parent, false);
        return new RvBottomAddressHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        bindItemView((RvBottomAddressHolder) viewHolder, position);
    }

    @SuppressLint({"SetTextI18n", "NotifyDataSetChanged"})
    private void bindItemView(RvBottomAddressHolder holder, final int position) {
        try {
            SavedAddress savedAddress = mList.get(position);

            holder.tvName.setText(savedAddress.getName());

            StringBuilder builder = new StringBuilder();

            if (savedAddress.getAddress1() != null && !TextUtils.isEmpty(savedAddress.getAddress1()))
                builder.append(savedAddress.getAddress1());

            if (savedAddress.getAddress2() != null && !TextUtils.isEmpty(savedAddress.getAddress2()))
                builder.append("\n").append(savedAddress.getAddress2());

            if (savedAddress.getCity() != null && !TextUtils.isEmpty(savedAddress.getCity()))
                builder.append("\n").append(savedAddress.getCity());

            if (savedAddress.getZipcode() != null && !TextUtils.isEmpty(savedAddress.getZipcode()))
                builder.append("\nZip Code ").append(savedAddress.getZipcode());

            if (savedAddress.getState() != null && !TextUtils.isEmpty(savedAddress.getState()))
                builder.append("\n").append(savedAddress.getState());
            if (savedAddress.getCountry() != null && !TextUtils.isEmpty(savedAddress.getCountry()))
                builder.append(",").append(savedAddress.getCountry());

            if (savedAddress.getMobile() != null && !TextUtils.isEmpty(savedAddress.getMobile()))
                builder.append("\n").append(savedAddress.getMobile());


            holder.tvAddress.setText(builder);



            holder.rbSelection.setChecked(!TextUtils.isEmpty(SELECTED_ADDRESS_ID) && SELECTED_ADDRESS_ID.equalsIgnoreCase(savedAddress.getAddressId()));
            holder.itemView.setOnClickListener(v -> {
                SELECTED_ADDRESS_ID = savedAddress.getAddressId();
                notifyDataSetChanged();

                if (listener != null)
                    listener.onAddressSelected(savedAddress, position);
            });

        } catch (Exception e) {
            e.fillInStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }


    static class RvBottomAddressHolder extends RecyclerView.ViewHolder {

        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.radiobutton_itemCheckoutSavedAddress)
        RadioButton rbSelection;

        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.textView_itemCheckoutSavedAddress_name)
        TextView tvName;

        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.textView_itemCheckoutSavedAddress_address)
        TextView tvAddress;


        RvBottomAddressHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    public interface OnAddressSelectListener {
        void onAddressSelected(SavedAddress savedAddress, int position);

    }

}
