package com.purposecodes.emetwafar.features.subscription;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.holder.RvBlankViewHolder;
import com.purposecodes.emetwafar.holder.RvFooterViewHolder;
import com.purposecodes.emetwafar.model.Subscription;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.purposecodes.emetwafar.utils.LogUtils.printLog;
import static com.purposecodes.emetwafar.utils.UtilsVariable.DEFAULT_CURRENCY;

public class RvSubscriptionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final String TAG = "RvSubscriptionAdapter";
    // Context
    private final Context mContext;

    private static final int TYPE_HEADER = 1;
    private static final int TYPE_ITEM = 2;
    private static final int TYPE_FOOTER = 3;
    private static final int TYPE_BLANK = 4;


    // Checking the its loading for new items
    private boolean mLoading = false;
    // List
    private final ArrayList<Subscription> mList;

    private final OnSubscriptionClickListener listener;

    /**
     * The number of available tile colors (see R.array.tile_colors)
     */
    private static final int NUM_OF_TILE_COLORS = 2;

private boolean isUpdate=false;
    int[] colorArray;

    public RvSubscriptionAdapter(Context mContext, ArrayList<Subscription> mList, OnSubscriptionClickListener mListener,boolean _isUpdate) {

        this.mContext = mContext;
        this.mList = mList;
        this.listener = mListener;
        this.isUpdate = _isUpdate;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        final Resources res = mContext.getResources();
        colorArray = res.getIntArray(R.array.tile_colors);

        View view;
        switch (viewType) {

            case TYPE_HEADER:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_subscription_header, parent, false);
                return new RvHeaderHolder(view);
            case TYPE_FOOTER:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_loading, parent, false);
                return new RvFooterViewHolder(view);

            case TYPE_ITEM:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_subscription, parent, false);
                return new RvSubscriptionHolder(view);

            default:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_blank, parent, false);
                return new RvBlankViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        int viewType = getItemViewType(position);
        switch (viewType) {

            case TYPE_HEADER:
                bindHeaderView((RvHeaderHolder) viewHolder);
                break;
                case TYPE_FOOTER:
                bindFooterView((RvFooterViewHolder) viewHolder);
                break;
            case TYPE_ITEM:
                bindItemView((RvSubscriptionHolder) viewHolder, position);
                break;
        }


    }


    private void bindHeaderView(RvHeaderHolder holder) {



              holder.ibBack.setVisibility(isUpdate?View.VISIBLE:View.GONE);

              holder.ibBack.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                     if (listener!=null)
                         listener.onClickBack();
                  }
              });


    }

    @SuppressLint("SetTextI18n")
    private void bindItemView(RvSubscriptionHolder holder, final int position) {
        try {

            int positionColor = position % NUM_OF_TILE_COLORS;  // get Colour from color array max length 2


//            holder.cvBg.setCardBackgroundColor(positionColor == 1 ? mContext.getResources().getColor(R.color.greenColor) : mContext.getResources().getColor(R.color.blackColor));
            holder.cvBg.setCardBackgroundColor(colorArray[positionColor]);
//            holder.viewDivider.setBackgroundColor(positionColor == 1 ? mContext.getResources().getColor(R.color.subscription_green) : mContext.getResources().getColor(R.color.subscription_black));

            Subscription subscription = mList.get(position - 1);

            holder.tvTitle.setText(subscription.getPlanName().replace(" ", "\n"));
            holder.tvAmount.setText(subscription.getCharge()+" "+DEFAULT_CURRENCY);
            holder.tvDays.setText("For " + subscription.getPlanPeriod() + " days");
            holder.tvDescription.setText("");


            SpannableStringBuilder sbb = new SpannableStringBuilder();
            if (subscription.getFeatures() != null && !subscription.getFeatures().isEmpty()) {


                for (Subscription.SubscriptionFeature f : subscription.getFeatures()) {
//                    sb.append("\u25CF  ");
//                    sb.append("\u2022");
                    sbb.append(getBullet((positionColor == 1) ? R.color.subscription_green : R.color.subscription_black));
                    sbb.append("  ");
                    sbb.append(f.getFeature());
                    sbb.append("\n");

                }

//                String s=sbb.delete(sbb.length() - 1,sbb.length()).toString();
//                String s = sbb.toString().trim();


//                    holder.tvDescription.setText(sb.deleteCharAt(sb.length() - 1));
//                    holder.tvDescription.setText(s.substring(0,s.length()-1));

                holder.tvDescription.setText(sbb.delete(sbb.length() - 1, sbb.length()));


            }


            holder.btnTry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        listener.onClickTryPlan(subscription, position - 1); //header view is 0 th position
                }
            });
        } catch (Exception e) {
            printLog(TAG, " Exception :   " + e.getMessage());
        }
    }


    private SpannableStringBuilder getBullet(int color) {

//                    sb.append("\u25CF  ");
//                    sb.append("\u2022");
        // Use a SpannableStringBuilder so that both the text and the spans are mutable
        SpannableStringBuilder ssb = new SpannableStringBuilder("\u25CF");
        try {

// Create a span that will make the text color
            ForegroundColorSpan blackForegroundColorSpan = new ForegroundColorSpan(mContext.getResources().getColor(color));

            ssb.setSpan(blackForegroundColorSpan, 0, ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        } catch (Exception e) {
            e.fillInStackTrace();
        }
        return ssb;

    }

    private void bindFooterView(RvFooterViewHolder holder) {
        if (mLoading) {
            holder.loadText.setText(R.string.loading);
            holder.loadText.setVisibility(View.VISIBLE);
            holder.loadProgress.setVisibility(View.VISIBLE);
        } else {
            holder.loadText.setText("");
            holder.loadText.setVisibility(View.GONE);
            holder.loadProgress.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() + 1 : 0;
    }

    @Override
    public int getItemViewType(int position) {

        try {
            if (position == 0) {
                return TYPE_HEADER;
            } else if (position == getItemCount()) {
                return TYPE_FOOTER;
            } else {

                return TYPE_ITEM;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            return TYPE_BLANK;
        }
    }

    public void setLoading(boolean loading) {
        mLoading = loading;
        notifyDataSetChanged();
    }


    static class RvSubscriptionHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cardView_subscription)
        CardView cvBg;

        @BindView(R.id.textView_subscription_title)
        TextView tvTitle;

        @BindView(R.id.textView_subscription_amount)
        TextView tvAmount;

        @BindView(R.id.textView_subscription_days)
        TextView tvDays;

        @BindView(R.id.textView_subscription_desc)
        TextView tvDescription;

//        @BindView(R.id.divider_subscription)
//        View viewDivider;


        @BindView(R.id.button_subscription_try)
        Button btnTry;


        RvSubscriptionHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    static class RvHeaderHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.imageButton_subscriptionHeader_back)
        ImageButton ibBack;


        RvHeaderHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public interface OnSubscriptionClickListener {
        void onClickTryPlan(Subscription subscription, int position);
        void onClickBack();

    }


}
