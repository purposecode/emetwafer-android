package com.purposecodes.emetwafar.features.subscription;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.api.ApiFailureMessageListener;
import com.purposecodes.emetwafar.api.ApiSuccessListener;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.model.SubscriptionHistoryItem;
import com.purposecodes.emetwafar.responsemodel.ErrorModel;
import com.purposecodes.emetwafar.responsemodel.SubscriptionHistoryResponse;
import com.purposecodes.emetwafar.view.EndlessScrollListener;
import com.sk.icode.errorview.ErrorView;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

import static com.purposecodes.emetwafar.utils.UtilsVariable.NO_DATA;
import static com.purposecodes.emetwafar.utils.UtilsVariable.NO_NETWORK;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_ERRORVIEW;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_PROGRESSBAR;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_RECYCLERVIEW;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_SWIPEREFRESH;
import static com.purposecodes.emetwafar.utils.UtilsVariable.WRONG_RESPONSE;

public class SubscriptionHistoryActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    private final String TAG = "SubscriptionHistoryActivity";

    @BindView(R.id.errorView)
    ErrorView errorView;

    @BindView(R.id.recyclerView_layout_list)
    RecyclerView recyclerView;


    @BindView(R.id.swipeRefreshLayout_layout_list)
    SwipeRefreshLayout swipeRefreshLayout;

    // RecyclerView date set
    private ArrayList<SubscriptionHistoryItem> subscriptionHistoryItems;
    private RvSubscriptionHistoryAdapter subscriptionHistoryAdapter;

    // Scroll Listener for pagination
    private EndlessScrollListener mScrollListener;

    // Current page
    private int mCurrentPage = 0;
    private final int mTotalPages = -1;

    private final String mPageCount = "15";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        swipeRefreshLayout.setOnRefreshListener(this);
        setUpRecyclerView();

        loadData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_list_activity;
    }


    private void setUpRecyclerView() {

        subscriptionHistoryItems = new ArrayList<>();

        subscriptionHistoryAdapter = new RvSubscriptionHistoryAdapter(this, subscriptionHistoryItems);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(subscriptionHistoryAdapter);
        recyclerView.setNestedScrollingEnabled(false);
        mScrollListener = new EndlessScrollListener(layoutManager, 7) {
            @Override
            public void onLoadMore(int currentPage) {
                if (mCurrentPage < mTotalPages) {
                    showRecyclerViewLoading(true);
                    getSubscriptionHistory(mCurrentPage + 1);
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        };

        recyclerView.addOnScrollListener(mScrollListener);
    }

    private void loadData() {

        mCurrentPage = 0;
        subscriptionHistoryAdapter.notifyDataSetChanged();


        if (AppController.get().isNetworkAvailable()) {
            updateViews(VIEW_SWIPEREFRESH);
            getSubscriptionHistory(mCurrentPage + 1);  // First page
        } else {
            setErrorView(getString(R.string.no_internet), "", false, NO_NETWORK);
        }


    }


    private void showRecyclerViewLoading(final boolean showLoading) {
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                subscriptionHistoryAdapter.setLoading(showLoading);
            }
        });
    }


    private void getSubscriptionHistory(int pageNo) {

        if (AppController.get().isNetworkAvailable()) {

            Map<String, String> options = new HashMap<>();
            options.put("count", mPageCount);
//            options.put("type", "1");  //1 - pending  2- return order 3 -completed
            options.put("page_no", String.valueOf(pageNo));
//            options.put("order_no", String.valueOf(pageNo)); //order no search   [{"key":"order_no","value":"18","equals":true,"description":"order no search","enabled":true}]

            AppController.get().getSubscriptionHistory(options);
        } else
            showToast(getResources().getString(R.string.no_internet));

    }


    //set ErrorView
    private void setErrorView(final String title, final String subTitle, boolean isRetry, int drawable) {

        updateViews(VIEW_ERRORVIEW);
        errorView.setConfig(ErrorView.Config.create()
                .title(title)
                .subtitle(subTitle)
                .retryVisible(isRetry)
                .image(drawable)
                .build());

        updateViews(VIEW_ERRORVIEW);

        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {
                loadData();
            }
        });
    }


    private void updateViews(int viewCode) {

        switch (viewCode) {
            case VIEW_PROGRESSBAR:
                showProgress();

                errorView.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                break;
            case VIEW_RECYCLERVIEW:
                dismissProgress();
                recyclerView.setVisibility(View.VISIBLE);
                errorView.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                break;

            case VIEW_ERRORVIEW:
                dismissProgress();
                recyclerView.setVisibility(View.GONE);
                errorView.setVisibility(View.VISIBLE);
                swipeRefreshLayout.setRefreshing(false);
                break;
            case VIEW_SWIPEREFRESH:
                dismissProgress();
                swipeRefreshLayout.setRefreshing(true);
                break;
        }
    }


    @Subscribe
    public void onHttpGetOrdersSuccess(ApiSuccessListener<SubscriptionHistoryResponse> response) {

        if (response.type.equalsIgnoreCase("getSubscriptionHistory")) {

            dismissProgress();
            SubscriptionHistoryResponse responseModel = response.response.body();
            if (responseModel != null && responseModel.getStatus()) {


//                mCurrentPage = Integer.parseInt(responseModel.getResponseResult().getCurrentPage());
//                mTotalPages = responseModel.getResponseResult().getTotalCount();

//                if (mCurrentPage == 1) {
                subscriptionHistoryItems.clear();
//                }
                List<SubscriptionHistoryItem> list = responseModel.getItems();
                if (list != null && list.size() > 0) {
                    subscriptionHistoryItems.addAll(list);
                }

                subscriptionHistoryAdapter.notifyDataSetChanged();
                updateViews(VIEW_RECYCLERVIEW);

            } else if (mCurrentPage == 0) {
                subscriptionHistoryItems.clear();
                subscriptionHistoryAdapter.notifyDataSetChanged();
                setErrorView(responseModel.getText(), "", false, NO_DATA);
            } else if (subscriptionHistoryItems.isEmpty() && responseModel.getText() != null && TextUtils.isEmpty(responseModel.getText()))

                setErrorView(responseModel.getText(), "", false, NO_DATA);
            else if (subscriptionHistoryItems.isEmpty())
                setErrorView("No History found!", "", false, NO_DATA);

            showRecyclerViewLoading(false);


        }
    }


    @Subscribe
    public void onHttpError(ApiFailureMessageListener<ErrorModel> response) {

        if (response.type.equalsIgnoreCase("getSubscriptionHistory")) {

            showRecyclerViewLoading(false);
            mScrollListener.setLoading(false);
            updateViews(VIEW_RECYCLERVIEW);
            String msg = "";
            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                msg = errorModel.getText();
            } catch (Exception e) {
                e.printStackTrace();
                msg = getResources().getString(R.string.error_try_again);
            }
            if (!subscriptionHistoryItems.isEmpty())
                showToast(msg);
            else
                setErrorView(msg, "", false, WRONG_RESPONSE);

        }
    }


    @Subscribe
    public void onHttpFailure(ApiFailureListener response) {

        if (response.type.equalsIgnoreCase("getSubscriptionHistory")) {

            if (!subscriptionHistoryItems.isEmpty()) {
                showToast(getResources().getString(R.string.error_try_again));
                updateViews(VIEW_RECYCLERVIEW);
            } else {
                setErrorView(getResources().getString(R.string.error_try_again), getString(R.string.error_subtitle_failed_one_more_time), true, WRONG_RESPONSE);
            }
            showRecyclerViewLoading(false);
            mScrollListener.setLoading(false);

        }
    }

    @Override
    public void onRefresh() {
        loadData();
    }


}
