package com.purposecodes.emetwafar.features.wish;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.holder.RvBlankViewHolder;
import com.purposecodes.emetwafar.holder.RvFooterViewHolder;
import com.purposecodes.emetwafar.model.WishItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.os.Build.VERSION_CODES.LOLLIPOP;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.dpToPx;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.getPixels;
import static com.purposecodes.emetwafar.utils.UtilsVariable.DEFAULT_CURRENCY;

public class RvWishListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final String TAG = "RvWishListAdapter";
    // Context
    private final Context mContext;

    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;
    private static final int TYPE_BLANK = 3;

    // Checking the its loading for new items
    private boolean mLoading = false;
    // List
    private final ArrayList<WishItem> mList;

    private static final int POPUP_WIDTH = 120;


    private final onRemoveWishListListener listListener;

    public RvWishListAdapter(Context mContext, ArrayList<WishItem> mList, onRemoveWishListListener _listListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.listListener = _listListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;
        switch (viewType) {

            case TYPE_FOOTER:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_loading, parent, false);
                return new RvFooterViewHolder(view);

            case TYPE_ITEM:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_wishlist, parent, false);
                return new RvWishListHolder(view);

            default:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_blank, parent, false);
                return new RvBlankViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        int viewType = getItemViewType(position);
        switch (viewType) {

            case TYPE_FOOTER:
                bindFooterView((RvFooterViewHolder) viewHolder);
                break;
            case TYPE_ITEM:
                bindItemView((RvWishListHolder) viewHolder, position);
                break;
        }


    }


    private void bindItemView(RvWishListHolder holder, final int position) {

        WishItem p = mList.get(position);

        Glide.with(mContext)
                .load(p.getImageUrl())
                .apply(new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.ic_upload_placeholder)
                        .error(R.drawable.ic_upload_placeholder))
                .into(holder.ivProduct);

        holder.tvName.setText(p.getProductName());
        holder.tvCategory.setText(p.getCategoryName());

        holder.tvCode.setText(convertCodeText(p.getProductCode()));


        holder.tvSellerId.setText(convertSellerIdrText(p.getSupplier()));


        holder.tvLocation.setText(p.getCountry());


        if (!TextUtils.isEmpty(p.getOfferPrice()) && !p.getOfferPrice().equalsIgnoreCase("0")) {
            holder.tvAmount.setText(convertPriceText(p.getOfferPrice()));
            holder.tvOffer.setText(convertOfferText(p.getProductPrice(), p.getDiscount()));
            holder.tvOffer.setVisibility(View.VISIBLE);
        } else {
            holder.tvAmount.setText(convertPriceText(p.getProductPrice()));
            holder.tvOffer.setVisibility(View.GONE);
        }


        holder.ibMore.setOnClickListener(v -> {


            PopupWindow popupWindow = createPopupWindow(holder.itemView.getContext(), p, position);
            int[] loc_int = new int[2];
            v.getLocationOnScreen(loc_int);
            int left = v.getRight() - dpToPx(POPUP_WIDTH);
            int top = loc_int[1];
            popupWindow.showAtLocation(v, Gravity.TOP | Gravity.END, 70, top);


        });

        holder.itemView.setOnClickListener(v -> {
            if (listListener != null)
                listListener.onWishListClicked(p, position);
        });


    }


    private SpannableStringBuilder convertCodeText(String _code) {

        String firstWord = "Code :";
        // Use a SpannableStringBuilder so that both the text and the spans are mutable
        SpannableStringBuilder ssb = new SpannableStringBuilder(firstWord);
        try {


            String secondWord = _code;

// Create a span that will make the text red
            ForegroundColorSpan blackForegroundColorSpan = new ForegroundColorSpan(mContext.getResources().getColor(android.R.color.black));

// Add a blank space
            ssb.append(" ");

// Add the secondWord and apply the strikethrough span to only the second word
            ssb.append(secondWord);
            ssb.setSpan(blackForegroundColorSpan, ssb.length() - secondWord.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ssb.setSpan(new StyleSpan(Typeface.BOLD), ssb.length() - secondWord.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);


// since it's a SpannableStringBuilder
        } catch (Exception e) {
            e.fillInStackTrace();
        }
        return ssb;

    }

    private SpannableStringBuilder convertPriceText(String _price) {

        String firstWord = "Price:";

// Use a SpannableStringBuilder so that both the text and the spans are mutable
        SpannableStringBuilder ssb = new SpannableStringBuilder(firstWord);
        try {
            String secondWord = _price;


// Create a span that will make the text red
            ForegroundColorSpan redForegroundColorSpan = new ForegroundColorSpan(mContext.getResources().getColor(R.color.redColor));


            ssb.append(" ");


            ssb.append(secondWord);
            // Apply the color span
            ssb.setSpan(redForegroundColorSpan, ssb.length() - secondWord.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ssb.setSpan(new StyleSpan(Typeface.BOLD), ssb.length() - secondWord.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            ssb.append(" ");
            ssb.append(DEFAULT_CURRENCY);

            ssb.setSpan(new StyleSpan(Typeface.BOLD), ssb.length() - DEFAULT_CURRENCY.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        } catch (Exception e) {
            e.fillInStackTrace();
        }

// Set the TextView text and denote that it is Editable
// since it's a SpannableStringBuilder
        return ssb;

    }

    private SpannableStringBuilder convertOfferText(String withoutOffer, String _offer) {
        String firstWord = withoutOffer + " " + DEFAULT_CURRENCY;

        // Use a SpannableStringBuilder so that both the text and the spans are mutable
        SpannableStringBuilder ssb = new SpannableStringBuilder(firstWord);
        try {
            String secondWord = _offer + "% Offer";

//        Typeface myTypeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/poppins_semi_bold.ttf");
            Typeface myTypeface = Typeface.create(ResourcesCompat.getFont(mContext, R.font.poppins_semi_bold), Typeface.BOLD);

// Create a span that will make the text red
            ForegroundColorSpan greenForegroundColorSpan = new ForegroundColorSpan(mContext.getResources().getColor(R.color.greenColor));


            // Create a span that will strikethrough the text
            StrikethroughSpan strikethroughSpan = new StrikethroughSpan();


// Apply the color span
            ssb.setSpan(
                    strikethroughSpan,            // the span to add
                    0,                                 // the start of the span (inclusive)
                    ssb.length(),                      // the end of the span (exclusive)
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE); // behavior when text is later inserted into the SpannableStringBuilder
            // SPAN_EXCLUSIVE_EXCLUSIVE means to not extend the span when additional
            // text is added in later
            ssb.setSpan(myTypeface, 0, ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
// Add a blank space
            ssb.append(" ");


// Add the secondWord and apply the strikethrough span to only the second word
            ssb.append(secondWord);
            ssb.setSpan(greenForegroundColorSpan, ssb.length() - secondWord.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

// Set the TextView text and denote that it is Editable
// since it's a SpannableStringBuilder
        } catch (Exception e) {
            e.fillInStackTrace();
        }
        return ssb;

    }

    private SpannableStringBuilder convertSellerIdrText(String _sellerId) throws NullPointerException {

        String firstWord = "Seller ID :";
        // Use a SpannableStringBuilder so that both the text and the spans are mutable
        SpannableStringBuilder ssb = new SpannableStringBuilder(firstWord);
        try {


//        Typeface myTypeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/poppins_semi_bold.ttf");


            String secondWord = _sellerId;

            Typeface myTypeface = Typeface.create(ResourcesCompat.getFont(mContext, R.font.poppins_semi_bold), Typeface.BOLD);

// Create a span that will make the text red
            ForegroundColorSpan blackForegroundColorSpan = new ForegroundColorSpan(mContext.getResources().getColor(android.R.color.black));


            // Apply the color span
            ssb.setSpan(myTypeface, 0, ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
// Add a blank space
            ssb.append(" ");


// Add the secondWord and apply the strikethrough span to only the second word
            ssb.append(secondWord);
            ssb.setSpan(blackForegroundColorSpan, ssb.length() - secondWord.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ssb.setSpan(myTypeface, ssb.length() - secondWord.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

// Set the TextView text and denote that it is Editable
// since it's a SpannableStringBuilder
        } catch (Exception e) {
            e.fillInStackTrace();
        }
        return ssb;

    }

    private void bindFooterView(RvFooterViewHolder holder) {
        if (mLoading) {
            holder.loadText.setText(R.string.loading);
            holder.loadText.setVisibility(View.VISIBLE);
            holder.loadProgress.setVisibility(View.VISIBLE);
        } else {
            holder.loadText.setText("");
            holder.loadText.setVisibility(View.GONE);
            holder.loadProgress.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {

        int count = 0;
        if (!mList.isEmpty()) {
            count = mList.size() + 1;
        }
        return count;
    }


    @Override
    public int getItemViewType(int position) {

        try {


            if (position == getItemCount() - 1) {
                return TYPE_FOOTER;
            } else {

                return TYPE_ITEM;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            return TYPE_BLANK;
        }
    }

    public void setLoading(boolean loading) {
        mLoading = loading;
        notifyDataSetChanged();
    }


    private PopupWindow createPopupWindow(final Context context, WishItem wishItem, final int position) {

        // initialize a pop up window type
        final PopupWindow popupWindow = new PopupWindow(context);
        popupWindow.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.curved_white_rect));
        popupWindow.setAnimationStyle(R.style.popup_window_animation_phone);
        // the drop down list is a list view
        ListView mPopupListView = new ListView(context);

// Check if we're running on Android 5.0 or higher
        if (Build.VERSION.SDK_INT >= LOLLIPOP) {
            // Call some material design APIs here
            popupWindow.setElevation(10);
        }


        final List<String> items = new ArrayList<>();

        items.add("Remove");


        ArrayAdapter<String> adapter = new ArrayAdapter<>(context, R.layout.item_menu, items);

        // set our adapter and pass our pop up window contents
        mPopupListView.setAdapter(adapter);

        // set the item click listener
        mPopupListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView textView = (TextView) view;
                String text = textView.getText().toString();

                if (text.equalsIgnoreCase("Remove")) {
                    if (listListener != null)
                        listListener.onRemovedWishList(wishItem, position);

                }

                popupWindow.dismiss();
            }
        });

        // some other visual settings
        popupWindow.setFocusable(true);
        popupWindow.setWidth(getPixels(POPUP_WIDTH, context));
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        // set the list view as pop up window content
        popupWindow.setContentView(mPopupListView);


        return popupWindow;
    }


    static class RvWishListHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageView_itemProduct_imageView)
        ImageView ivProduct;


        @BindView(R.id.textView_itemProduct_category)
        TextView tvCategory;

        @BindView(R.id.textView_itemProduct_name)
        TextView tvName;

        @BindView(R.id.textView_itemProduct_code)
        TextView tvCode;

        @BindView(R.id.textView_itemProduct_sellerId)
        TextView tvSellerId;

        @BindView(R.id.textView_itemProduct_amount)
        TextView tvAmount;

        @BindView(R.id.textView_itemProduct_offer)
        TextView tvOffer;

        @BindView(R.id.textView_itemProduct_location)
        TextView tvLocation;

        @BindView(R.id.imageButton_itemProduct_more)
        ImageButton ibMore;


        RvWishListHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public interface onRemoveWishListListener {
        void onWishListClicked(WishItem wishItem, int position);

        void onRemovedWishList(WishItem wishItem, int position);
    }
}
