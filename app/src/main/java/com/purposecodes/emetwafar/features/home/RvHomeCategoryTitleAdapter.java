package com.purposecodes.emetwafar.features.home;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.listener.OnCategoryListener;
import com.purposecodes.emetwafar.model.Category;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RvHomeCategoryTitleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final String TAG = "RvHomeCategoryTitleAdapter";
    // Context
    private final Context mContext;


    // List
    private final ArrayList<Category> mList;

    private final OnCategoryListener listener;

    public RvHomeCategoryTitleAdapter(Context mContext, ArrayList<Category> mList, OnCategoryListener listener) {
        this.mContext = mContext;
        this.mList = mList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_home_cate, parent, false);
        return new RvTitleHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        Category c = mList.get(position);

        RvTitleHolder titleHolder = (RvTitleHolder) holder;
        titleHolder.title.setText(c.getCategoryName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onCategoryClicked(c, position);
            }
        });
    }

    @Override
    public int getItemCount() {

        if (mList != null && mList.size() > 3)
            return 3;
        else
            return mList != null ? mList.size() : 0;

    }


    static class RvTitleHolder extends RecyclerView.ViewHolder {

        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.textView_itemHomeCate)
        TextView title;


        RvTitleHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

}
