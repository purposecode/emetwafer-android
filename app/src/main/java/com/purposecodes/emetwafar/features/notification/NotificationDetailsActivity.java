package com.purposecodes.emetwafar.features.notification;

import static com.purposecodes.emetwafar.utils.LogUtils.printLog;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_MESSAGE;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity;
import com.purposecodes.emetwafar.activity.SplashActivity;
import com.purposecodes.emetwafar.model.MessageItem;

import butterknife.BindView;
import butterknife.OnClick;

public class NotificationDetailsActivity extends BaseActivity {

    private final String TAG = "NotificationDetailsActivity";
    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.imageView_notificationDetails)
    ImageView ivImage;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_notificationDetails_title)
    TextView tvTitle;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_notificationDetails_body)
    TextView tvBody;


    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.progress_notificationDetails)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_notification_details);


        try {


            MessageItem msg = (MessageItem) getIntent().getSerializableExtra(TAG_MESSAGE);

            tvTitle.setText(msg.title != null ? msg.title : "");
            tvBody.setText(msg.body != null ? msg.body : "");


            if (msg.image_url != null && !TextUtils.isEmpty(msg.image_url)) {
                progressBar.setVisibility(View.VISIBLE);
                ivImage.setVisibility(View.VISIBLE);
                Glide.with(this)
                        .load(msg.image_url)
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                progressBar.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                progressBar.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(ivImage);


            }

        } catch (Exception e) {
            printLog(TAG, "Exception : " + e.getMessage());

        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_notification_details;
    }

    @SuppressLint("NonConstantResourceId")
    @OnClick(R.id.imageButton_notificationDetails_close)
    public void closeView() {
        finish();
    }


    public void openApp(View view) {

        Intent loginIntent = new Intent(NotificationDetailsActivity.this, SplashActivity.class);
        loginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(loginIntent);
        finish();
    }
}