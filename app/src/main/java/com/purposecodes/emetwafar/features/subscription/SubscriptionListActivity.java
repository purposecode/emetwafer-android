package com.purposecodes.emetwafar.features.subscription;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.purposecodes.emetwafar.features.payments.PaymentUtils.sendPlanUpdatePaymentRequest;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_EXPIRED_PLAN;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_NEW_PLAN;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_UPDATE_PLAN;
import static com.purposecodes.emetwafar.utils.LogUtils.printLog;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.getAppVersion;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.getTimeStamp;
import static com.purposecodes.emetwafar.utils.UtilsVariable.CALLING_ACTIVITY_KEY;
import static com.purposecodes.emetwafar.utils.UtilsVariable.DEVICE_OS;
import static com.purposecodes.emetwafar.utils.UtilsVariable.DEVICE_VERSION_KEY;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_POSITION;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_SUBSCRIPTIONS;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_TRIAL_MODE;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_ERRORVIEW;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_PROGRESSBAR;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_RECYCLERVIEW;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_SWIPEREFRESH;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.ViewGroup;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.api.ApiFailureMessageListener;
import com.purposecodes.emetwafar.api.ApiSuccessListener;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.model.PlanData;
import com.purposecodes.emetwafar.model.Profile;
import com.purposecodes.emetwafar.model.Subscription;
import com.purposecodes.emetwafar.responsemodel.ErrorModel;
import com.purposecodes.emetwafar.responsemodel.ProfileResponse;
import com.purposecodes.emetwafar.responsemodel.SubscriptionsResponse;
import com.sk.icode.errorview.ErrorView;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

public class SubscriptionListActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener, RvSubscriptionAdapter.OnSubscriptionClickListener {

    private final String TAG = "SubscriptionListActivity";

    private int callingActivity;
    @BindView(R.id.errorView)
    ErrorView errorView;

    @BindView(R.id.recyclerView_layout_list)
    RecyclerView recyclerView;

    @BindView(R.id.swipeRefreshLayout_layout_list)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.list_layout)
    ViewGroup parentView;


    // RecyclerView date set
    private ArrayList<Subscription> subscriptionList;
    private RvSubscriptionAdapter orderAdapter;

    private Profile profile;
    private Subscription selectedPlan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        callingActivity = getIntent().getIntExtra(CALLING_ACTIVITY_KEY, ACTIVITY_NEW_PLAN);
        parentView.setBackgroundColor(Color.WHITE);

        AppController.get().setPlanData(null);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView.setVisibility(GONE);
        setUpRecyclerView();

        getPlans();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_list;
    }


    private void setUpRecyclerView() {

        subscriptionList = new ArrayList<>();

        orderAdapter = new RvSubscriptionAdapter(this, subscriptionList, this, (callingActivity == ACTIVITY_UPDATE_PLAN));

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(orderAdapter);
        recyclerView.setNestedScrollingEnabled(false);

    }


    private void getPlans() {

        if (AppController.get().isNetworkAvailable()) {
            updateViews(VIEW_SWIPEREFRESH);
            AppController.get().getSubscriptionList();
        } else
            setErrorView(getString(R.string.no_internet), "", false);

    }


    //set ErrorView
    private void setErrorView(final String title, final String subTitle, boolean isRetry) {
        updateViews(VIEW_ERRORVIEW);

        errorView.setConfig(ErrorView.Config.create()
                .title(title)
                .subtitle(subTitle)
                .retryVisible(isRetry)
                .build());


        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {
                getPlans();
            }
        });
    }


    private void updateViews(int viewCode) {

        switch (viewCode) {
            case VIEW_PROGRESSBAR:
                showProgress();

                errorView.setVisibility(GONE);
                swipeRefreshLayout.setRefreshing(false);
                break;
            case VIEW_RECYCLERVIEW:
                dismissProgress();
                recyclerView.setVisibility(VISIBLE);
                errorView.setVisibility(GONE);
                swipeRefreshLayout.setRefreshing(false);
                break;

            case VIEW_ERRORVIEW:
                dismissProgress();
                recyclerView.setVisibility(GONE);
                errorView.setVisibility(VISIBLE);
                swipeRefreshLayout.setRefreshing(false);
                break;
            case VIEW_SWIPEREFRESH:
                dismissProgress();
                swipeRefreshLayout.setRefreshing(true);
                break;
        }
    }


    @SuppressLint("NotifyDataSetChanged")
    @Subscribe
    public void onHttpGetSuccess(ApiSuccessListener<SubscriptionsResponse> response) {

        if (response.type.equalsIgnoreCase("getSubscriptionList")) {

            dismissProgress();
            SubscriptionsResponse responseModel = response.response.body();
            if (responseModel != null && responseModel.getStatus()) {
                subscriptionList.clear();

                //1 = trial mode active,else inactive
                if (callingActivity == ACTIVITY_NEW_PLAN && responseModel.getSubscriptionResponseData().getTrialMode()!=null&& responseModel.getSubscriptionResponseData().getTrialMode().equalsIgnoreCase("1")) {
                    Intent intent = new Intent();
                    intent.putExtra(TAG_TRIAL_MODE, responseModel.getSubscriptionResponseData().getTrialMode());
                    intent.putExtra(TAG_SUBSCRIPTIONS, subscriptionList);
                    intent.putExtra(TAG_POSITION, 0);
                    setResult(RESULT_OK, intent);
                    onBackPressed();
                    return;
                }


                List<Subscription> list = responseModel.getSubscriptionResponseData().getSubscriptionList();
                if (list != null && list.size() > 0) {
                    subscriptionList.addAll(list);
                }

                orderAdapter.notifyDataSetChanged();
                updateViews(VIEW_RECYCLERVIEW);

            } else if (subscriptionList.isEmpty() && responseModel.getText() != null && TextUtils.isEmpty(responseModel.getText()))
                setErrorView(responseModel.getText(), "", false);
            else if (subscriptionList.isEmpty())
                setErrorView("No subscription found!", "", false);

        }
    }


    @Subscribe
    public void onHttpGetProfileSuccess(ApiSuccessListener<ProfileResponse> response) {

        if (response.type.equalsIgnoreCase("getProfile")) {
            updateViews(VIEW_RECYCLERVIEW);
            ProfileResponse responseModel = response.response.body();
            if (responseModel != null && responseModel.getStatus()) {

                profile = responseModel.getProfile();

                paymentInitiate();
            } else if (responseModel.getText() != null && TextUtils.isEmpty(responseModel.getText()))
                showToast(responseModel.getText());
            else
                showToast(getResources().getString(R.string.error_try_again));


        }

    }

    @Subscribe
    public void onHttpSuccess(ApiSuccessListener<JsonObject> response) {

        if (response.type.equalsIgnoreCase("initiatePayment")) {


            try {
                JsonObject jsonObject = response.response.body();


                assert jsonObject != null;
                if (jsonObject.get("status").getAsBoolean() && jsonObject.get("data") != null && !jsonObject.get("data").isJsonNull()) {


                    JsonObject dataObj = jsonObject.get("data").getAsJsonObject();


                    if (profile != null && selectedPlan != null) {
                        PlanData p = new PlanData(dataObj.get("subscription_id").getAsString(), dataObj.get("order_ref").getAsString(), profile.getTokenId(), callingActivity);
                        AppController.get().setPlanData(p);
                        setPayment(dataObj.get("order_ref").getAsString());
                    } else {
                        showToast("Try again");
                        updateViews(VIEW_RECYCLERVIEW);
                    }


                } else {
                    updateViews(VIEW_RECYCLERVIEW);
                    showToast(jsonObject.get("text").getAsString());
                }
            } catch (Exception e) {
                showToast("Something went wrong..!");
                updateViews(VIEW_RECYCLERVIEW);
                printLog(TAG, " onHttpSuccess Exception : " + e.getMessage());
            }

        }

    }


    @Subscribe
    public void onHttpError(ApiFailureMessageListener<ErrorModel> response) {

        if (response.type.equalsIgnoreCase("getSubscriptionList")) {

            String msg = "";
            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                msg = errorModel.getText();
            } catch (Exception e) {
                e.printStackTrace();
                msg = getResources().getString(R.string.error_try_again);
            }

            setErrorView(msg, "", false);

        } else if (response.type.equalsIgnoreCase("getProfile") || response.type.equalsIgnoreCase("initiatePayment")) {

            String msg = "";
            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                msg = errorModel.getText();
            } catch (Exception e) {
                e.printStackTrace();
                msg = getResources().getString(R.string.error_try_again);

            }

            updateViews(VIEW_RECYCLERVIEW);
            showToast(msg);

        }
    }


    @Subscribe
    public void onHttpFailure(ApiFailureListener response) {

        if (response.type.equalsIgnoreCase("getSubscriptionList")) {
            setErrorView(getResources().getString(R.string.error_try_again), getString(R.string.error_subtitle_failed_one_more_time), true);
        } else if (response.type.equalsIgnoreCase("getProfile") || response.type.equalsIgnoreCase("initiatePayment")) {
            updateViews(VIEW_RECYCLERVIEW);
            showToast(getResources().getString(R.string.error_try_again));
        }
    }

    @Override
    public void onRefresh() {
        getPlans();
    }

    @Override
    public void onClickTryPlan(Subscription subscription, int position) {

        printLog(TAG, "onClickTryPlan  : " + position);

        selectedPlan = subscription;
        if (callingActivity == ACTIVITY_NEW_PLAN) {
            Intent intent = new Intent();
            intent.putExtra(TAG_TRIAL_MODE, "0");
            intent.putExtra(TAG_SUBSCRIPTIONS, subscriptionList);
            intent.putExtra(TAG_POSITION, position);
            setResult(RESULT_OK, intent);
            onBackPressed();
        } else {
            getMyProfile();
        }

    }

    @Override
    public void onClickBack() {
        onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);

        if (callingActivity == ACTIVITY_EXPIRED_PLAN) {
            Intent setIntent = new Intent(Intent.ACTION_MAIN);
            setIntent.addCategory(Intent.CATEGORY_HOME);
            setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(setIntent);
        } else
            super.onBackPressed();
    }

    private void getMyProfile() {

        if (AppController.get().isNetworkAvailable()) {
            AppController.get().getProfile();
            updateViews(VIEW_PROGRESSBAR);
        } else
            showToast(getResources().getString(R.string.no_internet));
    }


    public void paymentInitiate() {

        if (AppController.get().isNetworkAvailable()) {
            hideSoftKeyboard();
            showProgress();
            Map<String, String> options = new HashMap<>();
            options.put(DEVICE_VERSION_KEY, DEVICE_OS + " " + getAppVersion());
            options.put("plan_id", selectedPlan.getPlanId());
            options.put("order_ref", getTimeStamp());
            options.put("payment_type", "telr");
            AppController.get().initiatePayment(options);

        } else
            showToast(getString(R.string.no_internet));

    }


    private void setPayment(String referenceId) {
        showToast("Please wait...");
        sendPlanUpdatePaymentRequest(this, referenceId, selectedPlan.getGrandTotal(), profile.getUserId(), "purchase plan", profile.getCity(), profile.getState(), profile.getAddress(), profile.getAddress2(), profile.getZipcode(), profile.getCompany(), "", profile.getEmail(), profile.getMobile());
    }
}
