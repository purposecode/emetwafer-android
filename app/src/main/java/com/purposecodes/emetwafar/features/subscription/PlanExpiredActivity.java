package com.purposecodes.emetwafar.features.subscription;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.api.ApiFailureMessageListener;
import com.purposecodes.emetwafar.api.ApiSuccessListener;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.controller.SessionController;
import com.purposecodes.emetwafar.model.UserData;
import com.purposecodes.emetwafar.responsemodel.ErrorModel;
import com.sk.icode.errorview.ErrorView;
import com.squareup.otto.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_EXPIRED;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_EXPIRE_REMAINDER;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_EXPIRE_FILED;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_UPDATE_PLAN;
import static com.purposecodes.emetwafar.utils.UtilsVariable.CALLING_ACTIVITY_KEY;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_REMAINDER_DAY;

public class PlanExpiredActivity extends BaseActivity {

    private final String TAG = "PlanExpiredActivity";
    private int callingActivity;

    @BindView(R.id.view_subscriptionSuccess)
    ViewGroup dataView;


    @BindView(R.id.button_subscriptionSuccess_continue)
    Button btnGoto;

    @BindView(R.id.textView_subscriptionSuccess_title)
    TextView tvTitle;

    @BindView(R.id.button_subscriptionSuccess_skip)
    Button btnSkip;

    @BindView(R.id.imageView_subscriptionSuccess)
    ImageView ivSuccess;

    @BindView(R.id.imageButton_expired_logout)
    ImageButton ibLogout;

    @BindView(R.id.textView_subscriptionSuccess_description)
    TextView tvDescription;


    @BindView(R.id.errorView)
    ErrorView errorView;

    @BindView(R.id.subscription_plan_bg)
    ViewGroup viewBackground;

    private String REMAINDER_DAY = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewBackground.setBackgroundColor(getResources().getColor(R.color.whiteColor));
        btnGoto.setBackground(getResources().getDrawable(R.drawable.primary_curved_bg));
        btnGoto.setTextColor(getResources().getColor(R.color.whiteColor));
        btnGoto.setText("Select your Plan");
        tvTitle.setTextColor(getResources().getColor(R.color.primaryText));
        tvDescription.setTextColor(getResources().getColor(R.color.primaryText));

        try {

            callingActivity = getIntent().getIntExtra(CALLING_ACTIVITY_KEY, ACTIVITY_EXPIRE_REMAINDER);
            REMAINDER_DAY = getIntent().getStringExtra(TAG_REMAINDER_DAY);

        } catch (Exception e) {
            e.printStackTrace();
        }




        tvDescription.setVisibility((callingActivity == ACTIVITY_EXPIRE_REMAINDER) ? View.GONE : View.VISIBLE); //
        btnSkip.setVisibility((callingActivity == ACTIVITY_EXPIRE_REMAINDER) ? View.VISIBLE : View.GONE); //
        ibLogout.setVisibility((callingActivity == ACTIVITY_EXPIRE_REMAINDER) ? View.GONE : View.VISIBLE);//


    /*    tvTitle.setText((callingActivity == ACTIVITY_EXPIRED) ? getBoldText("Plan Expired!!") : getExpiredTitleText(REMAINDER_DAY));
        ivSuccess.setImageDrawable((callingActivity == ACTIVITY_EXPIRED) ? getResources().getDrawable(R.drawable.ic_plan_expired_remainder) : getResources().getDrawable(R.drawable.ic_plan_expired));

//       "Your current plan expired.to continue using E-metwafer pick a plan that works for you"
        tvDescription.setText("Your current plan has been expired. choose a valid plan to continue E-metwafer.");
*/



        switch (callingActivity){
            case ACTIVITY_EXPIRED:
                tvTitle.setText( getBoldText("Plan Expired!!") );
                ivSuccess.setImageDrawable( getResources().getDrawable(R.drawable.ic_plan_expired_remainder));

//       "Your current plan expired.to continue using E-metwafer pick a plan that works for you"
                tvDescription.setText("Your current plan has been expired. choose a valid plan to continue E-metwafer.");


                break;
            case ACTIVITY_EXPIRE_FILED:
                tvTitle.setText( getBoldText("Not Subscribed!!") );
                ivSuccess.setImageDrawable( getResources().getDrawable(R.drawable.ic_plan_expired_remainder));
                tvDescription.setText("You have not selected a subscription plan. Select a plan to continue using E-metwafer.");


                break;
            case ACTIVITY_EXPIRE_REMAINDER:
                tvTitle.setText( getExpiredTitleText(REMAINDER_DAY));
                ivSuccess.setImageDrawable( getResources().getDrawable(R.drawable.ic_plan_expired));

                break;

        }





        if(callingActivity == ACTIVITY_EXPIRED)
            SessionController.setCheckExpiryLastDate(""); // isEligibleUpdateCheck need

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_subscription_success;
    }


    private SpannableStringBuilder getBoldText(String text) throws NullPointerException {
        // Use a SpannableStringBuilder so that both the text and the spans are mutable
        SpannableStringBuilder ssb = new SpannableStringBuilder(text);
        try {
// Create a span that will make the text bold
            ssb.setSpan(new StyleSpan(Typeface.BOLD), 0, ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

// since it's a SpannableStringBuilder
        } catch (Exception e) {
            e.fillInStackTrace();
        }
        return ssb;

    }

    private SpannableStringBuilder getRemainderTitleText(String days) throws NullPointerException {
        // Use a SpannableStringBuilder so that both the text and the spans are mutable
        SpannableStringBuilder ssb = new SpannableStringBuilder();
        String tStr = "";
        try {
            ssb.append("Your plan will be");

            tStr = " expired ";
            ssb.append(tStr);
            ssb.setSpan(new StyleSpan(Typeface.BOLD), ssb.length() - tStr.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ssb.append(" within ");
            tStr = days + " days.";
            ssb.append(tStr);
            ssb.setSpan(new StyleSpan(Typeface.BOLD), ssb.length() - tStr.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ssb.append(" most features will be disabled choose an option to reactivate");

        } catch (Exception e) {
            e.fillInStackTrace();
        }
        return ssb;

    }

    private SpannableStringBuilder getExpiredTitleText(String days) {
        // Use a SpannableStringBuilder so that both the text and the spans are mutable
        SpannableStringBuilder ssb = new SpannableStringBuilder();
        String tStr = "";
        try {
            ssb.append("Your plan will be");

            tStr = " expired ";
            ssb.append(tStr);
            ssb.setSpan(new StyleSpan(Typeface.BOLD), ssb.length() - tStr.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ssb.append(" within ");
            tStr = days + " days.";
            ssb.append(tStr);
            ssb.setSpan(new StyleSpan(Typeface.BOLD), ssb.length() - tStr.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ssb.append(" most features will be disabled choose an option to reactivate");

        } catch (Exception e) {
            e.fillInStackTrace();
        }
        return ssb;

    }


    @OnClick({R.id.button_subscriptionSuccess_continue, R.id.button_subscriptionSuccess_skip, R.id.imageButton_expired_logout})
    public void onButtonClick(View view) {

        switch (view.getId()) {
            case R.id.button_subscriptionSuccess_continue:
                Intent subscriptionIntent = new Intent(this, SubscriptionListActivity.class);
                subscriptionIntent.putExtra(CALLING_ACTIVITY_KEY, ACTIVITY_UPDATE_PLAN);
                startActivity(subscriptionIntent);
                break;
            case R.id.button_subscriptionSuccess_skip:
                finish();
                break;
                case R.id.imageButton_expired_logout:
                    showLogoutAlert();
                break;
        }


    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
        //        animation between activities left to right
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    private void showLogoutAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(PlanExpiredActivity.this);
        builder.setMessage("Are you sure you want to Log Out?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                UserData _user = AppController.get().getUser();

                if (_user != null) {


                    if (AppController.get().isNetworkAvailable()) {
                        showProgress();
                        AppController.get().logout();
                    } else
                        showToast(getString(R.string.no_internet));


                } else
                    AppController.get().logoutUser();

            }
        });
        builder.setNegativeButton("No", (dialog, which) -> dialog.dismiss());
        builder.create().show();
    }





    @Subscribe
    public void onHttpRegTokenSuccess(ApiSuccessListener<JsonObject> response) {

        if (response.type.equalsIgnoreCase("logout")) {
            dismissProgress();
            JsonObject jsonObject = response.response.body();
            assert jsonObject != null;
            if (jsonObject.get("status").getAsBoolean() && jsonObject.get("text").getAsString().equalsIgnoreCase("User logged out successfully!")) {
                AppController.get().logoutUser();
            } else
                showToast(jsonObject.get("text").getAsString());
        }
    }

    @Subscribe
    public void onHttpError(ApiFailureMessageListener<ErrorModel> response) {


        if (response.type.equalsIgnoreCase("logout") ) {
            dismissProgress();
            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                showToast(errorModel.getText());
            } catch (Exception e) {
                e.printStackTrace();
                showToast(getString(R.string.error_try_again));

            }
        }
    }


    @Subscribe
    public void onHttpFailure(ApiFailureListener response) {

        if (response.type.equalsIgnoreCase("logout") ) {

            dismissProgress();
            showToast(getResources().getString(R.string.error_try_again));
        }
    }
}
