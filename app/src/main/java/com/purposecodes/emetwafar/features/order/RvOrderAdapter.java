package com.purposecodes.emetwafar.features.order;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.holder.RvBlankViewHolder;
import com.purposecodes.emetwafar.holder.RvFooterViewHolder;
import com.purposecodes.emetwafar.model.CartItem;
import com.purposecodes.emetwafar.model.Order;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.purposecodes.emetwafar.utils.UtilsFunctions.convertPriceText;
import static com.purposecodes.emetwafar.utils.UtilsVariable.CANCELLED;
import static com.purposecodes.emetwafar.utils.UtilsVariable.CONFIRMED;
import static com.purposecodes.emetwafar.utils.UtilsVariable.DELIVERED;
import static com.purposecodes.emetwafar.utils.UtilsVariable.NOT_PLACED;
import static com.purposecodes.emetwafar.utils.UtilsVariable.RETURNED;

public class RvOrderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final String TAG = "RvOrderAdapter";
    // Context
    private final Context mContext;

    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;
    private static final int TYPE_BLANK = 3;

    // Checking the its loading for new items
    private boolean mLoading = false;
    // List
    private final ArrayList<Order> mList;

    private final OnOrderClickListener listener;

    private boolean IS_BOTTOM_VIEW = false;

    public RvOrderAdapter(Context mContext, ArrayList<Order> mList, OnOrderClickListener mListener, boolean isView) {
        this.mContext = mContext;
        this.mList = mList;
        this.listener = mListener;
        this.IS_BOTTOM_VIEW = isView;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(mContext).inflate(R.layout.item_product, parent, false);


        View view;
        switch (viewType) {

            case TYPE_FOOTER:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_loading, parent, false);
                return new RvFooterViewHolder(view);

            case TYPE_ITEM:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_order, parent, false);
                return new RvMyOrderHolder(view);

            default:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_blank, parent, false);
                return new RvBlankViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        int viewType = getItemViewType(position);
        switch (viewType) {

            case TYPE_FOOTER:
                bindFooterView((RvFooterViewHolder) viewHolder);
                break;
            case TYPE_ITEM:
                bindItemView((RvMyOrderHolder) viewHolder, position);
                break;
        }


    }


    private void bindItemView(RvMyOrderHolder holder, final int position) {
        try {

            holder.bottomView.setVisibility(IS_BOTTOM_VIEW ? View.VISIBLE : View.GONE);

            Order order = mList.get(position);

            holder.tvOrderNo.setText("Order #" + order.getOrderNo());
            holder.tvOrderDate.setText(order.getOrderDate());
            holder.tvGrandTotal.setText(convertPriceText(order.getOrderTotal()));
            holder.tvStatus.setText(order.getOrderStatus());


            if (order.getOrderStatus().equalsIgnoreCase(DELIVERED) || order.getOrderStatus().equalsIgnoreCase(CONFIRMED))  //CONFIRMED  RETURNED    DELIVERED  CANCELLED   NOT PLACED
                holder.tvStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_green_tick, 0, 0, 0);

            else
                holder.tvStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_gray_tick, 0, 0, 0);

            holder.btnCancel.setVisibility((order.getOrderStatus().equalsIgnoreCase(CANCELLED) || order.getOrderStatus().equalsIgnoreCase(NOT_PLACED) || order.getOrderStatus().equalsIgnoreCase(RETURNED) || order.getOrderStatus().equalsIgnoreCase(DELIVERED)) ? View.GONE : View.VISIBLE);


            holder.btnViewAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        listener.onClickViewAll(order, position);
                }
            });

            holder.btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        listener.onClickOrderCancel(order, position);
                }
            });

            List<CartItem> li = new ArrayList<>();

            int i = 0;
            while (i < 2 && order.getCartItems().size() > i) {
                li.add(order.getCartItems().get(i));
                ++i;
            }
            RvOrderItemAdapter itemAdapter = new RvOrderItemAdapter(li);

            LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
            holder.recyclerView.setHasFixedSize(true);
            holder.recyclerView.setLayoutManager(layoutManager);
            holder.recyclerView.setAdapter(itemAdapter);

          /*  Order p = mList.get(position);

            Glide.with(mContext)
                    .load(p.getImageUrl())
                    .apply(new RequestOptions()
                            .centerCrop()
                            .placeholder(R.drawable.ic_upload_placeholder)
                            .error(R.drawable.ic_upload_placeholder))
                    .into(holder.ivProduct);

            holder.tvName.setText(p.getProductName());


            holder.tvCode.setText("Code :" + p.getProductCode());
            holder.tvAmount.setText(convertPriceText(p.getProductPrice()));
            holder.tvOffer.setText(convertOfferText(p.getProductOffer()));

            holder.tvSellerId.setText(convertSellerIdrText(p.getSupplier()));



            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener!=null)
                        listener.onProductClicked(p, position);
                }
            });
*/
        } catch (Exception e) {
            e.fillInStackTrace();
        }
    }


    private void bindFooterView(RvFooterViewHolder holder) {
        if (mLoading) {
            holder.loadText.setText(R.string.loading);
            holder.loadText.setVisibility(View.VISIBLE);
            holder.loadProgress.setVisibility(View.VISIBLE);
        } else {
            holder.loadText.setText("");
            holder.loadText.setVisibility(View.GONE);
            holder.loadProgress.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() + 1 : 0;
    }

    @Override
    public int getItemViewType(int position) {

        try {
            if (position == getItemCount() - 1) {
                return TYPE_FOOTER;
            } else {

                return TYPE_ITEM;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            return TYPE_BLANK;
        }
    }

    public void setLoading(boolean loading) {
        mLoading = loading;
        notifyDataSetChanged();
    }


    static class RvMyOrderHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.textView_itemMyOrder_orderNo)
        TextView tvOrderNo;

        @BindView(R.id.textView_itemMyOrder_orderDate)
        TextView tvOrderDate;

        @BindView(R.id.textView_itemMyOrder_grandTotal)
        TextView tvGrandTotal;

        @BindView(R.id.textView_itemMyOrder_status)
        TextView tvStatus;

        @BindView(R.id.button_itemMyOrder_viewAll)
        MaterialButton btnViewAll;

        @BindView(R.id.button_itemMyOrder_cancel)
        Button btnCancel;

        @BindView(R.id.recyclerView_itemMyOrder)
        RecyclerView recyclerView;

        @BindView(R.id.view_itemMyOrder_bottom)
        ViewGroup bottomView;

        RvMyOrderHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    public interface OnOrderClickListener {
        void onClickOrderCancel(Order order, int position);

        void onClickViewAll(Order order, int position);
    }


    public class RvOrderItemAdapter extends RecyclerView.Adapter<RvOrderItemAdapter.RvOrderItemHolder> {

        List<CartItem> products;

        public RvOrderItemAdapter(List<CartItem> products) {
            this.products = products;
        }

        @NonNull
        @Override
        public RvOrderItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_order_product, parent, false);
            return new RvOrderItemHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull RvOrderItemHolder holder, int position) {

            CartItem p = products.get(position);
            holder.tvName.setText(p.getProductName());
            holder.tvQuantity.setText(p.getQuantity() + " QTY");
            holder.tvPrice.setText(convertPriceText(p.getProductPrice()));

        }

        @Override
        public int getItemCount() {
            return products != null ? products.size() : 0;
        }

        public class RvOrderItemHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.textView_itemMyOrderProduct_name)
            TextView tvName;
            @BindView(R.id.textView_itemMyOrderProduct_quantity)
            TextView tvQuantity;
            @BindView(R.id.textView_itemMyOrderProduct_price)
            TextView tvPrice;

            public RvOrderItemHolder(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }

}
