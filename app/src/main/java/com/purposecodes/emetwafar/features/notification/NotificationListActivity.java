package com.purposecodes.emetwafar.features.notification;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.api.ApiFailureMessageListener;
import com.purposecodes.emetwafar.api.ApiSuccessListener;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.features.home.HomeActivity;
import com.purposecodes.emetwafar.features.order.OrderDetailsActivity;
import com.purposecodes.emetwafar.features.productview.ProductDetailActivity;
import com.purposecodes.emetwafar.features.subscription.PlanExpiredActivity;
import com.purposecodes.emetwafar.model.NotificationItem;
import com.purposecodes.emetwafar.responsemodel.ErrorModel;
import com.purposecodes.emetwafar.responsemodel.NotificationResponse;
import com.purposecodes.emetwafar.view.EndlessScrollListener;
import com.sk.icode.errorview.ErrorView;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.purposecodes.emetwafar.controller.SessionController.getNotificationCount;
import static com.purposecodes.emetwafar.controller.SessionController.setNotificationCount;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_EXPIRED;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_EXPIRE_REMAINDER;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_HOME_MYPROFILE;
import static com.purposecodes.emetwafar.utils.LogUtils.printLog;
import static com.purposecodes.emetwafar.utils.UtilsVariable.CALLING_ACTIVITY_KEY;
import static com.purposecodes.emetwafar.utils.UtilsVariable.NO_DATA;
import static com.purposecodes.emetwafar.utils.UtilsVariable.NO_NETWORK;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_ORDER_ID;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_POSITION;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_PRODUCT_ID;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_REMAINDER_DAY;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TYPE_ORDER;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TYPE_PRODUCT;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TYPE_SUBSCRIPTION;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TYPE_SUBSCRIPTION_EXPIRY;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TYPE_SUBSCRIPTION_REMAINDER;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_ERRORVIEW;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_RECYCLERVIEW;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_SWIPEREFRESH;
import static com.purposecodes.emetwafar.utils.UtilsVariable.WRONG_RESPONSE;

public class NotificationListActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener, RvNotificationAdapter.OnNotificationListener {
    private final String TAG = "NotificationListActivity";

    @BindView(R.id.errorView)
    ErrorView errorView;


    @BindView(R.id.recyclerView_layout_list)
    RecyclerView recyclerView;


    @BindView(R.id.swipeRefreshLayout_layout_list)
    SwipeRefreshLayout swipeRefreshLayout;


    // RecyclerView date set
    private ArrayList<NotificationItem> notificationList;

    private RvNotificationAdapter notificationAdapter;

    // Scroll Listener for pagination
    private EndlessScrollListener mScrollListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        swipeRefreshLayout.setOnRefreshListener(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        notificationList = new ArrayList<>();

        setUpRecyclerView();
        loadData();
        setTitleName();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_list_activity;
    }


    private void setUpRecyclerView() {

        notificationAdapter = new RvNotificationAdapter(this, notificationList, this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(notificationAdapter);
        recyclerView.setNestedScrollingEnabled(false);

        mScrollListener = new EndlessScrollListener(layoutManager, 7) {
            @Override
            public void onLoadMore(int currentPage) {
//                if (mCurrentPage < mTotalPages) {
//                    showRecyclerViewLoading(true);
//                    getMyOrders(mCurrentPage + 1);
//                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        };

//        recyclerView.addOnScrollListener(mScrollListener);

    }

    private void loadData() {


        notificationAdapter.notifyDataSetChanged();

        if (AppController.get().isNetworkAvailable()) {
            getNotifications();
        } else if (notificationList.isEmpty()) {
            setErrorView(getString(R.string.no_internet), "", false, NO_NETWORK);
        } else {
            swipeRefreshLayout.setRefreshing(false);
            showToast(getResources().getString(R.string.no_internet));
        }

    }


    private void showRecyclerViewLoading(final boolean showLoading) {
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                notificationAdapter.setLoading(showLoading);
            }
        });
    }


    private void getNotifications() {

        if (AppController.get().isNetworkAvailable()) {

            updateViews(VIEW_SWIPEREFRESH);
            AppController.get().getNotifications();
        } else
            showToast(getResources().getString(R.string.no_internet));
    }


    //set ErrorView
    private void setErrorView(final String title, final String subTitle, boolean isRetry, int drawable) {

        updateViews(VIEW_ERRORVIEW);
        errorView.setConfig(ErrorView.Config.create()
                .title(title)
                .subtitle(subTitle)
                .retryVisible(isRetry)
                .image(drawable)
                .build());

        updateViews(VIEW_ERRORVIEW);

        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {
                loadData();
            }
        });
    }


    private void updateViews(int viewCode) {

        switch (viewCode) {

            case VIEW_RECYCLERVIEW:
                dismissProgress();
                recyclerView.setVisibility(VISIBLE);
                errorView.setVisibility(GONE);
                swipeRefreshLayout.setRefreshing(false);

                break;

            case VIEW_ERRORVIEW:
                dismissProgress();
                recyclerView.setVisibility(GONE);
                errorView.setVisibility(VISIBLE);
                swipeRefreshLayout.setRefreshing(false);
                break;

            case VIEW_SWIPEREFRESH:
                dismissProgress();
                swipeRefreshLayout.setRefreshing(true);
                break;
        }
    }


    @Subscribe
    public void onHttpGetNotificationSuccess(ApiSuccessListener<NotificationResponse> response) {

        if (response.type.equalsIgnoreCase("getNotifications")) {

            dismissProgress();
            NotificationResponse responseModel = response.response.body();
            if (responseModel != null && responseModel.getStatus()) {

                printLog(TAG, "onHttpGetNotificationSuccess  : " + new Gson().toJson(responseModel));

                notificationList.clear();

                setNotificationCount(responseModel.getNotificationData().getCount());

                setTitleName();
                List<NotificationItem> list = responseModel.getNotificationData().getNotificationList();
                if (list != null && list.size() > 0) {
                    notificationList.addAll(list);
                }



                notificationAdapter.notifyDataSetChanged();

                updateViews(VIEW_RECYCLERVIEW);


            } else if (notificationList.isEmpty() && responseModel.getText() != null && TextUtils.isEmpty(responseModel.getText()))

                setErrorView(responseModel.getText(), "", false, NO_DATA);
            else if (notificationList.isEmpty())
                setErrorView("No Notification found!", "", false, NO_DATA);

            showRecyclerViewLoading(false);


        }
    }


    @Subscribe
    public void onHttpError(ApiFailureMessageListener<ErrorModel> response) {

        if (response.type.equalsIgnoreCase("getNotifications")) {

            showRecyclerViewLoading(false);
            mScrollListener.setLoading(false);

            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                setErrorView(errorModel.getText(), "", false, WRONG_RESPONSE);

            } catch (Exception e) {
                e.printStackTrace();
                setErrorView(getResources().getString(R.string.error_try_again), getString(R.string.error_subtitle_failed_one_more_time), true, WRONG_RESPONSE);

            }
        }
    }


    @Subscribe
    public void onHttpFailure(ApiFailureListener response) {

        if (response.type.equalsIgnoreCase("getNotifications")) {
            setErrorView(getResources().getString(R.string.error_try_again), getString(R.string.error_subtitle_failed_one_more_time), true, WRONG_RESPONSE);
            showRecyclerViewLoading(false);
            mScrollListener.setLoading(false);

        }
    }


    @Override
    public void onRefresh() {
        loadData();
    }


    @Override
    public void onNotificationClicked(NotificationItem notification, int position) {


        if (AppController.get().isNetworkAvailable() && notification.getReadStatus().equalsIgnoreCase("0")) {
            notification.setReadStatus("1");
            notificationList.set(position, notification);
            notificationAdapter.notifyDataSetChanged();

            HashMap<String, String> options = new HashMap<>();
            options.put("notification_id", notification.getNotificationId());
            AppController.get().updateNotifications(options);

//notification count decrease
            if (getNotificationCount() != 0) {
                setNotificationCount(getNotificationCount() - 1);
                setTitleName();
            }
        }


        switch (notification.getNotificationType()) {
            case TYPE_ORDER:
                Intent intent = new Intent(NotificationListActivity.this, OrderDetailsActivity.class);
                intent.putExtra(TAG_ORDER_ID, notification.getId());
                intent.putExtra(TAG_POSITION, position);
                startActivity(intent);
                break;
            case TYPE_PRODUCT:
                intent = new Intent(NotificationListActivity.this, ProductDetailActivity.class);
                intent.putExtra(TAG_PRODUCT_ID, notification.getId());
                intent.putExtra(TAG_POSITION, position);
                startActivity(intent);
                break;
            case TYPE_SUBSCRIPTION_REMAINDER:
                intent = new Intent(this, PlanExpiredActivity.class);
                intent.putExtra(CALLING_ACTIVITY_KEY, ACTIVITY_EXPIRE_REMAINDER);
                intent.putExtra(TAG_REMAINDER_DAY, "0");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

                break;
            case TYPE_SUBSCRIPTION_EXPIRY:  //non working
                intent = new Intent(this, PlanExpiredActivity.class);
                intent.putExtra(CALLING_ACTIVITY_KEY, ACTIVITY_EXPIRED);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case TYPE_SUBSCRIPTION:
                intent = new Intent(this, HomeActivity.class);
                intent.putExtra(CALLING_ACTIVITY_KEY, ACTIVITY_HOME_MYPROFILE);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                break;


        }

    }

    private void setTitleName() {

        if (getNotificationCount() != 0)
            setTitle("Notifications (" + getNotificationCount() + ")");
        else
            setTitle("Notification");

    }
}
