package com.purposecodes.emetwafar.features.myaccount;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.purposecodes.emetwafar.controller.SessionController.updateUser;
import static com.purposecodes.emetwafar.utils.FileUtils.IMAGE;
import static com.purposecodes.emetwafar.utils.FileUtils.fileDirectoryGenerator;
import static com.purposecodes.emetwafar.utils.FileUtils.fileNameGenerator;
import static com.purposecodes.emetwafar.utils.FileUtils.getFileMimeType;
import static com.purposecodes.emetwafar.utils.FileUtils.getFileSizeMegaBytes;
import static com.purposecodes.emetwafar.utils.FileUtils.getPathFromURI;
import static com.purposecodes.emetwafar.utils.LogUtils.printLog;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.isValidMobile;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.isValidWeb;
import static com.purposecodes.emetwafar.utils.UtilsVariable.NO_DATA;
import static com.purposecodes.emetwafar.utils.UtilsVariable.OTHER_TAG;
import static com.purposecodes.emetwafar.utils.UtilsVariable.PLACE_HOLDER_TAG;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_DATA;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_ERRORVIEW;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_PROGRESSBAR;
import static com.purposecodes.emetwafar.utils.UtilsVariable.WRONG_RESPONSE;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity;
import com.purposecodes.emetwafar.adapter.AddressSpinnerArrayAdapter;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.api.ApiFailureMessageListener;
import com.purposecodes.emetwafar.api.ApiSuccessListener;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.features.home.HomeActivity;
import com.purposecodes.emetwafar.model.City;
import com.purposecodes.emetwafar.model.FileModal;
import com.purposecodes.emetwafar.model.Profile;
import com.purposecodes.emetwafar.model.State;
import com.purposecodes.emetwafar.model.UserData;
import com.purposecodes.emetwafar.responsemodel.ErrorModel;
import com.purposecodes.emetwafar.responsemodel.ProfileResponse;
import com.purposecodes.emetwafar.responsemodel.StateResponse;
import com.purposecodes.emetwafar.utils.ImageCompressor;
import com.purposecodes.emetwafar.utils.PermissionUtils;
import com.purposecodes.emetwafar.utils.UtilsVariable;
import com.sk.icode.errorview.ErrorView;
import com.squareup.otto.Subscribe;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class CompanyProfileActivity extends BaseActivity {

    private final String TAG = "CompanyProfileActivity";
    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.imageView_companyProfile_profile)
    CircleImageView ivProfile;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.imageView_companyProfile_verify)
    ImageView ivVerify;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_companyProfile_name)
    TextView tvName;


    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_companyProfile_email)
    TextView tvEmail;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_companyProfile_companyRegister)
    TextView tvCompanyRegister;


    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.editText_companyProfile_name)
    TextView etName;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_companyProfile_companyAddress)
    TextView tvCompanyAddress;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.editText_companyProfile_address1)
    EditText etAddress1;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.editText_companyProfile_address2)
    EditText etAddress2;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.editText_companyProfile_newCity)
    EditText etNewCity;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.editText_companyProfile_poBox)
    EditText etPoBox;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.editText_companyProfile_companyPhone)
    EditText etPhone;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.editText_companyProfile_mobile)
    EditText etMobile;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.editText_companyProfile_web)
    EditText etWeb;


    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.spinner_companyProfile_selectState)
    AppCompatSpinner spinnerState;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.spinner_companyProfile_selectCity)
    AppCompatSpinner spinnerCity;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.imageButton_companyProfile_changeDP)
    ImageButton ibChangeDp;


    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.button_companyProfile_editProfile)
    MaterialButton btnEditProfile;


    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.button_companyProfile_save)
    Button btnSave;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.view_companyProfile_otherCity)
    ViewGroup viewAddNewCity;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.view_companyProfile_viewableAddress)
    ViewGroup viewableAddressView;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.view_companyProfile_editableAddress)
    ViewGroup editableAddressView;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.view_bottom)
    ViewGroup viewBottom_;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.view_companyProfile)
    ViewGroup dataView;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.errorView)
    ErrorView errorView;


    public static final int PICKIMAGE_RESULT_CODE = 2;


    private Uri mCapturedImageUri;
    private String mCapturedImagePath;
    private String newProfileImagePath;
    private static final int PERMISSION_ACCESS_GALLERY = 102;
    private static final int PERMISSION_ACCESS_CAMERA = 103;


    private static final int REQUEST_CAMERA = 1;
    private Profile profile;

    // Checking the layout is in edit mode
    private boolean isEditMode = false;
    private static final int EDITABLE_PADDING = 25;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        getMyProfile();


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_company_profile;
    }


    private void getMyProfile() {

        if (AppController.get().isNetworkAvailable()) {
            AppController.get().getProfile();
            updateViews(VIEW_PROGRESSBAR);
        } else
            showToast(getResources().getString(R.string.no_internet));
    }

    private void setProfileView() {

        if (profile == null) {
            getMyProfile();
            return;
        }


        try {

            updateViews(VIEW_DATA);
            viewEditable(false);


            Glide.with(this)
                    .load(profile.getProfileimg())
                    .apply(new RequestOptions()
                            .centerCrop()
                            .placeholder(R.drawable.placeholder_user)
                            .error(R.drawable.placeholder_user))
                    .into(ivProfile);

            tvName.setText(profile.getCompany());
            etName.setText(profile.getCompany());
            ivVerify.setVisibility(profile.getVerifyStatus().equals("1") ? VISIBLE : GONE);

            StringBuilder builder = new StringBuilder();


            if (profile.getAddress() != null && !TextUtils.isEmpty(profile.getAddress()))
                builder.append(profile.getAddress());

            if (profile.getAddress2() != null && !TextUtils.isEmpty(profile.getAddress2()))
                builder.append(", ").append(profile.getAddress2());

            if (profile.getCity() != null && !TextUtils.isEmpty(profile.getCity()))
                builder.append(", ").append(profile.getCity());


            if (profile.getState() != null && !TextUtils.isEmpty(profile.getState()))
                builder.append(", ").append(profile.getState());

            if (profile.getCountry() != null && !TextUtils.isEmpty(profile.getCountry()))
                builder.append(", ").append(profile.getCountry());

            tvCompanyAddress.setText(builder);

            etAddress1.setText(profile.getAddress());
            etAddress2.setText(profile.getAddress2());
            etPoBox.setText(profile.getZipcode());
            etPhone.setText(profile.getPhone());
            etMobile.setText(profile.getMobile());
            etWeb.setText(profile.getWebsite());
            tvEmail.setText(profile.getEmail());
            tvCompanyRegister.setText(profile.getRegNo());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void getStateList() {

        if (AppController.get().isNetworkAvailable()) {
            showProgress();
            AppController.get().getStateAndCities();
        } else
            showToast(getResources().getString(R.string.no_internet));

    }


    //set ErrorView
    private void setErrorView(final String title, final String subTitle, boolean isRetry, int drawable) {

        updateViews(VIEW_ERRORVIEW);

        errorView.setConfig(ErrorView.Config.create()
                .title(title)
                .subtitle(subTitle)
                .retryVisible(isRetry)
                .image(drawable)
                .build());

        errorView.setOnRetryListener(this::getMyProfile);
    }

    private void updateViews(int viewCode) {
        switch (viewCode) {
            case VIEW_PROGRESSBAR:
                showProgress();
                errorView.setVisibility(View.GONE);
                break;
            case VIEW_DATA:
                dismissProgress();
                dataView.setVisibility(View.VISIBLE);
                errorView.setVisibility(View.GONE);

                break;

            case VIEW_ERRORVIEW:
                dismissProgress();
                dataView.setVisibility(View.GONE);
                errorView.setVisibility(View.VISIBLE);
                break;

        }
    }

    @SuppressLint("NonConstantResourceId")
    @OnClick({R.id.button_companyProfile_editProfile, R.id.button_companyProfile_save})
    public void onButtonClick(View view) {
        switch (view.getId()) {
            case R.id.button_companyProfile_editProfile:

                viewEditable(true);

                break;
            case R.id.button_companyProfile_save:

                if (isValidate() && newProfileImagePath != null && !TextUtils.isEmpty(newProfileImagePath))
                    uploadSelectedImage(newProfileImagePath);
                else if (isValidate())
                    profileUpdateRequest("");


                break;
        }


    }

    private void viewEditable(boolean _isEditable) {
        isEditMode = _isEditable;
        etName.setEnabled(_isEditable);
        etPoBox.setEnabled(_isEditable);
        etPhone.setEnabled(_isEditable);
        etMobile.setEnabled(_isEditable);
        etWeb.setEnabled(_isEditable);
        btnEditProfile.setEnabled(!_isEditable);
        ibChangeDp.setEnabled(_isEditable);

        tvEmail.setPadding(_isEditable ? EDITABLE_PADDING : 0, EDITABLE_PADDING, EDITABLE_PADDING, EDITABLE_PADDING);
        tvCompanyRegister.setPadding(_isEditable ? EDITABLE_PADDING : 0, EDITABLE_PADDING, EDITABLE_PADDING, EDITABLE_PADDING);
        etPoBox.setPadding(_isEditable ? EDITABLE_PADDING : 0, EDITABLE_PADDING, EDITABLE_PADDING, EDITABLE_PADDING);
        etPhone.setPadding(_isEditable ? EDITABLE_PADDING : 0, EDITABLE_PADDING, EDITABLE_PADDING, EDITABLE_PADDING);
        etMobile.setPadding(_isEditable ? EDITABLE_PADDING : 0, EDITABLE_PADDING, EDITABLE_PADDING, EDITABLE_PADDING);
        etWeb.setPadding(_isEditable ? EDITABLE_PADDING : 0, EDITABLE_PADDING, EDITABLE_PADDING, EDITABLE_PADDING);


        if (_isEditable) {

            getStateList();

            btnSave.setVisibility(VISIBLE);
//            viewBottom.setVisibility(VISIBLE);
            editableAddressView.setVisibility(VISIBLE);
            ibChangeDp.setVisibility(VISIBLE);
            viewableAddressView.setVisibility(GONE);
            btnEditProfile.setVisibility(GONE);


        } else {
            btnSave.setVisibility(GONE);
//            viewBottom.setVisibility(GONE);
            ibChangeDp.setVisibility(GONE);
            editableAddressView.setVisibility(GONE);
            viewableAddressView.setVisibility(VISIBLE);
            btnEditProfile.setVisibility(VISIBLE);

        }


    }


    @OnClick(R.id.imageButton_companyProfile_changeDP)
    public void showBottomDialog() {

        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_dialog_doc_chooser, null);
        Button btnCamera = view.findViewById(R.id.button_bottomSheet_camera);
        Button btnGallery = view.findViewById(R.id.button_bottomSheet_gallery);
        Button btnDoc = view.findViewById(R.id.button_bottomSheet_doc);
        btnDoc.setVisibility(GONE);

        BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(view);
        dialog.show();


        btnCamera.setOnClickListener(v -> {
            checkCameraPermission();
            dialog.dismiss();
        });
        btnGallery.setOnClickListener(v -> {
            checkGalleryPermission();
            dialog.dismiss();
        });

    }

    private void checkCameraPermission() {

        if (PermissionUtils.hasPermission(this,
                new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})) {
            cameraIntent();
        } else {
            PermissionUtils.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_ACCESS_CAMERA);
        }
    }

    private void checkGalleryPermission() {
        if (PermissionUtils.hasPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            galleryIntent();
        } else {
            PermissionUtils.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_ACCESS_GALLERY);
        }
    }

    private void cameraIntent() {

        try {

            String newPath = fileDirectoryGenerator(IMAGE);

            File f = new File(newPath);
            if (!f.exists()) {
                f.mkdirs();
            }

            f = new File(f, fileNameGenerator(IMAGE));

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mCapturedImageUri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", f);
//                mCapturedImageUri = FileProvider.getUriForFile(this,  "com.purposecodes.emetwafar.provider", f);

                PermissionUtils.grandUriPermission(this, intent, mCapturedImageUri);
            } else {
                mCapturedImageUri = Uri.fromFile(f);
            }

            mCapturedImagePath = f.getAbsolutePath();

//            printLog(TAG, "cameraIntent :  2" + mCapturedImageUri + " ,path : " + mCapturedImagePath);

            intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageUri);
            startActivityForResult(intent, REQUEST_CAMERA);

        } catch (Exception e) {
            printLog(TAG, "cameraIntent Exception :  " + e.getMessage());
            showToast("Camera Some went wrong ");
        }

    }


    private void galleryIntent() {

        Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, PICKIMAGE_RESULT_CODE);//one can be replaced with any action code
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        int permissionCheck = PackageManager.PERMISSION_GRANTED;
        switch (requestCode) {
            case PERMISSION_ACCESS_GALLERY:
                if (grantResults.length > 0 && grantResults[0] ==
                        PackageManager.PERMISSION_GRANTED) {

                    galleryIntent();
                }
                break;

            case PERMISSION_ACCESS_CAMERA:
                for (int permission : grantResults) {
                    permissionCheck = permissionCheck + permission;
                }

                if (grantResults.length > 0 && permissionCheck == PackageManager.PERMISSION_GRANTED) {
                    cameraIntent();
                }

                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {

                case REQUEST_CAMERA:
                    String filePath = mCapturedImageUri.getPath();

                    filePath = mCapturedImagePath;

                    File file = new File(filePath);
                    if (file.exists()) {
                        printLog(TAG, "is exist : " + file.getAbsolutePath());
                    } else {
                        printLog(TAG, "File doesn't exist");
                    }

                    showSelectedImage(filePath, true);
                    mCapturedImageUri = null;
                    mCapturedImagePath = null;


                    break;
                case PICKIMAGE_RESULT_CODE:

                    Uri selectedImage = data.getData();

                    printLog(TAG, "path  4 :  getPathFromURI  :  " + getPathFromURI(this, selectedImage));
                    showSelectedImage(getPathFromURI(this, selectedImage), false);
                    break;
            }
        }
    }


    private void showSelectedImage(String filePath, boolean isTakenFromCam) {

        try {


            File f = new File(filePath);

            double s = getFileSizeMegaBytes(f);

            if (s < 5)
                newProfileImagePath = ImageCompressor.compressImage(filePath, isTakenFromCam);
            else
                newProfileImagePath = ImageCompressor.compressImageHigh(filePath, isTakenFromCam);


        } catch (Exception e) {
            newProfileImagePath = filePath;
            printLog(TAG, "showSelectedImage Exception : " + e.getMessage());
        }

        File f = new File(newProfileImagePath);
        printLog(TAG, "showSelectedImage path : " + f.exists());

        FileModal fileModal = new FileModal();
        fileModal.setPath(newProfileImagePath);
        fileModal.setType(UtilsVariable.FileType.IMAGE);


        Glide.with(this)
                .load(newProfileImagePath)
                .apply(new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.placeholder_user)
                        .error(R.drawable.placeholder_user))
                .into(ivProfile);


    }


    private void uploadSelectedImage(String dpPath) {

        File file = new File(dpPath);
        if (!file.exists()) {
            showToast("Some went wrong ");
            return;
        }

        if (!AppController.get().isNetworkAvailable()) {
            showToast(getString(R.string.no_internet));
            return;
        }

        updateViews(VIEW_PROGRESSBAR);
        btnEditProfile.setEnabled(false);
        String mimeType = getFileMimeType(file);

        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create(MediaType.parse(mimeType), file);

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part partBody = MultipartBody.Part.createFormData("media_file", dpPath, requestFile);


        Map<String, RequestBody> options = new HashMap<>();
        options.put("type", getRequestBody("profile"));

        AppController.get().profileDpUpload(options, partBody);

    }

    private RequestBody getRequestBody(String value) {
        return RequestBody.create(MediaType.parse("multipart/form-data"), value);
    }


    @Subscribe
    public void onHttpGetProfileSuccess(ApiSuccessListener<ProfileResponse> response) {

        if (response.type.equalsIgnoreCase("getProfile")) {
            dismissProgress();
            ProfileResponse responseModel = response.response.body();
            if (responseModel != null && responseModel.getStatus()) {

                profile = responseModel.getProfile();
                setProfileView();

            } else if (responseModel.getText() != null && TextUtils.isEmpty(responseModel.getText()))
                showToast(responseModel.getText());
            else
                showToast(getResources().getString(R.string.error_try_again));

        }
    }


    //image uploaded respond
    @Subscribe
    public void onHttpSuccess(ApiSuccessListener<JsonObject> response) {
        try {


            if (response.type.equals("profileDpUpload")) {

                JsonObject jsonObject = response.response.body();


                if (jsonObject != null) {
                    if (jsonObject.get("status").getAsBoolean()) {
                        JsonObject dataObj = jsonObject.getAsJsonObject("data");

                        String tk = dataObj.get("media_file").getAsString();
                        profileUpdateRequest(tk);


                    } else {
                        showToast(jsonObject.get("text").getAsString());
                        updateViews(VIEW_DATA);
                    }
                } else {
                    updateViews(VIEW_DATA);
                    showToast(getResources().getString(R.string.error_try_again));
                }
            }

        } catch (Exception e) {
            printLog(TAG, " onHttpSuccess Exception :  " + e.getMessage());
        }
    }

    @Subscribe
    public void onHttpUpdateProfileSuccess(ApiSuccessListener<ProfileResponse> response) {

        if (response.type.equalsIgnoreCase("updateProfile")) {
            updateViews(VIEW_DATA);
            ProfileResponse responseModel = response.response.body();
            if (responseModel != null && responseModel.getStatus()) {

//                printLog(TAG, "onHttpUpdateProfileSuccess  responseModel :    " + new Gson().toJson(responseModel));

                profile = responseModel.getProfile();

                UserData userData = AppController.get().getUser();
                userData.setCompany(profile.getCompany());
                userData.setVerifyStatus(profile.getVerifyStatus());
                userData.setEmail(profile.getEmail());
                userData.setProfileurl(profile.getProfileimg());

                updateUser(userData);
                HomeActivity.getInstance().setProfile(); //checkout page
                setProfileView();
                showToast(responseModel.getText());
            } else if (responseModel.getText() != null && TextUtils.isEmpty(responseModel.getText()))
                showToast(responseModel.getText());
            else
                showToast(getResources().getString(R.string.error_try_again));

        }
    }


    @Subscribe
    public void onHttpGetStateAndCitiesSuccess(ApiSuccessListener<StateResponse> response) {

        if (response.type.equalsIgnoreCase("getStateAndCities")) {

            dismissProgress();
            StateResponse responseModel = response.response.body();
            if (responseModel != null && responseModel.getStatus()) {


                if (responseModel.getStates() != null && !responseModel.getStates().isEmpty())
                    setStates(responseModel.getStates());
                else
                    showToast("No States found!");
            }
        }
    }


    @Subscribe
    public void onHttpError(ApiFailureMessageListener<ErrorModel> response) {

        if (response.type.equalsIgnoreCase("getProfile")) {
            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                setErrorView(errorModel.getText(), "", false, NO_DATA);

            } catch (Exception e) {
                e.printStackTrace();
                setErrorView(getResources().getString(R.string.error_try_again), getString(R.string.error_subtitle_failed_one_more_time), true, NO_DATA);

            }
        } else if (response.type.equalsIgnoreCase("profileDpUpload") || response.type.equalsIgnoreCase("updateProfile") || response.type.equalsIgnoreCase("getStateAndCities")) {
            updateViews(VIEW_DATA);
            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                showToast(errorModel.getText());
            } catch (Exception e) {
                e.printStackTrace();
                showToast(getString(R.string.error_try_again));

            }
        }
    }

    @Subscribe
    public void onHttpFailure(ApiFailureListener response) {

        if (response.type.equalsIgnoreCase("getProfile"))
            setErrorView(getResources().getString(R.string.error_try_again), getString(R.string.error_subtitle_failed_one_more_time), true, WRONG_RESPONSE);
        else if (response.type.equalsIgnoreCase("profileDpUpload") || response.type.equalsIgnoreCase("updateProfile") || response.type.equalsIgnoreCase("getStateAndCities")) {
            updateViews(VIEW_DATA);
            showToast(getResources().getString(R.string.error_try_again));
        }
    }


    private void setStates(List<State> states) {


        State s = new State();
        s.setId(PLACE_HOLDER_TAG);
        s.setStateName("Select State");
        states.add(0, s);

        AddressSpinnerArrayAdapter adp = new AddressSpinnerArrayAdapter(this, states, false);

        spinnerState.setAdapter(adp);

        spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View arg1, int arg2, long arg3) {
                State item = (State) parent.getItemAtPosition(arg2);
                setCities(item.getCities());
            }

            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });


        for (int i = 0; i < states.size(); i++) {

            if (profile != null && profile.getState() != null && profile.getState().equalsIgnoreCase(states.get(i).getStateName()))
                spinnerState.setSelection(i, true);
        }


    }

    private void setCities(List<City> cities) {

        if (cities == null)
            cities = new ArrayList<>();

        City c = new City();
        c.setId(PLACE_HOLDER_TAG);
        c.setName("Select City");
        cities.add(0, c);

        c = new City();
        c.setId(OTHER_TAG);
        c.setName(OTHER_TAG);
        cities.add(c);


        AddressSpinnerArrayAdapter adp = new AddressSpinnerArrayAdapter(this, cities, false);
        spinnerCity.setAdapter(adp);
        spinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View arg1, int arg2, long arg3) {

                City item = (City) parent.getItemAtPosition(arg2);
                viewAddNewCity.setVisibility(item.getId().equalsIgnoreCase(OTHER_TAG) ? View.VISIBLE : View.GONE);

                if (item.getId().equalsIgnoreCase(OTHER_TAG)) {
                    etNewCity.getText().clear();
                    etNewCity.requestFocus();
                }

            }


            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });


        for (int i = 0; i < cities.size(); i++) {

            if (profile != null && profile.getCity() != null && profile.getCity().equalsIgnoreCase(cities.get(i).getName()))
                spinnerCity.setSelection(i, true);
        }


    }


    private boolean isValidate() {

        boolean _isValidate = false;
        hideSoftKeyboard();

        String name = etName.getText().toString().trim();
        String address1 = etAddress1.getText().toString().trim();
        String address2 = etAddress2.getText().toString().trim();


        State state = (State) spinnerState.getSelectedItem();
        City city = (City) spinnerCity.getSelectedItem();
        String poBox = etPoBox.getText().toString().trim();
        String companyPhone = etPhone.getText().toString().trim();
        String mobilePhone = etMobile.getText().toString().trim();
        String webAddress = etWeb.getText().toString().trim();
//        String email = etM.getText().toString().trim();
        String newCity = etNewCity.getText().toString().trim();


        if (TextUtils.isEmpty(name)) {
            showToast("Company name cannot be blank.");
            etName.requestFocus();
        } else if (TextUtils.isEmpty(address1)) {
            showToast("Address Line 1 cannot be blank.");
            etAddress1.requestFocus();
        } else if (TextUtils.isEmpty(address2)) {
            showToast("Address Line 2 cannot be blank.");
            etAddress2.requestFocus();
        } else if ((address1 + address2).trim().length() < 10 || (address1 + address2).trim().length() > 500) {
            showToast("Address Should be between 10 to 500 Characters");
            etAddress1.requestFocus();
        } else if (state == null) {
            showToast("Please select state");
            getStateList();
        } else if (state.getId().equalsIgnoreCase(PLACE_HOLDER_TAG)) {
            showToast("Please select state");
        } else if (city == null || city.getId().equalsIgnoreCase(PLACE_HOLDER_TAG)) {
            showToast("Please select city");
        } else if (city.getId().equalsIgnoreCase(OTHER_TAG) && TextUtils.isEmpty(newCity)) {
            showToast("City cannot be blank.");
            etNewCity.requestFocus();

        } else if (TextUtils.isEmpty(poBox)) {
            showToast("P.O Box cannot be blank.");
            etPoBox.requestFocus();

        } else if (!isValidMobile(companyPhone)) {
            showToast("Invalid Phone number ");
            etPhone.requestFocus();

        } else if (!isValidMobile(mobilePhone)) {
            showToast("Invalid Mobile number ");
            etMobile.requestFocus();

        } else if (!isValidWeb(webAddress)) {
            showToast("Invalid web address");
            etWeb.requestFocus();


        } else {
            _isValidate = true;
        }
        return _isValidate;
    }


    private void profileUpdateRequest(String fileCode) {

        hideSoftKeyboard();


        if (!AppController.get().isNetworkAvailable()) {
            updateViews(VIEW_DATA);
            btnEditProfile.setEnabled(true);
            showToast(getResources().getString(R.string.no_internet));
            return;
        }

        btnEditProfile.setEnabled(false);
        updateViews(VIEW_PROGRESSBAR);
        HashMap<String, String> options = new HashMap<>();

        String name = etName.getText().toString().trim();
        String address1 = etAddress1.getText().toString().trim();
        String address2 = etAddress2.getText().toString().trim();


        State state = (State) spinnerState.getSelectedItem();
        City city = (City) spinnerCity.getSelectedItem();
        String poBox = etPoBox.getText().toString().trim();
        String companyPhone = etPhone.getText().toString().trim();
        String mobilePhone = etMobile.getText().toString().trim();
        String webAddress = etWeb.getText().toString().trim();
        String newCity = etNewCity.getText().toString().trim();


        if (!TextUtils.isEmpty(name))
            options.put("company", name);
        if (!TextUtils.isEmpty(address1))
            options.put("address", address1);
        if (!TextUtils.isEmpty(address2))
            options.put("address2", address2);

        if (state != null && !state.getId().equalsIgnoreCase(PLACE_HOLDER_TAG))
            options.put("state", state.getId());

        if (city != null && !city.getId().equalsIgnoreCase(PLACE_HOLDER_TAG))
            options.put("city", city.getId());

        if (city != null && city.getId().equalsIgnoreCase(OTHER_TAG) && !TextUtils.isEmpty(newCity))
            options.put("new_city", city.getId().equalsIgnoreCase(OTHER_TAG) ? newCity : "");

        if (!TextUtils.isEmpty(poBox))
            options.put("zipcode", poBox);

        if (isValidMobile(companyPhone))
            options.put("phone", companyPhone);


        if (isValidMobile(mobilePhone))
            options.put("mobile", mobilePhone);


        if (isValidWeb(webAddress))
            options.put("website", webAddress);


        if (!TextUtils.isEmpty(fileCode))
            options.put("profileimg", fileCode);


        printLog(TAG, "profileUpdateRequest : " + options);
        AppController.get().updateProfile(options);


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (isEditMode)
            setProfileView();
        else
            super.onBackPressed();
    }


}
