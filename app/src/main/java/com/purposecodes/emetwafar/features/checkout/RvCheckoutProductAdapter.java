package com.purposecodes.emetwafar.features.checkout;

import static com.purposecodes.emetwafar.utils.UtilsFunctions.convertPriceText;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.convertWarrantyPriceFormatText;
import static com.purposecodes.emetwafar.utils.UtilsVariable.CouponViewType.APPLY;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.button.MaterialButton;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.holder.RvBlankViewHolder;
import com.purposecodes.emetwafar.holder.RvFooterViewHolder;
import com.purposecodes.emetwafar.model.CartItem;
import com.purposecodes.emetwafar.model.SavedAddress;
import com.purposecodes.emetwafar.utils.UtilsVariable;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RvCheckoutProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final String TAG = "RvCheckoutProductAdapter";
    // Context
    private final Context mContext;


    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;
    private static final int TYPE_COUPON = 3;
    private static final int TYPE_BLANK = 0;
    private UtilsVariable.CouponViewType couponViewType = APPLY;
    private String couponViewText = "";
    // List
    private final ArrayList<CartItem> mList;

    private final OnCheckoutProductClickListener listener;

    public RvCheckoutProductAdapter(Context mContext, ArrayList<CartItem> mList, OnCheckoutProductClickListener _listener) {
        this.mContext = mContext;
        this.mList = mList;
        this.listener = _listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(mContext).inflate(R.layout.item_checkout_product, parent, false);
//        return new RvCheckoutProductHolder(view);


        View view;
        switch (viewType) {

            case TYPE_FOOTER:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_loading, parent, false);
                return new RvFooterViewHolder(view);

            case TYPE_ITEM:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_checkout_product, parent, false);
                return new RvCheckoutProductHolder(view);
            case TYPE_COUPON:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_checkout_coupon, parent, false);
                return new RvCheckoutCouponHolder(view);

            default:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_blank, parent, false);
                return new RvBlankViewHolder(view);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {



        int viewType = getItemViewType(position);
        switch (viewType) {

            case TYPE_FOOTER:
                bindFooterView((RvFooterViewHolder) holder);
                break;
            case TYPE_ITEM:
                bindItemView((RvCheckoutProductHolder) holder, position);
                break;
            case TYPE_COUPON:
                bindCouponView((RvCheckoutCouponHolder) holder, position);
                break;
        }


    }


    private void bindFooterView(RvFooterViewHolder holder) {


    }


    private void bindItemView(RvCheckoutProductHolder holder, final int position) {


        CartItem cartItem = mList.get(position);

        Glide.with(mContext)
                .load(cartItem.getImageUrl())
                .apply(new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.ic_upload_placeholder)
                        .error(R.drawable.ic_upload_placeholder))
                .into(holder.ivProduct);

        holder.tvName.setText(cartItem.getProductName());
        holder.tvDesc.setText(cartItem.getCategoryName());


        if (!TextUtils.isEmpty(cartItem.getOfferPrice()) && !cartItem.getOfferPrice().equalsIgnoreCase("0"))
            holder.tvAmount.setText(convertPriceText(cartItem.getOfferPrice()));
        else
            holder.tvAmount.setText(convertPriceText(cartItem.getProductPrice()));


        holder.tvQuantity.setText("Quantity : " + cartItem.getQuantity());

        SavedAddress address = cartItem.getDeliveryAddress();
        StringBuilder builder = new StringBuilder();

        if (cartItem.getDeliveryAddress() != null) {

            if (address.getAddress1() != null && !TextUtils.isEmpty(address.getAddress1()))
                builder.append(address.getAddress1());

            if (address.getAddress2() != null && !TextUtils.isEmpty(address.getAddress2()))
                builder.append(", ").append(address.getAddress2()).append("\n");

            if (address.getCity() != null && !TextUtils.isEmpty(address.getCity()))
                builder.append(address.getCity());

            if (address.getState() != null && !TextUtils.isEmpty(address.getState()))
                builder.append(", ").append(address.getState());
            holder.tvDeliveryAddress.setText(builder.toString().trim());
            holder.btnChangeAddress.setText(mContext.getText(R.string.change_address));
        } else {
            holder.tvDeliveryAddress.setText("");
            holder.btnChangeAddress.setText("Add Address");
        }


        holder.viewWarranty.setVisibility(cartItem.getWarrantyPeriod().isEmpty() ? View.GONE : View.VISIBLE);

        if (!cartItem.getWarrantyPeriod().isEmpty()) {
            holder.tvWarrantyName.setText(cartItem.getWarrantyPeriod());
            holder.tvWarrantyPlan.setText(convertWarrantyPriceFormatText(cartItem.getQuantity() + " x " + cartItem.getWarrantyPrice() + " = ", cartItem.getWarrantyTotal()));
        }

        holder.btnChangeAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onChangeAddressClicked(cartItem, position);
            }
        });


    }


    private void bindCouponView(RvCheckoutCouponHolder holder, final int position) {


        switch (couponViewType) {
            case APPLY:
                holder.viewResult.setVisibility(View.INVISIBLE);
                holder.viewApply.setVisibility(View.VISIBLE);
                holder.etCode.setText("");
                holder.cardBg.setCardBackgroundColor(mContext.getResources().getColor(R.color.blackColor));
                break;
            case FAILED:
                holder.viewResult.setVisibility(View.VISIBLE);
                holder.viewApply.setVisibility(View.INVISIBLE);
                holder.cardBg.setCardBackgroundColor(mContext.getResources().getColor(R.color.redColor));
                holder.ivResult.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_expired));
                holder.tvResult.setText(couponViewText);
                holder.ibClose.setOnClickListener(v -> {
                    holder.onResultHide();
                });
                break;

            case SUCCUSS:
                holder.viewResult.setVisibility(View.VISIBLE);
                holder.viewApply.setVisibility(View.INVISIBLE);
                holder.cardBg.setCardBackgroundColor(mContext.getResources().getColor(R.color.greenColor));
                holder.ivResult.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_white_tick));
                holder.tvResult.setText(convertDiscount(couponViewText));
                holder.ibClose.setOnClickListener(v -> {

                    if (listener != null)
                        listener.onRemoveCouponClicked(position);
                });
                break;
        }


        holder.btnApply.setOnClickListener(v -> {

            String couponCode = holder.etCode.getText().toString().trim();
            if (TextUtils.isEmpty(couponCode)) {
                Toast.makeText(mContext, "Please enter valid coupon code", Toast.LENGTH_SHORT).show();
            } else if (listener != null)
                listener.onApplyCouponClicked(couponCode, position);


        });


    }


    private SpannableStringBuilder convertDiscount(String _percentage) {

        String firstWord = "Received a ";
        String discount = _percentage + "%";
        String lastWord = " discount";

// Use a SpannableStringBuilder so that both the text and the spans are mutable
        SpannableStringBuilder ssb = new SpannableStringBuilder(firstWord);
        try {

            Typeface myTypeface = Typeface.create(ResourcesCompat.getFont(mContext, R.font.poppins_semi_bold), Typeface.BOLD);
            // Initialize a new StyleSpan to display bold text
            StyleSpan boldSpan = new StyleSpan(Typeface.BOLD);
            ssb.append(discount);
            // Apply the color span
//            ssb.setSpan(boldSpan, ssb.length() - discount.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ssb.setSpan(boldSpan, firstWord.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ssb.append(lastWord);

//            ssb.setSpan(strikethroughSpan, ssb.length() - lastWord.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        } catch (Exception e) {
            e.fillInStackTrace();
        }


// Set the TextView text and denote that it is Editable
// since it's a SpannableStringBuilder
        return ssb;

    }


    public void changeCouponViewType(UtilsVariable.CouponViewType type, String couponResult) {
        couponViewType = type;
        couponViewText = couponResult;
        notifyItemChanged(mList.size());
    }

    @Override
    public int getItemCount() {

        int count = 0;
        if (!mList.isEmpty()) {
            count = mList.size() + 1;
        }
        return count;


    }


    @Override
    public int getItemViewType(int position) {

        try {
            if (position == getItemCount() - 1) {
                return TYPE_COUPON;
            } else {

                return TYPE_ITEM;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            return TYPE_BLANK;
        }
    }

    static class RvCheckoutProductHolder extends RecyclerView.ViewHolder {

        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.imageView_itemCheckoutCart_imageView)
        ImageView ivProduct;

        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.textView_itemCheckoutCart_name)
        TextView tvName;

        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.textView_itemCheckoutCart_desc)
        TextView tvDesc;

        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.textView_itemCheckoutCart_amount)
        TextView tvAmount;

        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.textView_itemCheckoutCart_quantity)
        TextView tvQuantity;

        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.textView_itemCheckoutCart_warrantyName)
        TextView tvWarrantyName;

        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.textView_itemCheckoutCart_warrantyPlan)
        TextView tvWarrantyPlan;

        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.view_itemCheckoutCart_warranty)
        ViewGroup viewWarranty;

        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.textView_itemCheckoutCart_deliveryAddress)
        TextView tvDeliveryAddress;

        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.button_itemCheckoutCart_changeAddress)
        MaterialButton btnChangeAddress;


        RvCheckoutProductHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }


    }


    static class RvCheckoutCouponHolder extends RecyclerView.ViewHolder {


        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.view_applyCoupon_result)
        ViewGroup viewResult;

        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.view_applyCoupon_apply)
        ViewGroup viewApply;

        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.cardView_checkout_coupon)
        CardView cardBg;

        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.imageButton_applyCoupon_close)
        ImageButton ibClose;

        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.imageView_applyCoupon_result)
        ImageView ivResult;

        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.textView_applyCoupon_result)
        TextView tvResult;

        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.editText_itemCheckout_applyCode)
        EditText etCode;


        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.button_itemCheckout_apply)
        Button btnApply;


        RvCheckoutCouponHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }


        public void onResultHide() {

            viewApply.setVisibility(View.VISIBLE);
            viewResult.setVisibility(View.INVISIBLE);
            cardBg.setCardBackgroundColor(itemView.getResources().getColor(R.color.blackColor));

        }

    }


    public interface OnCheckoutProductClickListener {
        void onChangeAddressClicked(CartItem item, int position);

        void onApplyCouponClicked(String couponCode, int position);

        void onRemoveCouponClicked(int position);

    }
}
