package com.purposecodes.emetwafar.features.checkout;

import static com.purposecodes.emetwafar.controller.SessionController.getUser;
import static com.purposecodes.emetwafar.controller.SessionController.setCartId;
import static com.purposecodes.emetwafar.controller.SessionController.setCartQuantity;
import static com.purposecodes.emetwafar.features.payments.PaymentUtils.sendOrderPaymentRequest;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_CHECKOUT;
import static com.purposecodes.emetwafar.utils.LogUtils.printLog;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.getAmount;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.getAppVersion;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.getTimeStamp;
import static com.purposecodes.emetwafar.utils.UtilsVariable.CALLING_ACTIVITY_KEY;
import static com.purposecodes.emetwafar.utils.UtilsVariable.DEFAULT_CURRENCY;
import static com.purposecodes.emetwafar.utils.UtilsVariable.DEVICE_OS;
import static com.purposecodes.emetwafar.utils.UtilsVariable.DEVICE_VERSION_KEY;
import static com.purposecodes.emetwafar.utils.UtilsVariable.REQUEST_EDIT_BILLING_ADDRESS;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_BILLING_ADDRESS;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_CHECKOUT;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.api.ApiFailureMessageListener;
import com.purposecodes.emetwafar.api.ApiSuccessListener;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.features.address.AddNewAddressActivity;
import com.purposecodes.emetwafar.features.address.EditBillingAddressActivity;
import com.purposecodes.emetwafar.model.BillingAddress;
import com.purposecodes.emetwafar.model.CartItem;
import com.purposecodes.emetwafar.model.CheckoutData;
import com.purposecodes.emetwafar.model.OrderPayment;
import com.purposecodes.emetwafar.model.SavedAddress;
import com.purposecodes.emetwafar.model.UserData;
import com.purposecodes.emetwafar.responsemodel.AddressResponse;
import com.purposecodes.emetwafar.responsemodel.CheckoutResponse;
import com.purposecodes.emetwafar.responsemodel.ErrorModel;
import com.purposecodes.emetwafar.responsemodel.SaveOrderResponse;
import com.purposecodes.emetwafar.utils.UtilsVariable;
import com.squareup.otto.Subscribe;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

public class CheckoutActivity extends BaseActivity implements RvCheckoutProductAdapter.OnCheckoutProductClickListener, RvPaymentTypeAdapter.OnPaymentTypeClickListener {


    private int callingActivity;
    private final String TAG = "CheckoutActivity";
    private CheckoutData checkoutData;

    @BindView(R.id.recyclerView_checkout_productList)
    RecyclerView rvProduct;

    @BindView(R.id.recyclerView_checkout_payment)
    RecyclerView rvPayment;

    @BindView(R.id.button_checkout_deliveryYes)
    Button btnDeliveryYes;

    @BindView(R.id.imageView_checkout_deliveryYes)
    ImageView ivDeliveryYes;

    @BindView(R.id.imageView_checkout_deliveryNo)
    ImageView ivDeliveryNo;

    @BindView(R.id.view_checkout_needDelivery)
    ConstraintLayout deliveryView;


    @BindView(R.id.button_checkout_changeBillingAddress)
    MaterialButton btnChangeBillingAddress;

    @BindView(R.id.button_checkout_deliveryNo)
    Button btnDeliveryNo;
    @BindView(R.id.button_checkout_placeOrder)
    Button btnProceed;

    @BindView(R.id.textView_checkout_billingAddressName)
    TextView tvBillingAddressName;

    @BindView(R.id.textView_checkout_billingAddressDesc)
    TextView tvBillingAddressDescription;

    @BindView(R.id.textView_checkout_billingAddressStatus)
    TextView tvBillingAddressStatus;

    @BindView(R.id.textView_checkout_deliveryHint)
    TextView tvDeliveryHint;


    @BindView(R.id.textView_checkout_subTotal)
    TextView tvSubTotal;

    @BindView(R.id.textView_checkout_vat)
    TextView tvVat;

    @BindView(R.id.textView_checkout_discount)
    TextView tvDiscount;
    @BindView(R.id.textView_checkout_verificationAmount)
    TextView tvVerificationAmount;

    @BindView(R.id.textView_checkout_warrantyAmount)
    TextView tvWarrantyAmount;
    @BindView(R.id.textView_checkout_totalAmount)
    TextView tvTotalAmount;

    @BindView(R.id.checkBox_checkout_verification)
    CheckBox cbProductVerification;


    private RvCheckoutProductAdapter cartAdapter;

    private ArrayList<CartItem> productList;

    private String CART_ID;


    String[] paymentList = {"Credit / Debit"}; //  "Cash on Delivery", "Wallet / UPI","Net Banking"
    private String paymentType = paymentList[0];


    @SuppressLint("StaticFieldLeak")
    private static CheckoutActivity checkoutActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        checkoutActivity = this;
        productList = new ArrayList<>();
        AppController.get().setCheckoutData(null); //temporary data null
        try {

            callingActivity = getIntent().getIntExtra(CALLING_ACTIVITY_KEY, ACTIVITY_CHECKOUT);
            checkoutData = (CheckoutData) getIntent().getSerializableExtra(TAG_CHECKOUT);

            assert checkoutData != null;
            CART_ID = checkoutData.getCartId();

        } catch (NullPointerException e) {
            printLog(TAG, "intent data Exception : " + e.getMessage());
        }

        initiate();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_checkout;
    }



    private void initiate() {


        cartAdapter = new RvCheckoutProductAdapter(this, productList, this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvProduct.setHasFixedSize(true);
        rvProduct.setLayoutManager(layoutManager);
        rvProduct.setAdapter(cartAdapter);
        rvProduct.setNestedScrollingEnabled(false);


        RvPaymentTypeAdapter paymentTypeAdapter = new RvPaymentTypeAdapter(this, paymentList, this);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 1);
        rvPayment.setHasFixedSize(true);
        rvPayment.setLayoutManager(gridLayoutManager);
        rvPayment.setAdapter(paymentTypeAdapter);
        rvPayment.setNestedScrollingEnabled(false);

        btnDeliveryYes.setSelected(true);
        btnDeliveryNo.setSelected(false);

        updateData();

        cbProductVerification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                NumberFormat format = NumberFormat.getInstance(Locale.US);

                try {
                    if (isChecked) {
                        Number numberTotal = format.parse(checkoutData.getGrandTotal());
                        Number numberVerify = format.parse(checkoutData.getVerifyCost());

                        assert numberTotal != null;
                        double resultT = numberTotal.doubleValue();
                        assert numberVerify != null;
                        double resultV = numberVerify.doubleValue();
                        tvTotalAmount.setText(convertPriceText(String.valueOf(getAmount(resultT + resultV))));

                    } else {
                        tvTotalAmount.setText(convertPriceText(checkoutData.getGrandTotal()));

                    }
                } catch (Exception e) {
                    printLog(TAG, " Number exception : " + e.getMessage());
                }
            }
        });

    }


    @SuppressLint("NotifyDataSetChanged")
    private void updateData() {

        try {

            if (productList != null)
                productList.clear();
            else
                productList = new ArrayList<>();

            productList.addAll(checkoutData.getCartItems());

            cartAdapter.notifyDataSetChanged();


            tvBillingAddressName.setText(checkoutData.getBillingAddress().getCompany());
            StringBuilder builder = new StringBuilder();
            BillingAddress _address = checkoutData.getBillingAddress();

            if (_address.getAddress() != null && !TextUtils.isEmpty(_address.getAddress()))
                builder.append(_address.getAddress());

            if (_address.getAddress2() != null && !TextUtils.isEmpty(_address.getAddress2()))
                builder.append("\n").append(_address.getAddress2());

            if (_address.getCity() != null && !TextUtils.isEmpty(_address.getCity()))
                builder.append("\n").append(_address.getCity());

            if (_address.getZipcode() != null && !TextUtils.isEmpty(_address.getZipcode()))
                builder.append("\nZip Code ").append(_address.getZipcode());

            if (_address.getState() != null && !TextUtils.isEmpty(_address.getState()))
                builder.append("\n").append(_address.getState());
            if (_address.getCountry() != null && !TextUtils.isEmpty(_address.getCountry()))
                builder.append(", ").append(_address.getCountry());

            if (_address.getEmail() != null && !TextUtils.isEmpty(_address.getEmail()))
                builder.append("\n").append(_address.getEmail());

            if (_address.getMobile() != null && !TextUtils.isEmpty(_address.getMobile()))
                builder.append("\n").append(_address.getMobile());


            tvBillingAddressDescription.setText(builder.toString().trim());

            tvBillingAddressStatus.setText("Billing Address");


            tvSubTotal.setText(checkoutData.getSubtotal());

            tvVat.setText(checkoutData.getVat());
            tvDiscount.setText(checkoutData.getDiscount().equalsIgnoreCase("0") ? "Not Applied" : checkoutData.getDiscount());

            tvVerificationAmount.setText(checkoutData.getVerifyCost());
//            tvVerificationAmount.setText(cbProductVerification.isChecked()?checkoutData.getVerifyCost():"0.00"); //future comment

            tvWarrantyAmount.setText(checkoutData.getWarrantyAmount());


            if (checkoutData.getCouponCode().equalsIgnoreCase("Not Applied any Coupon"))
                cartAdapter.changeCouponViewType(UtilsVariable.CouponViewType.APPLY, "");
            else if (!checkoutData.getDiscountPercent().equalsIgnoreCase("0"))
                cartAdapter.changeCouponViewType(UtilsVariable.CouponViewType.SUCCUSS, checkoutData.getDiscountPercent());
            else
                cartAdapter.changeCouponViewType(UtilsVariable.CouponViewType.FAILED, "Date expired");


            if (cbProductVerification.isChecked()) {
                NumberFormat format = NumberFormat.getInstance(Locale.US);

                Number numberTotal = format.parse(checkoutData.getGrandTotal());
                Number numberVerify = format.parse(checkoutData.getVerifyCost());

                assert numberTotal != null;
                double resultT = numberTotal.doubleValue();
                assert numberVerify != null;
                double resultV = numberVerify.doubleValue();
                tvTotalAmount.setText(convertPriceText(String.valueOf(getAmount(resultT + resultV))));
            } else
                tvTotalAmount.setText(convertPriceText(checkoutData.getGrandTotal()));


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("NonConstantResourceId")
    @OnClick({R.id.button_checkout_changeBillingAddress, R.id.button_checkout_deliveryYes, R.id.button_checkout_deliveryNo, R.id.button_checkout_placeOrder})
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.button_checkout_changeBillingAddress:

                Intent intent = new Intent(CheckoutActivity.this, EditBillingAddressActivity.class);
                intent.putExtra(TAG_BILLING_ADDRESS, checkoutData.getBillingAddress());

                startActivityForResult(intent, REQUEST_EDIT_BILLING_ADDRESS);

                break;
            case R.id.button_checkout_deliveryYes:
                btnDeliveryYes.setSelected(true);
                btnDeliveryNo.setSelected(false);
                ivDeliveryYes.setVisibility(View.VISIBLE);
                ivDeliveryNo.setVisibility(View.GONE);
                tvDeliveryHint.setText("Our company delivery boy will deliver the product you ordered");

                break;
            case R.id.button_checkout_deliveryNo:
                btnDeliveryYes.setSelected(false);
                btnDeliveryNo.setSelected(true);
                ivDeliveryYes.setVisibility(View.GONE);
                ivDeliveryNo.setVisibility(View.VISIBLE);
                tvDeliveryHint.setText("Representatives of your company should come directly and pick up the products");

                break;
            case R.id.button_checkout_placeOrder:

                //other order method no need now
//                if (paymentType.equalsIgnoreCase(paymentList[0]))
                paymentInitiate();
//                else
//                    checkoutProceed();


                break;
        }


    }

    private void getAddressLists(int position) {
        if (AppController.get().isNetworkAvailable()) {
            showProgress();
            AppController.get().getAddresses(position);
        } else
            showToast(getString(R.string.no_internet));
    }

    @Override
    public void onChangeAddressClicked(CartItem item, int position) {
        getAddressLists(position);
    }

    @Override
    public void onApplyCouponClicked(String couponCode, int position) {
        if (AppController.get().isNetworkAvailable()) {
            hideSoftKeyboard();
            showProgress();
            Map<String, String> options = new HashMap<>();
            options.put("cart_id", CART_ID);
            options.put("coupon_code", couponCode);
            AppController.get().applyCoupon(options);

        } else
            showToast(getString(R.string.no_internet));
    }

    @Override
    public void onRemoveCouponClicked(int position) {
        removeCouponConfirmation();
    }

    @Override
    public void onPaymentTypeSelected(String payment, int position) {
        paymentType = payment;
    }


    public void updateAddress(CartItem cartItem, String addressId, int position) {
        if (AppController.get().isNetworkAvailable()) {
            hideSoftKeyboard();
            showProgress();
            Map<String, String> options = new HashMap<>();
            options.put("cart_id", CART_ID);
            options.put("product_id", cartItem.getProductId());
            options.put("address_id", addressId);
            AppController.get().updateCheckout(options);

        } else
            showToast(getString(R.string.no_internet));
    }


    public void paymentInitiate() {

        if (AppController.get().isNetworkAvailable()) {
            hideSoftKeyboard();
            showProgress();
            Map<String, String> options = new HashMap<>();
            options.put(DEVICE_VERSION_KEY, DEVICE_OS + " " + getAppVersion());
            options.put("checkout_id", checkoutData.getCheckoutId());
            options.put("order_ref", CART_ID + "_" + getTimeStamp());

            options.put("cart_id", CART_ID);
            //0- no verify request 1 - need verify request
            options.put("verify_request", cbProductVerification.isChecked() ? "1" : "0");
            options.put("payment_type", "telr");

            printLog(TAG, "paymentInitiate : " + options);
            AppController.get().initiatePayment(options);

        } else
            showToast(getString(R.string.no_internet));

    }

    public void checkoutProceed() {
        if (AppController.get().isNetworkAvailable()) {
            hideSoftKeyboard();
            showProgress();
            Map<String, String> options = new HashMap<>();
            options.put(DEVICE_VERSION_KEY, DEVICE_OS + " " + getAppVersion());
            options.put("cart_id", CART_ID);
            options.put("checkout_id", checkoutData.getCheckoutId());
            //0- no verify request 1 - need verify request
            options.put("verify_request", cbProductVerification.isChecked() ? "1" : "0");
            options.put("payment_type", "cashondelivery");

            options.put("order_ref", "");
            options.put("transaction_no", "");

            AppController.get().saveOrder(options);

        } else
            showToast(getString(R.string.no_internet));
    }


    private SpannableStringBuilder convertPriceText(String _price) {

        String firstWord = _price;

// Use a SpannableStringBuilder so that both the text and the spans are mutable
        SpannableStringBuilder ssb = new SpannableStringBuilder(firstWord);
        try {
            String secondWord = DEFAULT_CURRENCY;
            Typeface myTypeface = Typeface.create(ResourcesCompat.getFont(this, R.font.poppins_semi_bold), Typeface.BOLD);
// Create a span that will make the text red
            ForegroundColorSpan redForegroundColorSpan = new ForegroundColorSpan(getResources().getColor(R.color.redColor));


            // text is added in later
            ssb.setSpan(redForegroundColorSpan, 0, ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
// Add a blank space
            ssb.append(" ");

            ssb.append(secondWord);
            // Apply the color span
            ssb.setSpan(myTypeface, ssb.length() - secondWord.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        } catch (Exception e) {
            e.fillInStackTrace();
        }

// Set the TextView text and denote that it is Editable
// since it's a SpannableStringBuilder
        return ssb;

    }


    @Subscribe
    public void onHttpGetAddressListSuccess(ApiSuccessListener<AddressResponse> response) {

        if (response.type.equalsIgnoreCase("getAddresses")) {

            AddressResponse responseModel = response.response.body();
            if (responseModel != null && responseModel.getStatus()) {
                dismissProgress();

                ArrayList<SavedAddress> addressList = responseModel.getSavedAddressList();

                if (addressList == null || addressList.isEmpty())
                    showToast("Your Addresses is empty!");
                else
                    showBottomAddressDialog(addressList, response.position);

            } else {
                assert responseModel != null;
                if (responseModel.getText() != null && TextUtils.isEmpty(responseModel.getText()))
                    showToast(responseModel.getText());
                else
                    showToast("Your Addresses is empty!");
            }
        }
    }

    @Subscribe
    public void onHttpGetCheckoutListSuccess(ApiSuccessListener<CheckoutResponse> response) {

        if (response.type.equalsIgnoreCase("checkout") || response.type.equalsIgnoreCase("updateCheckout")) {
            dismissProgress();
            CheckoutResponse responseModel = response.response.body();
            if (responseModel != null && responseModel.getStatus()) {
                checkoutData = responseModel.getCheckoutData();
                updateData();

            } else {
                showToast(responseModel.getText());

            }

        } else if (response.type.equalsIgnoreCase("removeSelectedCoupon")) {
            dismissProgress();
            CheckoutResponse responseModel = response.response.body();
            if (responseModel != null && responseModel.getStatus()) {
                checkoutData = responseModel.getCheckoutData();
                updateData();
                showToast("Coupon Removed");
            } else {
                showToast(responseModel.getText());
                cartAdapter.changeCouponViewType(UtilsVariable.CouponViewType.APPLY, "");
            }

        }
    }

    @Subscribe
    public void onHttpSaveOrderSuccess(ApiSuccessListener<SaveOrderResponse> response) {

        if (response.type.equalsIgnoreCase("saveOrder")) {

            SaveOrderResponse responseModel = response.response.body();
            if (responseModel != null && responseModel.getStatus()) {

                if (responseModel.getOrders() != null && !responseModel.getOrders().isEmpty()) {

                    if (callingActivity == ACTIVITY_CHECKOUT) {
                        setCartId(""); //cart id clear from session
                        setCartQuantity(0);
                    }

                    showToast(responseModel.getText());

                    dismissProgress();
                    Intent intent = new Intent(CheckoutActivity.this, OrderSuccessActivity.class);
                    intent.putExtra(TAG_CHECKOUT, responseModel.getOrders());
                    intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
                    intent.putExtra(CALLING_ACTIVITY_KEY, callingActivity);

                    startActivity(intent);
                    finish();


                }
            } else {
                dismissProgress();
                showToast(getResources().getString(R.string.error_try_again));

            }

        }
    }

    @Subscribe
    public void onHttpSuccess(ApiSuccessListener<JsonObject> response) {

        if (response.type.equalsIgnoreCase("applyCoupon")) {
            dismissProgress();

            try {
                JsonObject jsonObject = response.response.body();
                assert jsonObject != null;
                if (jsonObject.get("status").getAsBoolean()) {
                    JsonObject dataObj = jsonObject.get("data").getAsJsonObject();
                    checkoutData.setDiscount(dataObj.get("discount").getAsString());
                    checkoutData.setDiscountPercent(dataObj.get("discount_percent").getAsString());
                    checkoutData.setGrandTotal(dataObj.get("grand_total").getAsString());
                    checkoutData.setCouponCode(dataObj.get("coupon_code").getAsString());
                    updateData();
                    showToast("Coupon Applied");
                } else {
                    showToast(jsonObject.get("text").getAsString());
                }
            } catch (Exception e) {
                printLog(TAG, " onHttpSuccess Exception : " + e.getMessage());
            }
        } else if (response.type.equalsIgnoreCase("initiatePayment")) {
            dismissProgress();

            try {
                JsonObject jsonObject = response.response.body();
                assert jsonObject != null;
                if (jsonObject.get("status").getAsBoolean()) {
                    JsonObject dataObj = jsonObject.get("data").getAsJsonObject();


                    Map<String, String> options = new HashMap<>();
                    options.put("cart_id", CART_ID);
                    options.put("checkout_id", checkoutData.getCheckoutId());
                    options.put("verify_request", cbProductVerification.isChecked() ? "1" : "0");
                    options.put("payment_type", "telr");
                    options.put("order_ref", (dataObj.get("order_ref").getAsString()));


                    OrderPayment _orderPayment = new OrderPayment();
                    _orderPayment.setCallingActivity(callingActivity);
                    _orderPayment.setOptions(options);
                    AppController.get().setCheckoutData(_orderPayment); //save temporary data


                    setPayment(dataObj.get("order_ref").getAsString());

                } else {
                    showToast(jsonObject.get("text").getAsString());
                }
            } catch (Exception e) {
                printLog(TAG, " onHttpSuccess Exception : " + e.getMessage());
            }

        }


    }


    @Override
    protected void onStart() {
        AppController.get().setCheckoutData(null); //temporary data null
        super.onStart();
    }

    @Subscribe
    public void onHttpError(ApiFailureMessageListener<ErrorModel> response) {

        if (response.type.equalsIgnoreCase("getAddresses") || response.type.equalsIgnoreCase("checkout") || response.type.equalsIgnoreCase("updateCheckout") || response.type.equalsIgnoreCase("saveOrder")) {
            dismissProgress();
            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                showToast(errorModel.getText());

            } catch (Exception e) {
                showToast(getResources().getString(R.string.error_try_again));
            }
        } else if (response.type.equalsIgnoreCase("applyCoupon")) {
            dismissProgress();
            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                cartAdapter.changeCouponViewType(UtilsVariable.CouponViewType.FAILED, errorModel.getText());

            } catch (Exception e) {
                showToast(getResources().getString(R.string.error_try_again));
            }

        } else if (response.type.equalsIgnoreCase("removeSelectedCoupon") || response.type.equalsIgnoreCase("initiatePayment")) {
            dismissProgress();
            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                showToast(errorModel.getText());

            } catch (Exception e) {
                showToast(getResources().getString(R.string.error_try_again));
            }
        }
    }


    @Subscribe
    public void onHttpFailure(ApiFailureListener response) {

        if (response.type.equalsIgnoreCase("getAddresses") || response.type.equalsIgnoreCase("checkout") || response.type.equalsIgnoreCase("updateCheckout") || response.type.equalsIgnoreCase("applyCoupon") || response.type.equalsIgnoreCase("removeSelectedCoupon") || response.type.equalsIgnoreCase("saveOrder") || response.type.equalsIgnoreCase("initiatePayment")) {
            dismissProgress();
            showToast(getResources().getString(R.string.error_try_again));
        }

    }


    private void showBottomAddressDialog(ArrayList<SavedAddress> _addressList, int pos) {

        View view = getLayoutInflater().inflate(R.layout.bottom_dialog_delivery_address, null);

        RecyclerView rvBottomAddress = view.findViewById(R.id.recyclerView_checkoutBottom_addresses);
        Button btnAddAddress = view.findViewById(R.id.button_checkoutBottom_addAddress);

        BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(view);
        dialog.show();

        SavedAddress s = productList.get(pos).getDeliveryAddress();
        String adId = "";
        if (s != null && s.getAddressId() != null)
            adId = s.getAddressId();

        RvBottomAddressAdapter addressAdapter = new RvBottomAddressAdapter(this, adId, _addressList, (savedAddress, position) -> {

            if (productList.size() > pos) {
                CartItem p = productList.get(pos);
                updateAddress(p, savedAddress.getAddressId(), pos);
                dialog.dismiss();
            }

        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvBottomAddress.setHasFixedSize(true);

        rvBottomAddress.setLayoutManager(layoutManager);
        rvBottomAddress.setAdapter(addressAdapter);
//        rvBottomAddress.setNestedScrollingEnabled(true);
        addressAdapter.notifyDataSetChanged();


        btnAddAddress.setOnClickListener(v -> {
            Intent intent = new Intent(CheckoutActivity.this, AddNewAddressActivity.class);
            intent.putExtra(CALLING_ACTIVITY_KEY, callingActivity);
//            startActivityForResult(intent, REQUEST_ADD_NEW_ADDRESS);
            startActivity(intent);

            dialog.dismiss();
        });
//


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (resultCode == RESULT_OK) {
                if (requestCode == REQUEST_EDIT_BILLING_ADDRESS && data != null) {
                    BillingAddress billingAddress = (BillingAddress) data.getSerializableExtra(TAG_BILLING_ADDRESS);
                    checkoutData.setBillingAddress(billingAddress);
                    updateData();

//                } else if (requestCode == REQUEST_ADD_NEW_ADDRESS && data != null) {


                }

            }
        } catch (IndexOutOfBoundsException e) {
            e.getLocalizedMessage();
        }
    }


    private void removeCouponConfirmation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(CheckoutActivity.this);
//Are you sure you want to remove?
        builder.setMessage("Are you sure?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if (AppController.get().isNetworkAvailable()) {
                            showProgress();
                            AppController.get().removeSelectedCoupon(CART_ID);
                        } else
                            showToast(getString(R.string.no_internet));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();

    }


    private void setPayment(String referenceId) {


        UserData userData = getUser();
        if (userData != null) {

            showToast("Please wait...");
            sendOrderPaymentRequest(this, referenceId, checkoutData.getGrandTotal(), userData.getUserId(), "purchase product", checkoutData.getBillingAddress().getCity(), checkoutData.getBillingAddress().getState(), checkoutData.getBillingAddress().getAddress(), checkoutData.getBillingAddress().getAddress2(), checkoutData.getBillingAddress().getZipcode(), userData.getCompany(), "", checkoutData.getBillingAddress().getEmail(), checkoutData.getBillingAddress().getMobile());
        }
    }


    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    public static CheckoutActivity getInstance() {
        return checkoutActivity;
    }

}
