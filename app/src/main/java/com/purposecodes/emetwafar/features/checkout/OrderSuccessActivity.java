package com.purposecodes.emetwafar.features.checkout;

import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_CHECKOUT;
import static com.purposecodes.emetwafar.utils.UtilsVariable.CALLING_ACTIVITY_KEY;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_CHECKOUT;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_ORDER_ID;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.features.home.HomeActivity;
import com.purposecodes.emetwafar.features.mycart.MyCartActivity;
import com.purposecodes.emetwafar.features.order.OrderDetailsActivity;
import com.purposecodes.emetwafar.features.order.RvOrderAdapter;
import com.purposecodes.emetwafar.model.Order;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class OrderSuccessActivity extends BaseActivity implements RvOrderAdapter.OnOrderClickListener {

    private final String TAG = "OrderSuccessActivity";


    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.recyclerView_orderSuccess)
    RecyclerView recyclerView;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.button_orderSuccess_goto)
    MaterialButton btnGoto;
    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_orderSuccess_title)
    TextView tvTitle;

    private int callingActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            callingActivity = getIntent().getIntExtra(CALLING_ACTIVITY_KEY, ACTIVITY_CHECKOUT);
            ArrayList<Order> orders = (ArrayList<Order>) getIntent().getSerializableExtra(TAG_CHECKOUT);

            AppController.get().setCheckoutData(null);
            if (orders == null) {

                tvTitle.setText("Your Order and  Payment Successful");
            }


            if (orders != null)
                setUpRecyclerView(orders);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_order_success;
    }

    private void setUpRecyclerView(ArrayList<Order> orderList) {

        RvOrderAdapter orderAdapter = new RvOrderAdapter(this, orderList, this, false);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(orderAdapter);
        recyclerView.setNestedScrollingEnabled(false);

    }

//    @OnClick(R.id.button_goto)
//    public void onClickGo(){
//        Intent intent = new Intent();
//        setResult(RESULT_OK, intent);
//        onBackPressed();
//    }

    @Override
    public void onClickOrderCancel(Order order, int position) {

    }

    @Override
    public void onClickViewAll(Order order, int position) {
        Intent intent = new Intent(OrderSuccessActivity.this, OrderDetailsActivity.class);
        intent.putExtra(TAG_ORDER_ID, order.getOrderId());
        startActivity(intent);
    }


    @OnClick(R.id.button_orderSuccess_goto)
    @Override
    public void onBackPressed() {

        if (callingActivity == ACTIVITY_CHECKOUT) {

            MyCartActivity.getInstance().finish(); //checkout page

            Intent intent = new Intent(this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
        super.onBackPressed();
    }


/*    @OnClick(R.id.button_orderSuccess_goto)
    public void onGotoProduct() {

        if (callingActivity == ACTIVITY_CHECKOUT) {

            Intent intent = new Intent();
            setResult(RESULT_OK, intent);

            MyCartActivity.getInstance().finish(); //checkout page

            finish();
        }
    }*/
}
