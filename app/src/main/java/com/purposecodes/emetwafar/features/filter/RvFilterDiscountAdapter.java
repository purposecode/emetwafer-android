package com.purposecodes.emetwafar.features.filter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.purposecodes.emetwafar.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RvFilterDiscountAdapter extends RecyclerView.Adapter<RvFilterDiscountAdapter.RvFilterDiscountHolder> {
    private final String TAG = "RvFilterDiscountAdapter";
    // Context
    private final Context mContext;


    // List
    private final ArrayList<String> mList;

   private final OnFilterDiscountClickListener listener;
    private int SELECTED_POSITION=-1;
    private String selectedDiscount = "";

    public RvFilterDiscountAdapter(Context mContext, ArrayList<String> mList,   String _selectedDiscount, OnFilterDiscountClickListener _listener) {
        this.mContext = mContext;
        this.mList = mList;
        this.listener = _listener;
        this.selectedDiscount=_selectedDiscount;
    }


    @NonNull
    @Override
    public RvFilterDiscountHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_discount_offer, parent, false);
        return new RvFilterDiscountHolder(view);

    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onBindViewHolder(@NonNull RvFilterDiscountHolder holder, @SuppressLint("RecyclerView") int position) {
        String discount = mList.get(position);
        holder.btnDiscount.setText(discount);
        holder.btnDiscount.append("% Discount");


        if (SELECTED_POSITION==-1&& !TextUtils.isEmpty(selectedDiscount)&&selectedDiscount.equalsIgnoreCase(discount))
            holder.btnDiscount.setSelected(true);
        else
            holder.btnDiscount.setSelected(SELECTED_POSITION==position);

            holder.btnDiscount.setOnClickListener(v -> {
                SELECTED_POSITION=position;
                notifyDataSetChanged();

                if (listener!=null)
                    listener.onDiscountSelected(discount,position);
            });


    }


    @Override
    public int getItemCount() {
        return (mList != null ? mList.size() : 0);

    }


    static class RvFilterDiscountHolder extends RecyclerView.ViewHolder {

        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.button_filter_discount)
        Button btnDiscount;


        RvFilterDiscountHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    public interface OnFilterDiscountClickListener {
        void onDiscountSelected(String offer, int position);

    }
}
