package com.purposecodes.emetwafar.features.order;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.features.mycart.MyCartActivity;
import com.purposecodes.emetwafar.features.productview.ProductDetailActivity;
import com.purposecodes.emetwafar.model.CartItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.purposecodes.emetwafar.utils.UtilsFunctions.convertPriceText;
import static com.purposecodes.emetwafar.utils.UtilsVariable.DEFAULT_CURRENCY;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_POSITION;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_PRODUCT_ID;

public class RvOrderDetailItemAdapter extends RecyclerView.Adapter<RvOrderDetailItemAdapter.RvOrderItemHolder> {

    private final List<CartItem> products;
    private final Context context;

    RvOrderDetailItemAdapter(List<CartItem> products, Context context) {
        this.products = products;
        this.context = context;
    }

    @NonNull
    @Override
    public RvOrderItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_order_detailprduct, parent, false);
        return new RvOrderItemHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RvOrderItemHolder holder, @SuppressLint("RecyclerView") int position) {

        try {

            CartItem p = products.get(position);
            holder.tvName.setText(p.getProductName());
            holder.tvQuantity.setText(p.getQuantity() + " QTY");
            holder.tvPrice.setText(convertPriceText(p.getProductPrice()));


            if (p.getDeliveryAddress() != null) {
                StringBuilder builder = new StringBuilder();
                if (p.getDeliveryAddress().getAddress1() != null && !TextUtils.isEmpty(p.getDeliveryAddress().getAddress1()))
                    builder.append(p.getDeliveryAddress().getAddress1()).append(",");

                if (p.getDeliveryAddress().getAddress2() != null && !TextUtils.isEmpty(p.getDeliveryAddress().getAddress2()))
                    builder.append(p.getDeliveryAddress().getAddress2()).append(",");

                if (p.getDeliveryAddress().getCity() != null && !TextUtils.isEmpty(p.getDeliveryAddress().getCity()))
                    builder.append(p.getDeliveryAddress().getCity());

                if (p.getDeliveryAddress().getState() != null && !TextUtils.isEmpty(p.getDeliveryAddress().getState()))
                    builder.append(",").append(p.getDeliveryAddress().getState());

                holder.tvLocation.setText(builder.toString().trim());
                holder.tvLocation.setVisibility(View.VISIBLE);

            } else
                holder.tvLocation.setVisibility(View.GONE);


            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ProductDetailActivity.class);
                    intent.putExtra(TAG_PRODUCT_ID, p.getProductId());
                    intent.putExtra(TAG_POSITION,position);
                    context.startActivity(intent);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return products != null ? products.size() : 0;
    }



    static class RvOrderItemHolder extends RecyclerView.ViewHolder {
        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.textView_itemMyOrderProduct_name)
        TextView tvName;
        @BindView(R.id.textView_itemMyOrderProduct_quantity)
        TextView tvQuantity;
        @BindView(R.id.textView_itemMyOrderProduct_price)
        TextView tvPrice;
        @BindView(R.id.textView_itemOrderDetail_location)
        TextView tvLocation;

        RvOrderItemHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
