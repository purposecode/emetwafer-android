package com.purposecodes.emetwafar.features.order;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.model.TimeLineMessage;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RvTimeLineAdapter extends RecyclerView.Adapter<RvTimeLineAdapter.RvTimeLineHolder> {

    private final List<TimeLineMessage> messageList;
    private Context context;

    public RvTimeLineAdapter(List<TimeLineMessage> messageArray, Context context) {
        this.messageList = messageArray;
        this.context = context;
    }

    @NonNull
    @Override
    public RvTimeLineHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_timeline_message, parent, false);
        this.context = parent.getContext();
        return new RvTimeLineHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RvTimeLineHolder holder, int position) {

        try {
            TimeLineMessage trackMsg = messageList.get(position);
            String msg = trackMsg.getMessage();
            holder.tvName.setText(msg);

            holder.ivMarker.setImageDrawable(messageList.size()-1==position?context.getResources().getDrawable(R.drawable.ic_timeline_green):context.getResources().getDrawable(R.drawable.ic_timeline_blue));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return messageList != null ? messageList.size() : 0;
    }

    static class RvTimeLineHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textView_itemTimeLine_message)
        TextView tvName;

        @BindView(R.id.imageView_itemTimeLine_marker)
        ImageView ivMarker;

        RvTimeLineHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
