package com.purposecodes.emetwafar.features.checkout;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.purposecodes.emetwafar.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RvPaymentTypeAdapter extends RecyclerView.Adapter<RvPaymentTypeAdapter.RvPaymentTypeHolder> {
    private final String TAG = "RvPaymentTypeAdapter";
    // Context
    private final Context mContext;


    // List
    private final String[] array;

    private final OnPaymentTypeClickListener listener;
    private int SELECTED_POSITION = 0;

    public RvPaymentTypeAdapter(Context mContext, String[] mArray, OnPaymentTypeClickListener _listener) {
        this.mContext = mContext;
        this.array = mArray;
        this.listener = _listener;
    }


    @NonNull
    @Override
    public RvPaymentTypeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_payment_type, parent, false);
        return new RvPaymentTypeHolder(view);
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onBindViewHolder(@NonNull RvPaymentTypeHolder holder, @SuppressLint("RecyclerView") int position) {
        String offer = array[position];
        holder.btnPayment.setText(offer);

        holder.btnPayment.setSelected(SELECTED_POSITION == position);

        holder.btnPayment.setOnClickListener(v -> {
            SELECTED_POSITION = position;
            notifyDataSetChanged();

            if (listener != null)
                listener.onPaymentTypeSelected(offer, position);
        });
    }

    @Override
    public int getItemCount() {
        return (array != null ? array.length : 0);

    }


    static class RvPaymentTypeHolder extends RecyclerView.ViewHolder {

        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.button_itemPayment)
        Button btnPayment;


        RvPaymentTypeHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    public interface OnPaymentTypeClickListener {
        void onPaymentTypeSelected(String payment, int position);

    }
}
