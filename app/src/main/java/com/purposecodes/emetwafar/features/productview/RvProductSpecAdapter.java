package com.purposecodes.emetwafar.features.productview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.holder.RvBlankViewHolder;
import com.purposecodes.emetwafar.holder.RvTitleHolder;
import com.purposecodes.emetwafar.model.ProductSpec;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RvProductSpecAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final List<ProductSpec> specList;

    private static final int TYPE_TITLE = 1;
    private static final int TYPE_ITEM = 2;
    private static final int TYPE_BLANK = 0;

    public RvProductSpecAdapter(List<ProductSpec> list) {
        this.specList = list;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        View view;
        switch (viewType) {

            case TYPE_TITLE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_title, parent, false);
                return new RvTitleHolder(view);

            case TYPE_ITEM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_spec, parent, false);
                return new RvProductSpecHolder(view);

            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_blank, parent, false);
                return new RvBlankViewHolder(view);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {


        int viewType = getItemViewType(position);
        switch (viewType) {

            case TYPE_TITLE:
                RvTitleHolder titleHolder = (RvTitleHolder) viewHolder;
                titleHolder.tvTitle.setText("PRODUCT SPECIFICATION");

                break;
            case TYPE_ITEM:
                try {
                    RvProductSpecHolder holder = (RvProductSpecHolder) viewHolder;
                    ProductSpec spec = specList.get(position - 1);
                    holder.tvTitle.setText(spec.getTechnicalSpecName());
                    holder.tvDescription.setText(spec.getTechnicalSpecValue());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }

    }


    @Override
    public int getItemCount() {
        return specList != null ? specList.size() + 1 : 0;
    }


    @Override
    public int getItemViewType(int position) {

        try {
            if (position == 0) {
                return TYPE_TITLE;
            } else {

                return TYPE_ITEM;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            return TYPE_BLANK;
        }
    }

    public class RvProductSpecHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textView_itemProductSpec_title)
        TextView tvTitle;
        @BindView(R.id.textView_itemProductSpec_desc)
        TextView tvDescription;


        public RvProductSpecHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
