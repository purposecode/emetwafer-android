package com.purposecodes.emetwafar.features.address;

import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_ADD;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_EDIT;
import static com.purposecodes.emetwafar.utils.LogUtils.printLog;
import static com.purposecodes.emetwafar.utils.UtilsVariable.CALLING_ACTIVITY_KEY;
import static com.purposecodes.emetwafar.utils.UtilsVariable.NO_DATA;
import static com.purposecodes.emetwafar.utils.UtilsVariable.NO_NETWORK;
import static com.purposecodes.emetwafar.utils.UtilsVariable.REQUEST_ADD_EDIT_ADDRESS;
import static com.purposecodes.emetwafar.utils.UtilsVariable.REQUEST_ADD_NEW_ADDRESS;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_POSITION;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_SAVED_ADDRESS;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_DATA;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_ERRORVIEW;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_PROGRESSBAR;
import static com.purposecodes.emetwafar.utils.UtilsVariable.WRONG_RESPONSE;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.api.ApiFailureMessageListener;
import com.purposecodes.emetwafar.api.ApiSuccessListener;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.model.SavedAddress;
import com.purposecodes.emetwafar.responsemodel.AddressResponse;
import com.purposecodes.emetwafar.responsemodel.ErrorModel;
import com.sk.icode.errorview.ErrorView;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;

public class SavedAddressActivity extends BaseActivity implements RvAddressAdapter.OnAddressClickListener {

    private final String TAG = "SavedAddressActivity";
    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.errorView)
    ErrorView errorView;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.recyclerView_layout_list)
    RecyclerView recyclerView;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.view_savedList)
    ViewGroup viewData;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.button_savedAddress_addAddress)
    Button btnAddAddress;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_savedAddress_title)
    TextView tvTitle;

    private RvAddressAdapter addressAdapter;
    // RecyclerView date set
    private ArrayList<SavedAddress> addressList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        addressList = new ArrayList<>();

        setUpRecyclerView();

        getAddressLists();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_saved_address;
    }


    @SuppressLint("NotifyDataSetChanged")
    private void setUpRecyclerView() {

        updateViews(VIEW_DATA);

        addressAdapter = new RvAddressAdapter(this, addressList, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(addressAdapter);
        recyclerView.setNestedScrollingEnabled(true);
        addressAdapter.notifyDataSetChanged();

    }


    private void getAddressLists() {
        if (AppController.get().isNetworkAvailable()) {
            updateViews(VIEW_PROGRESSBAR);
            AppController.get().getAddresses();
        } else
            setErrorView(getString(R.string.no_internet), "", false, NO_NETWORK);
    }


    //set ErrorView
    private void setErrorView(final String title, final String subTitle, boolean isRetry, int drawable) {

        updateViews(VIEW_ERRORVIEW);

        errorView.setConfig(ErrorView.Config.create()
                .title(title)
                .subtitle(subTitle)
                .retryVisible(isRetry)
                .image(drawable)
                .build());

        errorView.setOnRetryListener(() -> getAddressLists());
    }

    private void updateViews(int viewCode) {
        switch (viewCode) {
            case VIEW_PROGRESSBAR:
                showProgress();

                errorView.setVisibility(View.GONE);
                break;
            case VIEW_DATA:
                dismissProgress();
                viewData.setVisibility(View.VISIBLE);
                errorView.setVisibility(View.GONE);

                break;

            case VIEW_ERRORVIEW:
                dismissProgress();
                viewData.setVisibility(View.GONE);
                errorView.setVisibility(View.VISIBLE);
                break;

        }
    }


    @SuppressLint("NotifyDataSetChanged")
    @Subscribe
    public void onHttpGetAddressListSuccess(ApiSuccessListener<AddressResponse> response) {

        if (response.type.equalsIgnoreCase("getAddresses")) {

            AddressResponse responseModel = response.response.body();
            if (responseModel != null && responseModel.getStatus()) {
                updateViews(VIEW_DATA);

                addressList.clear();
                List<SavedAddress> list = responseModel.getSavedAddressList();
                if (list != null && list.size() > 0) {
                    addressList.addAll(list);
                }

                if (addressList.isEmpty())
                    setErrorView("Your Addresses is empty!", "", false, NO_DATA);

                addressAdapter.notifyDataSetChanged();

            } else {
                assert responseModel != null;
                if (responseModel.getText() != null && TextUtils.isEmpty(responseModel.getText()))
                    setErrorView(responseModel.getText(), "", false, NO_DATA);
                else if (addressList.isEmpty())
                    setErrorView("Your Addresses is empty!", "", false, NO_DATA);
            }

            updateCount();

        }
    }

    @SuppressLint("NotifyDataSetChanged")
    @Subscribe
    public void onHttpRemoveAddressSuccess(ApiSuccessListener<JsonObject> response) {

        if (response.type.equalsIgnoreCase("removeAddress")) {
            updateViews(VIEW_DATA);

            try {
                JsonObject jsonObject = response.response.body();
                assert jsonObject != null;
                if (jsonObject.get("status").getAsBoolean()) {

                    addressList.remove(response.position);
                    addressAdapter.notifyDataSetChanged();
                    showToast(jsonObject.get("text").getAsString());

                    if (addressList.isEmpty())
                        setErrorView("Your Address is empty!", "", true, NO_DATA);

                } else {
                    showToast(jsonObject.get("text").getAsString());
                }

                updateCount();
            } catch (Exception e) {
                printLog(TAG, " onHttpRemoveAddressSuccess Exception : " + e.getMessage());
            }
        }
    }


    @Subscribe
    public void onHttpError(ApiFailureMessageListener<ErrorModel> response) {

        if (response.type.equalsIgnoreCase("getAddresses") || response.type.equalsIgnoreCase("removeAddress")) {
            updateViews(VIEW_DATA);
            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);

                if (addressList.isEmpty())
                    setErrorView(errorModel.getText(), "", false, WRONG_RESPONSE);
                else
                    showToast(errorModel.getText());

            } catch (Exception e) {
                if (addressList.isEmpty())
                    setErrorView(getResources().getString(R.string.error_try_again), getString(R.string.error_subtitle_failed_one_more_time), true, WRONG_RESPONSE);
                else
                    showToast(getResources().getString(R.string.error_try_again));


            }
        }
    }


    @Subscribe
    public void onHttpFailure(ApiFailureListener response) {

        if (response.type.equalsIgnoreCase("getAddresses") || response.type.equalsIgnoreCase("removeAddress")) {
            updateViews(VIEW_DATA);


            if (addressList.isEmpty())
                setErrorView(getResources().getString(R.string.error_try_again), getString(R.string.error_subtitle_failed_one_more_time), true, WRONG_RESPONSE);
            else
                showToast(getResources().getString(R.string.error_try_again));


        }
    }

    @OnClick({R.id.button_savedAddress_addAddress})
    public void onStartCreateAddress(View view) {


        Intent intent = new Intent(SavedAddressActivity.this, AddNewAddressActivity.class);
        intent.putExtra(CALLING_ACTIVITY_KEY, ACTIVITY_ADD);
        startActivityForResult(intent, REQUEST_ADD_NEW_ADDRESS);


    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {
                if (requestCode == REQUEST_ADD_NEW_ADDRESS && data != null) {
                    SavedAddress address = (SavedAddress) data.getSerializableExtra(TAG_SAVED_ADDRESS);

                    addressList.add(address);
                    if (addressList.isEmpty())
                        setErrorView("Your List is empty!", "", true, NO_DATA);
                    else
                        updateViews(VIEW_DATA);

                    addressAdapter.notifyDataSetChanged();
                    updateCount();
                } else if (requestCode == REQUEST_ADD_EDIT_ADDRESS && data != null) {
                    SavedAddress address = (SavedAddress) data.getSerializableExtra(TAG_SAVED_ADDRESS);
                    int pos = data.getIntExtra(TAG_POSITION, -1);


                    updateViews(VIEW_DATA);
                    if (pos != -1 && addressList.size() > pos) {
                        addressList.set(pos, address);
                        addressAdapter.notifyDataSetChanged();
                    } else
                        getAddressLists();


                }
            }
        } catch (IndexOutOfBoundsException e) {
            e.getLocalizedMessage();
        }
    }

    @Override
    public void onEditClicked(SavedAddress savedAddress, int position) {

        Intent intent = new Intent(SavedAddressActivity.this, AddNewAddressActivity.class);
        intent.putExtra(CALLING_ACTIVITY_KEY, ACTIVITY_EDIT);
        intent.putExtra(TAG_SAVED_ADDRESS, savedAddress);
        intent.putExtra(TAG_POSITION, position);
        startActivityForResult(intent, REQUEST_ADD_EDIT_ADDRESS);

    }

    @Override
    public void onDeleteClicked(SavedAddress savedAddress, int position) {

        if (AppController.get().isNetworkAvailable()) {

            showDeleteAlert(savedAddress, position);
        } else
            showToast(getString(R.string.no_internet));

    }


    private void showDeleteAlert(final SavedAddress savedAddress, final int position) {

        AlertDialog.Builder alert = new AlertDialog.Builder(SavedAddressActivity.this);
        alert.setMessage("Do you really want to remove the Address?");
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                updateViews(VIEW_PROGRESSBAR);
                AppController.get().removeAddress(savedAddress.getAddressId(), position);

            }
        });
        alert.setNegativeButton("No", (dialogInterface, i) -> dialogInterface.dismiss());
        alert.create().show();
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    private void updateCount() {

        if (addressList.size() != 0) {
            tvTitle.setText(String.valueOf(addressList.size()));
            tvTitle.append(" ");
            tvTitle.append(getText(R.string.saved_address));
        } else {
            tvTitle.setText(getText(R.string.saved_address));
        }
    }
}
