package com.purposecodes.emetwafar.features.productview;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity;
import com.purposecodes.emetwafar.activity.WebViewActivity;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.api.ApiFailureMessageListener;
import com.purposecodes.emetwafar.api.ApiSuccessListener;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.controller.SessionController;
import com.purposecodes.emetwafar.features.checkout.CheckoutActivity;
import com.purposecodes.emetwafar.features.mycart.MyCartActivity;
import com.purposecodes.emetwafar.model.Product;
import com.purposecodes.emetwafar.model.ProductSpec;
import com.purposecodes.emetwafar.model.SliderImage;
import com.purposecodes.emetwafar.model.Warranty;
import com.purposecodes.emetwafar.responsemodel.CheckoutResponse;
import com.purposecodes.emetwafar.responsemodel.ErrorModel;
import com.purposecodes.emetwafar.responsemodel.ProductDetailsResponse;
import com.purposecodes.emetwafar.utils.PermissionUtils;
import com.purposecodes.emetwafar.view.RecyclerViewMarginDividerItemDecoration;
import com.sk.icode.errorview.ErrorView;
import com.squareup.otto.Subscribe;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.purposecodes.emetwafar.controller.SessionController.getWishQuantity;
import static com.purposecodes.emetwafar.controller.SessionController.setCartQuantity;
import static com.purposecodes.emetwafar.controller.SessionController.setWishQuantity;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_BUYNOW;
import static com.purposecodes.emetwafar.utils.FileUtils.PDF;
import static com.purposecodes.emetwafar.utils.FileUtils.fileDirectoryGenerator;
import static com.purposecodes.emetwafar.utils.FileUtils.getFileNameWithExtension;
import static com.purposecodes.emetwafar.utils.LogUtils.printLog;
import static com.purposecodes.emetwafar.utils.UtilsVariable.CALLING_ACTIVITY_KEY;
import static com.purposecodes.emetwafar.utils.UtilsVariable.DEFAULT_CURRENCY;
import static com.purposecodes.emetwafar.utils.UtilsVariable.NO_DATA;
import static com.purposecodes.emetwafar.utils.UtilsVariable.NO_NETWORK;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_BUY_NOW_ID;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_CHECKOUT;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_IS_WISHED;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_PDF_URL;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_POSITION;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_PRODUCT_ID;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_DATA;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_ERRORVIEW;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_PROGRESSBAR;
import static com.purposecodes.emetwafar.utils.UtilsVariable.WRONG_RESPONSE;

public class ProductDetailActivity extends BaseActivity {


    @SuppressLint("StaticFieldLeak")
    private static ProductDetailActivity productDetailActivity;

    private final String TAG = "ProductDetailActivity";

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_productDetail_date)
    TextView tvDate;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_productDetail_category)
    TextView tvCategory;
    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_productDetail_name)
    TextView tvProductName;


    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_productDetail_description)
    TextView tvDescription;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_productDetail_code)
    TextView tvCode;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_productDetail_amount)
    TextView tvPrice;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_productDetail_offer)
    TextView tvOffer;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_productDetail_sellerId)
    TextView tvSellerId;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_productDetail_location)
    TextView tvLocation;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_productDetail_warrantyDays)
    TextView tvWarrantyDays;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_productDetail_notAvailable)
    TextView viewNotAvailable;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.checkBox_productDetail_warrantyNeed)
    CheckBox cbNeedWarranty;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.editText_productDetail_Qty)
    EditText etQuantity;


    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.spinner_productDetail_warranty)
    AppCompatSpinner spinnerWarranty;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.button_productDetail_buyNow)
    Button btnBuyNow;


    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.button_productDetail_addToCart)
    Button btnAddToCart;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.button_productDetail_download)
    Button btnDownloadTDS;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.recyclerView_productDetail_specification)
    RecyclerView rvSpecification;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.errorView)
    ErrorView errorView;


    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.imageButton_productDetail_wish)
    ImageButton ibWish;


    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.view_productDetail)
    ViewGroup dataView;


    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.view_productDetail_warrantyList)
    ViewGroup warrantyView;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.viewPager_productDetail_images)
    ViewPager viewPagerImage;


    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.layout_sliderDots)
    LinearLayout sliderDotspanel;

    private ImageView[] dots;

    private String SELECTED_PRODUCT_ID;
    private int SELECTED_POSITION;
    private String downloadTDSUrl = "";
    //    private String productNormalWarranty;
    private static final int PERMISSION_REQUEST_WRITE_STORAGE = 112;
    private static int currentImage = 0;

    private ProductDetailsResponse.ProductDetail productDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        try {
            SELECTED_PRODUCT_ID = getIntent().getStringExtra(TAG_PRODUCT_ID);
            SELECTED_POSITION = getIntent().getIntExtra(TAG_POSITION, -1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        productDetailActivity = this;
        loadProductDetail();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_product_detail;
    }

    private void loadProductDetail() {

        if (AppController.get().isNetworkAvailable()) {

            updateViews(VIEW_PROGRESSBAR);
            AppController.get().getProductDetail(SELECTED_PRODUCT_ID);
        } else
            setErrorView(getString(R.string.no_internet), "", false, NO_NETWORK);

    }


    //set ErrorView
    private void setErrorView(final String title, final String subTitle, boolean isRetry, int drawable) {

        updateViews(VIEW_ERRORVIEW);
        errorView.setConfig(ErrorView.Config.create()
                .title(title)
                .subtitle(subTitle)
                .retryVisible(isRetry)
                .image(drawable)
                .build());


        errorView.setOnRetryListener(() -> loadProductDetail());
    }

    private void updateViews(int viewCode) {

        switch (viewCode) {


            case VIEW_PROGRESSBAR:
                showProgress();

                errorView.setVisibility(GONE);
                dataView.setVisibility(GONE);
                break;
            case VIEW_DATA:
                dismissProgress();
                dataView.setVisibility(VISIBLE);
                errorView.setVisibility(GONE);

                break;

            case VIEW_ERRORVIEW:
                dismissProgress();
                dataView.setVisibility(GONE);
                errorView.setVisibility(VISIBLE);
                break;

        }
    }


    @Subscribe
    public void onHttpGetProductDetailsSuccess(ApiSuccessListener<ProductDetailsResponse> response) {

        if (response.type.equalsIgnoreCase("getProductDetail")) {

            ProductDetailsResponse responseModel = response.response.body();
            if (responseModel != null && responseModel.getStatus()) {

                productDetail = responseModel.getProductDetail();

                if (productDetail.getAvailableQuantity() == null || TextUtils.isEmpty(productDetail.getAvailableQuantity()))
                    productDetail.setAvailableQuantity(String.valueOf(1));


                initProductView(productDetail);

            } else if (responseModel.getText() != null && TextUtils.isEmpty(responseModel.getText()))

                setErrorView(responseModel.getText(), "", false, WRONG_RESPONSE);

            else
                setErrorView("No Detail found!", "", false, NO_DATA);


        }
    }


    @Subscribe
    public void onHttpWishChangeSuccess(ApiSuccessListener<JsonObject> response) {

        if (response.type.equalsIgnoreCase("addToWish_" + "detailPage")) {

            try {
                JsonObject jsonObject = response.response.body();
                assert jsonObject != null;
                if (jsonObject.get("status").getAsBoolean()) {
                    JsonObject productObject = jsonObject.get("data").getAsJsonObject().get("product").getAsJsonObject();
                    int wishQuantity = jsonObject.get("data").getAsJsonObject().get("wish_total").getAsInt();
                    Product p = new Gson().fromJson(productObject, Product.class);

                    productDetail.setIsInWishlist(p.getIsWishlist());
                    ibWish.setSelected(productDetail.getIsInWishlist() == 1);

                    setWishQuantity(wishQuantity);
                    showToast(jsonObject.get("text").getAsString());
                } else {
                    showToast(jsonObject.get("text").getAsString());
                }
            } catch (Exception e) {
                printLog(TAG, " onHttpWishChangeSuccess Exception : " + e.getMessage());
            }
        } else if (response.type.equalsIgnoreCase("removeFromWish_" + "detailPage")) {

            try {
                JsonObject jsonObject = response.response.body();
                assert jsonObject != null;
                if (jsonObject.get("status").getAsBoolean()) {
                    JsonObject productObject = jsonObject.get("data").getAsJsonObject().get("product").getAsJsonObject();
                    Product p = new Gson().fromJson(productObject, Product.class);


                    productDetail.setIsInWishlist(p.getIsWishlist());

                    ibWish.setSelected(productDetail.getIsInWishlist() == 1);

                    setWishQuantity((p.getIsWishlist() == 1) ? getWishQuantity() + 1 : getWishQuantity() - 1);

                    showToast(jsonObject.get("text").getAsString());
                } else {
                    showToast(jsonObject.get("text").getAsString());
                }
            } catch (Exception e) {
                printLog(TAG, " onHttpWishChangeSuccess removeFromWish Exception : " + e.getMessage());
            }

        } else if (response.type.equalsIgnoreCase("addToCart")) {
            updateViews(VIEW_DATA);
            try {
                JsonObject jsonObject = response.response.body();
                assert jsonObject != null;
                if (jsonObject.get("status").getAsBoolean() && jsonObject.get("text").getAsString().equalsIgnoreCase("Successfully added to cart")) {
                    int cartQuantity = jsonObject.get("data").getAsJsonObject().get("cart_total").getAsInt();

                    selectedCartButton(true);

                    showToast(jsonObject.get("text").getAsString());
                    setCartQuantity(cartQuantity);  // update cart count
                } else {
                    showToast(jsonObject.get("text").getAsString());
                }
            } catch (Exception e) {
                printLog(TAG, " onHttpWishChangeSuccess removeFromWish Exception : " + e.getMessage());
            }

        }
    }

    @Subscribe
    public void onHttpGetCheckoutListSuccess(ApiSuccessListener<CheckoutResponse> response) {

        if (response.type.equalsIgnoreCase("buyNow")) {

            CheckoutResponse responseModel = response.response.body();
            if (responseModel != null && responseModel.getStatus()) {

                if (responseModel.getCheckoutData().getCartItems() != null && responseModel.getCheckoutData().getCartItems().size() > 0) {

                    Intent checkoutIntent = new Intent(ProductDetailActivity.this, CheckoutActivity.class);
                    checkoutIntent.putExtra(TAG_CHECKOUT, responseModel.getCheckoutData());
                    checkoutIntent.putExtra(CALLING_ACTIVITY_KEY, ACTIVITY_BUYNOW);
                    checkoutIntent.putExtra(TAG_BUY_NOW_ID, responseModel.getCheckoutData().getCartId());
                    startActivity(checkoutIntent);
                } else {
                    showToast(getResources().getString(R.string.error_try_again));
                }

                updateViews(VIEW_DATA);


            } else if (responseModel.getText() != null && TextUtils.isEmpty(responseModel.getText()))
                showToast(responseModel.getText());
            else
                showToast(getResources().getString(R.string.error_try_again));
        }
    }

    @Subscribe
    public void onHttpError(ApiFailureMessageListener<ErrorModel> response) {

        if (response.type.equalsIgnoreCase("getProductDetail") || response.type.equalsIgnoreCase("addToCart") || response.type.equalsIgnoreCase("buyNow")) {

            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                if (response.type.equalsIgnoreCase("getProductDetail"))
                    setErrorView(errorModel.getText(), "", false, WRONG_RESPONSE);
                else {
                    updateViews(VIEW_DATA);
                    showToast(errorModel.getText());
                }

            } catch (Exception e) {
                e.printStackTrace();
                setErrorView(getResources().getString(R.string.error_try_again), getString(R.string.error_subtitle_failed_one_more_time), true, WRONG_RESPONSE);

            }
        } else if (response.type.equalsIgnoreCase("addToWish_detailPage") || response.type.equalsIgnoreCase("removeFromWish_detailPage")) {

            updateViews(VIEW_DATA);
            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);

                ibWish.setSelected(productDetail.getIsInWishlist() == 1);
                showToast(errorModel.getText());

            } catch (Exception e) {
                e.printStackTrace();
                showToast(getResources().getString(R.string.error_try_again));

            }
        }
    }


    @Subscribe
    public void onHttpFailure(ApiFailureListener response) {

        if (response.type.equalsIgnoreCase("getProductDetail")) {
            setErrorView(getResources().getString(R.string.error_try_again), getString(R.string.error_subtitle_failed_one_more_time), true, WRONG_RESPONSE);
        } else if (response.type.equalsIgnoreCase("addToCart") || response.type.equalsIgnoreCase("buyNow")) {

            updateViews(VIEW_DATA);
            showToast(getResources().getString(R.string.error_try_again));

        } else if (response.type.equalsIgnoreCase("addToWish_detailPage") || response.type.equalsIgnoreCase("removeFromWish_detailPage")) {

            updateViews(VIEW_DATA);

            ibWish.setSelected(productDetail.getIsInWishlist() == 1);
            showToast(getResources().getString(R.string.error_try_again));

        }
    }


    private void selectedCartButton(boolean isSelected) {

        btnAddToCart.setSelected(isSelected);
        btnAddToCart.setText(isSelected ? "GO TO CART" : "ADD TO CART");

    }

    private void initProductView(ProductDetailsResponse.ProductDetail _productDetail) {

        try {

            updateViews(VIEW_DATA);
            updateImageViewer(_productDetail.getSliderImages());

            if (_productDetail.getTechSpecUrl() != null && !TextUtils.isEmpty(_productDetail.getTechSpecUrl())) {
                btnDownloadTDS.setVisibility(VISIBLE);

                btnDownloadTDS.setOnClickListener(v -> {
                    downloadTDSUrl = _productDetail.getTechSpecUrl();
//                    checkWritePermission();

                    Intent webIntent = new Intent(ProductDetailActivity.this, WebViewActivity.class);
                    webIntent.putExtra(TAG_PDF_URL, downloadTDSUrl);
                    startActivity(webIntent);
                });

            }

            SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
            String currentDateandTime = sdf.format(new Date());


            ibWish.setSelected(_productDetail.getIsInWishlist() == 1);
            selectedCartButton(_productDetail.getIsInCart() == 1);


            if (_productDetail.getIsAvailable() != null) {
                viewNotAvailable.setVisibility(_productDetail.getIsAvailable() == 0 ? VISIBLE : GONE);
                btnBuyNow.setVisibility((_productDetail.getIsAvailable() == 1) ? VISIBLE : GONE);
                btnAddToCart.setVisibility((_productDetail.getIsAvailable() == 1) ? VISIBLE : GONE);
            }
            tvDate.setText(currentDateandTime);

            tvCategory.setText(_productDetail.getCategoryName());
            tvProductName.setText(_productDetail.getProductName());
            tvDescription.setText(_productDetail.getProductDesc());

            tvWarrantyDays.setText(_productDetail.getNormalWarranty());

            tvCode.setText(convertCodeText(_productDetail.getProductCode()));
            if (!TextUtils.isEmpty(_productDetail.getOfferPrice()) && !_productDetail.getOfferPrice().equalsIgnoreCase("0")) {
                tvPrice.setText(convertPriceText(_productDetail.getOfferPrice()));
                tvOffer.setText(convertOfferText(_productDetail.getProductPrice(), _productDetail.getDiscount()));
                tvOffer.setVisibility(VISIBLE);
            } else {
                tvPrice.setText(convertPriceText(_productDetail.getProductPrice()));
                tvOffer.setVisibility(GONE);
            }


            tvSellerId.setText(convertSellerIdText(_productDetail.getSupplier()));
            tvLocation.setText(_productDetail.getCountry());

            ibWish.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (AppController.get().isNetworkAvailable()) {
                        ibWish.setSelected(productDetail.getIsInWishlist() != 1);
                        Map<String, String> options = new HashMap<>();
                        options.put("product_id", _productDetail.getProductId());
                        AppController.get().addToWish(options, "detailPage", 0);
                    } else {
                        showToast(getString(R.string.no_internet));
                        ibWish.setSelected(productDetail.getIsInWishlist() == 1);

                    }
                }
            });

            btnAddToCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (!btnAddToCart.isSelected())
                        addToCart(_productDetail);
                    else
                        startActivity(new Intent(ProductDetailActivity.this, MyCartActivity.class));

                }
            });

            btnBuyNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    buyNow(_productDetail);
                }
            });


            if (_productDetail.getProductSpecs() != null && !_productDetail.getProductSpecs().isEmpty())
                setDescriptionRecyclerView(_productDetail.getProductSpecs());


            if (_productDetail.getWarranties() != null && !_productDetail.getWarranties().isEmpty())
                setWarrantyList(_productDetail.getWarranties());
            else
                warrantyView.setVisibility(GONE);


            spinnerWarranty.setEnabled(cbNeedWarranty.isChecked()); //default
            spinnerWarranty.setVisibility(cbNeedWarranty.isChecked() ? VISIBLE : GONE);
            cbNeedWarranty.setOnCheckedChangeListener((buttonView, isChecked) -> {

                try {
                    spinnerWarranty.setVisibility(isChecked ? VISIBLE : GONE);

                    spinnerWarranty.setSelection(0);
                    spinnerWarranty.setEnabled(isChecked);

                    Warranty item = (Warranty) spinnerWarranty.getSelectedItem();
//                tvWarrantyDays.setText((isChecked&&spinnerWarranty.getSelectedItemPosition()!=0)?item.getWarrantyPeriod():productNormalWarranty);

                } catch (Exception e) {
                    printLog(TAG, " Number exception : " + e.getMessage());
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateImageViewer(List<SliderImage> images) {

        if (images == null || images.isEmpty()) {


            if (productDetail.getImageUrl() != null && !TextUtils.isEmpty(productDetail.getImageUrl())) { //images can't case set default image
                if (images == null)
                    images = new ArrayList<SliderImage>();

                SliderImage im = new SliderImage();
                im.setDefault("0");
                im.setImageId("0");
                im.setProductId(productDetail.getProductId());
                im.setImageUrl(productDetail.getImageUrl());

                images.add(im);
            } else
                return;
        }

        List<SliderImage> finalImages = images;

        ImagePagerAdapter myCustomPagerAdapter = new ImagePagerAdapter(ProductDetailActivity.this, finalImages);
        viewPagerImage.setAdapter(myCustomPagerAdapter);


        int dotscount = myCustomPagerAdapter.getCount();

        dots = new ImageView[dotscount];

        for (int i = 0; i < dotscount; i++) {

            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.nonactive_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            params.setMargins(8, 0, 8, 0);

            sliderDotspanel.addView(dots[i], params);


        }

        dots[0].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));

        viewPagerImage.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                for (int i = 0; i < dotscount; i++) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.nonactive_dot));
                }

                dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        // Auto start of viewpager
        final Handler handler = new Handler();

        final Runnable Update = new Runnable() {
            public void run() {
                if (currentImage == finalImages.size()) {
                    currentImage = 0;
                }
                viewPagerImage.setCurrentItem(currentImage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 5000, 3000);

    }


    @OnClick({R.id.imageButton_productDetail_QtyDown, R.id.imageButton_productDetail_QtyUp, R.id.imageButton_productDetail_backBase, R.id.imageButton_productDetail_back, R.id.imageButton_productDetail_share})
    public void onButtonClick(View view) {
        hideSoftKeyboard();
        switch (view.getId()) {
            case R.id.imageButton_productDetail_QtyDown:

                try {

                    String text = etQuantity.getText().toString().trim();
                    long previousSize = TextUtils.isEmpty(text) ? 0 : Long.parseLong(text);


                    /**decrement numbers up to 1*/
                    if (previousSize > 1) {
                        previousSize--;
                        etQuantity.setText("");
                        etQuantity.append(String.valueOf(previousSize));
                    }

                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.imageButton_productDetail_QtyUp:

                try {
                    String text = etQuantity.getText().toString().trim();


                    long nextSize = TextUtils.isEmpty(text) ? 0 : Long.parseLong(text);


                    long stock = (TextUtils.isEmpty(productDetail.getAvailableQuantity())) ? 1 : Long.parseLong(productDetail.getAvailableQuantity());
                    /**increment numbers */
                    if (nextSize < stock) {
                        nextSize++;
                        etQuantity.setText("");
                        etQuantity.append(String.valueOf(nextSize));
                    } else
                        showToast("Maximum stock is " + productDetail.getAvailableQuantity());


                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }

                break;

            case R.id.imageButton_productDetail_backBase:
            case R.id.imageButton_productDetail_back:
                onBackPressed();

                break;
            case R.id.imageButton_productDetail_share:

                break;

        }


    }

    private void setDescriptionRecyclerView(List<ProductSpec> list) {

        rvSpecification.setVisibility(VISIBLE);
        RvProductSpecAdapter rvAdapter = new RvProductSpecAdapter(list);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvSpecification.addItemDecoration(new RecyclerViewMarginDividerItemDecoration(this));

        rvSpecification.setHasFixedSize(true);
        rvSpecification.setLayoutManager(layoutManager);
        rvSpecification.setAdapter(rvAdapter);
        rvSpecification.setNestedScrollingEnabled(false);

    }


    private void setWarrantyList(ArrayList<Warranty> warrantyList) {
        warrantyView.setVisibility(VISIBLE);

        Warranty w = new Warranty();
        w.setWarrantyPeriod("Add additional warranty");
        w.setProductId("");
        w.setStatus("");
        w.setWarrantyId("");
        w.setWarrantyPrice("");

        warrantyList.add(0, w);


        WarrantyArrayAdapter adapter = new WarrantyArrayAdapter(this, warrantyList);


        spinnerWarranty.setAdapter(adapter);

        spinnerWarranty.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View arg1, int arg2, long arg3) {
                String item = parent.getItemAtPosition(arg2).toString();


               /* if (cbNeedWarranty.isChecked() && arg2 != 0)
                    tvWarrantyDays.setText(item);
                else
                    tvWarrantyDays.setText(productNormalWarranty);*/


            }


            public void onNothingSelected(AdapterView<?> arg0) {


            }
        });


    }

    private SpannableStringBuilder convertPriceText(String _price) {

        String firstWord = "Price:";

// Use a SpannableStringBuilder so that both the text and the spans are mutable
        SpannableStringBuilder ssb = new SpannableStringBuilder(firstWord);
        try {
            String secondWord = _price;

// Create a span that will make the text red
            ForegroundColorSpan redForegroundColorSpan = new ForegroundColorSpan(getResources().getColor(R.color.redColor));

// Add a blank space
            ssb.append(" ");


            ssb.append(secondWord);
            // Apply the color span
            ssb.setSpan(redForegroundColorSpan, ssb.length() - secondWord.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ssb.setSpan(new StyleSpan(Typeface.BOLD), ssb.length() - secondWord.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ssb.append(" ");
            ssb.append(DEFAULT_CURRENCY);

            ssb.setSpan(new StyleSpan(Typeface.BOLD), ssb.length() - DEFAULT_CURRENCY.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        } catch (Exception e) {
            e.fillInStackTrace();
        }

// since it's a SpannableStringBuilder
        return ssb;

    }


    private SpannableStringBuilder convertOfferText(String withoutOffer, String _discount) {

        String firstWord = withoutOffer + " " + DEFAULT_CURRENCY;

        // Use a SpannableStringBuilder so that both the text and the spans are mutable
        SpannableStringBuilder ssb = new SpannableStringBuilder(firstWord);
        try {
            String secondWord = _discount + "% Offer";

//        Typeface myTypeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/poppins_semi_bold.ttf");
            Typeface myTypeface = Typeface.create(ResourcesCompat.getFont(this, R.font.poppins_semi_bold), Typeface.BOLD);

// Create a span that will make the text red
            ForegroundColorSpan greenForegroundColorSpan = new ForegroundColorSpan(getResources().getColor(R.color.greenColor));


            // Create a span that will strikethrough the text
            StrikethroughSpan strikethroughSpan = new StrikethroughSpan();


// Apply the color span
            ssb.setSpan(
                    strikethroughSpan,            // the span to add
                    0,                                 // the start of the span (inclusive)
                    ssb.length(),                      // the end of the span (exclusive)
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE); // behavior when text is later inserted into the SpannableStringBuilder
            // SPAN_EXCLUSIVE_EXCLUSIVE means to not extend the span when additional
            // text is added in later
            ssb.setSpan(myTypeface, 0, ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
// Add a blank space
            ssb.append("   ");


// Add the secondWord and apply the strikethrough span to only the second word
            ssb.append(secondWord);
            ssb.setSpan(greenForegroundColorSpan, ssb.length() - secondWord.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

// Set the TextView text and denote that it is Editable
// since it's a SpannableStringBuilder
        } catch (Exception e) {
            e.fillInStackTrace();
        }
        return ssb;

    }


    private SpannableStringBuilder convertSellerIdText(String _sellerId) throws NullPointerException {

        String firstWord = "Seller ID :";
        // Use a SpannableStringBuilder so that both the text and the spans are mutable
        SpannableStringBuilder ssb = new SpannableStringBuilder(firstWord);
        try {


            StyleSpan boldSpan = new StyleSpan(Typeface.BOLD);
            String secondWord = _sellerId;


// Create a span that will make the text red
            ForegroundColorSpan blackForegroundColorSpan = new ForegroundColorSpan(getResources().getColor(R.color.primaryText));
// Add a blank space
            ssb.append(" ");
// Add the secondWord and apply the strikethrough span to only the second word
            ssb.append(secondWord);
            ssb.setSpan(blackForegroundColorSpan, ssb.length() - secondWord.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ssb.setSpan(boldSpan, ssb.length() - secondWord.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

// Set the TextView text and denote that it is Editable
// since it's a SpannableStringBuilder
        } catch (Exception e) {
            e.fillInStackTrace();
        }
        return ssb;

    }

    private SpannableStringBuilder convertCodeText(String _code) {

        String firstWord = "Code :";
        // Use a SpannableStringBuilder so that both the text and the spans are mutable
        SpannableStringBuilder ssb = new SpannableStringBuilder(firstWord);
        try {


            String secondWord = _code;


            StyleSpan boldSpan = new StyleSpan(Typeface.BOLD);
// Create a span that will make the text red
            ForegroundColorSpan blackForegroundColorSpan = new ForegroundColorSpan(getResources().getColor(R.color.primaryText));


// Add a blank space
            ssb.append(" ");

// Add the secondWord and apply the strikethrough span to only the second word
            ssb.append(secondWord);
            ssb.setSpan(blackForegroundColorSpan, ssb.length() - secondWord.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ssb.setSpan(boldSpan, ssb.length() - secondWord.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

// Set the TextView text and denote that it is Editable
// since it's a SpannableStringBuilder
        } catch (Exception e) {
            e.fillInStackTrace();
        }
        return ssb;

    }


    private void addToCart(ProductDetailsResponse.ProductDetail productDetail) {

        hideSoftKeyboard();
        String qtyString = etQuantity.getText().toString().trim();

        long quantity = TextUtils.isEmpty(qtyString) ? 0 : Long.parseLong(qtyString);


        long stock = (TextUtils.isEmpty(productDetail.getAvailableQuantity())) ? 1 : Long.parseLong(productDetail.getAvailableQuantity());


        if (quantity == 0)
            showToast("Please enter a valid quantity");
        else if (stock < quantity)
            showToast("Maximum stock is " + productDetail.getAvailableQuantity());
        else if (cbNeedWarranty.isChecked() && spinnerWarranty.getSelectedItemPosition() == 0)
            showToast("Please add additional warranty");
        else if (AppController.get().isNetworkAvailable()) {

            updateViews(VIEW_PROGRESSBAR);


            Map<String, String> options = new HashMap<>();
            options.put("product_id", productDetail.getProductId());
            options.put("quantity", String.valueOf(quantity));
            options.put("cart_id", SessionController.getCartId());

            if (cbNeedWarranty.isChecked()) {
                Warranty item = (Warranty) spinnerWarranty.getSelectedItem();
                options.put("warranty_id", item.getWarrantyId());
            }


            AppController.get().addToCart(options, 0);

        } else
            showToast(getString(R.string.no_internet));

    }

    private void buyNow(ProductDetailsResponse.ProductDetail productDetail) {

        String qtyString = etQuantity.getText().toString().trim();
        long quantity = TextUtils.isEmpty(qtyString) ? 0 : Long.parseLong(qtyString);

        long stock = (TextUtils.isEmpty(productDetail.getAvailableQuantity())) ? 1 : Long.parseLong(productDetail.getAvailableQuantity());

        if (quantity == 0)
            showToast("Please enter a valid quantity");
        else if (stock < quantity)
            showToast("Maximum stock is " + productDetail.getAvailableQuantity());
        else if (cbNeedWarranty.isChecked() && spinnerWarranty.getSelectedItemPosition() == 0)
            showToast("Please add additional warranty");
        else if (AppController.get().isNetworkAvailable()) {

            updateViews(VIEW_PROGRESSBAR);

            Map<String, String> options = new HashMap<>();
            options.put("product_id", productDetail.getProductId());
            options.put("quantity", String.valueOf(quantity));
            if (cbNeedWarranty.isChecked()) {
                Warranty item = (Warranty) spinnerWarranty.getSelectedItem();
                options.put("warranty_id", item.getWarrantyId());
            }
            AppController.get().buyNow(options);

        } else
            showToast(getString(R.string.no_internet));

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent = new Intent();
        if (productDetail != null)
            intent.putExtra(TAG_IS_WISHED, productDetail.getIsInWishlist());
        intent.putExtra(TAG_POSITION, SELECTED_POSITION);
        setResult(RESULT_OK, intent);
        super.onBackPressed();
    }

    private void checkWritePermission() {
        if (PermissionUtils.hasPermission(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE})) {
            downloadFile();
        } else {
            PermissionUtils.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_WRITE_STORAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {


        if (requestCode == PERMISSION_REQUEST_WRITE_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                downloadFile();
            } else {
                showToast("Please enable all permissions from app settings.");
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }


    }

    private void downloadFile() {

        btnDownloadTDS.setEnabled(false);
        //working
        new DownloadFromURL().execute(downloadTDSUrl);


    }

    class DownloadFromURL extends AsyncTask<String, String, String> {
        private ProgressDialog progressDialog;
        public static final int progressType = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ProductDetailActivity.this);
            progressDialog.setMessage("File is Downloading. Please wait...");
            progressDialog.setIndeterminate(false);
            progressDialog.setMax(100);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... fileUrl) {
            int count;
            try {
                URL url = new URL(fileUrl[0]);
                URLConnection urlConnection = url.openConnection();
                urlConnection.connect();
                // show progress bar 0-100%
                int fileLength = urlConnection.getContentLength();
                InputStream inputStream = new BufferedInputStream(url.openStream(), 8192);


//                String path= Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + File.separator + getFileNameWithExtension(fileUrl[0]);
                String path = fileDirectoryGenerator(PDF) + File.separator + getFileNameWithExtension(fileUrl[0]);

                OutputStream outputStream = new FileOutputStream(path);

                byte[] data = new byte[1024];
                long total = 0;
                while ((count = inputStream.read(data)) != -1) {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / fileLength));
                    outputStream.write(data, 0, count);
                }
                // flushing output
                outputStream.flush();
                // closing streams
                outputStream.close();
                inputStream.close();

                return path;
            } catch (Exception e) {
                printLog("Error: ", e.getMessage());
            }
            return "";
        }

        // progress bar Updating

        protected void onProgressUpdate(String... progress) {
            // progress percentage
            progressDialog.setProgress(Integer.parseInt(progress[0]));
        }


        @Override
        protected void onPostExecute(String fileUrl) {
            if (progressDialog != null)
                progressDialog.dismiss();


            btnDownloadTDS.setEnabled(true);

            if (!TextUtils.isEmpty(fileUrl)) {
                showToast("Download Successfully Completed");
                openPDF(fileUrl);
            } else
                showToast("Download Failed!");

        }
    }

    public void openPDF(String filePath) {

        File file = new File(filePath);

//Open it up

        if (file.exists()) {
            try {

//            Uri path = Uri.fromFile(file);
                Uri path = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", file);
//                Uri path = FileProvider.getUriForFile(this, "com.purposecodes.emetwafar.provider", file);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(path, "application/pdf");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(intent);

            } catch (Exception e) {
                showToast("No Application available to view pdf");
            }
        } else
            showToast("File not found!");

    }

    public class ImagePagerAdapter extends PagerAdapter {
        Context context;
        List<SliderImage> images;
        LayoutInflater layoutInflater;


        public ImagePagerAdapter(Context context, List<SliderImage> _images) {
            this.context = context;

            this.images = _images;
//            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            layoutInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return images != null ? images.size() : 0;

        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == (object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            View itemView = layoutInflater.inflate(R.layout.item_image, container, false);

            ImageView imageView = itemView.findViewById(R.id.imageView_itemSlider);


            Glide.with(context)
                    .load(images.get(position).getImageUrl())
                    .apply(new RequestOptions()
                            .centerCrop()
                            .placeholder(R.drawable.ic_upload_placeholder)
                            .error(R.drawable.ic_upload_placeholder))
                    .into(imageView);

            container.addView(itemView);


            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((FrameLayout) object);
        }


    }


    public class WarrantyArrayAdapter extends ArrayAdapter<String> {

        private final LayoutInflater mInflater;
        private final Context mContext;
        private final List<Warranty> items;


        public WarrantyArrayAdapter(@NonNull Context context, @NonNull List objects) {
            super(context, 0, objects);

            mContext = context;
            mInflater = LayoutInflater.from(context);

            items = objects;
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {


            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.warranty_spinner_item, parent, false);
            }
            TextView _days = convertView.findViewById(R.id.textView_itemWarrant_day);
            TextView _price = convertView.findViewById(R.id.textView_itemWarrant_price);

            Warranty warranty = items.get(position);
            _days.setText(warranty.getWarrantyPeriod());

//            if (warranty.getAmount().contains("$"))
            if (position > 0)
                _price.setText(convertPriceText(warranty.getWarrantyPrice()));
            else
                _price.setText("");


            return convertView;
        }

        @Override
        public @NonNull
        View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.warranty_view_item, parent, false);
//                convertView = mInflater.inflate(R.layout.item_spinner_code, parent, false);
            }
//            TextView _days = (TextView) convertView.findViewById(R.id.textView_item);
            TextView _days = convertView.findViewById(R.id.textView_itemWarrant_day);
            TextView _price = convertView.findViewById(R.id.textView_itemWarrant_price);
            _price.setVisibility(GONE);

//            _days.setTextColor(getResources().getColor(R.color.whiteColor));

            Warranty warranty = items.get(position);

            _days.setText(warranty.getWarrantyPeriod());
//            _price.setText(warranty.getAmount());
            return convertView;

        }

//        private View createItemView(int position, View convertView, ViewGroup parent){
//            final View view = mInflater.inflate(mResource, parent, false);
//
//            TextView tvDays = (TextView) view.findViewById(R.id.textView_itemWarrant_day);
//            TextView tvPrice = (TextView) view.findViewById(R.id.textView_itemWarrant_price);
//
//            Warranty warranty = items.get(position);
//
//            tvDays.setText(warranty.getDays());
//            tvPrice.setText(warranty.getAmount());
//
//            return view;
//        }


        private SpannableStringBuilder convertPriceText(String _price) {


// Use a SpannableStringBuilder so that both the text and the spans are mutable
            SpannableStringBuilder ssb = new SpannableStringBuilder(_price);
            try {


                Typeface myTypeface = Typeface.create(ResourcesCompat.getFont(mContext, R.font.poppins_semi_bold), Typeface.BOLD);

// Create a span that will make the text red
                ForegroundColorSpan redForegroundColorSpan = new ForegroundColorSpan(mContext.getResources().getColor(R.color.redColor));

// Create a span that will strikethrough the text
                StrikethroughSpan strikethroughSpan = new StrikethroughSpan();
                // Add the secondWord and apply the strikethrough span to only the second word

                // text is added in later
//                ssb.setSpan(strikethroughSpan, 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
// Add a blank space
                ssb.append(" ");
                ssb.append(DEFAULT_CURRENCY);

                // Apply the color span
                ssb.setSpan(myTypeface, ssb.length() - DEFAULT_CURRENCY.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ssb.setSpan(redForegroundColorSpan, ssb.length() - DEFAULT_CURRENCY.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            } catch (Exception e) {
                e.fillInStackTrace();
            }

// Set the TextView text and denote that it is Editable
// since it's a SpannableStringBuilder
            return ssb;

        }

    }

    public static ProductDetailActivity getInstance() {
        return productDetailActivity;
    }


    public void notifyDataSetChange() {

        isRefreshPage = true;
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        if (isRefreshPage)
            loadProductDetail();

        isRefreshPage = false;
    }

    private boolean isRefreshPage = false;

}