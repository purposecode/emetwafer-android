package com.purposecodes.emetwafar.features.filter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.purposecodes.emetwafar.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RvFilterOfferAdapter extends RecyclerView.Adapter<RvFilterOfferAdapter.RvFilterOfferHolder> {
    private final String TAG = "RvFilterOfferAdapter";
    // Context
    private final Context mContext;


    // List
    private final ArrayList<String> mList;

  private final OnFilterOfferClickListener listener;

    public RvFilterOfferAdapter(Context mContext, ArrayList<String> mList, OnFilterOfferClickListener _listener) {
        this.mContext = mContext;
        this.mList = mList;
        this.listener = _listener;
    }


    @NonNull
    @Override
    public RvFilterOfferHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_check_box, parent, false);
        return new RvFilterOfferHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull RvFilterOfferHolder holder, @SuppressLint("RecyclerView") int position) {
        String offer = mList.get(position);
        holder.tvOffer.setText(offer);

        holder.tvOffer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // update your model (or other business logic) based on isChecked
                if (listener != null) {
                    if (isChecked)
                        listener.onOfferChecked(offer, position);
                    else
                        listener.onOfferUnChecked(offer, position);

                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return (mList != null ? mList.size() : 0);

    }


    static class RvFilterOfferHolder extends RecyclerView.ViewHolder {


        @SuppressLint("NonConstantResourceId")
        @BindView(R.id.checkedTextView_item)
        CheckBox tvOffer;


        RvFilterOfferHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    public interface OnFilterOfferClickListener {
        void onOfferChecked(String offer, int position);

        void onOfferUnChecked(String offer, int position);
    }
}
