package com.purposecodes.emetwafar.features.search;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.model.Product;

import java.util.ArrayList;

public class SearchListAdapter extends BaseAdapter implements Filterable {

    private final SearchableActivity activity;
    private FriendFilter friendFilter;
    private final ArrayList<Product> friendList;
    private ArrayList<Product> filteredList;

    /**
     * Initialize context variables
     *
     * @param activity   friend list activity
     * @param friendList friend list
     */
    public SearchListAdapter(SearchableActivity activity, ArrayList<Product> friendList) {
        this.activity = activity;
        this.friendList = friendList;
        this.filteredList = friendList;

        getFilter();
    }

    /**
     * Get size of user list
     *
     * @return userList size
     */
    @Override
    public int getCount() {
        return filteredList.size();
    }

    /**
     * Get specific item from user list
     *
     * @param i item index
     * @return list item
     */
    @Override
    public Object getItem(int i) {
        return filteredList.get(i);
    }

    /**
     * Get user list item id
     *
     * @param i item index
     * @return current item id
     */
    @Override
    public long getItemId(int i) {
        return i;
    }

    /**
     * Create list row view
     *
     * @param position index
     * @param view     current list item view
     * @param parent   parent
     * @return view
     */
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        // A ViewHolder keeps references to children views to avoid unnecessary calls
        // to findViewById() on each row.

        final Product user = (Product) getItem(position);

//        if (view == null) {
        LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.item_spinner_code, parent, false);


        TextView tvName = view.findViewById(R.id.textView_item);


        tvName.setText(user.getProductName());


//        }


        return view;
    }


    /**
     * Get custom filter
     *
     * @return filter
     */
    @Override
    public Filter getFilter() {
        if (friendFilter == null) {
            friendFilter = new FriendFilter();
        }

        return friendFilter;
    }

    /**
     * Keep reference to children view to avoid unnecessary calls
     */
    static class ViewHolder {
        TextView iconText;
        TextView name;
    }

    /**
     * Custom filter for friend list
     * Filter content in friend list according to the search text
     */
    private class FriendFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                ArrayList<Product> tempList = new ArrayList<Product>();

                // search content in friend list
                for (Product product : friendList) {
                    if (product.getProductName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        tempList.add(product);
                    }
                }

                filterResults.count = tempList.size();
                filterResults.values = tempList;
            } else {
                filterResults.count = friendList.size();
                filterResults.values = friendList;
            }

            return filterResults;
        }

        /**
         * Notify about filtered list to ui
         *
         * @param constraint text
         * @param results    filtered result
         */
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredList = (ArrayList<Product>) results.values;
            notifyDataSetChanged();
        }
    }
}
