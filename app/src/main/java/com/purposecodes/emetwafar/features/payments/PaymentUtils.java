package com.purposecodes.emetwafar.features.payments;

import static com.purposecodes.emetwafar.BuildConfig.DEBUG;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.getAppVersion;

import android.app.Activity;
import android.content.Intent;

import com.telr.mobile.sdk.activty.WebviewActivity;
import com.telr.mobile.sdk.entity.request.payment.Address;
import com.telr.mobile.sdk.entity.request.payment.App;
import com.telr.mobile.sdk.entity.request.payment.Billing;
import com.telr.mobile.sdk.entity.request.payment.MobileRequest;
import com.telr.mobile.sdk.entity.request.payment.Name;
import com.telr.mobile.sdk.entity.request.payment.Tran;

public class PaymentUtils {
    private final String TAG = "PaymentUtils";


    public static final String KEY = "KBrmq@TpKc~kpD5Z";
    public static final String STORE_ID = "24402";    // TODO: Insert your Store ID here

    //set static value
    private static final String APP_ID = "com.purposecodes.emetwafar";
    private static final String APPNAME = "E-Metwafer";
    private static final String CURRENCY = "SAR";
    private static final String LANGUAGE = "en";
/*
    private static final String MODE = "0"; //Test mode of zero indicates a live     1-test,0-Live
    public static final boolean isSecurityEnabled = true;      // Mark false to test on simulator, True to test on actual device and Production
*/

        private static final String MODE =DEBUG?"1":"0";
        public static final boolean isSecurityEnabled = !DEBUG;

    private static final String TRANSACTION_TYPE = "sale";//"auth";
    private static final String TRANSACTION_CLAZZ = "paypage";


    private static final String COUNTRY = "SA";
    private static final String USER_TITLE = "MR.";


    private static String referenceId;
    private static String amount;
    private static String userId;
    private static String description;
    private static String city;
    private static String region;
    private static String address1;
    private static String address2;
    private static String zipCode;
    private static String firstName;
    private static String lastName;
    private static String mail;
    private static String phone;


    public static void sendPlanUpdatePaymentRequest(Activity activity, String _referenceId, String _amount, String _userId, String _description, String _city, String _region, String _address1, String _address2, String _zipCode, String _firstName, String _lastName, String _mail, String _phone) {
        referenceId = _referenceId;
        amount = _amount;
        userId = _userId;
        description = _description;
        city = _city;
        region = _region;
        address1 = _address1;
        address2 = _address2;
        zipCode = _zipCode;
        firstName = _firstName;
        lastName = _lastName;
        mail = _mail;
        phone = _phone;

//        intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);

        Intent intent = new Intent(activity, WebviewActivity.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        // intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        intent.putExtra(WebviewActivity.EXTRA_MESSAGE, getMobileRequest());
        intent.putExtra(WebviewActivity.FAILED_ACTIVTY_CLASS_NAME, "com.purposecodes.emetwafar.features.subscription.SubscriptionFailureActivity");
        intent.putExtra(WebviewActivity.SUCCESS_ACTIVTY_CLASS_NAME, "com.purposecodes.emetwafar.features.subscription.PlanUpdatePaymentSuccessActivity");

        intent.putExtra(WebviewActivity.IS_SECURITY_ENABLED, isSecurityEnabled);
        activity.startActivity(intent);
        activity.finish();

    }


    public static void sendOrderPaymentRequest(Activity activity, String _referenceId, String _amount, String _userId, String _description, String _city, String _region, String _address1, String _address2, String _zipCode, String _firstName, String _lastName, String _mail, String _phone) {
        referenceId = _referenceId;
        amount = _amount;
        userId = _userId;
        description = _description;
        city = _city;
        region = _region;
        address1 = _address1;
        address2 = _address2;
        zipCode = _zipCode;
        firstName = _firstName;
        lastName = _lastName;
        mail = _mail;
        phone = _phone;


        Intent intent = new Intent(activity, WebviewActivity.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);

        intent.putExtra(WebviewActivity.EXTRA_MESSAGE, getMobileRequest());
        intent.putExtra(WebviewActivity.FAILED_ACTIVTY_CLASS_NAME, "com.purposecodes.emetwafar.features.checkout.OrderFailedActivity");
        intent.putExtra(WebviewActivity.SUCCESS_ACTIVTY_CLASS_NAME, "com.purposecodes.emetwafar.features.payments.OrderPaymentSuccessActivity");

        intent.putExtra(WebviewActivity.IS_SECURITY_ENABLED, isSecurityEnabled);
        activity.startActivity(intent);


    }


    // This example used for the first payment, or with new card details.
    private static MobileRequest getMobileRequest() {
        MobileRequest mobile = new MobileRequest();
        mobile.setStore(STORE_ID);                       // Store ID
        mobile.setKey(KEY);                              // Authentication Key : The Authentication Key will be supplied by Telr as part of the Mobile API setup process after you request that this integration type is enabled for your account. This should not be stored permanently within the App.
        App app = new App();
        app.setId(APP_ID);                          // Application installation ID
        app.setName(APPNAME);                    // Application name
        app.setUser(userId);                           // Application user ID : Your reference for the customer/user that is running the App. This should relate to their account within your systems.
        app.setVersion(getAppVersion());                         // Application version
        app.setSdk("123");
        mobile.setApp(app);
        Tran tran = new Tran();
        tran.setTest(MODE);                              // Test mode : Test mode of zero indicates a live transaction. If this is set to any other value the transaction will be treated as a test.
        tran.setType(TRANSACTION_TYPE);                           /* Transaction type
                                                            'auth'   : Seek authorisation from the card issuer for the amount specified. If authorised, the funds will be reserved but will not be debited until such time as a corresponding capture command is made. This is sometimes known as pre-authorisation.
                                                            'sale'   : Immediate purchase request. This has the same effect as would be had by performing an auth transaction followed by a capture transaction for the full amount. No additional capture stage is required.
                                                            'verify' : Confirm that the card details given are valid. No funds are reserved or taken from the card.
                                                        */
        tran.setClazz(TRANSACTION_CLAZZ);                       // Transaction class only 'paypage' is allowed on mobile, which means 'use the hosted payment page to capture and process the card details'
//        tran.setCartid(String.valueOf(new BigInteger(128, new Random()))); //// Transaction cart ID : An example use of the cart ID field would be your own transaction or order reference.
        tran.setCartid(referenceId); //// Transaction cart ID : An example use of the cart ID field would be your own transaction or order reference.
        tran.setDescription(description);         // Transaction description
        tran.setCurrency(CURRENCY);                        // Transaction currency : Currency must be sent as a 3 character ISO code. A list of currency codes can be found at the end of this document. For voids or refunds, this must match the currency of the original transaction.
        tran.setAmount(amount);                         // Transaction amount : The transaction amount must be sent in major units, for example 9 dollars 50 cents must be sent as 9.50 not 950. There must be no currency symbol, and no thousands separators. Thedecimal part must be separated using a dot.
        //tran.setRef(???);                           // (Optinal) Previous transaction reference : The previous transaction reference is required for any continuous authority transaction. It must contain the reference that was supplied in the response for the original transaction.
        tran.setLangauge(LANGUAGE);                        // (Optinal) default is en -> English
        mobile.setTran(tran);
        Billing billing = new Billing();
        Address address = new Address();
        address.setCity(city);                       // City : the minimum required details for a transaction to be processed
        address.setCountry(COUNTRY);                       // Country : Country must be sent as a 2 character ISO code. A list of country codes can be found at the end of this document. the minimum required details for a transaction to be processed
        address.setRegion(region);                     // Region
        address.setLine1(address1);                 // Street address – line 1: the minimum required details for a transaction to be processed
        address.setLine2(address2);               // (Optinal)
        //address.setLine3("SIT G=Towe");               // (Optinal)
        address.setZip(zipCode);                 // (Optinal)
        billing.setAddress(address);
        Name name = new Name();
        name.setFirst(firstName);                          // Forename : the minimum required details for a transaction to be processed
        name.setLast(lastName);                          // Surname : the minimum required details for a transaction to be processed
        name.setTitle(USER_TITLE);                           // Title
        billing.setName(name);
        billing.setEmail(mail);                 // TODO: Insert your email here : the minimum required details for a transaction to be processed.
        billing.setPhone(phone);                // Phone number, required if enabled in your merchant dashboard.
        mobile.setBilling(billing);
        return mobile;

    }


}
