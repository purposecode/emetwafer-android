package com.purposecodes.emetwafar.features.order;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.api.ApiFailureMessageListener;
import com.purposecodes.emetwafar.api.ApiSuccessListener;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.model.Order;
import com.purposecodes.emetwafar.responsemodel.ErrorModel;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;

import static com.purposecodes.emetwafar.utils.LogUtils.printLog;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_ORDER;

public class NeedHelpActivity extends BaseActivity {


    private final String TAG = "NeedHelpActivity";

    @BindView(R.id.editText_needHelp_message)
    EditText etMessage;

    @BindView(R.id.button_needHelp_submit)
    Button btnSubmit;

    @BindView(R.id.recyclerView_needHelp)
    RecyclerView recyclerView;

    private ArrayList<String> reasonList;
    private RvHelpListAdapter rvHelpAdapter;
    private Order ORDER;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_need_help);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        ORDER = (Order) getIntent().getSerializableExtra(TAG_ORDER);
        init();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_need_help;
    }

    private void init() {

        setTitle("Order No : #" + ORDER.getOrderNo());
        reasonList = new ArrayList<>();
        reasonList.add("Product not received");
        reasonList.add("Delay in delivery");
        reasonList.add("Payment issue");
        reasonList.add("Other");

        rvHelpAdapter = new RvHelpListAdapter(reasonList);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(rvHelpAdapter);


        btnSubmit.setOnClickListener(v -> {

            hideSoftKeyboard();
            String strMessage = etMessage.getText().toString().trim();

            if (rvHelpAdapter.getSelectedSelection() < 0) {
                showToast("Please select reason");
            } else if (TextUtils.isEmpty(strMessage)) {
                showToast("Message cannot be blank.");

            } else if (AppController.get().isNetworkAvailable()) {
                showProgress();
                Map<String, String> options = new HashMap<>();
                options.put("order_id", ORDER.getOrderId());
                options.put("subject", reasonList.get(rvHelpAdapter.getSelectedSelection()));
                options.put("message", strMessage);
                AppController.get().orderNeedHelp(options);

            } else {
                showToast(getString(R.string.no_internet));
            }


        });

    }


    @Subscribe
    public void onHttpSuccess(ApiSuccessListener<JsonObject> response) {

        if (response.type.equalsIgnoreCase("orderNeedHelp")) {
            dismissProgress();

            try {
                JsonObject jsonObject = response.response.body();
                assert jsonObject != null;
                if (jsonObject.get("status").getAsBoolean()) {


                    showToast(jsonObject.get("text").getAsString());
                    finish();

                } else {
                    showToast(jsonObject.get("text").getAsString());
                }
            } catch (Exception e) {
                printLog(TAG, " onHttpSuccess Exception : " + e.getMessage());
            }
        }
    }


    @Subscribe
    public void onHttpError(ApiFailureMessageListener<ErrorModel> response) {

        if (response.type.equalsIgnoreCase("orderNeedHelp")) {

            dismissProgress();
            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                showToast(errorModel.getText());
            } catch (Exception e) {
                e.printStackTrace();
                showToast(getResources().getString(R.string.error_try_again));
            }


        }
    }


    @Subscribe
    public void onHttpFailure(ApiFailureListener response) {

        if (response.type.equalsIgnoreCase("orderNeedHelp")) {
            dismissProgress();
            showToast(getResources().getString(R.string.error_try_again));
        }
    }


}
