package com.purposecodes.emetwafar.features.subscription;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity;
import com.purposecodes.emetwafar.activity.SplashActivity;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.model.PlanData;
import com.sk.icode.errorview.ErrorView;
import com.telr.mobile.sdk.activty.WebviewActivity;
import com.telr.mobile.sdk.entity.response.status.StatusResponse;

import butterknife.BindView;
import butterknife.OnClick;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_NEW_PLAN;
import static com.purposecodes.emetwafar.utils.LogUtils.printLog;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_DATA;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_ERRORVIEW;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_PROGRESSBAR;

public class SubscriptionSuccessActivity extends BaseActivity {

    private final String TAG = "SubscriptionSuccessActivity";
    @BindView(R.id.button_subscriptionSuccess_continue)
    Button btnGoto;

    @BindView(R.id.textView_subscriptionSuccess_title)
    TextView tvTitle;

    @BindView(R.id.textView_subscriptionSuccess_description)
    TextView tvDescription;


    @BindView(R.id.errorView)
    ErrorView errorView;

    @BindView(R.id.subscription_plan_bg)
    ViewGroup viewBackground;


    @BindView(R.id.view_subscriptionSuccess)
    ViewGroup dataView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        viewBackground.setBackgroundColor(getResources().getColor(R.color.greenColor));
        btnGoto.setBackground(getResources().getDrawable(R.drawable.curved_white_rect));
        btnGoto.setTextColor(getResources().getColor(R.color.primaryText));
        btnGoto.setText(getString(R.string.continue_with_login));
        tvTitle.setTextColor(getResources().getColor(R.color.whiteColor));
        tvDescription.setTextColor(getResources().getColor(R.color.whiteColor));


        SpannableStringBuilder sbb = new SpannableStringBuilder();
        tvTitle.append("Thank you for successfully completing your");
        tvTitle.append(getBoldText(" Registration"));
        tvTitle.append(" & ");
        tvTitle.append(getBoldText("Payment."));

//        tvTitle.setText(sbb);


        updateViews(VIEW_DATA);

        PlanData p = AppController.get().getPlanData();
        if (p != null && p.getCallingIntent() != ACTIVITY_NEW_PLAN) {
            btnGoto.setText("Continue");
        }

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_subscription_success;
    }


    @OnClick(R.id.button_subscriptionSuccess_continue)
    public void onContinueLogin() {
//        Intent intent = new Intent(this, LoginActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(intent);


        PlanData p = AppController.get().getPlanData();


        if (p != null && p.getCallingIntent() != ACTIVITY_NEW_PLAN) {
            Intent intent=new Intent(SubscriptionSuccessActivity.this, SplashActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else {
            AppController.get().logoutUser();
        }
        AppController.get().setPlanData(null);
    }


    //set ErrorView
    private void setErrorView(final String title, final String subTitle, boolean isRetry, int drawable) {

        int color = getResources().getColor(R.color.whiteColor);
//        int color=R.color.whiteColor;

        updateViews(VIEW_ERRORVIEW);

        errorView.setConfig(ErrorView.Config.create()
                .title(title)
                .titleColor(color)
                .subtitle(subTitle)
                .subtitleColor(color)
                .retryVisible(isRetry)
                .image(drawable)
                .build());

//
//        errorView.setConfig(ErrorView.Config.create()
//                .image(drawable)
//                .title(title)
//                .subtitle(subTitle)
//                .retryVisible(isRetry)
//                .retryText(getString(R.string.error_view_retry))
//                .build());

        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {
                updateViews(VIEW_DATA);

            }
        });
    }


    private void updateViews(int viewCode) {


        switch (viewCode) {


            case VIEW_PROGRESSBAR:
                showProgress();

                dataView.setVisibility(GONE);
                errorView.setVisibility(GONE);
                break;
            case VIEW_DATA:
                dismissProgress();
                dataView.setVisibility(VISIBLE);
                errorView.setVisibility(GONE);

                break;

            case VIEW_ERRORVIEW:
                dismissProgress();
                dataView.setVisibility(GONE);
                errorView.setVisibility(VISIBLE);
                break;

        }
    }


    private SpannableStringBuilder getBoldText(String text) throws NullPointerException {
        // Use a SpannableStringBuilder so that both the text and the spans are mutable
        SpannableStringBuilder ssb = new SpannableStringBuilder();
        try {


// Create a span that will make the text red
//            ForegroundColorSpan blackForegroundColorSpan = new ForegroundColorSpan(getResources().getColor(android.R.color.black));
// Add a blank space
            ssb.append(text);
// Add the secondWord and apply the strikethrough span to only the second word
//            ssb.setSpan(blackForegroundColorSpan, 0, ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ssb.setSpan(new StyleSpan(Typeface.BOLD), 0, ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

// Set the TextView text and denote that it is Editable
// since it's a SpannableStringBuilder
        } catch (Exception e) {
            e.fillInStackTrace();
        }
        return ssb;

    }


    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        StatusResponse status = intent.getParcelableExtra(WebviewActivity.PAYMENT_RESPONSE);
        if (status == null)
            return;


        printLog(TAG, " success data : " + new Gson().toJson(status));
//        if (AppController.get().getPaymentOrders()!=null)
//            printLog(TAG," success size: "+ AppController.get().getPaymentOrders().size());
// {"auth":{"avs":"X","ca_valid":"1","cardcode":"VC","cardlast4":"1111","code":"925810","cvv":"Y","message":"Authorised","status":"A","tranref":"040025617463"},"trace":"4000/6089/5f8e9ccc"}

        tvDescription.setVisibility(VISIBLE);
        tvDescription.setText(tvDescription.getText() + " : " + status.getTrace());

        if (status.getAuth() != null) {
            status.getAuth().getStatus();   // Authorisation status. A indicates an authorised transaction. H also indicates an authorised transaction, but where the transaction has been placed on hold. Any other value indicates that the request could not be processed.
            status.getAuth().getAvs();      /* Result of the AVS check:
                                            Y = AVS matched OK
                                            P = Partial match (for example, post-code only)
                                            N = AVS not matched
                                            X = AVS not checked
                                            E = Error, unable to check AVS */
            status.getAuth().getCode();     // If the transaction was authorised, this contains the authorisation code from the card issuer. Otherwise it contains a code indicating why the transaction could not be processed.
            status.getAuth().getMessage();  // The authorisation or processing error message.
            status.getAuth().getCa_valid();
            status.getAuth().getCardcode(); // Code to indicate the card type used in the transaction. See the code list at the end of the document for a list of card codes.
            status.getAuth().getCardlast4();// The last 4 digits of the card number used in the transaction. This is supplied for all payment types (including the Hosted Payment Page method) except for PayPal.
            status.getAuth().getCvv();      /* Result of the CVV check:
                                           Y = CVV matched OK
                                           N = CVV not matched
                                           X = CVV not checked
                                           E = Error, unable to check CVV */
            status.getAuth().getTranref(); //The payment gateway transaction reference allocated to this request.
            printLog(TAG, "hany : " + status.getAuth().getTranref());
            status.getAuth().getCardfirst6(); // The first 6 digits of the card number used in the transaction, only for version 2 is submitted in Tran -> Version


            setTransactionDetails(status.getAuth().getTranref(), status.getAuth().getCardlast4());
        }
    }

    private void setTransactionDetails(String ref, String last4) {
//        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("telr", Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPref.edit();
//        editor.putString("ref", ref);
//        editor.putString("last4", last4);
//        editor.commit();
    }


}
