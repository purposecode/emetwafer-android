package com.purposecodes.emetwafar.features.notification;

import android.content.Context;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.holder.RvBlankViewHolder;
import com.purposecodes.emetwafar.holder.RvFooterViewHolder;
import com.purposecodes.emetwafar.model.NotificationItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RvNotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final String TAG = "RvNotificationAdapter";
    // Context
    private final Context mContext;

    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;
    private static final int TYPE_BLANK = 3;

    // Checking the its loading for new items
    private boolean mLoading = false;
    // List
    private final ArrayList<NotificationItem> mList;

    private final OnNotificationListener listener;

    public RvNotificationAdapter(Context mContext, ArrayList<NotificationItem> mList, OnNotificationListener _listener) {
        this.mContext = mContext;
        this.mList = mList;
        this.listener = _listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;
        switch (viewType) {

            case TYPE_FOOTER:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_loading, parent, false);
                return new RvFooterViewHolder(view);

            case TYPE_ITEM:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_notification, parent, false);
                return new RvNotificationHolder(view);

            default:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_blank, parent, false);
                return new RvBlankViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        int viewType = getItemViewType(position);
        switch (viewType) {

            case TYPE_FOOTER:
                bindFooterView((RvFooterViewHolder) viewHolder);
                break;
            case TYPE_ITEM:
                bindItemView((RvNotificationHolder) viewHolder, position);
                break;
        }


    }


    private void bindItemView(RvNotificationHolder holder, final int position) {


        NotificationItem noti = mList.get(position);

        holder.cvBackground.setCardBackgroundColor(noti.getReadStatus().equalsIgnoreCase("1") ? mContext.getResources().getColor(R.color.whiteColor) : mContext.getResources().getColor(R.color.expiredBg));

        Glide.with(mContext)
                .load(noti.getNotificationImage())
                .apply(new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.ic_upload_placeholder)
                        .error(R.drawable.ic_upload_placeholder))
                .into(holder.ivProduct);

        holder.tvTitle.setText(noti.getNotificationTitle());
        holder.tvDescription.setText(noti.getNotificationMsg());

        holder.tvDescription.setSelected(true);

        if (!TextUtils.isEmpty(noti.getNotificationDate())) {

            String date = DateUtils.getRelativeDateTimeString(holder.itemView.getContext(), Long.parseLong(noti.getNotificationDate()) * 1000, DateUtils.MINUTE_IN_MILLIS, DateUtils.WEEK_IN_MILLIS, DateUtils.FORMAT_ABBREV_ALL).toString();

            holder.tvDate.setText(date);
            holder.tvDate.setVisibility(View.VISIBLE);
        } else {
            holder.tvDate.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onNotificationClicked(noti, position);
            }
        });

    }

    private void bindFooterView(RvFooterViewHolder holder) {
        if (mLoading) {
            holder.loadText.setText(R.string.loading);
            holder.loadText.setVisibility(View.VISIBLE);
            holder.loadProgress.setVisibility(View.VISIBLE);
        } else {
            holder.loadText.setText("");
            holder.loadText.setVisibility(View.GONE);
            holder.loadProgress.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {

        int count = 0;
        if (!mList.isEmpty()) {
            count = mList.size() + 1;
        }
        return count;


    }

    @Override
    public int getItemViewType(int position) {

        try {


            if (position == getItemCount() - 1) {
                return TYPE_FOOTER;
            } else {

                return TYPE_ITEM;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            return TYPE_BLANK;
        }
    }

    public void setLoading(boolean loading) {
        mLoading = loading;
        notifyDataSetChanged();
    }

    static class RvNotificationHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cardView_notification)
        CardView cvBackground;

        @BindView(R.id.imageView_itemNotification_imageView)
        ImageView ivProduct;

        @BindView(R.id.textView_itemNotification_title)
        TextView tvTitle;

        @BindView(R.id.textView_itemNotification_description)
        TextView tvDescription;
        @BindView(R.id.textView_itemNotification_date)
        TextView tvDate;

        RvNotificationHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public interface OnNotificationListener {
        void onNotificationClicked(NotificationItem notification, int position);
    }

}
