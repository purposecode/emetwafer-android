package com.purposecodes.emetwafar.features.order;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.purposecodes.emetwafar.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RvHelpListAdapter extends RecyclerView.Adapter<RvHelpListAdapter.RvHelpListHolder> {
    private final String TAG = "RvHelpListAdapter";


    private Context context;
    // List
    private final ArrayList<String> mList;

    private int SELECTED_POSITION = -1;

    public RvHelpListAdapter(ArrayList<String> mList) {
        this.mList = mList;
    }

    @NonNull
    @Override
    public RvHelpListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_help, parent, false);
        this.context = parent.getContext();
        return new RvHelpListHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RvHelpListHolder holder, @SuppressLint("RecyclerView") int position) {
        String offer = mList.get(position);
        holder.cbText.setText(offer);

        holder.cbText.setChecked(SELECTED_POSITION == position);
        holder.cbText.setSelected(SELECTED_POSITION == position);

        holder.cardBg.setCardBackgroundColor(SELECTED_POSITION == position ? context.getResources().getColor(R.color.blackColor) : context.getResources().getColor(R.color.whiteColor));

        holder.cardBg.setOnClickListener(v -> {
            SELECTED_POSITION = position;
            notifyDataSetChanged();
        });
    }

    @Override
    public int getItemCount() {
        return (mList != null ? mList.size() : 0);
    }


    public int getSelectedSelection() {
        return SELECTED_POSITION;
    }

    static class RvHelpListHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cardView_help_item)
        CardView cardBg;

        @BindView(R.id.checkBox_helpItem)
        CheckBox cbText;


        RvHelpListHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }


}
