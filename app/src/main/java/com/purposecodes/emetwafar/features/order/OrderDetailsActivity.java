package com.purposecodes.emetwafar.features.order;

import static com.purposecodes.emetwafar.utils.FileUtils.PDF;
import static com.purposecodes.emetwafar.utils.FileUtils.fileDirectoryGenerator;
import static com.purposecodes.emetwafar.utils.FileUtils.getFileNameWithExtension;
import static com.purposecodes.emetwafar.utils.LogUtils.printLog;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.convertPriceText;
import static com.purposecodes.emetwafar.utils.UtilsVariable.CANCELLED;
import static com.purposecodes.emetwafar.utils.UtilsVariable.DELIVERED;
import static com.purposecodes.emetwafar.utils.UtilsVariable.NOT_PLACED;
import static com.purposecodes.emetwafar.utils.UtilsVariable.NO_DATA;
import static com.purposecodes.emetwafar.utils.UtilsVariable.NO_NETWORK;
import static com.purposecodes.emetwafar.utils.UtilsVariable.PAID;
import static com.purposecodes.emetwafar.utils.UtilsVariable.REFUND_CONFIRMED;
import static com.purposecodes.emetwafar.utils.UtilsVariable.REFUND_REQUESTED;
import static com.purposecodes.emetwafar.utils.UtilsVariable.RETURNED;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_IS_UPDATE;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_ORDER;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_ORDER_ID;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_POSITION;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_DATA;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_ERRORVIEW;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_PROGRESSBAR;
import static com.purposecodes.emetwafar.utils.UtilsVariable.WRONG_RESPONSE;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.StyleSpan;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.api.ApiFailureMessageListener;
import com.purposecodes.emetwafar.api.ApiSuccessListener;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.model.CartItem;
import com.purposecodes.emetwafar.model.Order;
import com.purposecodes.emetwafar.model.TimeLineMessage;
import com.purposecodes.emetwafar.responsemodel.ErrorModel;
import com.purposecodes.emetwafar.responsemodel.OrderDetailsResponse;
import com.purposecodes.emetwafar.utils.PermissionUtils;
import com.sk.icode.errorview.ErrorView;
import com.squareup.otto.Subscribe;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

public class OrderDetailsActivity extends BaseActivity {

    String TAG = "OrderDetailsActivity";

    @BindView(R.id.textView_orderDetail_shippingStatus)
    TextView tvShippingStatus;

    @BindView(R.id.textView_orderDetail_orderNo)
    TextView tvOrderNumber;
    @BindView(R.id.textView_orderDetail_subTotal)
    TextView tvSubTotal;
    @BindView(R.id.textView_orderDetail_vatTotal)
    TextView tvVatTotal;
    @BindView(R.id.textView_orderDetail_discount)
    TextView tvDiscount;
    @BindView(R.id.textView_orderDetail_verificationAmount)
    TextView tvVerificationAmount;
    @BindView(R.id.textView_orderDetail_warrantyAmount)
    TextView tvWarrantyAmount;
    @BindView(R.id.textView_orderDetail_grandTotal)
    TextView tvGrandTotal;
    @BindView(R.id.textView_orderDetail_paymentType)
    TextView tvPaymentType;

    @BindView(R.id.textView_orderDetail_referenceNumber)
    TextView tvReferenceNumber;

    @BindView(R.id.button_orderDetail_download)
    Button btnDownload;

    @BindView(R.id.button_orderDetail_cancel)
    Button btnCancel;


    @BindView(R.id.errorView)
    ErrorView errorView;


    @BindView(R.id.recyclerView_orderDetail_productList)
    RecyclerView rvProducts;

    @BindView(R.id.recyclerView_orderDetail_timeLine)
    RecyclerView rvTimeLine;

    @BindView(R.id.view_orderDetail)
    ViewGroup dataView;

    @BindView(R.id.view_orderDetail_bottom)
    ViewGroup bottomView;

    private Order SELECTED_ORDER;
    private String SELECTED_ORDER_ID;
    private boolean IS_UPDATE = false;
    private int SELECTED_POSITION = -1;
    private ArrayList<CartItem> cartItems;
    private RvOrderDetailItemAdapter rvAdapter;

    private static final int PERMISSION_REQUEST_WRITE_STORAGE = 112;

    private String downloadInvoiceSUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        cartItems = new ArrayList<>();
        try {

            SELECTED_ORDER_ID = getIntent().getStringExtra(TAG_ORDER_ID);
            SELECTED_POSITION = getIntent().getIntExtra(TAG_POSITION, -1);

        } catch (Exception e) {

        }

        setProductView();
        loadOrderDetail();

/*
 https://uniqueandrocode.com/order-tracking-ui-design/
 https://github.com/vipulasri/Timeline-View
 https://android-arsenal.com/details/1/6540
 https://medium.com/@yongjhih/timeline-view-libraries-9fb77a590316


  */
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_order_details;
    }

    private void setProductView() {

        rvAdapter = new RvOrderDetailItemAdapter(cartItems,this);
        rvProducts.setHasFixedSize(true);
        rvProducts.setLayoutManager(new LinearLayoutManager(this));
        rvProducts.setAdapter(rvAdapter);
        rvProducts.setNestedScrollingEnabled(false);

    }

    private void setTimeLineStatus(List<TimeLineMessage> messages) {


        RvTimeLineAdapter rvTimeLineAdapter = new RvTimeLineAdapter(messages, this);
        rvTimeLine.setHasFixedSize(true);
        rvTimeLine.setLayoutManager(new LinearLayoutManager(this));
        rvTimeLine.setAdapter(rvTimeLineAdapter);
        rvTimeLine.setNestedScrollingEnabled(false);
        rvTimeLine.setVisibility(View.VISIBLE);

    }

    private void loadOrderDetail() {

        if (AppController.get().isNetworkAvailable()) {

            updateViews(VIEW_PROGRESSBAR);
            AppController.get().getOrderDetail(SELECTED_ORDER_ID);
        } else
            setErrorView(getString(R.string.no_internet), "", false,NO_NETWORK);

    }

    //set ErrorView
//set ErrorView
    private void setErrorView(final String title, final String subTitle, boolean isRetry, int drawable) {

        updateViews(VIEW_ERRORVIEW);
        errorView.setConfig(ErrorView.Config.create()
                .title(title)
                .subtitle(subTitle)
                .retryVisible(isRetry)
                .image(drawable)
                .build());


        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {

                loadOrderDetail();

            }
        });
    }

    private void updateViews(int viewCode) {

        switch (viewCode) {

            case VIEW_PROGRESSBAR:
                showProgress();

                errorView.setVisibility(View.GONE);
                dataView.setVisibility(View.GONE);
                break;
            case VIEW_DATA:
                dismissProgress();
                dataView.setVisibility(View.VISIBLE);
                errorView.setVisibility(View.GONE);

                break;

            case VIEW_ERRORVIEW:
                dismissProgress();
                dataView.setVisibility(View.GONE);
                errorView.setVisibility(View.VISIBLE);
                break;

        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void setProductView(Order _order) {

        try {


            SELECTED_ORDER = _order;
            cartItems.clear();
            if (_order.getCartItems() != null && !_order.getCartItems().isEmpty())
                cartItems.addAll(_order.getCartItems());

            rvAdapter.notifyDataSetChanged();

            updateViews(VIEW_DATA);

            tvShippingStatus.setText(_order.getShippingStatus() != null ? _order.getShippingStatus() : "");  //no api

            tvOrderNumber.setText("#" + _order.getOrderNo());
            tvSubTotal.setText(_order.getSubtotal());
            tvVatTotal.setText(_order.getVat()); //no api
            tvDiscount.setText(_order.getDiscountApplied());
            tvVerificationAmount.setText(_order.getVerifyCost());
            tvWarrantyAmount.setText(_order.getWarrantyTotal());
            tvGrandTotal.setText(convertPriceText(_order.getOrderTotal()));  //no api
            tvPaymentType.setText(_order.getPaymentStatus());

            if (_order.getRefundTransRef() != null && !TextUtils.isEmpty(_order.getRefundTransRef())) {
                tvReferenceNumber.setText(convertReferenceText("Refund Reference", _order.getRefundTransRef()));
                tvReferenceNumber.setVisibility(View.VISIBLE);
            } else if (_order.getTransactionNo() != null && !TextUtils.isEmpty(_order.getTransactionNo())) {
                tvReferenceNumber.setText(convertReferenceText("Transaction Reference", _order.getTransactionNo()));
                tvReferenceNumber.setVisibility(View.VISIBLE);
            } else
                tvReferenceNumber.setVisibility(View.GONE);

            if (_order.getPaymentStatus().equalsIgnoreCase(PAID))
                tvPaymentType.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_green_tick, 0, 0, 0);
            else
                tvPaymentType.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_gray_tick, 0, 0, 0);

            bottomView.setVisibility((_order.getOrderStatus().equalsIgnoreCase(CANCELLED) || _order.getOrderStatus().equalsIgnoreCase(RETURNED) || _order.getOrderStatus().equalsIgnoreCase(DELIVERED) || (_order.getOrderStatus().equalsIgnoreCase(NOT_PLACED) && _order.getPaymentStatus().equalsIgnoreCase(REFUND_CONFIRMED) || _order.getPaymentStatus().equalsIgnoreCase(REFUND_REQUESTED))) ? View.GONE : View.VISIBLE);

            btnCancel.setText(_order.getOrderStatus().equalsIgnoreCase(NOT_PLACED) && !_order.getPaymentStatus().equalsIgnoreCase(REFUND_CONFIRMED) ? getString(R.string.refund_order) : getString(R.string.cancel_order));
            btnCancel.setBackground(_order.getOrderStatus().equalsIgnoreCase(NOT_PLACED) && !_order.getPaymentStatus().equalsIgnoreCase(REFUND_CONFIRMED) ? getResources().getDrawable(R.drawable.black_curved_bg) : getResources().getDrawable(R.drawable.curved_rect_red));


            btnDownload.setVisibility((_order.getOrderStatus().equalsIgnoreCase(DELIVERED) && _order.getInvoice() != null && !TextUtils.isEmpty(_order.getInvoice().getInvoiceUrl()) ? View.VISIBLE : View.GONE));


            btnCancel.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View v) {
                    if (_order.getOrderStatus().equalsIgnoreCase(NOT_PLACED))
                        refundConfirmDialog(_order);
                    else
                        showBottomOrderCancelDialog();

                }
            });
            btnDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (_order.getInvoice() != null && !TextUtils.isEmpty(_order.getInvoice().getInvoiceUrl())) {
//                    Intent webIntent = new Intent(OrderDetailsActivity.this, WebViewActivity.class);
//                    webIntent.putExtra(TAG_PDF_URL, _order.getInvoice());
//                    startActivity(webIntent);

                        downloadInvoiceSUrl = _order.getInvoice().getInvoiceUrl();
                        checkWritePermission();
                    } else
                        showToast("No invoice Exist");
                }
            });


            if (_order.getTrackingObject() != null && _order.getTrackingObject().getMessages() != null && !_order.getTrackingObject().getMessages().isEmpty()) {
                setTimeLineStatus(_order.getTrackingObject().getMessages());
            }

        } catch (Exception e) {
            printLog(TAG, "" + e.getMessage());
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @OnClick({R.id.button_orderDetail_help})
    public void onButtonClick(View view) {
        switch (view.getId()) {

            case R.id.button_orderDetail_cancel:
                showBottomOrderCancelDialog();
                break;
            case R.id.button_orderDetail_help:

                if (SELECTED_ORDER != null) {
                    Intent intent = new Intent(OrderDetailsActivity.this, NeedHelpActivity.class);
                    intent.putExtra(TAG_ORDER, SELECTED_ORDER);
                    startActivity(intent);
                }
                break;
        }


    }

    private void refundConfirmDialog(Order _order) {

        AlertDialog.Builder builder = new AlertDialog.Builder(OrderDetailsActivity.this);
//Are you sure you want to remove?
        builder.setMessage("Are you sure?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if (AppController.get().isNetworkAvailable()) {
                            updateViews(VIEW_PROGRESSBAR);
                            Map<String, String> options = new HashMap<>();
                            options.put("order_id", SELECTED_ORDER_ID);
                            AppController.get().refundRequest(options, SELECTED_POSITION);
                        } else
                            showToast(getString(R.string.no_internet));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();

    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void showBottomOrderCancelDialog() {

        View view = getLayoutInflater().inflate(R.layout.bottom_dialog_order_cancel, null);

        RecyclerView rvReason = view.findViewById(R.id.recyclerView_orderCancelDialog_reason);
        TextView tvHint = view.findViewById(R.id.textView_orderCancelDialog_deliveryHint);
        TextView tvComment = view.findViewById(R.id.editText_orderCancelDialog_comment);

        Button btnAddCancel = view.findViewById(R.id.button_orderCancelDialog_cancel);
        Button btnAddSubmit = view.findViewById(R.id.button_orderCancelDialog_submit);

        BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(view);
        dialog.show();

        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(getString(R.string.order_cancel_info)).append("  ");
        builder.setSpan(new ImageSpan(this, R.drawable.ic_info_white), builder.length() - 1, builder.length(), 0);

        tvHint.setText(builder);

        ArrayList<String> reasonList = new ArrayList<>();
        reasonList.add("Not interested");
        reasonList.add("Don't Like product quality");
        reasonList.add("Changed my mind");
        reasonList.add("Other");

        RvCancelReasonAdapter cancelReasonAdapter = new RvCancelReasonAdapter(reasonList);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvReason.setHasFixedSize(true);

        rvReason.setLayoutManager(layoutManager);
        rvReason.setAdapter(cancelReasonAdapter);
//        rvBottomAddress.setNestedScrollingEnabled(true);
        cancelReasonAdapter.notifyDataSetChanged();

        btnAddSubmit.setOnClickListener(v -> {

            String strComment = tvComment.getText().toString().trim();

            if (cancelReasonAdapter.getSelectedSelection()<0)
                showToast("Please select a reason");
            else if (TextUtils.isEmpty(strComment)) {
                showToast("Comment cannot be blank.");
            } else if (AppController.get().isNetworkAvailable()) {
                updateViews(VIEW_PROGRESSBAR);
                Map<String, String> options = new HashMap<>();
                options.put("order_id", SELECTED_ORDER_ID);
                options.put("cancel_reason", reasonList.get(cancelReasonAdapter.getSelectedSelection()));
                options.put("cancel_comments", strComment);
                AppController.get().cancelOrder(options, SELECTED_POSITION);

                dialog.dismiss();
            } else {
                showToast(getString(R.string.no_internet));
            }


        });

        btnAddCancel.setOnClickListener(v -> {


            dialog.dismiss();
        });


    }


    @Subscribe
    public void onHttpGetOrderDetailsSuccess(ApiSuccessListener<OrderDetailsResponse> response) {

        if (response.type.equalsIgnoreCase("getOrderDetail")) {

            OrderDetailsResponse responseModel = response.response.body();
            if (responseModel != null && responseModel.getStatus()) {

                setProductView(responseModel.getOrderDetail());

            } else if (responseModel.getText() != null && TextUtils.isEmpty(responseModel.getText()))

                setErrorView(responseModel.getText(), "", false,NO_DATA);
            else
                setErrorView("No Order Detail found!", "", false,NO_DATA);

        }
    }


    @Subscribe
    public void onHttpSuccess(ApiSuccessListener<JsonObject> response) {

        if (response.type.equalsIgnoreCase("cancelOrder")) {
            dismissProgress();

            try {
                JsonObject jsonObject = response.response.body();
                assert jsonObject != null;
                if (jsonObject.get("status").getAsBoolean()) {


                    showToast(jsonObject.get("text").getAsString());
                    if (SELECTED_ORDER != null)
                        SELECTED_ORDER.setOrderStatus(CANCELLED);
                    IS_UPDATE = true;
                    bottomView.setVisibility(View.GONE);

                    setProductView(SELECTED_ORDER);

                } else {
                    showToast(jsonObject.get("text").getAsString());
                }
            } catch (Exception e) {
                printLog(TAG, " onHttpAddWishSuccess Exception : " + e.getMessage());
            }
        } else if (response.type.equalsIgnoreCase("refundRequest")) {
            dismissProgress();

            try {
                JsonObject jsonObject = response.response.body();
                assert jsonObject != null;
                if (jsonObject.get("status").getAsBoolean()) {

                    showToast(jsonObject.get("text").getAsString());
                    if (SELECTED_ORDER != null)
                        SELECTED_ORDER.setPaymentStatus(REFUND_REQUESTED);
                    IS_UPDATE = true;
                    bottomView.setVisibility(View.GONE);
                    setProductView(SELECTED_ORDER);

                } else {
                    showToast(jsonObject.get("text").getAsString());
                }
            } catch (Exception e) {
                printLog(TAG, " onHttpAddWishSuccess Exception : " + e.getMessage());
            }
        }
    }

    @Subscribe
    public void onHttpError(ApiFailureMessageListener<ErrorModel> response) {

        if (response.type.equalsIgnoreCase("getOrderDetail")) {

            dismissProgress();
            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                setErrorView(errorModel.getText(), "", false,NO_DATA);
            } catch (Exception e) {
                e.printStackTrace();
                showToast(getResources().getString(R.string.error_try_again));
            }


        } else if (response.type.equalsIgnoreCase("cancelOrder")) {

            dismissProgress();
            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                showToast(errorModel.getText());
            } catch (Exception e) {
                e.printStackTrace();
                showToast(getResources().getString(R.string.error_try_again));
            }
        } else if (response.type.equalsIgnoreCase("refundRequest")) {

            dismissProgress();
            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                showToast(errorModel.getText());
            } catch (Exception e) {
                e.printStackTrace();
                showToast(getResources().getString(R.string.error_try_again));
            }


        }
    }

    @Subscribe
    public void onHttpFailure(ApiFailureListener response) {

        if (response.type.equalsIgnoreCase("getOrderDetail")) {
            dismissProgress();
            setErrorView(getResources().getString(R.string.error_try_again), getString(R.string.error_subtitle_failed_one_more_time), true,WRONG_RESPONSE);
        } else if (response.type.equalsIgnoreCase("cancelOrder")) {
            dismissProgress();
            showToast(getResources().getString(R.string.error_try_again));
        } else if (response.type.equalsIgnoreCase("refundRequest")) {
            dismissProgress();
            showToast(getResources().getString(R.string.error_try_again));
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {


        Intent intent = new Intent();
        intent.putExtra(TAG_IS_UPDATE, IS_UPDATE);

        if (SELECTED_ORDER != null)
            intent.putExtra(TAG_ORDER, SELECTED_ORDER);


        intent.putExtra(TAG_POSITION, SELECTED_POSITION);
        setResult(RESULT_OK, intent);


        super.onBackPressed();
    }


    private SpannableStringBuilder convertReferenceText(String _title, String _number) {

        // Use a SpannableStringBuilder so that both the text and the spans are mutable
        SpannableStringBuilder ssb = new SpannableStringBuilder(_title);
        try {


            StyleSpan boldSpan = new StyleSpan(Typeface.BOLD);
// Create a span that will make the text red
            ForegroundColorSpan blackForegroundColorSpan = new ForegroundColorSpan(getResources().getColor(R.color.primaryText));

// Add a blank space
            ssb.append("\n");

// Add the secondWord and apply the strikethrough span to only the second word
            ssb.append("#");
            ssb.append(_number);
            ssb.setSpan(blackForegroundColorSpan, ssb.length() - _number.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ssb.setSpan(boldSpan, ssb.length() - _number.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

// Set the TextView text and denote that it is Editable
// since it's a SpannableStringBuilder
        } catch (Exception e) {
            e.fillInStackTrace();
        }
        return ssb;

    }


    private void checkWritePermission() {
        if (PermissionUtils.hasPermission(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE})) {
            downloadFile();
        } else {
            PermissionUtils.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_WRITE_STORAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {


        if (requestCode == PERMISSION_REQUEST_WRITE_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                downloadFile();
            } else {
                showToast("Please enable all permissions from app settings.");
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }

    private void downloadFile() {


        if (!TextUtils.isEmpty(downloadInvoiceSUrl)) {
            btnDownload.setEnabled(false);
            //working
            new DownloadFromURL().execute(downloadInvoiceSUrl);
        } else
            showToast("Invoice URL Failed!");


    }

    class DownloadFromURL extends AsyncTask<String, String, String> {
        private ProgressDialog progressDialog;
        public static final int progressType = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(OrderDetailsActivity.this);
            progressDialog.setMessage("File is Downloading. Please wait...");
            progressDialog.setIndeterminate(false);
            progressDialog.setMax(100);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... fileUrl) {
            int count;
            try {
                URL url = new URL(fileUrl[0]);
                URLConnection urlConnection = url.openConnection();
                urlConnection.connect();
                // show progress bar 0-100%
                int fileLength = urlConnection.getContentLength();
                InputStream inputStream = new BufferedInputStream(url.openStream(), 8192);


//                String path= Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + File.separator + getFileNameWithExtension(fileUrl[0]);
                String path = fileDirectoryGenerator(PDF) + File.separator + getFileNameWithExtension(fileUrl[0]);

                OutputStream outputStream = new FileOutputStream(path);

                byte[] data = new byte[1024];
                long total = 0;
                while ((count = inputStream.read(data)) != -1) {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / fileLength));
                    outputStream.write(data, 0, count);
                }
                // flushing output
                outputStream.flush();
                // closing streams
                outputStream.close();
                inputStream.close();

                return path;
            } catch (Exception e) {
                printLog("Error: ", e.getMessage());
            }
            return "";
        }

        // progress bar Updating

        protected void onProgressUpdate(String... progress) {
            // progress percentage
            progressDialog.setProgress(Integer.parseInt(progress[0]));
        }


        @Override
        protected void onPostExecute(String fileUrl) {
            if (progressDialog != null)
                progressDialog.dismiss();


            btnDownload.setEnabled(true);

            if (!TextUtils.isEmpty(fileUrl)) {
                showToast("Download Successfully Completed");
                openPDF(fileUrl);
            } else
                showToast("Download Failed!");

        }
    }

    public void openPDF(String filePath) {

        File file = new File(filePath);

//Open it up

        if (file.exists()) {
            try {

//            Uri path = Uri.fromFile(file);
                Uri path = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", file);
//                Uri path = FileProvider.getUriForFile(this, "com.purposecodes.emetwafar.provider", file);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(path, "application/pdf");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(intent);

            } catch (Exception e) {
                showToast("No Application available to view pdf");
            }
        } else
            showToast("File not found!");

    }
}
