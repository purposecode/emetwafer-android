package com.purposecodes.emetwafar.features.mycart;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.purposecodes.emetwafar.controller.SessionController.getCartQuantity;
import static com.purposecodes.emetwafar.controller.SessionController.setCartId;
import static com.purposecodes.emetwafar.controller.SessionController.setCartQuantity;
import static com.purposecodes.emetwafar.controller.SessionController.setTrialMode;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_EXPIRED;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_EXPIRE_FILED;
import static com.purposecodes.emetwafar.utils.LogUtils.printLog;
import static com.purposecodes.emetwafar.utils.UtilsVariable.CALLING_ACTIVITY_KEY;
import static com.purposecodes.emetwafar.utils.UtilsVariable.NO_DATA;
import static com.purposecodes.emetwafar.utils.UtilsVariable.NO_NETWORK;
import static com.purposecodes.emetwafar.utils.UtilsVariable.REQUEST_ORDER_CHECKOUT;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_CHECKOUT;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_PRODUCT_ID;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_DATA;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_ERRORVIEW;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_PROGRESSBAR;
import static com.purposecodes.emetwafar.utils.UtilsVariable.WRONG_RESPONSE;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.api.ApiFailureMessageListener;
import com.purposecodes.emetwafar.api.ApiSuccessListener;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.controller.SessionController;
import com.purposecodes.emetwafar.features.checkout.CheckoutActivity;
import com.purposecodes.emetwafar.features.productview.ProductDetailActivity;
import com.purposecodes.emetwafar.features.subscription.PlanExpiredActivity;
import com.purposecodes.emetwafar.model.CartItem;
import com.purposecodes.emetwafar.responsemodel.CartListResponse;
import com.purposecodes.emetwafar.responsemodel.CheckoutResponse;
import com.purposecodes.emetwafar.responsemodel.ErrorModel;
import com.sk.icode.errorview.ErrorView;
import com.squareup.otto.Subscribe;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

public class MyCartActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener, RvCartAdapter.OnCartClickListener {
    private final String TAG = "MyCartActivity";

    @BindView(R.id.errorView)
    ErrorView errorView;


    @BindView(R.id.recyclerView_myCart)
    RecyclerView recyclerView;


    @BindView(R.id.swipeRefreshLayout_myCart)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.view_cart)
    ViewGroup dataView;


    // RecyclerView date set
    private ArrayList<CartItem> cartList;

    private RvCartAdapter cartAdapter;

    private long EDITED_QUANTITY = 1;

    @SuppressLint("StaticFieldLeak")
    private static MyCartActivity myCartActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        swipeRefreshLayout.setOnRefreshListener(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        myCartActivity = this;
        cartList = new ArrayList<>();

        setUpRecyclerView();

        loadData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_my_cart;
    }


    private void setUpRecyclerView() {

        cartAdapter = new RvCartAdapter(this, cartList, this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(cartAdapter);
        recyclerView.setNestedScrollingEnabled(false);


    }

    @SuppressLint("NotifyDataSetChanged")
    private void loadData() {


        cartList.clear();


        updateViews(VIEW_PROGRESSBAR);
        cartAdapter.notifyDataSetChanged();

        if (AppController.get().isNetworkAvailable()) {
            getCartList();
        } else if (cartList.isEmpty()) {
            setErrorView(getString(R.string.no_internet), "", false, NO_NETWORK);
        }


    }


    private void getCartList() {

        if (AppController.get().isNetworkAvailable()) {
            if (!TextUtils.isEmpty(SessionController.getCartId()))
                AppController.get().getMyCart(SessionController.getCartId());
            else
                AppController.get().generateCartId();
        } else
            setErrorView(getString(R.string.no_internet), "", false, NO_NETWORK);

    }

    //set ErrorView
    private void setErrorView(final String title, final String subTitle, boolean isRetry, int drawable) {

        updateViews(VIEW_ERRORVIEW);

        errorView.setConfig(ErrorView.Config.create()
                .title(title)
                .subtitle(subTitle)
                .retryVisible(isRetry)
                .image(drawable)
                .build());


        errorView.setOnRetryListener(this::loadData);
    }

    private void updateViews(int viewCode) {

        swipeRefreshLayout.setRefreshing(false);

        switch (viewCode) {

            case VIEW_PROGRESSBAR:
                showProgress();

                errorView.setVisibility(GONE);
                break;
            case VIEW_DATA:
                dismissProgress();
                dataView.setVisibility(VISIBLE);
                errorView.setVisibility(GONE);

                break;

            case VIEW_ERRORVIEW:
                dismissProgress();
                dataView.setVisibility(GONE);
                errorView.setVisibility(VISIBLE);
                break;

        }
    }


    @SuppressLint("NotifyDataSetChanged")
    @Subscribe
    public void onHttpGetCartListSuccess(ApiSuccessListener<CartListResponse> response) {

        if (response.type.equalsIgnoreCase("getMyCart")) {

            CartListResponse responseModel = response.response.body();
            if (responseModel != null && responseModel.getStatus()) {

                cartList.clear();
                List<CartItem> list = responseModel.getCartData().getItems();
                if (list != null && list.size() > 0) {
                    cartList.addAll(list);
                    invalidateOptionsMenu();
                }


                updateViews(VIEW_DATA);
                cartAdapter.notifyDataSetChanged();

                if (cartList.isEmpty())
                    setCartQuantity(0);

            } else if (cartList.isEmpty() && responseModel.getText() != null && TextUtils.isEmpty(responseModel.getText()))
                setErrorView(responseModel.getText(), "", false, NO_DATA);
            else if (cartList.isEmpty())
                setErrorView("Your cart is empty!", "", false, NO_DATA);

        }
    }

    @SuppressLint("NotifyDataSetChanged")
    @Subscribe
    public void onHttpGetCheckoutListSuccess(ApiSuccessListener<CheckoutResponse> response) {

        if (response.type.equalsIgnoreCase("checkout")) {

            CheckoutResponse responseModel = response.response.body();
            if (responseModel != null && responseModel.getStatus()) {


                List<CartItem> list = responseModel.getCheckoutData().getCartItems();
                if (list != null && list.size() > 0) {

                    Intent checkoutIntent = new Intent(MyCartActivity.this, CheckoutActivity.class);
                    checkoutIntent.putExtra(TAG_CHECKOUT, responseModel.getCheckoutData());
                    startActivity(checkoutIntent);

                }

                cartAdapter.notifyDataSetChanged();
                updateViews(VIEW_DATA);


            } else if (cartList.isEmpty() && responseModel.getText() != null && TextUtils.isEmpty(responseModel.getText()))
                setErrorView(responseModel.getText(), "", false, NO_DATA);
            else if (cartList.isEmpty())
                setErrorView("Your cart is empty!", "", false, NO_DATA);
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    @Subscribe
    public void onHttpSuccess(ApiSuccessListener<JsonObject> response) {

        if (response.type.equalsIgnoreCase("removeFromMyCart")) {
            updateViews(VIEW_DATA);

            try {
                JsonObject jsonObject = response.response.body();
                assert jsonObject != null;
                if (jsonObject.get("status").getAsBoolean()) {

                    cartList.remove(response.position);
                    cartAdapter.notifyDataSetChanged();
                    showToast(jsonObject.get("text").getAsString());

                    setCartQuantity(getCartQuantity() - 1); // update cart count

                    if (cartList.isEmpty())
                        setErrorView("Your cart is empty!", "", true, NO_DATA);
                    invalidateOptionsMenu();

                    ProductDetailActivity.getInstance().notifyDataSetChange(); //detail  page refresh


                } else {
                    showToast(jsonObject.get("text").getAsString());
                }
            } catch (Exception e) {
                printLog(TAG, " onHttpRemoveCartSuccess Exception : " + e.getMessage());
            }
        } else if (response.type.equalsIgnoreCase("clearMyCart")) {
            updateViews(VIEW_DATA);

            try {
                JsonObject jsonObject = response.response.body();
                assert jsonObject != null;
                if (jsonObject.get("status").getAsBoolean()) {

                    cartList.clear();
                    cartAdapter.notifyDataSetChanged();

                    setCartQuantity(0);  // update cart count

                    setErrorView("Your cart is empty!", "", false, NO_DATA);
                    invalidateOptionsMenu();

                    ProductDetailActivity.getInstance().notifyDataSetChange(); //detail  page refresh

                } else {
                    showToast(jsonObject.get("text").getAsString());
                }
            } catch (Exception e) {
                printLog(TAG, " onHttpRemoveCartSuccess Exception : " + e.getMessage());
            }
        } else if (response.type.equalsIgnoreCase("generateCartId")) {

            try {
                JsonObject jsonObject = response.response.body();
                assert jsonObject != null;
                if (jsonObject.get("status").getAsBoolean()) {
                    String cartId = jsonObject.get("data").getAsJsonObject().get("cart_id").getAsString();
                    setCartId(cartId);


                    String trialMode = "0";
                    if (jsonObject.get("data").getAsJsonObject().get("android_trial_mode") != null)
                        trialMode = jsonObject.get("data").getAsJsonObject().get("android_trial_mode").getAsString();

                    setTrialMode(trialMode.equalsIgnoreCase("1"));


                    if (jsonObject.get("data").getAsJsonObject().get("is_subscribed") != null && jsonObject.get("data").getAsJsonObject().get("is_subscribed").getAsInt() == 0) {
                        Intent intent = new Intent(this, PlanExpiredActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra(CALLING_ACTIVITY_KEY, ACTIVITY_EXPIRE_FILED);
                        startActivity(intent);
                        finish();

                    } else if (jsonObject.get("data").getAsJsonObject().get("is_expired") != null && jsonObject.get("data").getAsJsonObject().get("is_expired").getAsInt() == 0) {// 0 - expired ,1 - not expired

                        Intent intent = new Intent(this, PlanExpiredActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra(CALLING_ACTIVITY_KEY, ACTIVITY_EXPIRED);
                        startActivity(intent);
                        finish();
                    } else
                        loadData();


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (response.type.equalsIgnoreCase("updateCart")) {

            updateViews(VIEW_DATA);
            try {
                JsonObject jsonObject = response.response.body();
                assert jsonObject != null;
                if (jsonObject.get("status").getAsBoolean()) {
                    int cartQuantity = jsonObject.get("data").getAsJsonObject().get("cart_total").getAsInt();

                    if (cartList.size() > response.position) {
                        CartItem c = cartList.get(response.position);


                        NumberFormat format = NumberFormat.getInstance(Locale.US);
                        Number _number = format.parse(c.getWarrantyPrice());
                        assert _number != null;
                        double _price = _number.doubleValue();
                        c.setWarrantyTotal(String.valueOf(EDITED_QUANTITY * _price));


                        c.setQuantity(String.valueOf(EDITED_QUANTITY));

                        cartList.set(response.position, c);
                        cartAdapter.notifyDataSetChanged();
                    }
                } else {
                    showToast(jsonObject.get("text").getAsString());
                }
            } catch (Exception e) {
                printLog(TAG, " Exception : " + e.getMessage());
            }

        }
    }


    @Subscribe
    public void onHttpError(ApiFailureMessageListener<ErrorModel> response) {
        if (response.type.equalsIgnoreCase("getMyCart")) {
            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                setErrorView(errorModel.getText(), "", false, NO_DATA);

            } catch (Exception e) {
                e.printStackTrace();
                setErrorView(getResources().getString(R.string.error_try_again), getString(R.string.error_subtitle_failed_one_more_time), true, WRONG_RESPONSE);

            }
        } else if (response.type.equalsIgnoreCase("updateCart") || response.type.equalsIgnoreCase("removeFromMyCart") || response.type.equalsIgnoreCase("clearMyCart") || response.type.equalsIgnoreCase("checkout") || response.type.equalsIgnoreCase("generateCartId")) {
            updateViews(VIEW_DATA);
            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                showToast(errorModel.getText());
                if (response.type.equalsIgnoreCase("checkout") && errorModel.getText().contains("Some products are not available now")) //reload data view
                    getCartList();


            } catch (Exception e) {
                e.printStackTrace();
                showToast(getResources().getString(R.string.error_try_again));
            }

            if (cartList.isEmpty())
                setErrorView("Your cart is empty!", "", false, NO_DATA);
        }
    }


    @Subscribe
    public void onHttpFailure(ApiFailureListener response) {

        if (response.type.equalsIgnoreCase("getMyCart")) {
            setErrorView(getResources().getString(R.string.error_try_again), getString(R.string.error_subtitle_failed_one_more_time), true, WRONG_RESPONSE);
        } else if (response.type.equalsIgnoreCase("updateCart") || response.type.equalsIgnoreCase("removeFromMyCart") || response.type.equalsIgnoreCase("clearMyCart") || response.type.equalsIgnoreCase("checkout") || response.type.equalsIgnoreCase("generateCartId")) {
            updateViews(VIEW_DATA);
            showToast(getResources().getString(R.string.error_try_again));

            if (cartList.isEmpty())
                setErrorView("Your cart is empty!", "", false, WRONG_RESPONSE);

        }
    }


    @Override
    public void onRefresh() {
        loadData();
    }

    @Override
    public void onRemoveClicked(CartItem item, int position) {


        AlertDialog.Builder builder = new AlertDialog.Builder(MyCartActivity.this);
//Are you sure you want to remove?
        builder.setMessage("Are you sure?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if (AppController.get().isNetworkAvailable()) {
                            updateViews(VIEW_PROGRESSBAR);
                            AppController.get().removeFromMyCart(SessionController.getCartId(), item.getProductId(), position);
                        } else
                            showToast(getString(R.string.no_internet));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();

    }

    @Override
    public void onQuantityClicked(CartItem item, int position) {

        showEditQuantityDialog(item, position);
    }

    @Override
    public void onCartClicked(CartItem item, int position) {
        Intent intent = new Intent(MyCartActivity.this, ProductDetailActivity.class);
        intent.putExtra(TAG_PRODUCT_ID, item.getProductId());
        startActivity(intent);
    }


    // Menu item
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_clear, menu);

        //hide edit option
        MenuItem item = menu.findItem(R.id.action_clear);
        if (cartList.isEmpty())
            item.setVisible(false);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {

            case R.id.action_clear:

                if (AppController.get().isNetworkAvailable()) {


                    if (!cartList.isEmpty())
                        clearCartDialog();
                    else
                        setErrorView("Your cart is empty!", "", false, NO_DATA);
                } else
                    showToast(getResources().getString(R.string.no_internet));
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
//            if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_ORDER_CHECKOUT && TextUtils.isEmpty(SessionController.getCartId())) {

                printLog(TAG, "onActivityResult requestCode 2: ");
                loadData();
                setErrorView("Your cart is empty!", "", false, NO_DATA);

            }
//            }
        } catch (IndexOutOfBoundsException e) {
            e.getLocalizedMessage();
        }
    }


    @Subscribe
    public void onHttpGenerateCartIdSuccess(ApiSuccessListener<JsonObject> response) {

        if (response.type.equalsIgnoreCase("generateCartId")) {

            try {
                JsonObject jsonObject = response.response.body();
                assert jsonObject != null;
                if (jsonObject.get("status").getAsBoolean()) {
                    String cartId = jsonObject.get("data").getAsJsonObject().get("cart_id").getAsString();
                    setCartId(cartId);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @OnClick(R.id.button_myCart_proceed)
    public void cartCheckout() {
        if (AppController.get().isNetworkAvailable()) {
            updateViews(VIEW_PROGRESSBAR);
            Map<String, String> options = new HashMap<>();
            options.put("cart_id", SessionController.getCartId());
            AppController.get().checkout(options);

        } else
            showToast(getString(R.string.no_internet));
    }

    @SuppressWarnings("ConstantConditions")
    private void showEditQuantityDialog(CartItem item, int position) {

        if (item.getAvailableQuantity() == null || TextUtils.isEmpty(item.getAvailableQuantity()))
            item.setAvailableQuantity(String.valueOf(1));

        EDITED_QUANTITY = 1;
        final Dialog dialog = new Dialog(MyCartActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_edit_qty);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);
        final EditText etQuantity = dialog.findViewById(R.id.editText_editCartDialog_Qty);
        ImageButton ibDown = dialog.findViewById(R.id.imageButton_editCartDialog_QtyDown);
        ImageButton ibUp = dialog.findViewById(R.id.imageButton_editCartDialog_QtyUp);
        ImageButton ibClose = dialog.findViewById(R.id.imageButton_editCartDialog_close);
        Button btnSubmit = dialog.findViewById(R.id.button_editCartDialog_submit);


        etQuantity.setText("");
        etQuantity.append(String.valueOf(item.getQuantity()));
        ibDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                hideSoftKeyboard(etQuantity);

                try {

                    String text = etQuantity.getText().toString().trim();
                    long previousSize = 0;

                    if (!text.isEmpty())
                        previousSize = Long.parseLong(text);

                    /**decrement numbers up to 1*/
                    if (previousSize > 1) {
                        previousSize--;
                        etQuantity.setText("");
                        etQuantity.append(String.valueOf(previousSize));
                    }

                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }


            }
        });

        ibUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                hideSoftKeyboard(etQuantity);

                try {
                    String text = etQuantity.getText().toString().trim();

                    long nextSize = 0;

                    if (!text.isEmpty())
                        nextSize = Long.parseLong(text);

                    /**increment numbers */

                    long stock = (item.getAvailableQuantity() == null || TextUtils.isEmpty(item.getAvailableQuantity())) ? 0 : Long.parseLong(item.getAvailableQuantity());

                    if (nextSize < stock) {
                        nextSize++;
                        etQuantity.setText("");
                        etQuantity.append(String.valueOf(nextSize));
                    } else
                        showToast("Maximum stock is " + item.getAvailableQuantity());


                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }

            }
        });


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etQuantity.clearFocus();
                hideSoftKeyboard(etQuantity);

                String qtyString = etQuantity.getText().toString().trim();
                long quantity = TextUtils.isEmpty(qtyString) ? 0 : Long.parseLong(qtyString);

                long stock = (item.getAvailableQuantity() == null || TextUtils.isEmpty(item.getAvailableQuantity())) ? 0 : Long.parseLong(item.getAvailableQuantity());

                if (quantity < 1)
                    showToast("Please enter a valid quantity");
                else if (stock < quantity)
                    showToast("Maximum stock is " + item.getAvailableQuantity());
                else if (AppController.get().isNetworkAvailable()) {
                    updateViews(VIEW_PROGRESSBAR);

                    EDITED_QUANTITY = quantity;
                    Map<String, String> options = new HashMap<>();
                    options.put("product_id", item.getProductId());
                    options.put("quantity", String.valueOf(quantity));
                    options.put("cart_id", SessionController.getCartId());
                    AppController.get().updateCart(options, position);
                    dialog.dismiss();

                } else
                    showToast(getResources().getString(R.string.no_internet));


            }
        });


        ibClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etQuantity.clearFocus();
                hideSoftKeyboard(etQuantity);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    //clear all cart items
    private void clearCartDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(MyCartActivity.this);
//Are you sure you want to remove?
        builder.setMessage("Are you sure?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        updateViews(VIEW_PROGRESSBAR);
                        AppController.get().clearMyCart(SessionController.getCartId());
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();

    }

    public static MyCartActivity getInstance() {
        return myCartActivity;
    }
}
