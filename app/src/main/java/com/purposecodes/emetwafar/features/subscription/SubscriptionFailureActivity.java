package com.purposecodes.emetwafar.features.subscription;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.model.PlanData;
import com.telr.mobile.sdk.activty.WebviewActivity;
import com.telr.mobile.sdk.entity.response.status.StatusResponse;

import butterknife.BindView;
import butterknife.OnClick;

import static android.view.View.VISIBLE;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_NEW_PLAN;

public class SubscriptionFailureActivity extends BaseActivity {
    @BindView(R.id.button_subscriptionFail_close)
    Button btnClose;


    @BindView(R.id.textView_subscriptionFail_title)
    TextView tvTitle;

    @BindView(R.id.textView_subscriptionFail_description)
    TextView tvDescription;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_subscription_failure;
    }


    @OnClick({R.id.button_subscriptionFail_close})
    public void onButtonClick(View view) {

        if (view.getId() == R.id.button_subscriptionFail_close) {
            PlanData p = AppController.get().getPlanData();

            if (p != null && p.getCallingIntent() != ACTIVITY_NEW_PLAN) {
                finish();
            } else {
                AppController.get().logoutUser();
            }
            AppController.get().setPlanData(null);
        }


    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        Object object = intent.getParcelableExtra(WebviewActivity.PAYMENT_RESPONSE);

//        if (object!=null){

        if (object instanceof StatusResponse) {
            StatusResponse status = (StatusResponse) object;

            tvDescription.setVisibility(VISIBLE);
            tvDescription.setText(tvDescription.getText() + " : " + status.getTrace() + " \n" + status.getAuth().getMessage());

            if (status.getAuth() != null) {
                status.getAuth().getStatus();   // Authorisation status. A indicates an authorised transaction. H also indicates an authorised transaction, but where the transaction has been placed on hold. Any other value indicates that the request could not be processed.
                status.getAuth().getAvs();      /* Result of the AVS check:
                                            Y = AVS matched OK
                                            P = Partial match (for example, post-code only)
                                            N = AVS not matched
                                            X = AVS not checked
                                            E = Error, unable to check AVS */
                status.getAuth().getCode();     // If the transaction was authorised, this contains the authorisation code from the card issuer. Otherwise it contains a code indicating why the transaction could not be processed.
                status.getAuth().getMessage();  // The authorisation or processing error message.
                status.getAuth().getCa_valid();
                status.getAuth().getCardcode(); // Code to indicate the card type used in the transaction. See the code list at the end of the document for a list of card codes.
                //  status.getAuth().getCardlast4();// The last 4 digits of the card number used in the transaction. This is supplied for all payment types (including the Hosted Payment Page method) except for PayPal.
                status.getAuth().getCvv();      /* Result of the CVV check:
                                           Y = CVV matched OK
                                           N = CVV not matched
                                           X = CVV not checked
                                           E = Error, unable to check CVV */
                status.getAuth().getTranref(); //The payment gateway transaction reference allocated to this request.
                status.getAuth().getAvs();     /* Result of the AVS check:
                                           Y = AVS matched OK
                                           P = Partial match (for example, post-code only)
                                           N = AVS not matched
                                           X = AVS not checked
                                           E = Error, unable to check AVS */
            }
        } else if (object instanceof String) {
            String errorMessage = (String) object;
            tvDescription.setText(tvDescription.getText() + " : " + errorMessage);
        }
    }
}
