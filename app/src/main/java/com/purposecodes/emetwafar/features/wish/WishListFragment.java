package com.purposecodes.emetwafar.features.wish;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.api.ApiFailureMessageListener;
import com.purposecodes.emetwafar.api.ApiSuccessListener;
import com.purposecodes.emetwafar.cache.CacheStorage;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.features.productview.ProductDetailActivity;
import com.purposecodes.emetwafar.fragment.BaseFragment;
import com.purposecodes.emetwafar.model.WishItem;
import com.purposecodes.emetwafar.responsemodel.ErrorModel;
import com.purposecodes.emetwafar.responsemodel.WishListResponse;
import com.purposecodes.emetwafar.view.EndlessScrollListener;
import com.sk.icode.errorview.ErrorView;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.purposecodes.emetwafar.cache.CacheStorage.WISHLIST_CACHE_KEY;
import static com.purposecodes.emetwafar.controller.SessionController.setWishQuantity;
import static com.purposecodes.emetwafar.utils.LogUtils.printLog;
import static com.purposecodes.emetwafar.utils.UtilsVariable.NO_DATA;
import static com.purposecodes.emetwafar.utils.UtilsVariable.NO_NETWORK;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_PRODUCT_ID;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_ERRORVIEW;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_PROGRESSBAR;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_RECYCLERVIEW;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_SWIPEREFRESH;
import static com.purposecodes.emetwafar.utils.UtilsVariable.WRONG_RESPONSE;


public class WishListFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, RvWishListAdapter.onRemoveWishListListener {
    private final String TAG = "WishListFragment";




    @BindView(R.id.errorView)
    ErrorView errorView;


    @BindView(R.id.recyclerView_layout_list)
    RecyclerView recyclerView;


    @BindView(R.id.swipeRefreshLayout_layout_list)
    SwipeRefreshLayout swipeRefreshLayout;

    // RecyclerView date set
    private ArrayList<WishItem> wishList;

    private RvWishListAdapter wisListAdapter;

    // Scroll Listener for pagination
    private EndlessScrollListener mScrollListener;

    // Current page
    private int mCurrentPage = 0;
    private final int mTotalPages = -1;

    private final String mPageCount = "15";

/*
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        dashboardViewModel = ViewModelProviders.of(this).get(DashboardViewModel.class);

        dashboardViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

            }
        });

        swipeRefreshLayout.setOnRefreshListener(this);

        setUpRecyclerView();

            loadData();

        return super.onCreateView(inflater, container, savedInstanceState);


    }
*/

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    /*    dashboardViewModel = ViewModelProviders.of(this).get(DashboardViewModel.class);

        dashboardViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

            }
        });
*/
        swipeRefreshLayout.setOnRefreshListener(this);

        setUpRecyclerView();

        loadData();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_list;
    }


    private void setUpRecyclerView() {

        wishList = new ArrayList<>();
        wisListAdapter = new RvWishListAdapter(getContext(), wishList, this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(wisListAdapter);
        recyclerView.setNestedScrollingEnabled(true);
        mScrollListener = new EndlessScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int currentPage) {
                if (mCurrentPage < mTotalPages) {
                    showRecyclerViewLoading(true);
                    getWishLists(mCurrentPage + 1);
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        };

        recyclerView.addOnScrollListener(mScrollListener);
    }

    private void loadData() {
        wishList.clear();
        updateViews(VIEW_SWIPEREFRESH);

        mCurrentPage = 0;
        wishList.addAll(readCache());


        wisListAdapter.notifyDataSetChanged();

        if (AppController.get().isNetworkAvailable()) {
            getWishLists(mCurrentPage + 1);  // First page
        } else if (wishList.isEmpty()) {
            setErrorView(getString(R.string.no_internet), "", false, NO_NETWORK);
        } else {
            swipeRefreshLayout.setRefreshing(false);
            showToast(getString(R.string.no_internet));
        }
    }


    private void showRecyclerViewLoading(final boolean showLoading) {
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                wisListAdapter.setLoading(showLoading);
            }
        });
    }

    private void getWishLists(int pageNo) {

        AppController.get().getWishList();
    }


    //set ErrorView
//    private void setErrorView(final String title, final String subTitle, boolean isRetry) {
//        updateViews(VIEW_ERRORVIEW);


    private void setErrorView(final String title, final String subTitle, boolean isRetry, int drawable) {

        updateViews(VIEW_ERRORVIEW);

        errorView.setConfig(ErrorView.Config.create()
                .title(title)
                .subtitle(subTitle)
                .retryVisible(isRetry)
                .image(drawable)
                .build());


        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {
                loadData();
            }
        });
    }

    private void updateViews(int viewCode) {
        switch (viewCode) {
            case VIEW_PROGRESSBAR:
                showProgress();

                errorView.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                break;
            case VIEW_RECYCLERVIEW:
                dismissProgress();
                recyclerView.setVisibility(View.VISIBLE);
                errorView.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                break;

            case VIEW_ERRORVIEW:
                dismissProgress();
                recyclerView.setVisibility(View.GONE);
                errorView.setVisibility(View.VISIBLE);
                swipeRefreshLayout.setRefreshing(false);
                break;
            case VIEW_SWIPEREFRESH:
                dismissProgress();
                swipeRefreshLayout.setRefreshing(true);
                break;
        }
    }


    @Subscribe
    public void onHttpGetWishListSuccess(ApiSuccessListener<WishListResponse> response) {

        try {


            if (response.type.equalsIgnoreCase("getWishList")) {
                WishListResponse responseModel = response.response.body();
                if (responseModel != null && responseModel.getStatus()) {


//                mCurrentPage = Integer.parseInt(responseModel.getResponseResult().getCurrentPage());
//                mTotalPages = responseModel.getResponseResult().getTotalCount();

//                mCurrentPage=1;
//                if (mCurrentPage == 1) {
//                    wishList.clear();
//                }

                    wishList.clear();
                    List<WishItem> list = responseModel.getWishItemList();
                    if (list != null && list.size() > 0) {
                        wishList.addAll(list);
                    }

                    setWishQuantity(wishList.size());


                    wisListAdapter.notifyDataSetChanged();

                    updateViews(VIEW_RECYCLERVIEW);


                } else if (mCurrentPage == 0) {
                    wishList.clear();
                    wisListAdapter.notifyDataSetChanged();
                    setErrorView(responseModel.getText(), "", false, NO_DATA);
                } else if (wishList.isEmpty() && responseModel.getText() != null && TextUtils.isEmpty(responseModel.getText()))
                    setErrorView(responseModel.getText(), "", false, NO_DATA);
                else
                    setErrorView("No Data found!", "", false, NO_DATA);


                showRecyclerViewLoading(false);

                String data = new Gson().toJson(wishList);
                CacheStorage.getInstance().writeCache(WISHLIST_CACHE_KEY, data);


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Subscribe
    public void onHttpRemoveWishSuccess(ApiSuccessListener<JsonObject> response) {

        if (response.type.equalsIgnoreCase("addToWish_wishList")) {
            updateViews(VIEW_RECYCLERVIEW);

            try {
                JsonObject jsonObject = response.response.body();
                assert jsonObject != null;
                if (jsonObject.get("status").getAsBoolean()) {

                    wishList.remove(response.position);
                    wisListAdapter.notifyDataSetChanged();
                    showToast(jsonObject.get("text").getAsString());
                    if (wishList.isEmpty())
                        loadData();

                    setWishQuantity(wishList.size());

                } else {
                    showToast(jsonObject.get("text").getAsString());
                }
            } catch (Exception e) {
                printLog(TAG, " onHttpAddWishSuccess Exception : " + e.getMessage());
            }
        } else if (response.type.equalsIgnoreCase("removeFromWish_wishList")) {
            updateViews(VIEW_RECYCLERVIEW);

            try {
                JsonObject jsonObject = response.response.body();
                assert jsonObject != null;
                if (jsonObject.get("status").getAsBoolean()) {


                    wishList.remove(response.position);
                    wisListAdapter.notifyDataSetChanged();
                    showToast(jsonObject.get("text").getAsString());

                    setWishQuantity(wishList.size());

                    if (wishList.isEmpty())
                        loadData();

                } else {
                    showToast(jsonObject.get("text").getAsString());
                }
            } catch (Exception e) {
                printLog(TAG, " onHttpAddWishSuccess Exception : " + e.getMessage());
            }
        }
    }


    @Subscribe
    public void onHttpError(ApiFailureMessageListener<ErrorModel> response) {
        //

        if (response.type.equalsIgnoreCase("getWishList")) {

            showRecyclerViewLoading(false);
            mScrollListener.setLoading(false);

            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);

                if (mCurrentPage != 0) {
                    showToast(errorModel.getText());
                    updateViews(VIEW_RECYCLERVIEW);
                } else {
                    setErrorView(errorModel.getText(), "", false, NO_DATA);
                }
            } catch (Exception e) {
                e.printStackTrace();
                setErrorView(getResources().getString(R.string.error_try_again), getString(R.string.error_subtitle_failed_one_more_time), true, WRONG_RESPONSE);

            }


        } else if (response.type.equalsIgnoreCase("addToWish_wishList") || response.type.equalsIgnoreCase("removeFromWish_wishList")) {

            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                showToast(errorModel.getText());
                updateViews(VIEW_RECYCLERVIEW);
            } catch (Exception e) {
                e.printStackTrace();
                showToast(getResources().getString(R.string.error_try_again));

            }

        }
    }


    @Subscribe
    public void onHttpFailure(ApiFailureListener response) {

        if (response.type.equalsIgnoreCase("getWishList")) {

            if (mCurrentPage != 0) {
                showToast(getResources().getString(R.string.error_try_again));
                updateViews(VIEW_RECYCLERVIEW);
            } else {
                setErrorView(getResources().getString(R.string.error_try_again), getString(R.string.error_subtitle_failed_one_more_time), true, WRONG_RESPONSE);

            }
            showRecyclerViewLoading(false);
            mScrollListener.setLoading(false);

        } else if (response.type.equalsIgnoreCase("addToWish_wishList") || response.type.equalsIgnoreCase("removeFromWish_wishList")) {

            updateViews(VIEW_RECYCLERVIEW);
            showToast(getResources().getString(R.string.error_try_again));


        }

    }


    @Override
    public void onRefresh() {
        loadData();
    }

    @Override
    public void onWishListClicked(WishItem wishItem, int position) {
        Intent intent = new Intent(getActivity(), ProductDetailActivity.class);
        intent.putExtra(TAG_PRODUCT_ID, wishItem.getProductId());
        startActivity(intent);
    }

    @Override
    public void onRemovedWishList(WishItem wishItem, int position) {
        showDeleteAlert(wishItem, position);
    }

    private void showDeleteAlert(final WishItem wishItem, final int position) {

        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setMessage("Do you  want to remove from the Wishlist?");
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (AppController.get().isNetworkAvailable()) {
                    Map<String, String> options = new HashMap<>();
                    options.put("product_id", wishItem.getProductId());
//                    AppController.get().addToWish(options, "wishList", position);
                    AppController.get().removeFromWish(wishItem.getWishId(), "wishList", position);

                    updateViews(VIEW_PROGRESSBAR);
                } else {
                    showToast(getString(R.string.no_internet));
                }

            }
        });
        alert.setNegativeButton("No", (dialogInterface, i) -> dialogInterface.dismiss());
        alert.create().show();
    }


    private List<WishItem> readCache() {
        List<WishItem> list = new ArrayList<>();
        try {
            Gson gson = new Gson();
            list = Arrays.asList(gson.fromJson(CacheStorage.getInstance().readCache(WISHLIST_CACHE_KEY), WishItem[].class));
        } catch (NullPointerException e) {
            list = new ArrayList<>();
        }
        return list;
    }
}
