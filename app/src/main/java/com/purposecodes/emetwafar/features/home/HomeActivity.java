package com.purposecodes.emetwafar.features.home;

import static com.purposecodes.emetwafar.controller.SessionController.getCartQuantity;
import static com.purposecodes.emetwafar.controller.SessionController.getNotificationCount;
import static com.purposecodes.emetwafar.controller.SessionController.getTrialMode;
import static com.purposecodes.emetwafar.controller.SessionController.getWishQuantity;
import static com.purposecodes.emetwafar.controller.SessionController.setCartId;
import static com.purposecodes.emetwafar.controller.SessionController.setCartQuantity;
import static com.purposecodes.emetwafar.controller.SessionController.setNotificationCount;
import static com.purposecodes.emetwafar.controller.SessionController.setTrialMode;
import static com.purposecodes.emetwafar.controller.SessionController.setWishQuantity;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_EXPIRED;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_EXPIRE_FILED;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_HOME_DEFAULT;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_HOME_MYPROFILE;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.capitalizeString;
import static com.purposecodes.emetwafar.utils.UtilsVariable.CALLING_ACTIVITY_KEY;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.api.ApiFailureMessageListener;
import com.purposecodes.emetwafar.api.ApiSuccessListener;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.features.coupons.MyCouponsActivity;
import com.purposecodes.emetwafar.features.myaccount.CompanyProfileActivity;
import com.purposecodes.emetwafar.features.mycart.MyCartActivity;
import com.purposecodes.emetwafar.features.notification.NotificationListActivity;
import com.purposecodes.emetwafar.features.order.MyOrdersActivity;
import com.purposecodes.emetwafar.features.subscription.PlanExpiredActivity;
import com.purposecodes.emetwafar.features.subscription.SubscriptionHistoryActivity;
import com.purposecodes.emetwafar.model.UserData;
import com.purposecodes.emetwafar.responsemodel.ErrorModel;
import com.squareup.otto.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class HomeActivity extends BaseActivity {

    private final String TAG = "HomeActivity";

    private int callingActivity;

    // TextView to show cart count  on action bar
    private TextView tvCartCount;
    private TextView tvNotificationCount;

    // Widgets
    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.imageView_nav_profile)
    CircleImageView ivProfile;
    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_nav_userName)
    TextView tvNavUsername;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_nav_email)
    TextView tvNavEmail;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.imageView_navProfile_verify)
    ImageView ivProfileVerify;

    private UserData userData;

    private ActionBarDrawerToggle toggle;


    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.nav_view)
    BottomNavigationView navView;


    private View notificationsBadge = null;

    private TextView wishBadge;

    @SuppressLint("StaticFieldLeak")
    private static HomeActivity homeActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);
//        BottomNavigationView navView = findViewById(R.id.nav_view);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(R.id.navigation_products, R.id.navigation_wishlist, R.id.navigation_myaccount).build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);


        toggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setHomeAsUpIndicator(R.drawable.ic_nav_menu); //set your own
        mDrawerLayout.addDrawerListener(toggle);

        homeActivity = this;

        toggle.syncState();

        userData = AppController.get().getUser();

        if (userData == null) {
            AppController.get().logoutUser();
            return;
        }
        generateCartId();

        try {


            callingActivity = getIntent().getIntExtra(CALLING_ACTIVITY_KEY, ACTIVITY_HOME_DEFAULT);


        } catch (Exception e) {
            e.printStackTrace();
        }


        setProfile();




    }


    private View getBadge() {
        if (notificationsBadge != null) {
            return notificationsBadge;
        }
        ViewGroup mbottomNavigationMenuView = (BottomNavigationMenuView) navView.getChildAt(0);

        notificationsBadge = LayoutInflater.from(HomeActivity.this).inflate(R.layout.item_nav_badge, mbottomNavigationMenuView, false);

        wishBadge = notificationsBadge.findViewById(R.id.textView_badge);
        navView.addView(notificationsBadge);


        return notificationsBadge;
    }


    private void addBadge(int count) {

        getBadge();

        if (count > 0)
            wishBadge.setText(badgeParser(count));

        wishBadge.setVisibility((count > 0) ? View.VISIBLE : View.GONE);

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_home;
    }

    /**
     * Setting up Navigation drawer items in navigation recycler view.
     */
    private void setUpNavigationDrawerItems() {

        mDrawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
    }


    @OnClick({R.id.imageView_nav_profile, R.id.button_nav_products, R.id.button_nav_offer, R.id.button_nav_myCoupons, R.id.button_nav_myOrders, R.id.button_nav_companyProfile, R.id.button_nav_mayCart, R.id.button_nav_subscriptionHistory, R.id.button_nav_help, R.id.button_nav_logout})
    public void onNavItemClick(View view) {

        switch (view.getId()) {
            case R.id.imageView_nav_profile:
                navView.setSelectedItemId(R.id.navigation_myaccount);
                break;
            case R.id.button_nav_products:
                navView.setSelectedItemId(R.id.navigation_products);
                break;
            case R.id.button_nav_offer:
                showToast("Offer products not available");
                break;
            case R.id.button_nav_myCoupons:
                startActivity(new Intent(HomeActivity.this, MyCouponsActivity.class));
                break;
            case R.id.button_nav_myOrders:
                startActivity(new Intent(HomeActivity.this, MyOrdersActivity.class));
                break;
            case R.id.button_nav_companyProfile:
                Intent intent = new Intent(HomeActivity.this, CompanyProfileActivity.class);
                startActivity(intent);
//                navView.setSelectedItemId(R.id.navigation_myaccount);
                break;
            case R.id.button_nav_mayCart:
                startActivity(new Intent(HomeActivity.this, MyCartActivity.class));
                break;
            case R.id.button_nav_subscriptionHistory:
                startActivity(new Intent(HomeActivity.this, SubscriptionHistoryActivity.class));
                break;
            case R.id.button_nav_help:


                break;

            case R.id.button_nav_logout:
                showLogoutAlert();
                break;
        }

        mDrawerLayout.closeDrawer(GravityCompat.START);
    }


    public void setProfile() {

        try {

//            printLog(TAG, " setProfile  token :  " + userData.getTokenId());
            Glide.with(this)
                    .load(userData.getProfileurl())
                    .apply(new RequestOptions()
                            .centerCrop()
                            .placeholder(R.drawable.placeholder_user)
                            .error(R.drawable.placeholder_user))
                    .into(ivProfile);


            tvNavUsername.setText(capitalizeString(userData.getCompany()));
            tvNavEmail.setText(userData.getEmail());

            ivProfileVerify.setVisibility(userData.getVerifyStatus().equals("1") ? View.VISIBLE : View.GONE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Menu item
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);

        MenuItem itemCart = menu.findItem(R.id.action_cart);
        MenuItem itemNotification = menu.findItem(R.id.action_notification);

        View actionCartView = itemCart.getActionView();
        View actionNotificationView = itemNotification.getActionView();
        tvCartCount = actionCartView.findViewById(R.id.tvCartCount);
        tvNotificationCount = actionNotificationView.findViewById(R.id.textView_NotificationCount);

        actionCartView.findViewById(R.id.ivCartIcon).setOnClickListener(view -> {
            startActivity(new Intent(HomeActivity.this, MyCartActivity.class));

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    tvCartCount.setText("");
                    tvCartCount.setVisibility(View.GONE);
                }
            }, 500);
        });


        actionNotificationView.findViewById(R.id.imageView_NotificationIcon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, NotificationListActivity.class));

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        tvNotificationCount.setText("");
                        tvNotificationCount.setVisibility(View.GONE);
                    }
                }, 500);
            }
        });


        if (getCartQuantity() != 0) {
            tvCartCount.setText(badgeParser(getCartQuantity()));
            tvCartCount.setVisibility(View.VISIBLE);
        }

        if (getNotificationCount() != 0) {
            tvNotificationCount.setText(badgeParser(getNotificationCount()));
            tvNotificationCount.setVisibility(View.VISIBLE);
        }


        //wish badge count
        addBadge(getWishQuantity());


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {

            case android.R.id.home:

                return true;

            case R.id.action_cart:
                startActivity(new Intent(HomeActivity.this, MyCartActivity.class));
                return true;
            case R.id.action_notification:
                startActivity(new Intent(HomeActivity.this, NotificationListActivity.class));
                return true;
//                case R.id.action_search:
//                startActivity(new Intent(HomeActivity.this, SearchableActivity.class));
//                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void generateCartId() {

        if (AppController.get().isNetworkAvailable()) {
            AppController.get().generateCartId();
        }

    }

    @Subscribe
    public void invalidateUpdateDataChange(ApiFailureListener response) {

        if (response.type.equalsIgnoreCase("inValidateWishBadge")) {
            addBadge(getWishQuantity());
        }
        if (response.type.equalsIgnoreCase("inValidateTrialMode")) {
            generateCartId();
            findViewById(R.id.button_nav_subscriptionHistory).setVisibility(getTrialMode() ? View.GONE : View.VISIBLE);

        }
    }

    @Subscribe
    public void onHttpGenerateCartIdSuccess(ApiSuccessListener<JsonObject> response) {

        if (response.type.equalsIgnoreCase("generateCartId")) {

            try {
                JsonObject jsonObject = response.response.body();
                assert jsonObject != null;
                if (jsonObject.get("status").getAsBoolean()) {
                    String cartId = jsonObject.get("data").getAsJsonObject().get("cart_id").getAsString();
                    int cartQuantity = jsonObject.get("data").getAsJsonObject().get("cart_total").getAsInt();
                    int wishTotal = jsonObject.get("data").getAsJsonObject().get("wish_total").getAsInt();

                    String trialMode = "0";
                    if (jsonObject.get("data").getAsJsonObject().get("android_trial_mode") != null)
                        trialMode = jsonObject.get("data").getAsJsonObject().get("android_trial_mode").getAsString();

                    findViewById(R.id.button_nav_subscriptionHistory).setVisibility(trialMode.equalsIgnoreCase("1") ? View.GONE : View.VISIBLE);
                    int notiCount = jsonObject.get("data").getAsJsonObject().get("notification_count").getAsInt();

                    setCartQuantity(cartQuantity);
                    setNotificationCount(notiCount);
                    setWishQuantity(wishTotal);

                    if (getTrialMode() != trialMode.equalsIgnoreCase("1"))//check saved mode
                        setTrialMode(trialMode.equalsIgnoreCase("1"));

                    invalidateOptionsMenu();


                    setCartId(cartId);


                    if (jsonObject.get("data").getAsJsonObject().get("is_subscribed") != null && jsonObject.get("data").getAsJsonObject().get("is_subscribed").getAsInt() == 0) {
                        Intent intent = new Intent(this, PlanExpiredActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra(CALLING_ACTIVITY_KEY, ACTIVITY_EXPIRE_FILED);
                        startActivity(intent);
                        finish();

                    } else if (jsonObject.get("data").getAsJsonObject().get("is_expired") != null && jsonObject.get("data").getAsJsonObject().get("is_expired").getAsInt() == 0) {// 0 - expired ,1 - not expired

                        Intent intent = new Intent(this, PlanExpiredActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra(CALLING_ACTIVITY_KEY, ACTIVITY_EXPIRED);
                        startActivity(intent);
                        finish();
                    }


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Subscribe
    public void onHttpRegTokenSuccess(ApiSuccessListener<JsonObject> response) {

        if (response.type.equalsIgnoreCase("logout")) {
            dismissProgress();
            JsonObject jsonObject = response.response.body();
            assert jsonObject != null;
            if (jsonObject.get("status").getAsBoolean() && jsonObject.get("text").getAsString().equalsIgnoreCase("User logged out successfully!")) {
                AppController.get().logoutUser();
            } else
                showToast(jsonObject.get("text").getAsString());
        }
    }

    @Subscribe
    public void onHttpError(ApiFailureMessageListener<ErrorModel> response) {


        if (response.type.equalsIgnoreCase("logout") || response.type.equalsIgnoreCase("changePassword")) {
            dismissProgress();
            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                showToast(errorModel.getText());
            } catch (Exception e) {
                e.printStackTrace();
                showToast(getString(R.string.error_try_again));

            }
        }
    }


    @Subscribe
    public void onHttpFailure(ApiFailureListener response) {

        if (response.type.equalsIgnoreCase("logout") || response.type.equalsIgnoreCase("changePassword")) {

            dismissProgress();
            showToast(getResources().getString(R.string.error_try_again));
        }
    }

    @Override
    public void onBackPressed() {


        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
            return;
        } else {
            super.onBackPressed();
        }

    }


    private void showLogoutAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
        builder.setMessage("Are you sure you want to Log Out?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                UserData _user = AppController.get().getUser();

                if (_user != null) {
                    if (AppController.get().isNetworkAvailable()) {
                        showProgress();
                        AppController.get().logout();
                    } else
                        showToast(getString(R.string.no_internet));
                } else
                    AppController.get().logoutUser();

            }
        });
        builder.setNegativeButton("No", (dialog, which) -> dialog.dismiss());
        builder.create().show();
    }


    private String badgeParser(int count) {
        String mCount = String.valueOf(count);
        if (mCount.length() > 2)
            mCount = "99+";
        return mCount;
    }

    @Override
    protected void onStart() {
        super.onStart();
        invalidateOptionsMenu();

        if (callingActivity == ACTIVITY_HOME_MYPROFILE)
            navView.setSelectedItemId(R.id.navigation_myaccount);

    }


    public static HomeActivity getInstance() {
        return homeActivity;
    }


}
