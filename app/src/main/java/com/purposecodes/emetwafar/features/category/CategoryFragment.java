package com.purposecodes.emetwafar.features.category;

import static com.purposecodes.emetwafar.cache.CacheStorage.CATEGORY_CACHE_KEY;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TYPE_TAG_CATEGORY;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_DATA;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_ERRORVIEW;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.api.ApiFailureMessageListener;
import com.purposecodes.emetwafar.api.ApiSuccessListener;
import com.purposecodes.emetwafar.cache.CacheStorage;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.features.home.RvHomeCategoryTitleAdapter;
import com.purposecodes.emetwafar.features.productview.ProductFilterActivity;
import com.purposecodes.emetwafar.features.productview.ProductListFragment;
import com.purposecodes.emetwafar.features.search.SearchableActivity;
import com.purposecodes.emetwafar.fragment.BaseFragment;
import com.purposecodes.emetwafar.listener.OnCategoryListener;
import com.purposecodes.emetwafar.model.Category;
import com.purposecodes.emetwafar.responsemodel.CategoryResponse;
import com.purposecodes.emetwafar.responsemodel.ErrorModel;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class CategoryFragment extends BaseFragment implements OnCategoryListener {

    private final String TAG = "CategoryFragment";


    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.textView_category_homeError)
    TextView errorText;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.recyclerView_category_home)
    RecyclerView recyclerView;

    private ArrayList<Category> categories;

    private RvHomeCategoryTitleAdapter categoryTitleAdapter;


    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_category;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        categories = new ArrayList<>();

        setUpRecyclerView();

        getCategories();


        ProductListFragment fragment = new ProductListFragment();
        FragmentManager fm = requireActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.product_container, fragment);
        fragmentTransaction.commit();
    }


    @SuppressLint("NotifyDataSetChanged")
    private void setUpRecyclerView() {


        categoryTitleAdapter = new RvHomeCategoryTitleAdapter(getContext(), categories, this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);

        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(categoryTitleAdapter);
        recyclerView.setNestedScrollingEnabled(false);
        categoryTitleAdapter.notifyDataSetChanged();

    }

    @SuppressLint({"NonConstantResourceId", "NotifyDataSetChanged"})
    @OnClick(R.id.textView_category_homeError)
    public void getCategories() {

        categories.clear();
        categories.addAll(readCache());
        categoryTitleAdapter.notifyDataSetChanged();

        if (AppController.get().isNetworkAvailable()) {
            AppController.get().getCategories();

        } else if (categories.isEmpty())
            setErrorView(getResources().getString(R.string.no_internet));

    }

    @SuppressLint("NonConstantResourceId")
    @OnClick(R.id.button_category_seeAll)
    public void onClickAllCategories(View view) {

        Intent categoryIntent = new Intent(getActivity(), AllCategoryActivity.class);
        categoryIntent.putExtra(TYPE_TAG_CATEGORY, categories);
        startActivity(categoryIntent);
    }


    @SuppressLint("NotifyDataSetChanged")
    @Subscribe
    public void onHttpGetCategoriesSuccess(ApiSuccessListener<CategoryResponse> response) {

        if (response.type.equalsIgnoreCase("getCategories")) {

            updateViews(VIEW_DATA);
            CategoryResponse responseModel = response.response.body();
            if (responseModel != null && responseModel.getStatus()) {
                categories.clear();
                if (responseModel.getCategories() != null) {

                    categories.addAll(responseModel.getCategories());

                }

                categoryTitleAdapter.notifyDataSetChanged();


            } else if (responseModel != null)
                setErrorView(responseModel.getText());
            else
                setErrorView("No Category found");


            String data = new Gson().toJson(categories);
            CacheStorage.getInstance().writeCache(CATEGORY_CACHE_KEY, data);

        }
    }


    @Subscribe
    public void onHttpError(ApiFailureMessageListener<ErrorModel> response) {

        if (response.type.equalsIgnoreCase("getCategories")) {
            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);

                setErrorView(errorModel.getText());
            } catch (Exception e) {
                e.printStackTrace();
                setErrorView(getResources().getString(R.string.error_try_again));
            }
        }
    }


    @Subscribe
    public void onHttpFailure(ApiFailureListener response) {


        if (response.type.equalsIgnoreCase("getCategories")) {
            setErrorView(getResources().getString(R.string.error_try_again));

        }
    }

    //set ErrorView
    private void setErrorView(final String message) {

        updateViews(VIEW_ERRORVIEW);

        errorText.setText(message);


    }


    public void updateViews(int viewCode) {

        switch (viewCode) {

            case VIEW_DATA:

                recyclerView.setVisibility(View.VISIBLE);
                errorText.setVisibility(View.GONE);

                break;

            case VIEW_ERRORVIEW:

                errorText.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);

                break;

        }

    }

    @Override
    public void onCategoryClicked(Category category, int position) {
        Intent intent = new Intent(getActivity(), ProductFilterActivity.class);
        intent.putExtra(TYPE_TAG_CATEGORY, category);
        startActivity(intent);
    }


    private List<Category> readCache() {
        List<Category> list = new ArrayList<>();
        try {
            Gson gson = new Gson();
            list = Arrays.asList(gson.fromJson(CacheStorage.getInstance().readCache(CATEGORY_CACHE_KEY), Category[].class));
        } catch (NullPointerException e) {
            list = new ArrayList<>();
        }
        return list;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onPrepareOptionsMenu(menu);
        inflater.inflate(R.menu.menu_search, menu);
//        MenuItem item = menu.findItem(R.id.action_search);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_search) {
            startActivity(new Intent(getActivity(), SearchableActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
