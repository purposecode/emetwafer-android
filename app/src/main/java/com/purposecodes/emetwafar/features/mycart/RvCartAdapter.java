package com.purposecodes.emetwafar.features.mycart;

import android.content.Context;
import android.graphics.Typeface;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.holder.RvBlankViewHolder;
import com.purposecodes.emetwafar.holder.RvFooterViewHolder;
import com.purposecodes.emetwafar.model.CartItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.convertPriceText;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.convertWarrantyPriceFormatText;

public class RvCartAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final String TAG = "RvCartAdapter";
    // Context
    private final Context mContext;

    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;
    private static final int TYPE_BLANK = 3;

    // Checking the its loading for new items
    private boolean mLoading = false;
    // List
    private final ArrayList<CartItem> mList;

    private final OnCartClickListener listener;

    public RvCartAdapter(Context mContext, ArrayList<CartItem> mList, OnCartClickListener _listener) {
        this.mContext = mContext;
        this.mList = mList;
        this.listener = _listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {

            case TYPE_FOOTER:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_loading, parent, false);
                return new RvFooterViewHolder(view);

            case TYPE_ITEM:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_cart, parent, false);
                return new RvCartHolder(view);

            default:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_blank, parent, false);
                return new RvBlankViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        int viewType = getItemViewType(position);
        switch (viewType) {

            case TYPE_FOOTER:
                bindFooterView((RvFooterViewHolder) holder);
                break;
            case TYPE_ITEM:
                bindItemView((RvCartHolder) holder, position);
                break;
        }

    }


    private void bindItemView(RvCartHolder holder, final int position) {

        CartItem cartItem = mList.get(position);

        Glide.with(mContext)
                .load(cartItem.getImageUrl())
                .apply(new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.ic_upload_placeholder)
                        .error(R.drawable.ic_upload_placeholder))
                .into(holder.ivProduct);

        holder.tvName.setText(cartItem.getProductName());
        holder.tvDesc.setText(cartItem.getCategoryName());

        holder.btnQuantity.setText(convertQuantityText(cartItem.getQuantity()));


        if (!TextUtils.isEmpty(cartItem.getOfferPrice()) && !cartItem.getOfferPrice().equalsIgnoreCase("0")) {
            holder.tvAmount.setText(convertPriceText(cartItem.getOfferPrice()));
            holder.tvOffer.setVisibility(View.VISIBLE);
            holder.tvWithoutOffer.setVisibility(View.VISIBLE);
            holder.tvOffer.setText(cartItem.getDiscount() + "% OFF");
            holder.tvWithoutOffer.setText(convertOfferText(cartItem.getProductPrice()));
        } else {
            holder.tvAmount.setText(convertPriceText(cartItem.getProductPrice()));
            holder.tvOffer.setVisibility(View.GONE);
            holder.tvWithoutOffer.setVisibility(View.GONE);
        }

        holder.viewWarranty.setVisibility(cartItem.getWarrantyPeriod().isEmpty() ? View.GONE : View.VISIBLE);
        if (!cartItem.getWarrantyPeriod().isEmpty()) {
            holder.tvWarrantyName.setText(cartItem.getWarrantyPeriod());
            holder.tvWarrantyPlan.setText(convertWarrantyPriceFormatText(cartItem.getQuantity() + " x " + cartItem.getWarrantyPrice() + " = ", cartItem.getWarrantyTotal()));
        }


        if (cartItem.getIsAvailable() != null) {
            holder.tvNotAvailable.setVisibility(cartItem.getIsAvailable() == 0 ? VISIBLE : GONE);
            holder.btnQuantity.setVisibility(cartItem.getIsAvailable() == 1 ? VISIBLE : GONE);

        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onCartClicked(cartItem, position);
            }
        });

        holder.btnQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onQuantityClicked(cartItem, position);
            }
        });


        holder.btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onRemoveClicked(cartItem, position);
            }
        });


    }


    private SpannableStringBuilder convertQuantityText(String _code) {

        String firstWord = "Quantity :";
        // Use a SpannableStringBuilder so that both the text and the spans are mutable
        SpannableStringBuilder ssb = new SpannableStringBuilder(firstWord);
        try {


            String secondWord = _code;


// Create a span that will make the text red
            ForegroundColorSpan blackForegroundColorSpan = new ForegroundColorSpan(mContext.getResources().getColor(R.color.primaryText));


// Add a blank space
            ssb.append(" ");


// Add the secondWord and apply the strikethrough span to only the second word
            ssb.append(secondWord);
            ssb.setSpan(blackForegroundColorSpan, ssb.length() - secondWord.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ssb.setSpan(new StyleSpan(Typeface.BOLD), ssb.length() - secondWord.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

// Set the TextView text and denote that it is Editable
// since it's a SpannableStringBuilder
        } catch (Exception e) {
            e.fillInStackTrace();
        }
        return ssb;

    }


    private SpannableStringBuilder convertOfferText(String withoutOffer) {

        String firstWord = withoutOffer;

        // Use a SpannableStringBuilder so that both the text and the spans are mutable
        SpannableStringBuilder ssb = new SpannableStringBuilder(firstWord);
        try {


// Create a span that will make the text red
            ForegroundColorSpan greenForegroundColorSpan = new ForegroundColorSpan(mContext.getResources().getColor(android.R.color.holo_green_dark));


            // Create a span that will strikethrough the text
            StrikethroughSpan strikethroughSpan = new StrikethroughSpan();


// Apply the color span
            ssb.setSpan(
                    strikethroughSpan,            // the span to add
                    0,                                 // the start of the span (inclusive)
                    ssb.length(),                      // the end of the span (exclusive)
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE); // behavior when text is later inserted into the SpannableStringBuilder
            // SPAN_EXCLUSIVE_EXCLUSIVE means to not extend the span when additional

// Add a blank space


// Add the secondWord and apply the strikethrough span to only the second word

//            ssb.setSpan(greenForegroundColorSpan, 0, ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

// Set the TextView text and denote that it is Editable
// since it's a SpannableStringBuilder
        } catch (Exception e) {
            e.fillInStackTrace();
        }
        return ssb;

    }

    private void bindFooterView(RvFooterViewHolder holder) {
        if (mLoading) {
            holder.loadText.setText(R.string.loading);
            holder.loadText.setVisibility(View.VISIBLE);
            holder.loadProgress.setVisibility(View.VISIBLE);
        } else {
            holder.loadText.setText("");
            holder.loadText.setVisibility(View.GONE);
            holder.loadProgress.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {

        int count = 0;
        if (!mList.isEmpty()) {
            count = mList.size() + 1;
//            count = mList.size() ;
        }
        return count;


    }


    @Override
    public int getItemViewType(int position) {

        try {


            if (position == getItemCount() - 1) {
                return TYPE_FOOTER;
            } else {

                return TYPE_ITEM;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            return TYPE_BLANK;
        }
    }

    public void setLoading(boolean loading) {
        mLoading = loading;
        notifyDataSetChanged();
    }

    static class RvCartHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageView_itemCart_imageView)
        ImageView ivProduct;

        @BindView(R.id.textView_itemCart_name)
        TextView tvName;

        @BindView(R.id.textView_itemCart_category)
        TextView tvDesc;

        @BindView(R.id.textView_itemCart_offer)
        TextView tvOffer;

        @BindView(R.id.textView_itemCart_amount)
        TextView tvAmount;

        @BindView(R.id.textView_itemCart_woPrice)
        TextView tvWithoutOffer;

        @BindView(R.id.textView_itemCart_warrantyName)
        TextView tvWarrantyName;

        @BindView(R.id.textView_itemCart_warrantyPlan)
        TextView tvWarrantyPlan;

        @BindView(R.id.textView_itemCart_notAvailable)
        TextView tvNotAvailable;

        @BindView(R.id.view_itemCart_warranty)
        ViewGroup viewWarranty;


        @BindView(R.id.button_itemCart_quantity)
        Button btnQuantity;

        @BindView(R.id.button_itemCart_remove)
        Button btnRemove;


        RvCartHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }


    public interface OnCartClickListener {
        void onRemoveClicked(CartItem item, int position);

        void onQuantityClicked(CartItem item, int position);

        void onCartClicked(CartItem item, int position);
    }
}
