
package com.purposecodes.emetwafar.features.productview;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.api.ApiFailureMessageListener;
import com.purposecodes.emetwafar.api.ApiSuccessListener;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.features.filter.FilterActivity;
import com.purposecodes.emetwafar.listener.OnProductClickListener;
import com.purposecodes.emetwafar.model.Category;
import com.purposecodes.emetwafar.model.Product;
import com.purposecodes.emetwafar.responsemodel.ErrorModel;
import com.purposecodes.emetwafar.responsemodel.ProductResponse;
import com.purposecodes.emetwafar.view.EndlessScrollListener;
import com.sk.icode.errorview.ErrorView;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

import static com.purposecodes.emetwafar.controller.SessionController.setWishQuantity;
import static com.purposecodes.emetwafar.utils.LogUtils.printLog;
import static com.purposecodes.emetwafar.utils.UtilsVariable.NO_DATA;
import static com.purposecodes.emetwafar.utils.UtilsVariable.NO_NETWORK;
import static com.purposecodes.emetwafar.utils.UtilsVariable.REQUEST_PRODUCT_DETAILS;
import static com.purposecodes.emetwafar.utils.UtilsVariable.REQUEST_PRODUCT_FILTER;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_DISCOUNT;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_IS_WISHED;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_OFFERS;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_POSITION;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_PRICE_MAX;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_PRICE_MIN;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_PRODUCT_ID;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TYPE_TAG_CATEGORY;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_ERRORVIEW;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_PROGRESSBAR;
import static com.purposecodes.emetwafar.utils.UtilsVariable.VIEW_RECYCLERVIEW;
import static com.purposecodes.emetwafar.utils.UtilsVariable.WRONG_RESPONSE;

public class ProductFilterActivity extends BaseActivity implements OnProductClickListener {


    private final String TAG = "ProductFilterActivity";


    @BindView(R.id.errorView)
    ErrorView errorView;

    @BindView(R.id.recyclerView_layout_list)
    RecyclerView recyclerView;


    // RecyclerView date set
    private ArrayList<Product> productList;

    private RvProductAdapter productAdapter;

    // Scroll Listener for pagination
    private EndlessScrollListener mScrollListener;

    private Category selected_category = null;
    private String SORT_BY_FILTER = "relevance";
    private String PRICE_MIN = "";
    private String PRICE_MAX = "";

    // Current page
    private int mCurrentPage = 0, mTotalPages = -1;

    private final String mPageCount = "15";


    private String selectedDiscount = "";
    private ArrayList<String> selectedOffers;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        selected_category = (Category) getIntent().getSerializableExtra(TYPE_TAG_CATEGORY);
        setTitle(selected_category.getCategoryName());


        selectedOffers = new ArrayList<>();
        productList = new ArrayList<>();
        productAdapter = new RvProductAdapter(this, productList, this);


        setUpRecyclerView();
        if (selected_category == null)
            setErrorView("Category doesn't exist", "Retry again", false, WRONG_RESPONSE);
        else
            loadData();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_product_filter;
    }


    private void setUpRecyclerView() {


        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(productAdapter);
        recyclerView.setNestedScrollingEnabled(false);
        mScrollListener = new EndlessScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int currentPage) {
                if (mCurrentPage < mTotalPages) {
                    showRecyclerViewLoading(true);
                    getProducts(mCurrentPage + 1);
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        };

//        recyclerView.addOnScrollListener(mScrollListener);
    }

    @OnClick({R.id.button_productFilter_sort, R.id.button_productFilter_filter})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_productFilter_sort:
                showBottomDialog();
                break;
            case R.id.button_productFilter_filter:
                Intent intent = new Intent(ProductFilterActivity.this, FilterActivity.class);

                intent.putExtra(TAG_DISCOUNT, selectedDiscount);
                intent.putExtra(TAG_OFFERS, selectedOffers);
                intent.putExtra(TAG_PRICE_MIN, PRICE_MIN);
                intent.putExtra(TAG_PRICE_MAX,PRICE_MAX);

                startActivityForResult(intent, REQUEST_PRODUCT_FILTER);
                break;
        }

    }


    private void showBottomDialog() {

        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_dialog_sort, null);
        Button btnRelevance = view.findViewById(R.id.button_sortDialog_relevance);
        Button btnPopularity = view.findViewById(R.id.button_sortDialog_popularity);
        Button btnHighToLow = view.findViewById(R.id.button_sortDialog_highToLow);
        Button btnLowToHigh = view.findViewById(R.id.button_sortDialog_lowToHigh);
        Button btnNewest = view.findViewById(R.id.button_sortDialog_new);


        BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(view);
        dialog.show();

        switch (SORT_BY_FILTER) {
            case "relevance":
                btnRelevance.setSelected(true);
                break;
            case "popularity":
                btnPopularity.setSelected(true);
                break;
            case "high_to_low":
                btnHighToLow.setSelected(true);
                break;
            case "low_to_high":
                btnLowToHigh.setSelected(true);
                break;
            case "newest_first":
                btnNewest.setSelected(true);
                break;
        }

        btnRelevance.setOnClickListener(v -> {

            SORT_BY_FILTER = "relevance";
            loadData();
            dialog.dismiss();
        });

        btnPopularity.setOnClickListener(v -> {

            SORT_BY_FILTER = "popularity";
            loadData();
            dialog.dismiss();
        });
        btnHighToLow.setOnClickListener(v -> {

            SORT_BY_FILTER = "high_to_low";
            loadData();
            dialog.dismiss();
        });
        btnLowToHigh.setOnClickListener(v -> {

            SORT_BY_FILTER = "low_to_high";
            loadData();
            dialog.dismiss();
        });
        btnNewest.setOnClickListener(v -> {

            SORT_BY_FILTER = "newest_first";
            loadData();
            dialog.dismiss();
        });

    }


    private void loadData() {

        updateViews(VIEW_PROGRESSBAR);

        mCurrentPage = 0;
        productAdapter.notifyDataSetChanged();

        if (AppController.get().isNetworkAvailable()) {
            getProducts(mCurrentPage + 1);  // First page
        } else if (productList.isEmpty()) {
            setErrorView(getString(R.string.no_internet), "", false, NO_NETWORK);
        }
//        else
//            mSwipeRefreshLayout.setRefreshing(false);


    }


    private void showRecyclerViewLoading(final boolean showLoading) {
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                productAdapter.setLoading(showLoading);
            }
        });
    }

    private void getProducts(int pageNo) {

        Map<String, String> options = new HashMap<>();
//        options.put("count", mPageCount);
        options.put("page_no", String.valueOf(pageNo));

        options.put("category_id", selected_category.getCategoryId());
        options.put("sort_by", SORT_BY_FILTER);
        options.put("price_from", PRICE_MIN);
        options.put("price_to", PRICE_MAX);
        options.put("discount", selectedDiscount);
//        options.put("offer", selectedOffers.toString());

        printLog(TAG, "options  : -- " + options);
        AppController.get().getProducts(options, selected_category.getCategoryId());
    }

    private void addToWishList(Product product, int position) {

        if (AppController.get().isNetworkAvailable()) {
            Map<String, String> options = new HashMap<>();
            options.put("product_id", product.getProductId());
            AppController.get().addToWish(options, selected_category.getCategoryId(), position);
        } else {
            showToast(getString(R.string.no_internet));
            productAdapter.notifyDataSetChanged();
        }


    }

    private void removeFromWishList(Product product, int position) {
        AppController.get().removeFromWish(product.getProductId(), selected_category.getCategoryId(), position);
    }


    //set ErrorView
    private void setErrorView(final String title, final String subTitle, boolean isRetry, int drawable) {

        updateViews(VIEW_ERRORVIEW);

        errorView.setConfig(ErrorView.Config.create()
                .title(title)
                .subtitle(subTitle)
                .retryVisible(isRetry)
                .image(drawable)
                .build());


        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {
                loadData();

            }
        });
    }

    private void updateViews(int viewCode) {
        switch (viewCode) {
            case VIEW_PROGRESSBAR:
                showProgress();

                errorView.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
                break;

            case VIEW_RECYCLERVIEW:
                dismissProgress();
                recyclerView.setVisibility(View.VISIBLE);
                errorView.setVisibility(View.GONE);

                break;

            case VIEW_ERRORVIEW:
                dismissProgress();
                recyclerView.setVisibility(View.GONE);
                errorView.setVisibility(View.VISIBLE);

                break;

        }
    }


    @Subscribe
    public void onHttpGetProductListSuccess(ApiSuccessListener<ProductResponse> response) {

        if (response.type.equalsIgnoreCase("getProducts/" + selected_category.getCategoryId())) {
            ProductResponse responseModel = response.response.body();
            if (responseModel != null && responseModel.getStatus()) {

                mCurrentPage = Integer.parseInt(responseModel.getResponseResult().getCurrentPage());
                mTotalPages = responseModel.getResponseResult().getTotalCount();

                if (mCurrentPage == 1) {
                    productList.clear();
                }

                List<Product> list = responseModel.getResponseResult().getProducts();
                if (list != null && list.size() > 0) {
                    productList.addAll(list);
                }

                productAdapter.notifyDataSetChanged();
                updateViews(VIEW_RECYCLERVIEW);

            } else if (mCurrentPage == 0) {
                productList.clear();
                productAdapter.notifyDataSetChanged();
                setErrorView(responseModel.getText(), "", false, NO_DATA);
            } else if (productList.isEmpty() && responseModel.getText() != null && TextUtils.isEmpty(responseModel.getText()))
                setErrorView(responseModel.getText(), "", false, NO_DATA);
            else if (productList.isEmpty())
                setErrorView("No Data found!", "", false, NO_DATA);
            showRecyclerViewLoading(false);
        }
    }


    @Subscribe
    public void onHttpAddWishSuccess(ApiSuccessListener<JsonObject> response) {

        if (response.type.equalsIgnoreCase("addToWish_" + selected_category.getCategoryId())) {

            try {
                JsonObject jsonObject = response.response.body();
                assert jsonObject != null;
                if (jsonObject.get("status").getAsBoolean()) {
                    JsonObject productObject = jsonObject.get("data").getAsJsonObject().get("product").getAsJsonObject();
                    int wishQuantity = jsonObject.get("data").getAsJsonObject().get("wish_total").getAsInt();
                    setWishQuantity(wishQuantity);

                    if (productList.size() > response.position) {
                        Product p = new Gson().fromJson(productObject, Product.class);
                        productList.set(response.position, p);
                        productAdapter.notifyDataSetChanged();

                    }
                    showToast(jsonObject.get("text").getAsString());
                } else {
                    showToast(jsonObject.get("text").getAsString());
                }
            } catch (Exception e) {
                printLog(TAG, " onHttpAddWishSuccess Exception : " + e.getMessage());
            }

        }
    }


    @Subscribe
    public void onHttpError(ApiFailureMessageListener<ErrorModel> response) {

        if (response.type.equalsIgnoreCase("getProducts/" + selected_category.getCategoryId())) {

            showRecyclerViewLoading(false);
            mScrollListener.setLoading(false);
            updateViews(VIEW_RECYCLERVIEW);
            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);

                if (mCurrentPage > 1) {
                    showToast(errorModel.getText());
                } else {
                    setErrorView(errorModel.getText(), "", false, NO_DATA);
                }
            } catch (Exception e) {
                e.printStackTrace();
                setErrorView(getResources().getString(R.string.error_try_again), getString(R.string.error_subtitle_failed_one_more_time), true, WRONG_RESPONSE);
            }


        } else if (response.type.equalsIgnoreCase("addToWish_" + selected_category.getCategoryId())) {

            updateViews(VIEW_RECYCLERVIEW);
            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                showToast(errorModel.getText());
                productAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();

                showToast(getResources().getString(R.string.error_try_again));

            }
        }
    }


    @Subscribe
    public void onHttpFailure(ApiFailureListener response) {

        if (response.type.equalsIgnoreCase("getProducts/" + selected_category.getCategoryId())) {

            if (mCurrentPage != 0) {
                showToast(getResources().getString(R.string.error_try_again));
                updateViews(VIEW_RECYCLERVIEW);
            } else {
                setErrorView(getResources().getString(R.string.error_try_again), getString(R.string.error_subtitle_failed_one_more_time), true, WRONG_RESPONSE);
            }
            showRecyclerViewLoading(false);
            mScrollListener.setLoading(false);

        } else if (response.type.equalsIgnoreCase("addToWish_" + selected_category.getCategoryId())) {
            showToast(getResources().getString(R.string.error_try_again));
            updateViews(VIEW_RECYCLERVIEW);
            productAdapter.notifyDataSetChanged();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {
                if (requestCode == REQUEST_PRODUCT_FILTER && data != null) {
                    selectedDiscount = data.getStringExtra(TAG_DISCOUNT);
                    selectedOffers = data.getStringArrayListExtra(TAG_OFFERS);
                    PRICE_MIN = data.getStringExtra(TAG_PRICE_MIN);
                    PRICE_MAX = data.getStringExtra(TAG_PRICE_MAX);


                    loadData();
                }else if (requestCode == REQUEST_PRODUCT_DETAILS && data != null) {

                    int position = data.getIntExtra(TAG_POSITION, -1);
                    int isWished = data.getIntExtra(TAG_IS_WISHED, -1);

                    if (position != -1 && isWished != -1) {
                        Product p = productList.get(position);
                        p.setIsWishlist(isWished);
                        productList.set(position, p);
                        productAdapter.notifyItemChanged(position);

                        printLog(TAG, " onActivityResult bindItemView position :  " + position + ", isWished  : " + (p.getIsWishlist() == 1));
                    }
                }

            }
        } catch (IndexOutOfBoundsException e) {
            e.getLocalizedMessage();
        }
    }






    @Override
    public void onProductClicked(Product product, int position) {


        Intent intent = new Intent(ProductFilterActivity.this, ProductDetailActivity.class);
        intent.putExtra(TAG_PRODUCT_ID, product.getProductId());
        intent.putExtra(TAG_POSITION, position);
        startActivityForResult(intent, REQUEST_PRODUCT_DETAILS);


    }

    @Override
    public void onAddToWishClicked(Product product, int position) {
        addToWishList(product, position);

    }


}
