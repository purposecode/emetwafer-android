package com.purposecodes.emetwafar.features.payments;

import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.api.ApiFailureMessageListener;
import com.purposecodes.emetwafar.api.ApiSuccessListener;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.features.checkout.CheckoutActivity;
import com.purposecodes.emetwafar.features.checkout.OrderSuccessActivity;
import com.purposecodes.emetwafar.model.OrderPayment;
import com.purposecodes.emetwafar.responsemodel.ErrorModel;
import com.purposecodes.emetwafar.responsemodel.SaveOrderResponse;
import com.squareup.otto.Subscribe;
import com.telr.mobile.sdk.activty.WebviewActivity;
import com.telr.mobile.sdk.entity.response.status.StatusResponse;

import java.util.Map;

import butterknife.BindView;

import static com.purposecodes.emetwafar.controller.SessionController.setCartId;
import static com.purposecodes.emetwafar.controller.SessionController.setCartQuantity;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_CHECKOUT;
import static com.purposecodes.emetwafar.utils.LogUtils.printLog;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.getAppVersion;
import static com.purposecodes.emetwafar.utils.UtilsVariable.CALLING_ACTIVITY_KEY;
import static com.purposecodes.emetwafar.utils.UtilsVariable.DEVICE_OS;
import static com.purposecodes.emetwafar.utils.UtilsVariable.DEVICE_VERSION_KEY;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_CHECKOUT;

public class OrderPaymentSuccessActivity extends BaseActivity {

    @BindView(R.id.view_paymentSuccess)
    View viewError;


    @BindView(R.id.textView_paymentSuccess_title)
    TextView tvTitle;

    @BindView(R.id.textView_paymentSuccess_description)
    TextView tvDescription;

    @BindView(R.id.button_paymentSuccess_retry)
    Button btnRetry;

    private final String TAG = "OrderPaymentSuccessActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_order_payment_success);
        showProgress();


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_order_payment_success;
    }

    public void checkoutProceed(String transNo) {
        viewError.setVisibility(View.GONE);
        showProgress();
        hideSoftKeyboard();
        if (AppController.get().isNetworkAvailable()) {

            if (AppController.get().getCheckoutData() != null) {
                OrderPayment _orderPayment = AppController.get().getCheckoutData(); //save temporary data
                Map<String, String> options = _orderPayment.getOptions();
                options.put(DEVICE_VERSION_KEY, DEVICE_OS + " " + getAppVersion());
                options.put("transaction_no", transNo);
                AppController.get().saveOrder(options);

            }

        } else
            setErrorView(getString(R.string.no_internet));
    }

    @Subscribe
    public void onHttpSaveOrderSuccess(ApiSuccessListener<SaveOrderResponse> response) {


        if (response.type.equalsIgnoreCase("saveOrder")) {

            SaveOrderResponse responseModel = response.response.body();
            if (responseModel != null && responseModel.getStatus()) {

                if (responseModel.getOrders() != null && !responseModel.getOrders().isEmpty()) {
                    OrderPayment _orderPayment = AppController.get().getCheckoutData();

//                    printLog(TAG," calling activity : "+(_orderPayment.getCallingActivity() == ACTIVITY_CHECKOUT)+"print log : "+new Gson().toJson(_orderPayment));

                    if (_orderPayment.getCallingActivity() == ACTIVITY_CHECKOUT) {
                        setCartId(""); //cart id clear from session
                        setCartQuantity(0);

                    }
                    Intent intent = new Intent(OrderPaymentSuccessActivity.this, OrderSuccessActivity.class);
                    intent.putExtra(TAG_CHECKOUT, responseModel.getOrders());
                    intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
                    intent.putExtra(CALLING_ACTIVITY_KEY, _orderPayment.getCallingActivity());
                    AppController.get().setCheckoutData(null);
                    dismissProgress();
                    startActivity(intent);
                    CheckoutActivity.getInstance().finish(); //checkout page
                    finish();

                }
            } else {
                setErrorView(responseModel.getText());


            }

        }
    }

    @Subscribe
    public void onHttpError(ApiFailureMessageListener<ErrorModel> response) {

        if (response.type.equalsIgnoreCase("saveOrder")) {
            dismissProgress();
            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                setErrorView(errorModel.getText());


            } catch (Exception e) {
                setErrorView("Something went wrong.!");

            }

        }
    }


    @Subscribe
    public void onHttpFailure(ApiFailureListener response) {

        if (response.type.equalsIgnoreCase("saveOrder")) {
            setErrorView(getResources().getString(R.string.error_try_again));
        }
    }


    private SpannableStringBuilder getTitleText() {
        // Use a SpannableStringBuilder so that both the text and the spans are mutable
        SpannableStringBuilder ssb = new SpannableStringBuilder();
        String tStr = "";
        try {


            ssb.append("Your Payment Success");
            ssb.append("\n");
            tStr = "order hasn't been not placed";
            ssb.append(tStr);
//            ssb.setSpan(new StyleSpan(Typeface.BOLD), ssb.length() - tStr.length(), ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        } catch (Exception e) {
            e.fillInStackTrace();
        }
        return ssb;

    }

    //set ErrorView
    private void setErrorView(final String title) {

        viewError.setVisibility(View.VISIBLE);
        dismissProgress();
        tvTitle.setText(title);
//        tvDescription.setText(getResources().getString(R.string.error_try_again));
        tvDescription.setText(getTitleText());


    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        StatusResponse status = intent.getParcelableExtra(WebviewActivity.PAYMENT_RESPONSE);
        if (status == null)
            return;

//        printLog(TAG, " success data : "  + new Gson().toJson(status));
//        if (AppController.get().getPaymentOrders()!=null)
//            printLog(TAG," success size: "+ AppController.get().getPaymentOrders().size());
// {"auth":{"avs":"X","ca_valid":"1","cardcode":"VC","cardlast4":"1111","code":"925810","cvv":"Y","message":"Authorised","status":"A","tranref":"040025617463"},"trace":"4000/6089/5f8e9ccc"}


        if (status.getAuth() != null) {
            status.getAuth().getStatus();   // Authorisation status. A indicates an authorised transaction. H also indicates an authorised transaction, but where the transaction has been placed on hold. Any other value indicates that the request could not be processed.
            status.getAuth().getAvs();      /* Result of the AVS check:
                                            Y = AVS matched OK
                                            P = Partial match (for example, post-code only)
                                            N = AVS not matched
                                            X = AVS not checked
                                            E = Error, unable to check AVS */
            status.getAuth().getCode();     // If the transaction was authorised, this contains the authorisation code from the card issuer. Otherwise it contains a code indicating why the transaction could not be processed.
            status.getAuth().getMessage();  // The authorisation or processing error message.
            status.getAuth().getCa_valid();
            status.getAuth().getCardcode(); // Code to indicate the card type used in the transaction. See the code list at the end of the document for a list of card codes.
            status.getAuth().getCardlast4();// The last 4 digits of the card number used in the transaction. This is supplied for all payment types (including the Hosted Payment Page method) except for PayPal.
            status.getAuth().getCvv();      /* Result of the CVV check:
                                           Y = CVV matched OK
                                           N = CVV not matched
                                           X = CVV not checked
                                           E = Error, unable to check CVV */
            status.getAuth().getTranref(); //The payment gateway transaction reference allocated to this request.
            printLog(TAG, "tranref hany : " + status.getAuth().getTranref());
            status.getAuth().getCardfirst6(); // The first 6 digits of the card number used in the transaction, only for version 2 is submitted in Tran -> Version


            checkoutProceed(status.getAuth().getTranref());

            setTransactionDetails(status.getAuth().getTranref(), status.getAuth().getCardlast4());
/*
            Gson gson = new Gson();
            String st = gson.toJson(status);
            JsonObject jsonObject = new JsonParser().parse(st).getAsJsonObject();

            printLog(TAG, "StatusResponse jsonObject  : " + jsonObject);
         */


            btnRetry.setOnClickListener(v -> checkoutProceed(status.getAuth().getTranref()));


        }
    }


    private void setTransactionDetails(String ref, String last4) {

//        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("telr", Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPref.edit();
//        editor.putString("ref", ref);
//        editor.putString("last4", last4);
//        editor.commit();

    }

    @Override
    public void onBackPressed() {
//        Intent intent = new Intent();
//        setResult(RESULT_OK, intent);
//        super.onBackPressed();
    }
}
