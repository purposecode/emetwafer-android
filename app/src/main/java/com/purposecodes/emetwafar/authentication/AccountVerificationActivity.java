package com.purposecodes.emetwafar.authentication;

import static com.purposecodes.emetwafar.features.payments.PaymentUtils.sendPlanUpdatePaymentRequest;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_NEW_PLAN;
import static com.purposecodes.emetwafar.utils.FileUtils.IMAGE;
import static com.purposecodes.emetwafar.utils.FileUtils.fileDirectoryGenerator;
import static com.purposecodes.emetwafar.utils.FileUtils.getFileMimeType;
import static com.purposecodes.emetwafar.utils.FileUtils.getFileSizeMegaBytes;
import static com.purposecodes.emetwafar.utils.FileUtils.getPathFromURI;
import static com.purposecodes.emetwafar.utils.LogUtils.printLog;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.getTimeStamp;
import static com.purposecodes.emetwafar.utils.UtilsVariable.AGREE_TEXT;
import static com.purposecodes.emetwafar.utils.UtilsVariable.ARG_COMPANY_REGISTER;
import static com.purposecodes.emetwafar.utils.UtilsVariable.ARG_OTPMODEL;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.jaiselrahman.filepicker.activity.FilePickerActivity;
import com.jaiselrahman.filepicker.config.Configurations;
import com.jaiselrahman.filepicker.model.MediaFile;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.SplashActivity;
import com.purposecodes.emetwafar.adapter.RvDocumentAdapter;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.api.ApiFailureMessageListener;
import com.purposecodes.emetwafar.api.ApiSuccessListener;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.model.CompanyRegister;
import com.purposecodes.emetwafar.model.FileModal;
import com.purposecodes.emetwafar.model.PlanData;
import com.purposecodes.emetwafar.responsemodel.ErrorModel;
import com.purposecodes.emetwafar.responsemodel.OtpResponseModel;
import com.purposecodes.emetwafar.utils.ImageCompressor;
import com.purposecodes.emetwafar.utils.PermissionUtils;
import com.purposecodes.emetwafar.utils.UtilsVariable;
import com.squareup.otto.Subscribe;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class AccountVerificationActivity extends BaseLoginActivity implements RvDocumentAdapter.OnRemoveListener {

    private final String TAG = "AccountVerificationActivity";
    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.editText_accountVerification_companyName)
    TextView tvCompanyName;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.editText_accountVerification_companyRegistration)
    EditText etCompanyRegistration;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.imageView_accountVerification_noData)
    ImageView ivNoData;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.button_accountVerification_chooseFile)
    Button btnChooseFile;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.checkBox_accountVerification)
    CheckBox cbVerification;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.conditions_text)
    TextView tvTermsCondition;


    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.button_accountVerification_save)
    Button btnSave;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.recyclerView_accountVerification)
    RecyclerView recyclerView;

    private OtpResponseModel otpResponseModel;
    private CompanyRegister companyRegister;

    //  list
    private ArrayList<FileModal> selectedList;

    // Uploaded images list
    private ArrayList<String> mUploadedList;
    private RvDocumentAdapter documentAdapter;


    public static final int PICKIMAGE_RESULT_CODE = 2;
    public static final int PICKDOC_RESULT_CODE = 3;

    private static final int PERMISSION_ACCESS_GALLERY = 102;
    private static final int PERMISSION_ACCESS_CAMERA = 103;

    private static final int REQUEST_CAMERA = 1;


    private Uri mCapturedImageUri;
    private String mCapturedImagePath;

    static AccountVerificationActivity accountVerificationActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        accountVerificationActivity = this;
        otpResponseModel = (OtpResponseModel) getIntent().getSerializableExtra(ARG_OTPMODEL);
        companyRegister = (CompanyRegister) getIntent().getSerializableExtra(ARG_COMPANY_REGISTER);
        if (companyRegister != null)
            tvCompanyName.setText(companyRegister.getCompany());


        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(AGREE_TEXT, Html.FROM_HTML_MODE_COMPACT);
        } else {
            result = Html.fromHtml(AGREE_TEXT);
        }

        tvTermsCondition.setText(result);
        tvTermsCondition.setPaintFlags(0);
        tvTermsCondition.setMovementMethod(LinkMovementMethod.getInstance());


        setUpRecyclerView();

    }

    private void setUpRecyclerView() {

        ivNoData.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);

        selectedList = new ArrayList<>();
        documentAdapter = new RvDocumentAdapter(this, selectedList, this);

        GridLayoutManager layoutManager = new GridLayoutManager(this, 3);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(documentAdapter);
        recyclerView.setNestedScrollingEnabled(false);

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_account_verification;
    }

    @SuppressLint("NonConstantResourceId")
    @OnClick({R.id.button_accountVerification_chooseFile, R.id.button_accountVerification_save})
    public void onClick(View v) {
        hideSoftKeyboard();
        switch (v.getId()) {
            case R.id.button_accountVerification_chooseFile:

                if (selectedList == null || selectedList.isEmpty() || selectedList.size() <= 6)
                    showBottomDialog();
                else
                    toast("Maximum 6 attachment is allowed");

                break;
            case R.id.button_accountVerification_save:
                submitData();
                break;
        }

    }


    private void showBottomDialog() {

        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_dialog_doc_chooser, null);
        Button btnCamera = view.findViewById(R.id.button_bottomSheet_camera);
        Button btnGallery = view.findViewById(R.id.button_bottomSheet_gallery);
        Button btnDoc = view.findViewById(R.id.button_bottomSheet_doc);

        BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(view);
        dialog.show();


        btnCamera.setOnClickListener(v -> {
            checkCameraPermission();
            dialog.dismiss();
        });
        btnGallery.setOnClickListener(v -> {
            checkGalleryPermission();
            dialog.dismiss();
        });
        btnDoc.setOnClickListener(v -> {
            openDocChooser();
            dialog.dismiss();
        });
    }


    private void galleryIntent() {


        Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, PICKIMAGE_RESULT_CODE);//one can be replaced with any action code
    }


    private void openDocChooser() {


        Intent intent = new Intent(this, FilePickerActivity.class);
        intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                .setCheckPermission(true)
                .setShowImages(false)
                .setShowVideos(false)
                .setShowAudios(false)
                .setShowFiles(true)
                .enableImageCapture(false)
                .setMaxSelection(1)
                .setSkipZeroSizeFiles(true)

//                .setSingleChoiceMode(true)
                .setSingleClickSelection(true)
                .build());
        startActivityForResult(intent, PICKDOC_RESULT_CODE);

    }


    private void cameraIntent() {


        String newPath = fileDirectoryGenerator(IMAGE);

        File f = new File(newPath);
        if (!f.exists()) {
            f.mkdirs();
        }

        f = new File(f, System.currentTimeMillis() + ".jpg");

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            mCapturedImageUri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", f);
//            mCapturedImageUri = FileProvider.getUriForFile(this,  "com.purposecodes.emetwafar.provider", f);
            PermissionUtils.grandUriPermission(this, intent, mCapturedImageUri);
        } else {
            mCapturedImageUri = Uri.fromFile(f);
        }

        mCapturedImagePath = f.getAbsolutePath();

//        printLog(TAG, "cameraIntent :  2" + mCapturedImageUri + " ,path : " + mCapturedImagePath);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageUri);
        startActivityForResult(intent, REQUEST_CAMERA);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case PICKDOC_RESULT_CODE:


                    ArrayList<MediaFile> files = data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES);

                    String docPath = files.get(0).getPath();

                    //Do something with files
                    showSelectedDoc(docPath);

                    break;
                case REQUEST_CAMERA:
                    String filePath = mCapturedImageUri.getPath();

                    filePath = mCapturedImagePath;

                    File file = new File(filePath);
                    if (file.exists()) {
                        printLog(TAG, "is exist : " + file.getAbsolutePath());
                    } else {
                        printLog(TAG, "File doesn't exist");
                    }

                    showSelectedImage(filePath, true);
                    mCapturedImageUri = null;
                    mCapturedImagePath = null;


                    break;
                case PICKIMAGE_RESULT_CODE:

                    Uri selectedImage = data.getData();

                    printLog(TAG, "path  4 :  getPathFromURI  :  " + getPathFromURI(this, selectedImage));
                    showSelectedImage(getPathFromURI(this, selectedImage), false);


                    break;
            }
        }
    }


    @SuppressLint("NotifyDataSetChanged")
    private void showSelectedImage(String filePath, boolean isTakenFromCam) {

        String path;
        try {

            File f = new File(filePath);

//            printLog(TAG, "showSelectedImage path  1 : " + f.exists() + ", path : " + filePath);

            double s = getFileSizeMegaBytes(f);

            if (s < 5)
                path = ImageCompressor.compressImage(filePath, isTakenFromCam);
            else
                path = ImageCompressor.compressImageHigh(filePath, isTakenFromCam);


        } catch (Exception e) {
            path = filePath;
        }

        File f = new File(path);

        FileModal fileModal = new FileModal();
        fileModal.setPath(path);
        fileModal.setType(UtilsVariable.FileType.IMAGE);

        selectedList.add(fileModal);

        if (!selectedList.isEmpty() && recyclerView.getVisibility() != View.VISIBLE) {
            recyclerView.setVisibility(View.VISIBLE);
            ivNoData.setVisibility(View.GONE);
        } else if (selectedList.isEmpty() && ivNoData.getVisibility() != View.VISIBLE) {
            ivNoData.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else if (selectedList.isEmpty()) {
            recyclerView.setVisibility(View.GONE);
            ivNoData.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            ivNoData.setVisibility(View.GONE);
        }


        documentAdapter.notifyDataSetChanged();
        ivNoData.setVisibility(View.GONE);


    }


    @SuppressLint("NotifyDataSetChanged")
    private void showSelectedDoc(String filePath) {

        FileModal fileModal = new FileModal();
        fileModal.setPath(filePath);
        fileModal.setType(UtilsVariable.FileType.DOC);

        selectedList.add(fileModal);

        if (!selectedList.isEmpty() && recyclerView.getVisibility() != View.VISIBLE) {
            recyclerView.setVisibility(View.VISIBLE);
            ivNoData.setVisibility(View.GONE);
        } else if (selectedList.isEmpty() && ivNoData.getVisibility() != View.VISIBLE) {
            ivNoData.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else if (selectedList.isEmpty()) {
            recyclerView.setVisibility(View.GONE);
            ivNoData.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            ivNoData.setVisibility(View.GONE);
        }

        documentAdapter.notifyDataSetChanged();
        ivNoData.setVisibility(View.GONE);

    }

    private void submitData() {


        String cRegisterNo = etCompanyRegistration.getText().toString().trim();

        if (TextUtils.isEmpty(cRegisterNo))
            toast("Company register cannot be blank.");
        else if (!cbVerification.isChecked()) {
            toast("Please accept the terms and conditions");

        } else if (selectedList == null || selectedList.isEmpty()) {

            mUploadedList = new ArrayList<>();
            companyRegister.setReg_no(cRegisterNo);

            uploadCompanyRegister();

        } else {

            if (mUploadedList == null) {
                mUploadedList = new ArrayList<>();
            }

            companyRegister.setReg_no(cRegisterNo);

            mUploadedList.clear();
            uploadSelectedImage(0);

        }


    }

    private void uploadSelectedImage(int position) {


        File file = new File(selectedList.get(position).getPath());
        if (!file.exists()) {
            toast("Some went wrong ");
            return;
        }
        showProgress();
        String mimeType = getFileMimeType(file);

        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create(MediaType.parse(mimeType), file);

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part partBody = MultipartBody.Part.createFormData("verify_doc", selectedList.get(position).getPath(), requestFile);


        Map<String, RequestBody> options = new HashMap<>();
        options.put("company_name", getRequestBody(companyRegister.getCompany()));

        AppController.get().mediaUpload(options, partBody, position);


    }

    private RequestBody getRequestBody(String value) {
        return RequestBody.create(MediaType.parse("multipart/form-data"), value);
    }


    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onRemove(int position) {
        try {

            selectedList.remove(position);
            documentAdapter.notifyItemRemoved(position);

            if (selectedList.isEmpty()) {
                recyclerView.setVisibility(View.GONE);
                ivNoData.setVisibility(View.VISIBLE);
            }

        } catch (IndexOutOfBoundsException e) {
            printLog(TAG, " IndexOutOfBoundsException  :    " + e.getLocalizedMessage());
            documentAdapter.notifyDataSetChanged();
        }
    }


    //image uploaded respond
    @Subscribe
    public void onHttpMediaUpload(ApiSuccessListener<JsonObject> response) {
        if (response.type.equals("mediaUpload")) {
            JsonObject jsonObject = response.response.body();


            dismissProgress();
            if (jsonObject != null) {
                if (jsonObject.get("status").getAsBoolean()) {
                    JsonObject dataObj = jsonObject.getAsJsonObject("data");
                    mUploadedList.add(dataObj.get("verify_doc").getAsString());
                    if (selectedList.size() > response.position + 1) {
                        uploadSelectedImage(response.position + 1);
                    } else {
                        uploadCompanyRegister();
                    }
                    printLog(TAG, " onHttpImageUpload :  " + mUploadedList.toString());
                } else {
                    toast(jsonObject.get("text").getAsString());
                }
            }
        }
    }


    @Subscribe
    public void onHttpCompanyRegisterComplete(ApiSuccessListener<JsonObject> response) {

        if (response.type.equalsIgnoreCase("companyRegister")) {
            try {

                dismissProgress();
                JsonObject jsonObject = response.response.body();

                if (jsonObject != null) {


                    Toast.makeText(getApplicationContext(), jsonObject.get("text").getAsString(), Toast.LENGTH_LONG).show();

                    if (jsonObject.get("status").getAsBoolean()) {

                        if (!companyRegister.getTrialMode().equalsIgnoreCase("1")) { //paid mode

                            JsonObject dataObject = jsonObject.get("data").getAsJsonObject();
                            PlanData p = new PlanData(dataObject.get("subscription_id").getAsString(), dataObject.get("order_ref").getAsString(), dataObject.get("token_id").getAsString(), ACTIVITY_NEW_PLAN);
                            AppController.get().setPlanData(p);
                            setPayment(dataObject.get("order_ref").getAsString(), dataObject.get("user_id").getAsString(), dataObject.get("subscription_id").getAsString());
                        } else {
                            Intent intent = new Intent(AccountVerificationActivity.this, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);

                        }
                        finish();
                    }
                }

            } catch (Exception e) {
                toast(getString(R.string.error_try_again));
            }


        }
    }


    private void uploadCompanyRegister() {
        showProgress();

        companyRegister.setOrderRef(getTimeStamp());
        HashMap<String, String> options = new HashMap<>();

        if (!companyRegister.getTrialMode().equalsIgnoreCase("1")) {
            options.put("plan_id", companyRegister.getPlan().getPlanId());
            options.put("order_ref", companyRegister.getOrderRef());
        }
        options.put("email", companyRegister.getEmail());
        options.put("company", companyRegister.getCompany());
        options.put("password", companyRegister.getPassword());
        options.put("role", companyRegister.getRole());
        options.put("address", companyRegister.getAddress1());
        options.put("address2", companyRegister.getAddress2());
        options.put("state", companyRegister.getState().getId());
        options.put("city", companyRegister.getCity().getId());
        options.put("country", companyRegister.getCountry());
        options.put("zipcode", companyRegister.getZipCode());
        options.put("website", companyRegister.getWebsite());
        options.put("mobile", companyRegister.getMobile());
        options.put("phone", companyRegister.getPhone());
        options.put("fax", companyRegister.getFax());
        options.put("reg_no", companyRegister.getReg_no());
        options.put("new_city", companyRegister.getOtherCity());


        AppController.get().companyRegister(options, mUploadedList);

    }


    @Subscribe
    public void onHttpError(ApiFailureMessageListener<ErrorModel> response) {

        if (response.type.equalsIgnoreCase("companyRegister") || response.type.equalsIgnoreCase("mediaUpload")) {
            dismissProgress();
            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                toast(errorModel.getText());
            } catch (Exception e) {
                e.printStackTrace();
                toast(getString(R.string.error_try_again));

            }
        }
    }

    @Subscribe
    public void onHttpFailure(ApiFailureListener response) {

        if (response.type.equalsIgnoreCase("companyRegister") || response.type.equalsIgnoreCase("mediaUpload")) {
            dismissProgress();
            toast(getResources().getString(R.string.error_try_again));
        }
    }


    private void checkCameraPermission() {

        if (PermissionUtils.hasPermission(this,
                new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})) {

            cameraIntent();
        } else {
            PermissionUtils.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_ACCESS_CAMERA);
        }
    }

    private void checkGalleryPermission() {
        if (PermissionUtils.hasPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            galleryIntent();
        } else {
            PermissionUtils.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_ACCESS_GALLERY);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        int permissionCheck = PackageManager.PERMISSION_GRANTED;
        switch (requestCode) {
            case PERMISSION_ACCESS_GALLERY:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    galleryIntent();
                }
                break;

            case PERMISSION_ACCESS_CAMERA:
                for (int permission : grantResults) {
                    permissionCheck = permissionCheck + permission;
                }

                if (grantResults.length > 0 && permissionCheck == PackageManager.PERMISSION_GRANTED) {
                    cameraIntent();
                }

                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    private void setPayment(String referenceId, String userId, String subscriptionId) {

        toast("Please wait...", Toast.LENGTH_LONG);
        sendPlanUpdatePaymentRequest(this, referenceId, companyRegister.getPlan().getGrandTotal(), userId, "purchase plan", companyRegister.getCity().getName(), companyRegister.getState().getStateName(), companyRegister.getAddress1(), companyRegister.getAddress2(), companyRegister.getZipCode(), companyRegister.getCompany(), "", companyRegister.getEmail(), companyRegister.getMobile());
    }

    public static AccountVerificationActivity getInstance() {
        return accountVerificationActivity;
    }
}
