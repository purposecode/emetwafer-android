package com.purposecodes.emetwafar.authentication;

import static com.purposecodes.emetwafar.fcm.MyFirebaseMessagingService.notificationTopicSubscription;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_NEW_PLAN;
import static com.purposecodes.emetwafar.utils.UtilsVariable.CALLING_ACTIVITY_KEY;
import static com.purposecodes.emetwafar.utils.UtilsVariable.REQUEST_SUBSCRIPTION;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_POSITION;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_SUBSCRIPTIONS;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_TRIAL_MODE;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Patterns;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.Nullable;

import com.google.gson.JsonObject;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.api.ApiFailureMessageListener;
import com.purposecodes.emetwafar.api.ApiSuccessListener;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.controller.SessionController;
import com.purposecodes.emetwafar.features.subscription.SubscriptionListActivity;
import com.purposecodes.emetwafar.model.Subscription;
import com.purposecodes.emetwafar.responsemodel.ErrorModel;
import com.purposecodes.emetwafar.responsemodel.RegisterResponse;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;

public class LoginActivity extends BaseLoginActivity {


    private final String TAG = "LoginActivity";
    // Widgets
    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.editText_login_userName)
    EditText etEmail;
    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.editText_login_password)
    EditText etPassword;


    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.icon_password)
    ToggleButton tbShowPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tbShowPassword.setOnCheckedChangeListener((buttonView, isChecked) -> {
            etPassword.setTransformationMethod(isChecked ? HideReturnsTransformationMethod.getInstance() : PasswordTransformationMethod.getInstance());
            etPassword.setSelection(etPassword.getText().length());

        });


        SessionController.logoutSession();



        notificationTopicSubscription();


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_login;
    }

    @SuppressLint("NonConstantResourceId")
    @OnClick({R.id.button_login_create, R.id.textView_click_login_forgot, R.id.button_login})
    public void onClick(View v) {
        hideSoftKeyboard();
        int id = v.getId();
        switch (id) {

            case R.id.button_login_create:
                Intent intent = new Intent(LoginActivity.this, SubscriptionListActivity.class);
                intent.putExtra(CALLING_ACTIVITY_KEY, ACTIVITY_NEW_PLAN);
                startActivityForResult(intent, REQUEST_SUBSCRIPTION);


                break;

            case R.id.textView_click_login_forgot:
                showForgetPasswordDialog();
                break;

            case R.id.button_login:
                validateFields();
                break;
        }
    }


    private void toastMessage(String message) {
        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    private boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target)
                && Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    @SuppressWarnings("ConstantConditions")
    private void showForgetPasswordDialog() {
        final Dialog dialog = new Dialog(LoginActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_forgot_password);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);
        final EditText emailEdit = dialog.findViewById(R.id.email);
        Button sendButton = dialog.findViewById(R.id.button_forgotPassword_submit);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailEdit.clearFocus();
                hideSoftKeyboard();
                String email = emailEdit.getText().toString();
                if (!isValidEmail(email)) {
                    toastMessage("Please enter a valid email.");
                } else {
                    showProgress();
                    dialog.dismiss();


                    if (AppController.get().isNetworkAvailable()) {
                        HashMap<String, String> options = new HashMap<>();
                        options.put("role", "1"); //1,2
                        options.put("email", email);
                        AppController.get().forgotPassword(options);

                    } else
                        toastMessage(getResources().getString(R.string.no_internet));


                }
            }
        });

        dialog.show();
    }

    private void validateFields() {
        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();

        if (!isValidEmail(email)) {
            toastMessage("Invalid email address");
        } else if (TextUtils.isEmpty(password)) {
            toastMessage("Please enter password");
        } else if (AppController.get().isNetworkAvailable()) {
            hideSoftKeyboard();
            loginWithEmail(email, password);
        } else {
            toastMessage(getResources().getString(R.string.no_internet));
        }
    }



    @Subscribe
    public void onHttpLoginSuccess(ApiSuccessListener<JsonObject> response) {
        if (response.type.equals("loginUser")) {
            super.onHttpLoginSuccess(response);
        }
    }

    @Subscribe
    public void onForgetPasswordSuccess(ApiSuccessListener<RegisterResponse> response) {
        if (response.type.equalsIgnoreCase("forgotPassword")) {
            dismissProgress();
            toastMessage(response.response.body().getText());
        }
    }

    @Subscribe
    public void onHttpRegTokenSuccess(ApiSuccessListener<JsonObject> response) {
        if (response.type.equalsIgnoreCase("sendUserToken")) {
            super.onHttpRegTokenSuccess(response);
        }
    }

    @Subscribe
    public void onHttpError(ApiFailureMessageListener<ErrorModel> response) {
        if (response.type.equals("loginUser")
                || response.type.equals("loginWithSocialMedia") || response.type.equalsIgnoreCase("sendUserToken") || response.type.equalsIgnoreCase("forgotPassword")) {
            super.onHttpError(response);
        }
    }


    @Subscribe
    public void onHttpFailure(ApiFailureListener response) {
        if (response.type.equalsIgnoreCase("forgotPassword")) {
            toastMessage(getResources().getString(R.string.error_try_again));
            dismissProgress();

        } else {
            super.onHttpFailure(response);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {
                if (requestCode == REQUEST_SUBSCRIPTION && data != null) {

                    String trialMode = data.getStringExtra(TAG_TRIAL_MODE);
                    int position = data.getIntExtra(TAG_POSITION, 0);
                    ArrayList<Subscription> subscriptions = (ArrayList<Subscription>) data.getSerializableExtra((TAG_SUBSCRIPTIONS));
                    Intent intent = new Intent(this, RegisterActivity.class);
                    intent.putExtra(TAG_TRIAL_MODE, trialMode);
                    intent.putExtra(TAG_POSITION, position);
                    intent.putExtra(TAG_SUBSCRIPTIONS, subscriptions);
                    startActivity(intent);
                }
            }
        } catch (IndexOutOfBoundsException e) {
            e.getLocalizedMessage();
        }
    }

    @Override
    protected void onDestroy() {
        etEmail = null;
        etPassword = null;

        super.onDestroy();
    }

    protected void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
        //        animation between activities left to right
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }


}
