package com.purposecodes.emetwafar.authentication;

import static com.purposecodes.emetwafar.controller.SessionController.setWishQuantity;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_EXPIRED;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_EXPIRE_FILED;
import static com.purposecodes.emetwafar.utils.LogUtils.printLog;
import static com.purposecodes.emetwafar.utils.UtilsVariable.CALLING_ACTIVITY_KEY;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.api.ApiFailureListener;
import com.purposecodes.emetwafar.api.ApiFailureMessageListener;
import com.purposecodes.emetwafar.api.ApiSuccessListener;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.controller.SessionController;
import com.purposecodes.emetwafar.features.home.HomeActivity;
import com.purposecodes.emetwafar.features.subscription.PlanExpiredActivity;
import com.purposecodes.emetwafar.model.UserData;
import com.purposecodes.emetwafar.otto.BusProvider;
import com.purposecodes.emetwafar.responsemodel.ErrorModel;
import com.purposecodes.emetwafar.responsemodel.LoginResponse;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseLoginActivity extends AppCompatActivity {

    public static final String LOGIN_EMAIL = "email";

    private static final String TAG = "BaseLoginActivity";


    // Otto handler
    private final Bus mBus = BusProvider.getInstance();

    // Logged User Details
    private UserData mUserData;

    // Firebase reg token
    private String mRefreshToken;

    // Which type user used to login
    private String mLoginType = "";


    @BindView(R.id.layout_prgressBg)
    ViewGroup mProgressOverlay;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());

        ButterKnife.bind(this);

    }


    protected void loginWithEmail(String email, String password) {

        showProgress();
        mLoginType = LOGIN_EMAIL;

        HashMap<String, String> options = new HashMap<>();
        options.put("email", email);
        options.put("password", password);

        AppController.get().loginUser(options);
    }


    @Subscribe
    public void onHttpLoginSuccess(ApiSuccessListener<JsonObject> response) {

        if (response.type.equalsIgnoreCase("loginUser")) {

            JsonObject jsonObject = response.response.body();

            assert jsonObject != null;

            // Sending loginType to google analytics
            LoginResponse loginResponse = new Gson().fromJson(jsonObject, LoginResponse.class);

            printLog(TAG, "onHttpLoginSuccess loginResponse : " + new Gson().toJson(loginResponse));

            mUserData = loginResponse.getUserData();

            if (loginResponse.getStatus() && loginResponse.getText().equalsIgnoreCase("Login successfull!")) {

                FirebaseMessaging.getInstance().getToken()
                        .addOnCompleteListener(new OnCompleteListener<String>() {
                            @Override
                            public void onComplete(@NonNull Task<String> task) {
                                if (!task.isSuccessful()) {
                                    printLog("LoginActivity", "getInstanceId failed" + task.getException());
                                    toast(getResources().getString(R.string.error_try_again));
                                    return;
                                }
                                // Get new FCM registration token

                                mRefreshToken = task.getResult();


                                Map<String, String> options = new HashMap<>();
                                options.put("registration_id", mRefreshToken);
                                options.put("device_os", "android");
                                options.put("user_id", mUserData.getUserId());

//                                printLog(TAG,"onHttpLoginSuccess send usertoken : "+ options);

                                AppController.get().sendUserToken(options, mUserData.getTokenId());

                            }
                        });


            } else {
                toast(loginResponse.getText());
                dismissProgress();
            }
        }
    }


    @Subscribe
    public void onHttpRegTokenSuccess(ApiSuccessListener<JsonObject> response) {

        if (response.type.equalsIgnoreCase("sendUserToken")) {

            dismissProgress();
            JsonObject jsonObject = response.response.body();

            assert jsonObject != null;
            if (jsonObject.get("status").getAsBoolean() && jsonObject.get("text").getAsString().equalsIgnoreCase("User Token saved successfully")) {

                // Saving user data to singleton class
                SessionController.saveUser(mUserData, mRefreshToken, mLoginType);

                setWishQuantity(mUserData.getWishTotal());

//               // 0 - expired ,1 - not expired     1-paid. 0 not paid

                if (mUserData.getIsSubscribed() != null && mUserData.getIsSubscribed() == 0) {

                    Intent intent = new Intent(this, PlanExpiredActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(CALLING_ACTIVITY_KEY, ACTIVITY_EXPIRE_FILED);
                    startActivity(intent);
                    finish();
                } else if (mUserData.getIsExpired() == 0) {
                    Intent intent = new Intent(this, PlanExpiredActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(CALLING_ACTIVITY_KEY, ACTIVITY_EXPIRED);
                    startActivity(intent);
                    finish();

                } else {
                    startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                    finish();

                }
            } else {
                toast(getResources().getString(R.string.error_try_again));

            }
        }
    }


    @Subscribe
    public void onHttpFailure(ApiFailureListener response) {
        if (response.type.equalsIgnoreCase("loginWithSocialMedia")
                || response.type.equalsIgnoreCase("loginUser")
                || response.type.equalsIgnoreCase("sendUserToken")) {


            dismissProgress();
            toast(getString(R.string.error_try_again));

        }
    }

    @Subscribe
    public void onHttpError(ApiFailureMessageListener<ErrorModel> response) {

        if (response.type.equalsIgnoreCase("loginWithSocialMedia")
                || response.type.equalsIgnoreCase("loginUser") || response.type.equalsIgnoreCase("forgotPassword")) {
            dismissProgress();

            try {
                assert response.response.errorBody() != null;
                String responseBody = response.response.errorBody().string();
                ErrorModel errorModel = new Gson().fromJson(responseBody, ErrorModel.class);
                toast(errorModel.getText(), Toast.LENGTH_LONG);
            } catch (Exception e) {
                e.printStackTrace();
                toast(getString(R.string.error_try_again));

            }
        } else if (response.type.equalsIgnoreCase("sendUserToken")) {
            dismissProgress();
            toast(getString(R.string.error_try_again));
        }
    }


    @Override
    protected void onStop() {
        mBus.unregister(this);
        super.onStop();
    }


    @Override
    protected void onDestroy() {


        mRefreshToken = null;
        mLoginType = null;

        mProgressOverlay = null;

        super.onDestroy();
    }

//////////////////////////////******/////////////////////


    @Override
    protected void onStart() {
        super.onStart();
        mBus.register(this);
    }

    protected abstract int getLayoutResource();


    protected void toast(String message) {

        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }


    protected void toast(String message, int length) {

        Toast.makeText(this, message, length).show();
    }


    protected void showProgress() {
        if (mProgressOverlay != null) {
            mProgressOverlay.setVisibility(View.VISIBLE);
        }
    }

    protected void dismissProgress() {

        if (mProgressOverlay != null) {
            mProgressOverlay.setVisibility(View.GONE);
        }
    }

    protected void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}
