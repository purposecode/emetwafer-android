package com.purposecodes.emetwafar.authentication;

import static com.purposecodes.emetwafar.utils.LogUtils.printLog;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_POSITION;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_SUBSCRIPTIONS;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_TRIAL_MODE;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.fragment.RegistrationFragment;
import com.purposecodes.emetwafar.model.Subscription;

import java.util.ArrayList;

public class RegisterActivity extends AppCompatActivity {

    private final String TAG = "RegisterActivity";


    private ArrayList<Subscription> subscriptions;
    private int position;
    private String trialMode="0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        try {


            trialMode = getIntent().getStringExtra(TAG_TRIAL_MODE);
            position = getIntent().getIntExtra(TAG_POSITION, 0);
            subscriptions = (ArrayList<Subscription>) getIntent().getSerializableExtra((TAG_SUBSCRIPTIONS));

            RegistrationFragment fragment = new RegistrationFragment().newInstance(subscriptions,trialMode, position);
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.register_container, fragment);
            fragmentTransaction.commit();

        } catch (Exception e) {
            printLog(TAG, " exception : " + e.getMessage());
        }
    }


}
