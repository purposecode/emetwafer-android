package com.purposecodes.emetwafar.listener;

public interface ActivityConstants {
    int ACTIVITY_ADD = 1001;
    int ACTIVITY_EDIT = 1002;

    int ACTIVITY_BUYNOW = 1010;
    int ACTIVITY_CHECKOUT = 1011;
    int ACTIVITY_PAYMENT = 1012;

    int ACTIVITY_NEW_PLAN = 1020;
    int ACTIVITY_UPDATE_PLAN = 1021;
    int ACTIVITY_EXPIRED_PLAN = 1022;

    int ACTIVITY_EXPIRED = 1030;
    int ACTIVITY_EXPIRE_REMAINDER = 1031;
    int ACTIVITY_EXPIRE_FILED = 1032; //subscription filed


    int ACTIVITY_HOME_DEFAULT = 1040;
    int ACTIVITY_HOME_MYPROFILE = 1041;
    int ACTIVITY_PUSH_MESSAGE = 1042;
}
