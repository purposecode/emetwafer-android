package com.purposecodes.emetwafar.listener;


import com.purposecodes.emetwafar.model.Category;

public interface OnCategoryListener {
    void onCategoryClicked(Category category, int position);
}
