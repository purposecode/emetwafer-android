package com.purposecodes.emetwafar.listener;

public interface ConnectivityReceiverListener {
    void onNetworkConnectionChanged(boolean isConnected);
}
