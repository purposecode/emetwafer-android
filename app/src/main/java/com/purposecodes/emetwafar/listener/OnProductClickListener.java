package com.purposecodes.emetwafar.listener;

import com.purposecodes.emetwafar.model.Product;

public interface OnProductClickListener {
    void onProductClicked(Product product,int position);
    void onAddToWishClicked(Product product,int position);
}
