package com.purposecodes.emetwafar.fcm;

import static com.purposecodes.emetwafar.BuildConfig.DEBUG;
import static com.purposecodes.emetwafar.controller.SessionController.getNotificationCount;
import static com.purposecodes.emetwafar.controller.SessionController.isUserLoggedIn;
import static com.purposecodes.emetwafar.controller.SessionController.setNotificationCount;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_EXPIRED;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_EXPIRE_REMAINDER;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_HOME_MYPROFILE;
import static com.purposecodes.emetwafar.listener.ActivityConstants.ACTIVITY_PUSH_MESSAGE;
import static com.purposecodes.emetwafar.utils.LogUtils.printLog;
import static com.purposecodes.emetwafar.utils.UtilsFunctions.isUsingLatestVersion;
import static com.purposecodes.emetwafar.utils.UtilsVariable.CALLING_ACTIVITY_KEY;
import static com.purposecodes.emetwafar.utils.UtilsVariable.DEBUG_TOPIC_MESSAGE;
import static com.purposecodes.emetwafar.utils.UtilsVariable.DEBUG_TOPIC_VERSION_UPDATE;
import static com.purposecodes.emetwafar.utils.UtilsVariable.RELEASE_TOPIC_MESSAGE;
import static com.purposecodes.emetwafar.utils.UtilsVariable.RELEASE_TOPIC_VERSION_UPDATE;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_MESSAGE;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_ORDER_ID;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_POSITION;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_PRODUCT_ID;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TAG_REMAINDER_DAY;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TYPE_LINK;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TYPE_MESSAGE;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TYPE_ORDER;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TYPE_PRODUCT;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TYPE_SUBSCRIPTION;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TYPE_SUBSCRIPTION_EXPIRY;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TYPE_SUBSCRIPTION_REMAINDER;
import static com.purposecodes.emetwafar.utils.UtilsVariable.TYPE_UPDATE;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.SplashActivity;
import com.purposecodes.emetwafar.controller.AppController;
import com.purposecodes.emetwafar.features.home.HomeActivity;
import com.purposecodes.emetwafar.features.notification.NotificationDetailsActivity;
import com.purposecodes.emetwafar.features.order.OrderDetailsActivity;
import com.purposecodes.emetwafar.features.productview.ProductDetailActivity;
import com.purposecodes.emetwafar.features.subscription.PlanExpiredActivity;
import com.purposecodes.emetwafar.model.MessageItem;
import com.purposecodes.emetwafar.utils.NotificationID;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import me.leolin.shortcutbadger.ShortcutBadger;


@SuppressLint("Registered")
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";


    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages
        // are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data
        // messages are the type
        // traditionally used with GCM. Notification messages are only received here in
        // onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated
        // notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages
        // containing both notification
        // and data payloads are treated as notification messages. The Firebase console always
        // sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]


        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ


        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {


            final Map<String, String> data = remoteMessage.getData();
            final String notifType = data.get("notification_type");
            Handler handler = new Handler(Looper.getMainLooper());

            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (notifType != null && !TextUtils.isEmpty(notifType)) {

                        if (notifType.equals(TYPE_UPDATE)) {

                            String version = "";
                            if (data.containsKey("version") && !TextUtils.isEmpty(data.get("version")))
                                version = data.get("version");

                            if (!isUsingLatestVersion(version))
                                openStore(data.get("body"));
                        } else if (notifType.equals(TYPE_MESSAGE)) {
                            sendNotification(notifType, remoteMessage.getData());

                        } else {
                            if (isUserLoggedIn()) {

                                setNotificationCount(getNotificationCount() + 1);

                                if (data.containsKey("badge")) {
                                    int badgeCount = Integer.parseInt(Objects.requireNonNull(data.get("badge")));
                                    ShortcutBadger.applyCount(MyFirebaseMessagingService.this, badgeCount);
                                    setNotificationCount(badgeCount);
                                }

                                ShortcutBadger.applyCount(MyFirebaseMessagingService.this, getNotificationCount());

                                if (notifType.equals(TYPE_LINK) || notifType.equals(TYPE_ORDER) || notifType.equals(TYPE_PRODUCT) || notifType.equals(TYPE_SUBSCRIPTION_REMAINDER) || notifType.equals(TYPE_SUBSCRIPTION_EXPIRY) || notifType.equals(TYPE_SUBSCRIPTION)) {

                                    sendNotification(notifType, remoteMessage.getData());

                                } else {
                                    sendNotification(remoteMessage.getMessageType());
                                }


                            }

                        }


                    }

                }
            });


        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            printLog(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }


    }


    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onNewToken(String token) {
        printLog(TAG, "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(token);
    }
    // [END on_new_token]


    private void sendRegistrationToServer(String token) {
        // Log.e("Refresh Token", mRefreshToken);
        try {


            if (AppController.get().getUser() != null) {
                Map<String, String> options = new HashMap<>();
                options.put("registration_id", token);
                options.put("device_os", "android");
                options.put("user_id", AppController.get().getUser().getUserId());

                AppController.get().sendUserToken(options, AppController.get().getUser().getTokenId());
            }
        } catch (Exception e) {

        }
    }


    @SuppressLint("UnspecifiedImmutableFlag")
    private void sendNotification(String notifType, Map<String, String> data) {
        Intent backIntent = new Intent(this, HomeActivity.class);
        backIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        Intent intent = new Intent();
        String notificationText;
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Intent[] intents;

        switch (notifType) {

            case TYPE_ORDER:
                intent.setClass(this, OrderDetailsActivity.class);

                intent.putExtra(TAG_POSITION, -1);
                intent.putExtra(TAG_ORDER_ID, data.get("id"));

                notificationText = data.get("body");

                intents = new Intent[]{backIntent, intent};
                break;
            case TYPE_PRODUCT:
                intent.setClass(this, ProductDetailActivity.class);
                intent.putExtra(TAG_POSITION, -1);
                intent.putExtra(TAG_PRODUCT_ID, data.get("id"));

                notificationText = data.get("body");
                intents = new Intent[]{backIntent, intent};
                break;


            case TYPE_SUBSCRIPTION_REMAINDER:

                notificationText = data.get("body");

                String day = "";
                if (data.get("remainder_day") != null && !Objects.requireNonNull(data.get("remainder_day")).isEmpty())
                    day = data.get("remainder_day");


                backIntent = new Intent(this, PlanExpiredActivity.class);
                backIntent.putExtra(CALLING_ACTIVITY_KEY, ACTIVITY_EXPIRE_REMAINDER);
                backIntent.putExtra(TAG_REMAINDER_DAY, day);
                backIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intents = new Intent[]{backIntent, intent};
                break;

            case TYPE_SUBSCRIPTION_EXPIRY:

                notificationText = data.get("body");

                backIntent = new Intent(this, PlanExpiredActivity.class);
                backIntent.putExtra(CALLING_ACTIVITY_KEY, ACTIVITY_EXPIRED);
                backIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intents = new Intent[]{backIntent, intent};
                break;

            case TYPE_SUBSCRIPTION:

                notificationText = data.get("body");

                backIntent = new Intent(this, HomeActivity.class);
                backIntent.putExtra(CALLING_ACTIVITY_KEY, ACTIVITY_HOME_MYPROFILE);
                backIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intents = new Intent[]{backIntent, intent};
                break;
            case TYPE_MESSAGE:

                String title = "";
                String body = "";
                String image_url = "";

                if (data.containsKey("title") && !TextUtils.isEmpty(data.get("title")))
                    title = data.get("title");


                if (data.containsKey("body") && !TextUtils.isEmpty(data.get("body")))
                    body = data.get("body");

                if (data.containsKey("image_url") && !TextUtils.isEmpty(data.get("image_url")))
                    image_url = data.get("image_url");

                notificationText = body;

                MessageItem msg = new MessageItem(title, body, image_url);



                if(!isApplicationBroughtToBackground()) {
                    backIntent = new Intent(this, NotificationDetailsActivity.class);


                    backIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                }   else {
                    backIntent = new Intent(this, SplashActivity.class);
                    backIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK );
                    backIntent.putExtra(CALLING_ACTIVITY_KEY, ACTIVITY_PUSH_MESSAGE);
                }

                backIntent.putExtra(TAG_MESSAGE, msg);

                intents = new Intent[]{backIntent, intent};
                break;


            default:

                intent.setClass(this, SplashActivity.class);
                notificationText = data.get("body");
                intents = new Intent[]{backIntent, intent};
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setAction(Long.toString(System.currentTimeMillis()));
//        PendingIntent pendingIntent = PendingIntent.getActivities(this, 0, intents, PendingIntent.FLAG_ONE_SHOT);


        @SuppressLint("UnspecifiedImmutableFlag") PendingIntent pendingIntent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S)
            pendingIntent = PendingIntent.getActivities(this, 0, intents, PendingIntent.FLAG_IMMUTABLE);
        else
            pendingIntent = PendingIntent.getActivities(this, 0, intents, PendingIntent.FLAG_ONE_SHOT);


        String CHANNEL_ID = "my_channel_01";// The id of the channel.
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                // .setStyle(new NotificationCompat.BigTextStyle().bigText(notificationText))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setSmallIcon(R.drawable.ic_state_notification)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setContentTitle(getResources().getString(R.string.app_name))
                .setTicker(notificationText)
                .setContentText(notificationText)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        Bitmap bitmap = null;
        if (data.containsKey("image_url") && !TextUtils.isEmpty(data.get("image_url"))) {
            bitmap = getBitmapFromUrl(data.get("image_url"));
        }

        if (bitmap != null) {
            notificationBuilder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap).setSummaryText(notificationText));
        } else {
            notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(notificationText));
        }

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (notificationManager != null) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                CharSequence name = getString(R.string.channel_name); // The user-visible name of the channel.
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);

                notificationManager.createNotificationChannel(mChannel);
            }

            notificationManager.notify(NotificationID.getID(), notificationBuilder.build());
        }


    }
    private boolean isApplicationBroughtToBackground() {
        ActivityManager am = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            return !topActivity.getPackageName().equals(this.getPackageName());
        }
        return false;
    }
    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    @SuppressLint("UnspecifiedImmutableFlag")
    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


        @SuppressLint("UnspecifiedImmutableFlag") PendingIntent pendingIntent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S)
            pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_IMMUTABLE);
        else
            pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);


        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_state_notification)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name); // The user-visible name of the channel.
            NotificationChannel channel = new NotificationChannel(channelId, name, NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(NotificationID.getID() /* ID of notification */, notificationBuilder.build());
    }


    @SuppressLint("UnspecifiedImmutableFlag")
    public void openStore(String notificationText) {
        final String appPackageName = getPackageName();
        Intent intent;

        try {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName));
        } catch (android.content.ActivityNotFoundException anfe) {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName));
        }

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

//        @SuppressLint("UnspecifiedImmutableFlag") PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        @SuppressLint("UnspecifiedImmutableFlag") PendingIntent pendingIntent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S)
            pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_IMMUTABLE);
        else
            pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);


        String CHANNEL_ID = getString(R.string.default_notification_channel_id);
        // The id of the channel.
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                // .setStyle(new NotificationCompat.BigTextStyle().bigText(notificationText))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setSmallIcon(R.drawable.ic_state_notification)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setContentTitle(getResources().getString(R.string.app_name))
                .setTicker(notificationText)
                .setContentText(notificationText)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(notificationText));

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (notificationManager != null) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                CharSequence name = getString(R.string.channel_name); // The user-visible name of the channel.
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
                notificationManager.createNotificationChannel(mChannel);
            }

            notificationManager.notify(NotificationID.getID(), notificationBuilder.build());
        }


    }


    /**
     * To get a Bitmap image from the URL received
     */
    public Bitmap getBitmapFromUrl(String imageUrl) {
        try {

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public static void notificationTopicSubscription(){
        FirebaseMessaging.getInstance().subscribeToTopic(DEBUG? DEBUG_TOPIC_VERSION_UPDATE : RELEASE_TOPIC_VERSION_UPDATE)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        String msg = "Subscription success '";
                        if (!task.isSuccessful()) {
                            msg = "Subscription failed '";

                        }
                        printLog("AppController.......", msg+ (DEBUG? DEBUG_TOPIC_VERSION_UPDATE : RELEASE_TOPIC_VERSION_UPDATE)+"'");
//
                    }
                });

        FirebaseMessaging.getInstance().subscribeToTopic(DEBUG? DEBUG_TOPIC_MESSAGE : RELEASE_TOPIC_MESSAGE)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        String msg = "Subscription success '";
                        if (!task.isSuccessful()) {
                            msg = "Subscription failed '";

                        }
                        printLog("AppController.......", msg+ (DEBUG? DEBUG_TOPIC_MESSAGE : RELEASE_TOPIC_MESSAGE)+"'");
//
                    }
                });


    }
}
