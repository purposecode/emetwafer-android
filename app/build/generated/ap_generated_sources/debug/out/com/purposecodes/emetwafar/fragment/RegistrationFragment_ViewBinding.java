// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.fragment;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ToggleButton;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.AppCompatSpinner;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RegistrationFragment_ViewBinding extends BaseFragment_ViewBinding {
  private RegistrationFragment target;

  private View view7f0a00b6;

  @UiThread
  public RegistrationFragment_ViewBinding(final RegistrationFragment target, View source) {
    super(target, source);

    this.target = target;

    View view;
    target.etCompanyName = Utils.findRequiredViewAsType(source, R.id.editText_registration_companyName, "field 'etCompanyName'", EditText.class);
    target.etAddress1 = Utils.findRequiredViewAsType(source, R.id.editText_registration_addressLine_1, "field 'etAddress1'", EditText.class);
    target.etAddress2 = Utils.findRequiredViewAsType(source, R.id.editText_registration_addressLine_2, "field 'etAddress2'", EditText.class);
    target.etPoBox = Utils.findRequiredViewAsType(source, R.id.editText_registration_poBox, "field 'etPoBox'", EditText.class);
    target.etCompanyPhone = Utils.findRequiredViewAsType(source, R.id.editText_registration_companyPhoneNumber, "field 'etCompanyPhone'", EditText.class);
    target.etMobileNumber = Utils.findRequiredViewAsType(source, R.id.editText_registration_mobileNumber, "field 'etMobileNumber'", EditText.class);
    target.etWeb = Utils.findRequiredViewAsType(source, R.id.editText_registration_webAddress, "field 'etWeb'", EditText.class);
    target.etEmail = Utils.findRequiredViewAsType(source, R.id.editText_registration_email, "field 'etEmail'", EditText.class);
    target.etOtherCity = Utils.findRequiredViewAsType(source, R.id.editText_registration_otherCity, "field 'etOtherCity'", EditText.class);
    target.etPassword = Utils.findRequiredViewAsType(source, R.id.editText_registration_password, "field 'etPassword'", EditText.class);
    target.etConfirmPassword = Utils.findRequiredViewAsType(source, R.id.editText_registration_confirmPassword, "field 'etConfirmPassword'", EditText.class);
    target.spinnerSubscription = Utils.findRequiredViewAsType(source, R.id.spinner_register_subscription, "field 'spinnerSubscription'", AppCompatSpinner.class);
    target.spinnerState = Utils.findRequiredViewAsType(source, R.id.spinner_registration_selectState, "field 'spinnerState'", AppCompatSpinner.class);
    target.spinnerCity = Utils.findRequiredViewAsType(source, R.id.spinner_registration_selectCity, "field 'spinnerCity'", AppCompatSpinner.class);
    target.viewOtherCity = Utils.findRequiredViewAsType(source, R.id.layout_register_otherCity, "field 'viewOtherCity'", ViewGroup.class);
    target.progressLayer = Utils.findRequiredViewAsType(source, R.id.layout_prgressBg, "field 'progressLayer'", ViewGroup.class);
    view = Utils.findRequiredView(source, R.id.button_reg_next, "field 'btnNext' and method 'onClickNext'");
    target.btnNext = Utils.castView(view, R.id.button_reg_next, "field 'btnNext'", Button.class);
    view7f0a00b6 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickNext();
      }
    });
    target.tbShowPassword = Utils.findRequiredViewAsType(source, R.id.icon_registration_password, "field 'tbShowPassword'", ToggleButton.class);
    target.tbShowConfPassword = Utils.findRequiredViewAsType(source, R.id.icon_registration_confirmPassword, "field 'tbShowConfPassword'", ToggleButton.class);
  }

  @Override
  public void unbind() {
    RegistrationFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.etCompanyName = null;
    target.etAddress1 = null;
    target.etAddress2 = null;
    target.etPoBox = null;
    target.etCompanyPhone = null;
    target.etMobileNumber = null;
    target.etWeb = null;
    target.etEmail = null;
    target.etOtherCity = null;
    target.etPassword = null;
    target.etConfirmPassword = null;
    target.spinnerSubscription = null;
    target.spinnerState = null;
    target.spinnerCity = null;
    target.viewOtherCity = null;
    target.progressLayer = null;
    target.btnNext = null;
    target.tbShowPassword = null;
    target.tbShowConfPassword = null;

    view7f0a00b6.setOnClickListener(null);
    view7f0a00b6 = null;

    super.unbind();
  }
}
