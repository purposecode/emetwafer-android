// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.subscription;

import android.view.View;
import android.widget.ImageButton;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RvSubscriptionAdapter$RvHeaderHolder_ViewBinding implements Unbinder {
  private RvSubscriptionAdapter.RvHeaderHolder target;

  @UiThread
  public RvSubscriptionAdapter$RvHeaderHolder_ViewBinding(
      RvSubscriptionAdapter.RvHeaderHolder target, View source) {
    this.target = target;

    target.ibBack = Utils.findRequiredViewAsType(source, R.id.imageButton_subscriptionHeader_back, "field 'ibBack'", ImageButton.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RvSubscriptionAdapter.RvHeaderHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ibBack = null;
  }
}
