// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.mycart;

import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity_ViewBinding;
import com.sk.icode.errorview.ErrorView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MyCartActivity_ViewBinding extends BaseActivity_ViewBinding {
  private MyCartActivity target;

  private View view7f0a009d;

  @UiThread
  public MyCartActivity_ViewBinding(MyCartActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MyCartActivity_ViewBinding(final MyCartActivity target, View source) {
    super(target, source);

    this.target = target;

    View view;
    target.errorView = Utils.findRequiredViewAsType(source, R.id.errorView, "field 'errorView'", ErrorView.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView_myCart, "field 'recyclerView'", RecyclerView.class);
    target.swipeRefreshLayout = Utils.findRequiredViewAsType(source, R.id.swipeRefreshLayout_myCart, "field 'swipeRefreshLayout'", SwipeRefreshLayout.class);
    target.dataView = Utils.findRequiredViewAsType(source, R.id.view_cart, "field 'dataView'", ViewGroup.class);
    view = Utils.findRequiredView(source, R.id.button_myCart_proceed, "method 'cartCheckout'");
    view7f0a009d = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.cartCheckout();
      }
    });
  }

  @Override
  public void unbind() {
    MyCartActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.errorView = null;
    target.recyclerView = null;
    target.swipeRefreshLayout = null;
    target.dataView = null;

    view7f0a009d.setOnClickListener(null);
    view7f0a009d = null;

    super.unbind();
  }
}
