// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.coupons;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.google.android.material.button.MaterialButton;
import com.purposecodes.emetwafar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RvMyCouponsAdapter$RvMyCouponsHolder_ViewBinding implements Unbinder {
  private RvMyCouponsAdapter.RvMyCouponsHolder target;

  @UiThread
  public RvMyCouponsAdapter$RvMyCouponsHolder_ViewBinding(
      RvMyCouponsAdapter.RvMyCouponsHolder target, View source) {
    this.target = target;

    target.tvCode = Utils.findRequiredViewAsType(source, R.id.textView_itemMyCoupon_no, "field 'tvCode'", TextView.class);
    target.tvDate = Utils.findRequiredViewAsType(source, R.id.textView_itemMyCoupon_date, "field 'tvDate'", TextView.class);
    target.btnCopy = Utils.findRequiredViewAsType(source, R.id.button_itemMyCoupon_copy, "field 'btnCopy'", MaterialButton.class);
    target.btnRemove = Utils.findRequiredViewAsType(source, R.id.button_itemMyCoupon_remove, "field 'btnRemove'", MaterialButton.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RvMyCouponsAdapter.RvMyCouponsHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvCode = null;
    target.tvDate = null;
    target.btnCopy = null;
    target.btnRemove = null;
  }
}
