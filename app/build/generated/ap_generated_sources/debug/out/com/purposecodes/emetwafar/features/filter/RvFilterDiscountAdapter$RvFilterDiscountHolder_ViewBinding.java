// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.filter;

import android.view.View;
import android.widget.Button;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RvFilterDiscountAdapter$RvFilterDiscountHolder_ViewBinding implements Unbinder {
  private RvFilterDiscountAdapter.RvFilterDiscountHolder target;

  @UiThread
  public RvFilterDiscountAdapter$RvFilterDiscountHolder_ViewBinding(
      RvFilterDiscountAdapter.RvFilterDiscountHolder target, View source) {
    this.target = target;

    target.btnDiscount = Utils.findRequiredViewAsType(source, R.id.button_filter_discount, "field 'btnDiscount'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RvFilterDiscountAdapter.RvFilterDiscountHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.btnDiscount = null;
  }
}
