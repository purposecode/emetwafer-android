// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.notification;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.UiThread;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class NotificationDetailsActivity_ViewBinding extends BaseActivity_ViewBinding {
  private NotificationDetailsActivity target;

  private View view7f0a01ab;

  @UiThread
  public NotificationDetailsActivity_ViewBinding(NotificationDetailsActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public NotificationDetailsActivity_ViewBinding(final NotificationDetailsActivity target,
      View source) {
    super(target, source);

    this.target = target;

    View view;
    target.ivImage = Utils.findRequiredViewAsType(source, R.id.imageView_notificationDetails, "field 'ivImage'", ImageView.class);
    target.tvTitle = Utils.findRequiredViewAsType(source, R.id.textView_notificationDetails_title, "field 'tvTitle'", TextView.class);
    target.tvBody = Utils.findRequiredViewAsType(source, R.id.textView_notificationDetails_body, "field 'tvBody'", TextView.class);
    target.progressBar = Utils.findRequiredViewAsType(source, R.id.progress_notificationDetails, "field 'progressBar'", ProgressBar.class);
    view = Utils.findRequiredView(source, R.id.imageButton_notificationDetails_close, "method 'closeView'");
    view7f0a01ab = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.closeView();
      }
    });
  }

  @Override
  public void unbind() {
    NotificationDetailsActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivImage = null;
    target.tvTitle = null;
    target.tvBody = null;
    target.progressBar = null;

    view7f0a01ab.setOnClickListener(null);
    view7f0a01ab = null;

    super.unbind();
  }
}
