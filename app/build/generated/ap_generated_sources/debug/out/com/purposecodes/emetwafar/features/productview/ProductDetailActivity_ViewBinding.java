// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.productview;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity_ViewBinding;
import com.sk.icode.errorview.ErrorView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProductDetailActivity_ViewBinding extends BaseActivity_ViewBinding {
  private ProductDetailActivity target;

  private View view7f0a01ac;

  private View view7f0a01ad;

  private View view7f0a01af;

  private View view7f0a01ae;

  private View view7f0a01b0;

  @UiThread
  public ProductDetailActivity_ViewBinding(ProductDetailActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ProductDetailActivity_ViewBinding(final ProductDetailActivity target, View source) {
    super(target, source);

    this.target = target;

    View view;
    target.tvDate = Utils.findRequiredViewAsType(source, R.id.textView_productDetail_date, "field 'tvDate'", TextView.class);
    target.tvCategory = Utils.findRequiredViewAsType(source, R.id.textView_productDetail_category, "field 'tvCategory'", TextView.class);
    target.tvProductName = Utils.findRequiredViewAsType(source, R.id.textView_productDetail_name, "field 'tvProductName'", TextView.class);
    target.tvDescription = Utils.findRequiredViewAsType(source, R.id.textView_productDetail_description, "field 'tvDescription'", TextView.class);
    target.tvCode = Utils.findRequiredViewAsType(source, R.id.textView_productDetail_code, "field 'tvCode'", TextView.class);
    target.tvPrice = Utils.findRequiredViewAsType(source, R.id.textView_productDetail_amount, "field 'tvPrice'", TextView.class);
    target.tvOffer = Utils.findRequiredViewAsType(source, R.id.textView_productDetail_offer, "field 'tvOffer'", TextView.class);
    target.tvSellerId = Utils.findRequiredViewAsType(source, R.id.textView_productDetail_sellerId, "field 'tvSellerId'", TextView.class);
    target.tvLocation = Utils.findRequiredViewAsType(source, R.id.textView_productDetail_location, "field 'tvLocation'", TextView.class);
    target.tvWarrantyDays = Utils.findRequiredViewAsType(source, R.id.textView_productDetail_warrantyDays, "field 'tvWarrantyDays'", TextView.class);
    target.viewNotAvailable = Utils.findRequiredViewAsType(source, R.id.textView_productDetail_notAvailable, "field 'viewNotAvailable'", TextView.class);
    target.cbNeedWarranty = Utils.findRequiredViewAsType(source, R.id.checkBox_productDetail_warrantyNeed, "field 'cbNeedWarranty'", CheckBox.class);
    target.etQuantity = Utils.findRequiredViewAsType(source, R.id.editText_productDetail_Qty, "field 'etQuantity'", EditText.class);
    target.spinnerWarranty = Utils.findRequiredViewAsType(source, R.id.spinner_productDetail_warranty, "field 'spinnerWarranty'", AppCompatSpinner.class);
    target.btnBuyNow = Utils.findRequiredViewAsType(source, R.id.button_productDetail_buyNow, "field 'btnBuyNow'", Button.class);
    target.btnAddToCart = Utils.findRequiredViewAsType(source, R.id.button_productDetail_addToCart, "field 'btnAddToCart'", Button.class);
    target.btnDownloadTDS = Utils.findRequiredViewAsType(source, R.id.button_productDetail_download, "field 'btnDownloadTDS'", Button.class);
    target.rvSpecification = Utils.findRequiredViewAsType(source, R.id.recyclerView_productDetail_specification, "field 'rvSpecification'", RecyclerView.class);
    target.errorView = Utils.findRequiredViewAsType(source, R.id.errorView, "field 'errorView'", ErrorView.class);
    target.ibWish = Utils.findRequiredViewAsType(source, R.id.imageButton_productDetail_wish, "field 'ibWish'", ImageButton.class);
    target.dataView = Utils.findRequiredViewAsType(source, R.id.view_productDetail, "field 'dataView'", ViewGroup.class);
    target.warrantyView = Utils.findRequiredViewAsType(source, R.id.view_productDetail_warrantyList, "field 'warrantyView'", ViewGroup.class);
    target.viewPagerImage = Utils.findRequiredViewAsType(source, R.id.viewPager_productDetail_images, "field 'viewPagerImage'", ViewPager.class);
    target.sliderDotspanel = Utils.findRequiredViewAsType(source, R.id.layout_sliderDots, "field 'sliderDotspanel'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.imageButton_productDetail_QtyDown, "method 'onButtonClick'");
    view7f0a01ac = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onButtonClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.imageButton_productDetail_QtyUp, "method 'onButtonClick'");
    view7f0a01ad = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onButtonClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.imageButton_productDetail_backBase, "method 'onButtonClick'");
    view7f0a01af = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onButtonClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.imageButton_productDetail_back, "method 'onButtonClick'");
    view7f0a01ae = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onButtonClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.imageButton_productDetail_share, "method 'onButtonClick'");
    view7f0a01b0 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onButtonClick(p0);
      }
    });
  }

  @Override
  public void unbind() {
    ProductDetailActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvDate = null;
    target.tvCategory = null;
    target.tvProductName = null;
    target.tvDescription = null;
    target.tvCode = null;
    target.tvPrice = null;
    target.tvOffer = null;
    target.tvSellerId = null;
    target.tvLocation = null;
    target.tvWarrantyDays = null;
    target.viewNotAvailable = null;
    target.cbNeedWarranty = null;
    target.etQuantity = null;
    target.spinnerWarranty = null;
    target.btnBuyNow = null;
    target.btnAddToCart = null;
    target.btnDownloadTDS = null;
    target.rvSpecification = null;
    target.errorView = null;
    target.ibWish = null;
    target.dataView = null;
    target.warrantyView = null;
    target.viewPagerImage = null;
    target.sliderDotspanel = null;

    view7f0a01ac.setOnClickListener(null);
    view7f0a01ac = null;
    view7f0a01ad.setOnClickListener(null);
    view7f0a01ad = null;
    view7f0a01af.setOnClickListener(null);
    view7f0a01af = null;
    view7f0a01ae.setOnClickListener(null);
    view7f0a01ae = null;
    view7f0a01b0.setOnClickListener(null);
    view7f0a01b0 = null;

    super.unbind();
  }
}
