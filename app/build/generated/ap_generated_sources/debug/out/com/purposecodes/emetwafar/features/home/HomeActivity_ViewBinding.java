// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.home;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.UiThread;
import androidx.drawerlayout.widget.DrawerLayout;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity_ViewBinding;
import de.hdodenhof.circleimageview.CircleImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class HomeActivity_ViewBinding extends BaseActivity_ViewBinding {
  private HomeActivity target;

  private View view7f0a01c8;

  private View view7f0a00a5;

  private View view7f0a00a4;

  private View view7f0a00a2;

  private View view7f0a00a3;

  private View view7f0a009e;

  private View view7f0a00a1;

  private View view7f0a00a6;

  private View view7f0a009f;

  private View view7f0a00a0;

  @UiThread
  public HomeActivity_ViewBinding(HomeActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public HomeActivity_ViewBinding(final HomeActivity target, View source) {
    super(target, source);

    this.target = target;

    View view;
    target.mDrawerLayout = Utils.findRequiredViewAsType(source, R.id.drawer_layout, "field 'mDrawerLayout'", DrawerLayout.class);
    view = Utils.findRequiredView(source, R.id.imageView_nav_profile, "field 'ivProfile' and method 'onNavItemClick'");
    target.ivProfile = Utils.castView(view, R.id.imageView_nav_profile, "field 'ivProfile'", CircleImageView.class);
    view7f0a01c8 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onNavItemClick(p0);
      }
    });
    target.tvNavUsername = Utils.findRequiredViewAsType(source, R.id.textView_nav_userName, "field 'tvNavUsername'", TextView.class);
    target.tvNavEmail = Utils.findRequiredViewAsType(source, R.id.textView_nav_email, "field 'tvNavEmail'", TextView.class);
    target.ivProfileVerify = Utils.findRequiredViewAsType(source, R.id.imageView_navProfile_verify, "field 'ivProfileVerify'", ImageView.class);
    target.navView = Utils.findRequiredViewAsType(source, R.id.nav_view, "field 'navView'", BottomNavigationView.class);
    view = Utils.findRequiredView(source, R.id.button_nav_products, "method 'onNavItemClick'");
    view7f0a00a5 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onNavItemClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.button_nav_offer, "method 'onNavItemClick'");
    view7f0a00a4 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onNavItemClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.button_nav_myCoupons, "method 'onNavItemClick'");
    view7f0a00a2 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onNavItemClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.button_nav_myOrders, "method 'onNavItemClick'");
    view7f0a00a3 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onNavItemClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.button_nav_companyProfile, "method 'onNavItemClick'");
    view7f0a009e = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onNavItemClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.button_nav_mayCart, "method 'onNavItemClick'");
    view7f0a00a1 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onNavItemClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.button_nav_subscriptionHistory, "method 'onNavItemClick'");
    view7f0a00a6 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onNavItemClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.button_nav_help, "method 'onNavItemClick'");
    view7f0a009f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onNavItemClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.button_nav_logout, "method 'onNavItemClick'");
    view7f0a00a0 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onNavItemClick(p0);
      }
    });
  }

  @Override
  public void unbind() {
    HomeActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mDrawerLayout = null;
    target.ivProfile = null;
    target.tvNavUsername = null;
    target.tvNavEmail = null;
    target.ivProfileVerify = null;
    target.navView = null;

    view7f0a01c8.setOnClickListener(null);
    view7f0a01c8 = null;
    view7f0a00a5.setOnClickListener(null);
    view7f0a00a5 = null;
    view7f0a00a4.setOnClickListener(null);
    view7f0a00a4 = null;
    view7f0a00a2.setOnClickListener(null);
    view7f0a00a2 = null;
    view7f0a00a3.setOnClickListener(null);
    view7f0a00a3 = null;
    view7f0a009e.setOnClickListener(null);
    view7f0a009e = null;
    view7f0a00a1.setOnClickListener(null);
    view7f0a00a1 = null;
    view7f0a00a6.setOnClickListener(null);
    view7f0a00a6 = null;
    view7f0a009f.setOnClickListener(null);
    view7f0a009f = null;
    view7f0a00a0.setOnClickListener(null);
    view7f0a00a0 = null;

    super.unbind();
  }
}
