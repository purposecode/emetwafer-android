// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.fragment;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.UiThread;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OTPFragment_ViewBinding extends BaseFragment_ViewBinding {
  private OTPFragment target;

  private View view7f0a00af;

  private View view7f0a00ae;

  @UiThread
  public OTPFragment_ViewBinding(final OTPFragment target, View source) {
    super(target, source);

    this.target = target;

    View view;
    target.tvContent = Utils.findRequiredViewAsType(source, R.id.textView_otp_content, "field 'tvContent'", TextView.class);
    target.etOtp1 = Utils.findRequiredViewAsType(source, R.id.editText_otp_1, "field 'etOtp1'", EditText.class);
    target.etOtp2 = Utils.findRequiredViewAsType(source, R.id.editText_otp_2, "field 'etOtp2'", EditText.class);
    target.etOtp3 = Utils.findRequiredViewAsType(source, R.id.editText_otp_3, "field 'etOtp3'", EditText.class);
    target.etOtp4 = Utils.findRequiredViewAsType(source, R.id.editText_otp_4, "field 'etOtp4'", EditText.class);
    view = Utils.findRequiredView(source, R.id.button_otp_verify, "field 'btnVerify' and method 'onClickNext'");
    target.btnVerify = Utils.castView(view, R.id.button_otp_verify, "field 'btnVerify'", Button.class);
    view7f0a00af = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickNext(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.button_otp_resend, "field 'btnResend' and method 'onClickNext'");
    target.btnResend = Utils.castView(view, R.id.button_otp_resend, "field 'btnResend'", Button.class);
    view7f0a00ae = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickNext(p0);
      }
    });
  }

  @Override
  public void unbind() {
    OTPFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvContent = null;
    target.etOtp1 = null;
    target.etOtp2 = null;
    target.etOtp3 = null;
    target.etOtp4 = null;
    target.btnVerify = null;
    target.btnResend = null;

    view7f0a00af.setOnClickListener(null);
    view7f0a00af = null;
    view7f0a00ae.setOnClickListener(null);
    view7f0a00ae = null;

    super.unbind();
  }
}
