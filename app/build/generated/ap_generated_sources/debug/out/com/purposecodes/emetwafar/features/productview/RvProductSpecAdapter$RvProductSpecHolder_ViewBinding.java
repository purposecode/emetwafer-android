// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.productview;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RvProductSpecAdapter$RvProductSpecHolder_ViewBinding implements Unbinder {
  private RvProductSpecAdapter.RvProductSpecHolder target;

  @UiThread
  public RvProductSpecAdapter$RvProductSpecHolder_ViewBinding(
      RvProductSpecAdapter.RvProductSpecHolder target, View source) {
    this.target = target;

    target.tvTitle = Utils.findRequiredViewAsType(source, R.id.textView_itemProductSpec_title, "field 'tvTitle'", TextView.class);
    target.tvDescription = Utils.findRequiredViewAsType(source, R.id.textView_itemProductSpec_desc, "field 'tvDescription'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RvProductSpecAdapter.RvProductSpecHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvTitle = null;
    target.tvDescription = null;
  }
}
