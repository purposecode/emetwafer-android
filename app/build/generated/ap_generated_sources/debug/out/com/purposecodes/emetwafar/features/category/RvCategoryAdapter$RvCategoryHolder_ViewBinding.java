// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.category;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RvCategoryAdapter$RvCategoryHolder_ViewBinding implements Unbinder {
  private RvCategoryAdapter.RvCategoryHolder target;

  @UiThread
  public RvCategoryAdapter$RvCategoryHolder_ViewBinding(RvCategoryAdapter.RvCategoryHolder target,
      View source) {
    this.target = target;

    target.ivProduct = Utils.findRequiredViewAsType(source, R.id.imageView_itemCategory, "field 'ivProduct'", ImageView.class);
    target.tvName = Utils.findRequiredViewAsType(source, R.id.textView_itemCategory, "field 'tvName'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RvCategoryAdapter.RvCategoryHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivProduct = null;
    target.tvName = null;
  }
}
