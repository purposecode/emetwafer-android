// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.notification;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.cardview.widget.CardView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RvNotificationAdapter$RvNotificationHolder_ViewBinding implements Unbinder {
  private RvNotificationAdapter.RvNotificationHolder target;

  @UiThread
  public RvNotificationAdapter$RvNotificationHolder_ViewBinding(
      RvNotificationAdapter.RvNotificationHolder target, View source) {
    this.target = target;

    target.cvBackground = Utils.findRequiredViewAsType(source, R.id.cardView_notification, "field 'cvBackground'", CardView.class);
    target.ivProduct = Utils.findRequiredViewAsType(source, R.id.imageView_itemNotification_imageView, "field 'ivProduct'", ImageView.class);
    target.tvTitle = Utils.findRequiredViewAsType(source, R.id.textView_itemNotification_title, "field 'tvTitle'", TextView.class);
    target.tvDescription = Utils.findRequiredViewAsType(source, R.id.textView_itemNotification_description, "field 'tvDescription'", TextView.class);
    target.tvDate = Utils.findRequiredViewAsType(source, R.id.textView_itemNotification_date, "field 'tvDate'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RvNotificationAdapter.RvNotificationHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.cvBackground = null;
    target.ivProduct = null;
    target.tvTitle = null;
    target.tvDescription = null;
    target.tvDate = null;
  }
}
