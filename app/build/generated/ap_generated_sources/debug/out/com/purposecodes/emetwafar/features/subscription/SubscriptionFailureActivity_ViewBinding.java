// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.subscription;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.UiThread;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SubscriptionFailureActivity_ViewBinding extends BaseActivity_ViewBinding {
  private SubscriptionFailureActivity target;

  private View view7f0a00bd;

  @UiThread
  public SubscriptionFailureActivity_ViewBinding(SubscriptionFailureActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SubscriptionFailureActivity_ViewBinding(final SubscriptionFailureActivity target,
      View source) {
    super(target, source);

    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.button_subscriptionFail_close, "field 'btnClose' and method 'onButtonClick'");
    target.btnClose = Utils.castView(view, R.id.button_subscriptionFail_close, "field 'btnClose'", Button.class);
    view7f0a00bd = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onButtonClick(p0);
      }
    });
    target.tvTitle = Utils.findRequiredViewAsType(source, R.id.textView_subscriptionFail_title, "field 'tvTitle'", TextView.class);
    target.tvDescription = Utils.findRequiredViewAsType(source, R.id.textView_subscriptionFail_description, "field 'tvDescription'", TextView.class);
  }

  @Override
  public void unbind() {
    SubscriptionFailureActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.btnClose = null;
    target.tvTitle = null;
    target.tvDescription = null;

    view7f0a00bd.setOnClickListener(null);
    view7f0a00bd = null;

    super.unbind();
  }
}
