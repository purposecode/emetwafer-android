// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.activity;

import android.view.View;
import androidx.annotation.UiThread;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import com.sk.icode.errorview.ErrorView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class NoInternetActivity_ViewBinding extends BaseActivity_ViewBinding {
  private NoInternetActivity target;

  @UiThread
  public NoInternetActivity_ViewBinding(NoInternetActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public NoInternetActivity_ViewBinding(NoInternetActivity target, View source) {
    super(target, source);

    this.target = target;

    target.errorView = Utils.findRequiredViewAsType(source, R.id.errorView, "field 'errorView'", ErrorView.class);
  }

  @Override
  public void unbind() {
    NoInternetActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.errorView = null;

    super.unbind();
  }
}
