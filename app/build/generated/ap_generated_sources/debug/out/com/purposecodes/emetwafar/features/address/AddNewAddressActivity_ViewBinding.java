// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.address;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.AppCompatSpinner;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AddNewAddressActivity_ViewBinding extends BaseActivity_ViewBinding {
  private AddNewAddressActivity target;

  private View view7f0a0077;

  @UiThread
  public AddNewAddressActivity_ViewBinding(AddNewAddressActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AddNewAddressActivity_ViewBinding(final AddNewAddressActivity target, View source) {
    super(target, source);

    this.target = target;

    View view;
    target.etName = Utils.findRequiredViewAsType(source, R.id.editText_addNewAddress_name, "field 'etName'", EditText.class);
    target.etAddress1 = Utils.findRequiredViewAsType(source, R.id.editText_addNewAddress_addressLine_1, "field 'etAddress1'", EditText.class);
    target.etAddress2 = Utils.findRequiredViewAsType(source, R.id.editText_addNewAddress_addressLine_2, "field 'etAddress2'", EditText.class);
    target.etPoBox = Utils.findRequiredViewAsType(source, R.id.editText_addNewAddress_poBox, "field 'etPoBox'", EditText.class);
    target.etNewCity = Utils.findRequiredViewAsType(source, R.id.editText_addNewAddress_otherCity, "field 'etNewCity'", EditText.class);
    target.etLandMark = Utils.findRequiredViewAsType(source, R.id.editText_addNewAddress_landMark, "field 'etLandMark'", EditText.class);
    target.etMobile = Utils.findRequiredViewAsType(source, R.id.editText_addNewAddress_mobileNumber, "field 'etMobile'", EditText.class);
    target.etAlternative = Utils.findRequiredViewAsType(source, R.id.editText_addNewAddress_alternativeNumber, "field 'etAlternative'", EditText.class);
    view = Utils.findRequiredView(source, R.id.button_addNewAddress, "field 'btnSave' and method 'onClickSave'");
    target.btnSave = Utils.castView(view, R.id.button_addNewAddress, "field 'btnSave'", Button.class);
    view7f0a0077 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickSave();
      }
    });
    target.spinnerState = Utils.findRequiredViewAsType(source, R.id.spinner_addNewAddress_selectState, "field 'spinnerState'", AppCompatSpinner.class);
    target.spinnerCity = Utils.findRequiredViewAsType(source, R.id.spinner_addNewAddress_selectCity, "field 'spinnerCity'", AppCompatSpinner.class);
  }

  @Override
  public void unbind() {
    AddNewAddressActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.etName = null;
    target.etAddress1 = null;
    target.etAddress2 = null;
    target.etPoBox = null;
    target.etNewCity = null;
    target.etLandMark = null;
    target.etMobile = null;
    target.etAlternative = null;
    target.btnSave = null;
    target.spinnerState = null;
    target.spinnerCity = null;

    view7f0a0077.setOnClickListener(null);
    view7f0a0077 = null;

    super.unbind();
  }
}
