// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.subscription;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.UiThread;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity_ViewBinding;
import com.sk.icode.errorview.ErrorView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SubscriptionSuccessActivity_ViewBinding extends BaseActivity_ViewBinding {
  private SubscriptionSuccessActivity target;

  private View view7f0a00be;

  @UiThread
  public SubscriptionSuccessActivity_ViewBinding(SubscriptionSuccessActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SubscriptionSuccessActivity_ViewBinding(final SubscriptionSuccessActivity target,
      View source) {
    super(target, source);

    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.button_subscriptionSuccess_continue, "field 'btnGoto' and method 'onContinueLogin'");
    target.btnGoto = Utils.castView(view, R.id.button_subscriptionSuccess_continue, "field 'btnGoto'", Button.class);
    view7f0a00be = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onContinueLogin();
      }
    });
    target.tvTitle = Utils.findRequiredViewAsType(source, R.id.textView_subscriptionSuccess_title, "field 'tvTitle'", TextView.class);
    target.tvDescription = Utils.findRequiredViewAsType(source, R.id.textView_subscriptionSuccess_description, "field 'tvDescription'", TextView.class);
    target.errorView = Utils.findRequiredViewAsType(source, R.id.errorView, "field 'errorView'", ErrorView.class);
    target.viewBackground = Utils.findRequiredViewAsType(source, R.id.subscription_plan_bg, "field 'viewBackground'", ViewGroup.class);
    target.dataView = Utils.findRequiredViewAsType(source, R.id.view_subscriptionSuccess, "field 'dataView'", ViewGroup.class);
  }

  @Override
  public void unbind() {
    SubscriptionSuccessActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.btnGoto = null;
    target.tvTitle = null;
    target.tvDescription = null;
    target.errorView = null;
    target.viewBackground = null;
    target.dataView = null;

    view7f0a00be.setOnClickListener(null);
    view7f0a00be = null;

    super.unbind();
  }
}
