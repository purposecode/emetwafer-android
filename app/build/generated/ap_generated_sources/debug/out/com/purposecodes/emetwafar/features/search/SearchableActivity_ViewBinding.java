// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.search;

import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import androidx.annotation.UiThread;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity_ViewBinding;
import com.sk.icode.errorview.ErrorView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SearchableActivity_ViewBinding extends BaseActivity_ViewBinding {
  private SearchableActivity target;

  @UiThread
  public SearchableActivity_ViewBinding(SearchableActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SearchableActivity_ViewBinding(SearchableActivity target, View source) {
    super(target, source);

    this.target = target;

    target.errorView = Utils.findRequiredViewAsType(source, R.id.errorView, "field 'errorView'", ErrorView.class);
    target.productListView = Utils.findRequiredViewAsType(source, R.id.listView_search, "field 'productListView'", ListView.class);
    target.etSearch = Utils.findRequiredViewAsType(source, R.id.editText_searchProduct, "field 'etSearch'", EditText.class);
  }

  @Override
  public void unbind() {
    SearchableActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.errorView = null;
    target.productListView = null;
    target.etSearch = null;

    super.unbind();
  }
}
