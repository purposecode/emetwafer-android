// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.order;

import android.view.View;
import android.widget.CheckBox;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.cardview.widget.CardView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RvHelpListAdapter$RvHelpListHolder_ViewBinding implements Unbinder {
  private RvHelpListAdapter.RvHelpListHolder target;

  @UiThread
  public RvHelpListAdapter$RvHelpListHolder_ViewBinding(RvHelpListAdapter.RvHelpListHolder target,
      View source) {
    this.target = target;

    target.cardBg = Utils.findRequiredViewAsType(source, R.id.cardView_help_item, "field 'cardBg'", CardView.class);
    target.cbText = Utils.findRequiredViewAsType(source, R.id.checkBox_helpItem, "field 'cbText'", CheckBox.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RvHelpListAdapter.RvHelpListHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.cardBg = null;
    target.cbText = null;
  }
}
