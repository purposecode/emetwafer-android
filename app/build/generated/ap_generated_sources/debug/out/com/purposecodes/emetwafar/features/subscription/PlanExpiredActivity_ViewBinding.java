// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.subscription;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.UiThread;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity_ViewBinding;
import com.sk.icode.errorview.ErrorView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PlanExpiredActivity_ViewBinding extends BaseActivity_ViewBinding {
  private PlanExpiredActivity target;

  private View view7f0a00be;

  private View view7f0a00bf;

  private View view7f0a01a6;

  @UiThread
  public PlanExpiredActivity_ViewBinding(PlanExpiredActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public PlanExpiredActivity_ViewBinding(final PlanExpiredActivity target, View source) {
    super(target, source);

    this.target = target;

    View view;
    target.dataView = Utils.findRequiredViewAsType(source, R.id.view_subscriptionSuccess, "field 'dataView'", ViewGroup.class);
    view = Utils.findRequiredView(source, R.id.button_subscriptionSuccess_continue, "field 'btnGoto' and method 'onButtonClick'");
    target.btnGoto = Utils.castView(view, R.id.button_subscriptionSuccess_continue, "field 'btnGoto'", Button.class);
    view7f0a00be = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onButtonClick(p0);
      }
    });
    target.tvTitle = Utils.findRequiredViewAsType(source, R.id.textView_subscriptionSuccess_title, "field 'tvTitle'", TextView.class);
    view = Utils.findRequiredView(source, R.id.button_subscriptionSuccess_skip, "field 'btnSkip' and method 'onButtonClick'");
    target.btnSkip = Utils.castView(view, R.id.button_subscriptionSuccess_skip, "field 'btnSkip'", Button.class);
    view7f0a00bf = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onButtonClick(p0);
      }
    });
    target.ivSuccess = Utils.findRequiredViewAsType(source, R.id.imageView_subscriptionSuccess, "field 'ivSuccess'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.imageButton_expired_logout, "field 'ibLogout' and method 'onButtonClick'");
    target.ibLogout = Utils.castView(view, R.id.imageButton_expired_logout, "field 'ibLogout'", ImageButton.class);
    view7f0a01a6 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onButtonClick(p0);
      }
    });
    target.tvDescription = Utils.findRequiredViewAsType(source, R.id.textView_subscriptionSuccess_description, "field 'tvDescription'", TextView.class);
    target.errorView = Utils.findRequiredViewAsType(source, R.id.errorView, "field 'errorView'", ErrorView.class);
    target.viewBackground = Utils.findRequiredViewAsType(source, R.id.subscription_plan_bg, "field 'viewBackground'", ViewGroup.class);
  }

  @Override
  public void unbind() {
    PlanExpiredActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.dataView = null;
    target.btnGoto = null;
    target.tvTitle = null;
    target.btnSkip = null;
    target.ivSuccess = null;
    target.ibLogout = null;
    target.tvDescription = null;
    target.errorView = null;
    target.viewBackground = null;

    view7f0a00be.setOnClickListener(null);
    view7f0a00be = null;
    view7f0a00bf.setOnClickListener(null);
    view7f0a00bf = null;
    view7f0a01a6.setOnClickListener(null);
    view7f0a01a6 = null;

    super.unbind();
  }
}
