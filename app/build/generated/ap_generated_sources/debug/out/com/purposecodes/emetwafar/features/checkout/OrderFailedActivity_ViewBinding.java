// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.checkout;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.UiThread;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OrderFailedActivity_ViewBinding extends BaseActivity_ViewBinding {
  private OrderFailedActivity target;

  private View view7f0a0088;

  @UiThread
  public OrderFailedActivity_ViewBinding(OrderFailedActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public OrderFailedActivity_ViewBinding(final OrderFailedActivity target, View source) {
    super(target, source);

    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.button_failedTransaction_retry, "field 'btnRetry' and method 'onBackPressed'");
    target.btnRetry = Utils.castView(view, R.id.button_failedTransaction_retry, "field 'btnRetry'", Button.class);
    view7f0a0088 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onBackPressed();
      }
    });
    target.tvTitle = Utils.findRequiredViewAsType(source, R.id.textView_failedTransaction_title, "field 'tvTitle'", TextView.class);
    target.tvDescription = Utils.findRequiredViewAsType(source, R.id.textView_failedTransaction_description, "field 'tvDescription'", TextView.class);
  }

  @Override
  public void unbind() {
    OrderFailedActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.btnRetry = null;
    target.tvTitle = null;
    target.tvDescription = null;

    view7f0a0088.setOnClickListener(null);
    view7f0a0088 = null;

    super.unbind();
  }
}
