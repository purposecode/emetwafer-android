// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.checkout;

import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.UiThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.google.android.material.button.MaterialButton;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CheckoutActivity_ViewBinding extends BaseActivity_ViewBinding {
  private CheckoutActivity target;

  private View view7f0a0081;

  private View view7f0a007f;

  private View view7f0a0080;

  private View view7f0a0082;

  @UiThread
  public CheckoutActivity_ViewBinding(CheckoutActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CheckoutActivity_ViewBinding(final CheckoutActivity target, View source) {
    super(target, source);

    this.target = target;

    View view;
    target.rvProduct = Utils.findRequiredViewAsType(source, R.id.recyclerView_checkout_productList, "field 'rvProduct'", RecyclerView.class);
    target.rvPayment = Utils.findRequiredViewAsType(source, R.id.recyclerView_checkout_payment, "field 'rvPayment'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.button_checkout_deliveryYes, "field 'btnDeliveryYes' and method 'onButtonClicked'");
    target.btnDeliveryYes = Utils.castView(view, R.id.button_checkout_deliveryYes, "field 'btnDeliveryYes'", Button.class);
    view7f0a0081 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onButtonClicked(p0);
      }
    });
    target.ivDeliveryYes = Utils.findRequiredViewAsType(source, R.id.imageView_checkout_deliveryYes, "field 'ivDeliveryYes'", ImageView.class);
    target.ivDeliveryNo = Utils.findRequiredViewAsType(source, R.id.imageView_checkout_deliveryNo, "field 'ivDeliveryNo'", ImageView.class);
    target.deliveryView = Utils.findRequiredViewAsType(source, R.id.view_checkout_needDelivery, "field 'deliveryView'", ConstraintLayout.class);
    view = Utils.findRequiredView(source, R.id.button_checkout_changeBillingAddress, "field 'btnChangeBillingAddress' and method 'onButtonClicked'");
    target.btnChangeBillingAddress = Utils.castView(view, R.id.button_checkout_changeBillingAddress, "field 'btnChangeBillingAddress'", MaterialButton.class);
    view7f0a007f = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onButtonClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.button_checkout_deliveryNo, "field 'btnDeliveryNo' and method 'onButtonClicked'");
    target.btnDeliveryNo = Utils.castView(view, R.id.button_checkout_deliveryNo, "field 'btnDeliveryNo'", Button.class);
    view7f0a0080 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onButtonClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.button_checkout_placeOrder, "field 'btnProceed' and method 'onButtonClicked'");
    target.btnProceed = Utils.castView(view, R.id.button_checkout_placeOrder, "field 'btnProceed'", Button.class);
    view7f0a0082 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onButtonClicked(p0);
      }
    });
    target.tvBillingAddressName = Utils.findRequiredViewAsType(source, R.id.textView_checkout_billingAddressName, "field 'tvBillingAddressName'", TextView.class);
    target.tvBillingAddressDescription = Utils.findRequiredViewAsType(source, R.id.textView_checkout_billingAddressDesc, "field 'tvBillingAddressDescription'", TextView.class);
    target.tvBillingAddressStatus = Utils.findRequiredViewAsType(source, R.id.textView_checkout_billingAddressStatus, "field 'tvBillingAddressStatus'", TextView.class);
    target.tvDeliveryHint = Utils.findRequiredViewAsType(source, R.id.textView_checkout_deliveryHint, "field 'tvDeliveryHint'", TextView.class);
    target.tvSubTotal = Utils.findRequiredViewAsType(source, R.id.textView_checkout_subTotal, "field 'tvSubTotal'", TextView.class);
    target.tvVat = Utils.findRequiredViewAsType(source, R.id.textView_checkout_vat, "field 'tvVat'", TextView.class);
    target.tvDiscount = Utils.findRequiredViewAsType(source, R.id.textView_checkout_discount, "field 'tvDiscount'", TextView.class);
    target.tvVerificationAmount = Utils.findRequiredViewAsType(source, R.id.textView_checkout_verificationAmount, "field 'tvVerificationAmount'", TextView.class);
    target.tvWarrantyAmount = Utils.findRequiredViewAsType(source, R.id.textView_checkout_warrantyAmount, "field 'tvWarrantyAmount'", TextView.class);
    target.tvTotalAmount = Utils.findRequiredViewAsType(source, R.id.textView_checkout_totalAmount, "field 'tvTotalAmount'", TextView.class);
    target.cbProductVerification = Utils.findRequiredViewAsType(source, R.id.checkBox_checkout_verification, "field 'cbProductVerification'", CheckBox.class);
  }

  @Override
  public void unbind() {
    CheckoutActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rvProduct = null;
    target.rvPayment = null;
    target.btnDeliveryYes = null;
    target.ivDeliveryYes = null;
    target.ivDeliveryNo = null;
    target.deliveryView = null;
    target.btnChangeBillingAddress = null;
    target.btnDeliveryNo = null;
    target.btnProceed = null;
    target.tvBillingAddressName = null;
    target.tvBillingAddressDescription = null;
    target.tvBillingAddressStatus = null;
    target.tvDeliveryHint = null;
    target.tvSubTotal = null;
    target.tvVat = null;
    target.tvDiscount = null;
    target.tvVerificationAmount = null;
    target.tvWarrantyAmount = null;
    target.tvTotalAmount = null;
    target.cbProductVerification = null;

    view7f0a0081.setOnClickListener(null);
    view7f0a0081 = null;
    view7f0a007f.setOnClickListener(null);
    view7f0a007f = null;
    view7f0a0080.setOnClickListener(null);
    view7f0a0080 = null;
    view7f0a0082.setOnClickListener(null);
    view7f0a0082 = null;

    super.unbind();
  }
}
