// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.authentication;

import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AccountVerificationActivity_ViewBinding extends BaseLoginActivity_ViewBinding {
  private AccountVerificationActivity target;

  private View view7f0a0075;

  private View view7f0a0076;

  @UiThread
  public AccountVerificationActivity_ViewBinding(AccountVerificationActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AccountVerificationActivity_ViewBinding(final AccountVerificationActivity target,
      View source) {
    super(target, source);

    this.target = target;

    View view;
    target.tvCompanyName = Utils.findRequiredViewAsType(source, R.id.editText_accountVerification_companyName, "field 'tvCompanyName'", TextView.class);
    target.etCompanyRegistration = Utils.findRequiredViewAsType(source, R.id.editText_accountVerification_companyRegistration, "field 'etCompanyRegistration'", EditText.class);
    target.ivNoData = Utils.findRequiredViewAsType(source, R.id.imageView_accountVerification_noData, "field 'ivNoData'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.button_accountVerification_chooseFile, "field 'btnChooseFile' and method 'onClick'");
    target.btnChooseFile = Utils.castView(view, R.id.button_accountVerification_chooseFile, "field 'btnChooseFile'", Button.class);
    view7f0a0075 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.cbVerification = Utils.findRequiredViewAsType(source, R.id.checkBox_accountVerification, "field 'cbVerification'", CheckBox.class);
    target.tvTermsCondition = Utils.findRequiredViewAsType(source, R.id.conditions_text, "field 'tvTermsCondition'", TextView.class);
    view = Utils.findRequiredView(source, R.id.button_accountVerification_save, "field 'btnSave' and method 'onClick'");
    target.btnSave = Utils.castView(view, R.id.button_accountVerification_save, "field 'btnSave'", Button.class);
    view7f0a0076 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView_accountVerification, "field 'recyclerView'", RecyclerView.class);
  }

  @Override
  public void unbind() {
    AccountVerificationActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvCompanyName = null;
    target.etCompanyRegistration = null;
    target.ivNoData = null;
    target.btnChooseFile = null;
    target.cbVerification = null;
    target.tvTermsCondition = null;
    target.btnSave = null;
    target.recyclerView = null;

    view7f0a0075.setOnClickListener(null);
    view7f0a0075 = null;
    view7f0a0076.setOnClickListener(null);
    view7f0a0076 = null;

    super.unbind();
  }
}
