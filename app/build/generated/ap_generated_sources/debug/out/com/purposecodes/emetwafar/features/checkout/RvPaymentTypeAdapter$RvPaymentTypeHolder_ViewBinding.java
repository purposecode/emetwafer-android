// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.checkout;

import android.view.View;
import android.widget.Button;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RvPaymentTypeAdapter$RvPaymentTypeHolder_ViewBinding implements Unbinder {
  private RvPaymentTypeAdapter.RvPaymentTypeHolder target;

  @UiThread
  public RvPaymentTypeAdapter$RvPaymentTypeHolder_ViewBinding(
      RvPaymentTypeAdapter.RvPaymentTypeHolder target, View source) {
    this.target = target;

    target.btnPayment = Utils.findRequiredViewAsType(source, R.id.button_itemPayment, "field 'btnPayment'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RvPaymentTypeAdapter.RvPaymentTypeHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.btnPayment = null;
  }
}
