// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.address;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.AppCompatSpinner;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EditBillingAddressActivity_ViewBinding extends BaseActivity_ViewBinding {
  private EditBillingAddressActivity target;

  private View view7f0a0078;

  @UiThread
  public EditBillingAddressActivity_ViewBinding(EditBillingAddressActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public EditBillingAddressActivity_ViewBinding(final EditBillingAddressActivity target,
      View source) {
    super(target, source);

    this.target = target;

    View view;
    target.etName = Utils.findRequiredViewAsType(source, R.id.editText_billingAddress_name, "field 'etName'", EditText.class);
    target.etAddress1 = Utils.findRequiredViewAsType(source, R.id.editText_billingAddress_addressLine_1, "field 'etAddress1'", EditText.class);
    target.etAddress2 = Utils.findRequiredViewAsType(source, R.id.editText_billingAddress_addressLine_2, "field 'etAddress2'", EditText.class);
    target.etPoBox = Utils.findRequiredViewAsType(source, R.id.editText_billingAddress_poBox, "field 'etPoBox'", EditText.class);
    target.etEmail = Utils.findRequiredViewAsType(source, R.id.editText_billingAddress_email, "field 'etEmail'", EditText.class);
    target.etNewCity = Utils.findRequiredViewAsType(source, R.id.editText_billingAddress_otherCity, "field 'etNewCity'", EditText.class);
    target.etMobile = Utils.findRequiredViewAsType(source, R.id.editText_billingAddress_mobileNumber, "field 'etMobile'", EditText.class);
    view = Utils.findRequiredView(source, R.id.button_billingAddress_update, "field 'btnSave' and method 'onClickSave'");
    target.btnSave = Utils.castView(view, R.id.button_billingAddress_update, "field 'btnSave'", Button.class);
    view7f0a0078 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickSave();
      }
    });
    target.spinnerState = Utils.findRequiredViewAsType(source, R.id.spinner_billingAddress_selectState, "field 'spinnerState'", AppCompatSpinner.class);
    target.spinnerCity = Utils.findRequiredViewAsType(source, R.id.spinner_billingAddress_selectCity, "field 'spinnerCity'", AppCompatSpinner.class);
  }

  @Override
  public void unbind() {
    EditBillingAddressActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.etName = null;
    target.etAddress1 = null;
    target.etAddress2 = null;
    target.etPoBox = null;
    target.etEmail = null;
    target.etNewCity = null;
    target.etMobile = null;
    target.btnSave = null;
    target.spinnerState = null;
    target.spinnerCity = null;

    view7f0a0078.setOnClickListener(null);
    view7f0a0078 = null;

    super.unbind();
  }
}
