// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.authentication;

import android.view.View;
import android.widget.EditText;
import android.widget.ToggleButton;
import androidx.annotation.UiThread;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LoginActivity_ViewBinding extends BaseLoginActivity_ViewBinding {
  private LoginActivity target;

  private View view7f0a0096;

  private View view7f0a0308;

  private View view7f0a0095;

  @UiThread
  public LoginActivity_ViewBinding(LoginActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public LoginActivity_ViewBinding(final LoginActivity target, View source) {
    super(target, source);

    this.target = target;

    View view;
    target.etEmail = Utils.findRequiredViewAsType(source, R.id.editText_login_userName, "field 'etEmail'", EditText.class);
    target.etPassword = Utils.findRequiredViewAsType(source, R.id.editText_login_password, "field 'etPassword'", EditText.class);
    target.tbShowPassword = Utils.findRequiredViewAsType(source, R.id.icon_password, "field 'tbShowPassword'", ToggleButton.class);
    view = Utils.findRequiredView(source, R.id.button_login_create, "method 'onClick'");
    view7f0a0096 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.textView_click_login_forgot, "method 'onClick'");
    view7f0a0308 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.button_login, "method 'onClick'");
    view7f0a0095 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  public void unbind() {
    LoginActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.etEmail = null;
    target.etPassword = null;
    target.tbShowPassword = null;

    view7f0a0096.setOnClickListener(null);
    view7f0a0096 = null;
    view7f0a0308.setOnClickListener(null);
    view7f0a0308 = null;
    view7f0a0095.setOnClickListener(null);
    view7f0a0095 = null;

    super.unbind();
  }
}
