// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.filter;

import android.view.View;
import android.widget.CheckBox;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RvFilterOfferAdapter$RvFilterOfferHolder_ViewBinding implements Unbinder {
  private RvFilterOfferAdapter.RvFilterOfferHolder target;

  @UiThread
  public RvFilterOfferAdapter$RvFilterOfferHolder_ViewBinding(
      RvFilterOfferAdapter.RvFilterOfferHolder target, View source) {
    this.target = target;

    target.tvOffer = Utils.findRequiredViewAsType(source, R.id.checkedTextView_item, "field 'tvOffer'", CheckBox.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RvFilterOfferAdapter.RvFilterOfferHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvOffer = null;
  }
}
