// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.order;

import android.view.View;
import android.widget.CheckBox;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RvCancelReasonAdapter$RvCancelReasonHolder_ViewBinding implements Unbinder {
  private RvCancelReasonAdapter.RvCancelReasonHolder target;

  @UiThread
  public RvCancelReasonAdapter$RvCancelReasonHolder_ViewBinding(
      RvCancelReasonAdapter.RvCancelReasonHolder target, View source) {
    this.target = target;

    target.cbReason = Utils.findRequiredViewAsType(source, R.id.checkedTextView_item, "field 'cbReason'", CheckBox.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RvCancelReasonAdapter.RvCancelReasonHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.cbReason = null;
  }
}
