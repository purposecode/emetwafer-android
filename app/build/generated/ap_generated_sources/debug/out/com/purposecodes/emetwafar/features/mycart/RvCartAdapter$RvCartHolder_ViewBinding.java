// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.mycart;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RvCartAdapter$RvCartHolder_ViewBinding implements Unbinder {
  private RvCartAdapter.RvCartHolder target;

  @UiThread
  public RvCartAdapter$RvCartHolder_ViewBinding(RvCartAdapter.RvCartHolder target, View source) {
    this.target = target;

    target.ivProduct = Utils.findRequiredViewAsType(source, R.id.imageView_itemCart_imageView, "field 'ivProduct'", ImageView.class);
    target.tvName = Utils.findRequiredViewAsType(source, R.id.textView_itemCart_name, "field 'tvName'", TextView.class);
    target.tvDesc = Utils.findRequiredViewAsType(source, R.id.textView_itemCart_category, "field 'tvDesc'", TextView.class);
    target.tvOffer = Utils.findRequiredViewAsType(source, R.id.textView_itemCart_offer, "field 'tvOffer'", TextView.class);
    target.tvAmount = Utils.findRequiredViewAsType(source, R.id.textView_itemCart_amount, "field 'tvAmount'", TextView.class);
    target.tvWithoutOffer = Utils.findRequiredViewAsType(source, R.id.textView_itemCart_woPrice, "field 'tvWithoutOffer'", TextView.class);
    target.tvWarrantyName = Utils.findRequiredViewAsType(source, R.id.textView_itemCart_warrantyName, "field 'tvWarrantyName'", TextView.class);
    target.tvWarrantyPlan = Utils.findRequiredViewAsType(source, R.id.textView_itemCart_warrantyPlan, "field 'tvWarrantyPlan'", TextView.class);
    target.tvNotAvailable = Utils.findRequiredViewAsType(source, R.id.textView_itemCart_notAvailable, "field 'tvNotAvailable'", TextView.class);
    target.viewWarranty = Utils.findRequiredViewAsType(source, R.id.view_itemCart_warranty, "field 'viewWarranty'", ViewGroup.class);
    target.btnQuantity = Utils.findRequiredViewAsType(source, R.id.button_itemCart_quantity, "field 'btnQuantity'", Button.class);
    target.btnRemove = Utils.findRequiredViewAsType(source, R.id.button_itemCart_remove, "field 'btnRemove'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RvCartAdapter.RvCartHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivProduct = null;
    target.tvName = null;
    target.tvDesc = null;
    target.tvOffer = null;
    target.tvAmount = null;
    target.tvWithoutOffer = null;
    target.tvWarrantyName = null;
    target.tvWarrantyPlan = null;
    target.tvNotAvailable = null;
    target.viewWarranty = null;
    target.btnQuantity = null;
    target.btnRemove = null;
  }
}
