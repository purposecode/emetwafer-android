// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.home;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RvHomeCategoryTitleAdapter$RvTitleHolder_ViewBinding implements Unbinder {
  private RvHomeCategoryTitleAdapter.RvTitleHolder target;

  @UiThread
  public RvHomeCategoryTitleAdapter$RvTitleHolder_ViewBinding(
      RvHomeCategoryTitleAdapter.RvTitleHolder target, View source) {
    this.target = target;

    target.title = Utils.findRequiredViewAsType(source, R.id.textView_itemHomeCate, "field 'title'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RvHomeCategoryTitleAdapter.RvTitleHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.title = null;
  }
}
