// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.myaccount;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.UiThread;
import androidx.cardview.widget.CardView;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.fragment.BaseFragment_ViewBinding;
import com.sk.icode.errorview.ErrorView;
import de.hdodenhof.circleimageview.CircleImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MyAccountFragment_ViewBinding extends BaseFragment_ViewBinding {
  private MyAccountFragment target;

  private View view7f0a0098;

  private View view7f0a009b;

  private View view7f0a009a;

  private View view7f0a0097;

  private View view7f0a0099;

  @UiThread
  public MyAccountFragment_ViewBinding(final MyAccountFragment target, View source) {
    super(target, source);

    this.target = target;

    View view;
    target.ivProfile = Utils.findRequiredViewAsType(source, R.id.imageView_myAccount_profile, "field 'ivProfile'", CircleImageView.class);
    target.ivVerify = Utils.findRequiredViewAsType(source, R.id.imageView_myAccount_verify, "field 'ivVerify'", ImageView.class);
    target.tvName = Utils.findRequiredViewAsType(source, R.id.textView_myAccount_name, "field 'tvName'", TextView.class);
    target.tvLocation = Utils.findRequiredViewAsType(source, R.id.textView_myAccount_location, "field 'tvLocation'", TextView.class);
    target.tvOrderCount = Utils.findRequiredViewAsType(source, R.id.title_myAccount_companyOrderCount, "field 'tvOrderCount'", TextView.class);
    target.tvReviewCount = Utils.findRequiredViewAsType(source, R.id.title_myAccount_companyReviewCount, "field 'tvReviewCount'", TextView.class);
    target.tvSavedAddress = Utils.findRequiredViewAsType(source, R.id.textView_myAccount_savedAddress, "field 'tvSavedAddress'", TextView.class);
    target.tvPlanName = Utils.findRequiredViewAsType(source, R.id.textView_myAccount_planName, "field 'tvPlanName'", TextView.class);
    target.tvPlanAmount = Utils.findRequiredViewAsType(source, R.id.textView_myAccount_planAmount, "field 'tvPlanAmount'", TextView.class);
    target.tvPlanDay = Utils.findRequiredViewAsType(source, R.id.textView_myAccount_planDays, "field 'tvPlanDay'", TextView.class);
    target.tvPlanExpiredDate = Utils.findRequiredViewAsType(source, R.id.textView_myAccount_planExpiredDate, "field 'tvPlanExpiredDate'", TextView.class);
    target.tvPendingDays = Utils.findRequiredViewAsType(source, R.id.textView_myAccount_pendingDays, "field 'tvPendingDays'", TextView.class);
    target.planView = Utils.findRequiredViewAsType(source, R.id.cardView_myAccount_planView, "field 'planView'", CardView.class);
    target.dataView = Utils.findRequiredViewAsType(source, R.id.view_myAccount, "field 'dataView'", ViewGroup.class);
    target.errorView = Utils.findRequiredViewAsType(source, R.id.errorView, "field 'errorView'", ErrorView.class);
    view = Utils.findRequiredView(source, R.id.button_myAccount_companyOrder, "method 'onClickButton'");
    view7f0a0098 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickButton(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.button_myAccount_viewMore, "method 'onClickButton'");
    view7f0a009b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickButton(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.button_myAccount_planUpdate, "method 'onClickButton'");
    view7f0a009a = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickButton(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.button_myAccount_changePassword, "method 'onClickButton'");
    view7f0a0097 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickButton(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.button_myAccount_logout, "method 'onClickButton'");
    view7f0a0099 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickButton(p0);
      }
    });
  }

  @Override
  public void unbind() {
    MyAccountFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivProfile = null;
    target.ivVerify = null;
    target.tvName = null;
    target.tvLocation = null;
    target.tvOrderCount = null;
    target.tvReviewCount = null;
    target.tvSavedAddress = null;
    target.tvPlanName = null;
    target.tvPlanAmount = null;
    target.tvPlanDay = null;
    target.tvPlanExpiredDate = null;
    target.tvPendingDays = null;
    target.planView = null;
    target.dataView = null;
    target.errorView = null;

    view7f0a0098.setOnClickListener(null);
    view7f0a0098 = null;
    view7f0a009b.setOnClickListener(null);
    view7f0a009b = null;
    view7f0a009a.setOnClickListener(null);
    view7f0a009a = null;
    view7f0a0097.setOnClickListener(null);
    view7f0a0097 = null;
    view7f0a0099.setOnClickListener(null);
    view7f0a0099 = null;

    super.unbind();
  }
}
