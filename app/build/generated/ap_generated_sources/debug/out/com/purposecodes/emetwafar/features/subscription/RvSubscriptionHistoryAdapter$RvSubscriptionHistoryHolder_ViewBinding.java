// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.subscription;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.cardview.widget.CardView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RvSubscriptionHistoryAdapter$RvSubscriptionHistoryHolder_ViewBinding implements Unbinder {
  private RvSubscriptionHistoryAdapter.RvSubscriptionHistoryHolder target;

  @UiThread
  public RvSubscriptionHistoryAdapter$RvSubscriptionHistoryHolder_ViewBinding(
      RvSubscriptionHistoryAdapter.RvSubscriptionHistoryHolder target, View source) {
    this.target = target;

    target.tvTitle = Utils.findRequiredViewAsType(source, R.id.textView_itemSubscription_title, "field 'tvTitle'", TextView.class);
    target.tvStatus = Utils.findRequiredViewAsType(source, R.id.textView_itemSubscription_status, "field 'tvStatus'", TextView.class);
    target.tvPlanDays = Utils.findRequiredViewAsType(source, R.id.textView_itemSubscription_planPeriod, "field 'tvPlanDays'", TextView.class);
    target.tvStartDate = Utils.findRequiredViewAsType(source, R.id.textView_itemSubscription_startDate, "field 'tvStartDate'", TextView.class);
    target.tvBalanceDays = Utils.findRequiredViewAsType(source, R.id.textView_itemSubscription_balanceDays, "field 'tvBalanceDays'", TextView.class);
    target.tvExpireDate = Utils.findRequiredViewAsType(source, R.id.textView_itemSubscription_expireDate, "field 'tvExpireDate'", TextView.class);
    target.tvTitleExpireDate = Utils.findRequiredViewAsType(source, R.id.title_itemSubscription_expireDate, "field 'tvTitleExpireDate'", TextView.class);
    target.tvPlanCharge = Utils.findRequiredViewAsType(source, R.id.textView_itemSubscription_planCharge, "field 'tvPlanCharge'", TextView.class);
    target.tvVat = Utils.findRequiredViewAsType(source, R.id.textView_itemSubscription_vat, "field 'tvVat'", TextView.class);
    target.tvGrandTotal = Utils.findRequiredViewAsType(source, R.id.textView_itemSubscription_grandTotal, "field 'tvGrandTotal'", TextView.class);
    target.viewBalanceDays = Utils.findRequiredView(source, R.id.view_itemSubscription_balanceDays, "field 'viewBalanceDays'");
    target.viewDivider = Utils.findRequiredView(source, R.id.view_itemSubscriptionHistory_divider, "field 'viewDivider'");
    target.cvBackground = Utils.findRequiredViewAsType(source, R.id.cardView_itemSubscriptionHistory, "field 'cvBackground'", CardView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RvSubscriptionHistoryAdapter.RvSubscriptionHistoryHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvTitle = null;
    target.tvStatus = null;
    target.tvPlanDays = null;
    target.tvStartDate = null;
    target.tvBalanceDays = null;
    target.tvExpireDate = null;
    target.tvTitleExpireDate = null;
    target.tvPlanCharge = null;
    target.tvVat = null;
    target.tvGrandTotal = null;
    target.viewBalanceDays = null;
    target.viewDivider = null;
    target.cvBackground = null;
  }
}
