// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.checkout;

import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RvBottomAddressAdapter$RvBottomAddressHolder_ViewBinding implements Unbinder {
  private RvBottomAddressAdapter.RvBottomAddressHolder target;

  @UiThread
  public RvBottomAddressAdapter$RvBottomAddressHolder_ViewBinding(
      RvBottomAddressAdapter.RvBottomAddressHolder target, View source) {
    this.target = target;

    target.rbSelection = Utils.findRequiredViewAsType(source, R.id.radiobutton_itemCheckoutSavedAddress, "field 'rbSelection'", RadioButton.class);
    target.tvName = Utils.findRequiredViewAsType(source, R.id.textView_itemCheckoutSavedAddress_name, "field 'tvName'", TextView.class);
    target.tvAddress = Utils.findRequiredViewAsType(source, R.id.textView_itemCheckoutSavedAddress_address, "field 'tvAddress'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RvBottomAddressAdapter.RvBottomAddressHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rbSelection = null;
    target.tvName = null;
    target.tvAddress = null;
  }
}
