// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.filter;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FilterActivity_ViewBinding extends BaseActivity_ViewBinding {
  private FilterActivity target;

  private View view7f0a0089;

  @UiThread
  public FilterActivity_ViewBinding(FilterActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public FilterActivity_ViewBinding(final FilterActivity target, View source) {
    super(target, source);

    this.target = target;

    View view;
    target.rangeSeekbar = Utils.findOptionalViewAsType(source, R.id.rangeSeekbar_filter_priceRange, "field 'rangeSeekbar'", CrystalRangeSeekbar.class);
    target.tvMinPrice = Utils.findRequiredViewAsType(source, R.id.textView_filter_minPrice, "field 'tvMinPrice'", TextView.class);
    target.tvMaxPrice = Utils.findRequiredViewAsType(source, R.id.textView_filter_maxPrice, "field 'tvMaxPrice'", TextView.class);
    target.rvOffers = Utils.findRequiredViewAsType(source, R.id.recyclerView_filter_offer, "field 'rvOffers'", RecyclerView.class);
    target.rvDiscounts = Utils.findRequiredViewAsType(source, R.id.recyclerView_filter_discounts, "field 'rvDiscounts'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.button_filter_apply, "field 'btnApply' and method 'onApply'");
    target.btnApply = Utils.castView(view, R.id.button_filter_apply, "field 'btnApply'", Button.class);
    view7f0a0089 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onApply(p0);
      }
    });
  }

  @Override
  public void unbind() {
    FilterActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rangeSeekbar = null;
    target.tvMinPrice = null;
    target.tvMaxPrice = null;
    target.rvOffers = null;
    target.rvDiscounts = null;
    target.btnApply = null;

    view7f0a0089.setOnClickListener(null);
    view7f0a0089 = null;

    super.unbind();
  }
}
