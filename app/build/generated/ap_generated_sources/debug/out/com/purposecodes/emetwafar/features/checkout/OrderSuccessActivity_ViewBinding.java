// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.checkout;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.google.android.material.button.MaterialButton;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OrderSuccessActivity_ViewBinding extends BaseActivity_ViewBinding {
  private OrderSuccessActivity target;

  private View view7f0a00ad;

  @UiThread
  public OrderSuccessActivity_ViewBinding(OrderSuccessActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public OrderSuccessActivity_ViewBinding(final OrderSuccessActivity target, View source) {
    super(target, source);

    this.target = target;

    View view;
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView_orderSuccess, "field 'recyclerView'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.button_orderSuccess_goto, "field 'btnGoto' and method 'onBackPressed'");
    target.btnGoto = Utils.castView(view, R.id.button_orderSuccess_goto, "field 'btnGoto'", MaterialButton.class);
    view7f0a00ad = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onBackPressed();
      }
    });
    target.tvTitle = Utils.findRequiredViewAsType(source, R.id.textView_orderSuccess_title, "field 'tvTitle'", TextView.class);
  }

  @Override
  public void unbind() {
    OrderSuccessActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.recyclerView = null;
    target.btnGoto = null;
    target.tvTitle = null;

    view7f0a00ad.setOnClickListener(null);
    view7f0a00ad = null;

    super.unbind();
  }
}
