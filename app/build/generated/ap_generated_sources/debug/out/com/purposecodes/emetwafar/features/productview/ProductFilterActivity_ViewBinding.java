// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.productview;

import android.view.View;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity_ViewBinding;
import com.sk.icode.errorview.ErrorView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProductFilterActivity_ViewBinding extends BaseActivity_ViewBinding {
  private ProductFilterActivity target;

  private View view7f0a00b5;

  private View view7f0a00b4;

  @UiThread
  public ProductFilterActivity_ViewBinding(ProductFilterActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ProductFilterActivity_ViewBinding(final ProductFilterActivity target, View source) {
    super(target, source);

    this.target = target;

    View view;
    target.errorView = Utils.findRequiredViewAsType(source, R.id.errorView, "field 'errorView'", ErrorView.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView_layout_list, "field 'recyclerView'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.button_productFilter_sort, "method 'onClick'");
    view7f0a00b5 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.button_productFilter_filter, "method 'onClick'");
    view7f0a00b4 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  public void unbind() {
    ProductFilterActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.errorView = null;
    target.recyclerView = null;

    view7f0a00b5.setOnClickListener(null);
    view7f0a00b5 = null;
    view7f0a00b4.setOnClickListener(null);
    view7f0a00b4 = null;

    super.unbind();
  }
}
