// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.order;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RvTimeLineAdapter$RvTimeLineHolder_ViewBinding implements Unbinder {
  private RvTimeLineAdapter.RvTimeLineHolder target;

  @UiThread
  public RvTimeLineAdapter$RvTimeLineHolder_ViewBinding(RvTimeLineAdapter.RvTimeLineHolder target,
      View source) {
    this.target = target;

    target.tvName = Utils.findRequiredViewAsType(source, R.id.textView_itemTimeLine_message, "field 'tvName'", TextView.class);
    target.ivMarker = Utils.findRequiredViewAsType(source, R.id.imageView_itemTimeLine_marker, "field 'ivMarker'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RvTimeLineAdapter.RvTimeLineHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvName = null;
    target.ivMarker = null;
  }
}
