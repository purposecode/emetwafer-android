// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.checkout;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.google.android.material.button.MaterialButton;
import com.purposecodes.emetwafar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RvCheckoutProductAdapter$RvCheckoutProductHolder_ViewBinding implements Unbinder {
  private RvCheckoutProductAdapter.RvCheckoutProductHolder target;

  @UiThread
  public RvCheckoutProductAdapter$RvCheckoutProductHolder_ViewBinding(
      RvCheckoutProductAdapter.RvCheckoutProductHolder target, View source) {
    this.target = target;

    target.ivProduct = Utils.findRequiredViewAsType(source, R.id.imageView_itemCheckoutCart_imageView, "field 'ivProduct'", ImageView.class);
    target.tvName = Utils.findRequiredViewAsType(source, R.id.textView_itemCheckoutCart_name, "field 'tvName'", TextView.class);
    target.tvDesc = Utils.findRequiredViewAsType(source, R.id.textView_itemCheckoutCart_desc, "field 'tvDesc'", TextView.class);
    target.tvAmount = Utils.findRequiredViewAsType(source, R.id.textView_itemCheckoutCart_amount, "field 'tvAmount'", TextView.class);
    target.tvQuantity = Utils.findRequiredViewAsType(source, R.id.textView_itemCheckoutCart_quantity, "field 'tvQuantity'", TextView.class);
    target.tvWarrantyName = Utils.findRequiredViewAsType(source, R.id.textView_itemCheckoutCart_warrantyName, "field 'tvWarrantyName'", TextView.class);
    target.tvWarrantyPlan = Utils.findRequiredViewAsType(source, R.id.textView_itemCheckoutCart_warrantyPlan, "field 'tvWarrantyPlan'", TextView.class);
    target.viewWarranty = Utils.findRequiredViewAsType(source, R.id.view_itemCheckoutCart_warranty, "field 'viewWarranty'", ViewGroup.class);
    target.tvDeliveryAddress = Utils.findRequiredViewAsType(source, R.id.textView_itemCheckoutCart_deliveryAddress, "field 'tvDeliveryAddress'", TextView.class);
    target.btnChangeAddress = Utils.findRequiredViewAsType(source, R.id.button_itemCheckoutCart_changeAddress, "field 'btnChangeAddress'", MaterialButton.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RvCheckoutProductAdapter.RvCheckoutProductHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivProduct = null;
    target.tvName = null;
    target.tvDesc = null;
    target.tvAmount = null;
    target.tvQuantity = null;
    target.tvWarrantyName = null;
    target.tvWarrantyPlan = null;
    target.viewWarranty = null;
    target.tvDeliveryAddress = null;
    target.btnChangeAddress = null;
  }
}
