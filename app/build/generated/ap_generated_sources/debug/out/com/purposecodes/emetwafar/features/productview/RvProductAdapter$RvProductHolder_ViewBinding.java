// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.productview;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RvProductAdapter$RvProductHolder_ViewBinding implements Unbinder {
  private RvProductAdapter.RvProductHolder target;

  @UiThread
  public RvProductAdapter$RvProductHolder_ViewBinding(RvProductAdapter.RvProductHolder target,
      View source) {
    this.target = target;

    target.ivProduct = Utils.findRequiredViewAsType(source, R.id.imageView_itemProduct_imageView, "field 'ivProduct'", ImageView.class);
    target.tvCategory = Utils.findRequiredViewAsType(source, R.id.textView_itemProduct_category, "field 'tvCategory'", TextView.class);
    target.tvName = Utils.findRequiredViewAsType(source, R.id.textView_itemProduct_name, "field 'tvName'", TextView.class);
    target.tvCode = Utils.findRequiredViewAsType(source, R.id.textView_itemProduct_code, "field 'tvCode'", TextView.class);
    target.tvSellerId = Utils.findRequiredViewAsType(source, R.id.textView_itemProduct_sellerId, "field 'tvSellerId'", TextView.class);
    target.tvAmount = Utils.findRequiredViewAsType(source, R.id.textView_itemProduct_amount, "field 'tvAmount'", TextView.class);
    target.tvOffer = Utils.findRequiredViewAsType(source, R.id.textView_itemProduct_offer, "field 'tvOffer'", TextView.class);
    target.tvLocation = Utils.findRequiredViewAsType(source, R.id.textView_itemProduct_location, "field 'tvLocation'", TextView.class);
    target.ibWish = Utils.findRequiredViewAsType(source, R.id.imageButton_itemProduct_wish, "field 'ibWish'", ImageButton.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RvProductAdapter.RvProductHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivProduct = null;
    target.tvCategory = null;
    target.tvName = null;
    target.tvCode = null;
    target.tvSellerId = null;
    target.tvAmount = null;
    target.tvOffer = null;
    target.tvLocation = null;
    target.ibWish = null;
  }
}
