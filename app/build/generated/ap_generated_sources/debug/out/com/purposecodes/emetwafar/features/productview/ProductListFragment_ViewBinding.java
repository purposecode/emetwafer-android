// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.productview;

import android.view.View;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.fragment.BaseFragment_ViewBinding;
import com.sk.icode.errorview.ErrorView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProductListFragment_ViewBinding extends BaseFragment_ViewBinding {
  private ProductListFragment target;

  @UiThread
  public ProductListFragment_ViewBinding(ProductListFragment target, View source) {
    super(target, source);

    this.target = target;

    target.errorView = Utils.findRequiredViewAsType(source, R.id.errorView, "field 'errorView'", ErrorView.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView_layout_list, "field 'recyclerView'", RecyclerView.class);
    target.swipeRefreshLayout = Utils.findRequiredViewAsType(source, R.id.swipeRefreshLayout_layout_list, "field 'swipeRefreshLayout'", SwipeRefreshLayout.class);
  }

  @Override
  public void unbind() {
    ProductListFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.errorView = null;
    target.recyclerView = null;
    target.swipeRefreshLayout = null;

    super.unbind();
  }
}
