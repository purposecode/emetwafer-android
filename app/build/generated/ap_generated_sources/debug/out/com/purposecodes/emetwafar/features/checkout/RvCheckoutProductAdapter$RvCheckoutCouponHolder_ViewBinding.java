// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.checkout;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.cardview.widget.CardView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RvCheckoutProductAdapter$RvCheckoutCouponHolder_ViewBinding implements Unbinder {
  private RvCheckoutProductAdapter.RvCheckoutCouponHolder target;

  @UiThread
  public RvCheckoutProductAdapter$RvCheckoutCouponHolder_ViewBinding(
      RvCheckoutProductAdapter.RvCheckoutCouponHolder target, View source) {
    this.target = target;

    target.viewResult = Utils.findRequiredViewAsType(source, R.id.view_applyCoupon_result, "field 'viewResult'", ViewGroup.class);
    target.viewApply = Utils.findRequiredViewAsType(source, R.id.view_applyCoupon_apply, "field 'viewApply'", ViewGroup.class);
    target.cardBg = Utils.findRequiredViewAsType(source, R.id.cardView_checkout_coupon, "field 'cardBg'", CardView.class);
    target.ibClose = Utils.findRequiredViewAsType(source, R.id.imageButton_applyCoupon_close, "field 'ibClose'", ImageButton.class);
    target.ivResult = Utils.findRequiredViewAsType(source, R.id.imageView_applyCoupon_result, "field 'ivResult'", ImageView.class);
    target.tvResult = Utils.findRequiredViewAsType(source, R.id.textView_applyCoupon_result, "field 'tvResult'", TextView.class);
    target.etCode = Utils.findRequiredViewAsType(source, R.id.editText_itemCheckout_applyCode, "field 'etCode'", EditText.class);
    target.btnApply = Utils.findRequiredViewAsType(source, R.id.button_itemCheckout_apply, "field 'btnApply'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RvCheckoutProductAdapter.RvCheckoutCouponHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.viewResult = null;
    target.viewApply = null;
    target.cardBg = null;
    target.ibClose = null;
    target.ivResult = null;
    target.tvResult = null;
    target.etCode = null;
    target.btnApply = null;
  }
}
