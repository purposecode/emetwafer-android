// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.category;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.fragment.BaseFragment_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CategoryFragment_ViewBinding extends BaseFragment_ViewBinding {
  private CategoryFragment target;

  private View view7f0a02fd;

  private View view7f0a007c;

  @UiThread
  public CategoryFragment_ViewBinding(final CategoryFragment target, View source) {
    super(target, source);

    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.textView_category_homeError, "field 'errorText' and method 'getCategories'");
    target.errorText = Utils.castView(view, R.id.textView_category_homeError, "field 'errorText'", TextView.class);
    view7f0a02fd = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.getCategories();
      }
    });
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView_category_home, "field 'recyclerView'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.button_category_seeAll, "method 'onClickAllCategories'");
    view7f0a007c = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickAllCategories(p0);
      }
    });
  }

  @Override
  public void unbind() {
    CategoryFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.errorText = null;
    target.recyclerView = null;

    view7f0a02fd.setOnClickListener(null);
    view7f0a02fd = null;
    view7f0a007c.setOnClickListener(null);
    view7f0a007c = null;

    super.unbind();
  }
}
