// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.order;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RvOrderAdapter$RvOrderItemAdapter$RvOrderItemHolder_ViewBinding implements Unbinder {
  private RvOrderAdapter.RvOrderItemAdapter.RvOrderItemHolder target;

  @UiThread
  public RvOrderAdapter$RvOrderItemAdapter$RvOrderItemHolder_ViewBinding(
      RvOrderAdapter.RvOrderItemAdapter.RvOrderItemHolder target, View source) {
    this.target = target;

    target.tvName = Utils.findRequiredViewAsType(source, R.id.textView_itemMyOrderProduct_name, "field 'tvName'", TextView.class);
    target.tvQuantity = Utils.findRequiredViewAsType(source, R.id.textView_itemMyOrderProduct_quantity, "field 'tvQuantity'", TextView.class);
    target.tvPrice = Utils.findRequiredViewAsType(source, R.id.textView_itemMyOrderProduct_price, "field 'tvPrice'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RvOrderAdapter.RvOrderItemAdapter.RvOrderItemHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvName = null;
    target.tvQuantity = null;
    target.tvPrice = null;
  }
}
