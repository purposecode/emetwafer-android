// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.authentication;

import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class BaseLoginActivity_ViewBinding implements Unbinder {
  private BaseLoginActivity target;

  @UiThread
  public BaseLoginActivity_ViewBinding(BaseLoginActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public BaseLoginActivity_ViewBinding(BaseLoginActivity target, View source) {
    this.target = target;

    target.mProgressOverlay = Utils.findRequiredViewAsType(source, R.id.layout_prgressBg, "field 'mProgressOverlay'", ViewGroup.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    BaseLoginActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mProgressOverlay = null;
  }
}
