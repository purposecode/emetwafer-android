// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.order;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class NeedHelpActivity_ViewBinding extends BaseActivity_ViewBinding {
  private NeedHelpActivity target;

  @UiThread
  public NeedHelpActivity_ViewBinding(NeedHelpActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public NeedHelpActivity_ViewBinding(NeedHelpActivity target, View source) {
    super(target, source);

    this.target = target;

    target.etMessage = Utils.findRequiredViewAsType(source, R.id.editText_needHelp_message, "field 'etMessage'", EditText.class);
    target.btnSubmit = Utils.findRequiredViewAsType(source, R.id.button_needHelp_submit, "field 'btnSubmit'", Button.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView_needHelp, "field 'recyclerView'", RecyclerView.class);
  }

  @Override
  public void unbind() {
    NeedHelpActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.etMessage = null;
    target.btnSubmit = null;
    target.recyclerView = null;

    super.unbind();
  }
}
