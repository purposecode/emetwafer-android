// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.subscription;

import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity_ViewBinding;
import com.sk.icode.errorview.ErrorView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SubscriptionListActivity_ViewBinding extends BaseActivity_ViewBinding {
  private SubscriptionListActivity target;

  @UiThread
  public SubscriptionListActivity_ViewBinding(SubscriptionListActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SubscriptionListActivity_ViewBinding(SubscriptionListActivity target, View source) {
    super(target, source);

    this.target = target;

    target.errorView = Utils.findRequiredViewAsType(source, R.id.errorView, "field 'errorView'", ErrorView.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView_layout_list, "field 'recyclerView'", RecyclerView.class);
    target.swipeRefreshLayout = Utils.findRequiredViewAsType(source, R.id.swipeRefreshLayout_layout_list, "field 'swipeRefreshLayout'", SwipeRefreshLayout.class);
    target.parentView = Utils.findRequiredViewAsType(source, R.id.list_layout, "field 'parentView'", ViewGroup.class);
  }

  @Override
  public void unbind() {
    SubscriptionListActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.errorView = null;
    target.recyclerView = null;
    target.swipeRefreshLayout = null;
    target.parentView = null;

    super.unbind();
  }
}
