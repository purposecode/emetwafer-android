// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.address;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RvAddressAdapter$RvSavedAddressHolder_ViewBinding implements Unbinder {
  private RvAddressAdapter.RvSavedAddressHolder target;

  @UiThread
  public RvAddressAdapter$RvSavedAddressHolder_ViewBinding(
      RvAddressAdapter.RvSavedAddressHolder target, View source) {
    this.target = target;

    target.tvName = Utils.findRequiredViewAsType(source, R.id.textView_itemSavedAddress_name, "field 'tvName'", TextView.class);
    target.tvAddress = Utils.findRequiredViewAsType(source, R.id.textView_itemSavedAddress_address, "field 'tvAddress'", TextView.class);
    target.ibMore = Utils.findRequiredViewAsType(source, R.id.imageButton_itemSavedAddress_more, "field 'ibMore'", ImageButton.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RvAddressAdapter.RvSavedAddressHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvName = null;
    target.tvAddress = null;
    target.ibMore = null;
  }
}
