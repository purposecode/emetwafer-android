// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.payments;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.UiThread;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OrderPaymentSuccessActivity_ViewBinding extends BaseActivity_ViewBinding {
  private OrderPaymentSuccessActivity target;

  @UiThread
  public OrderPaymentSuccessActivity_ViewBinding(OrderPaymentSuccessActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public OrderPaymentSuccessActivity_ViewBinding(OrderPaymentSuccessActivity target, View source) {
    super(target, source);

    this.target = target;

    target.viewError = Utils.findRequiredView(source, R.id.view_paymentSuccess, "field 'viewError'");
    target.tvTitle = Utils.findRequiredViewAsType(source, R.id.textView_paymentSuccess_title, "field 'tvTitle'", TextView.class);
    target.tvDescription = Utils.findRequiredViewAsType(source, R.id.textView_paymentSuccess_description, "field 'tvDescription'", TextView.class);
    target.btnRetry = Utils.findRequiredViewAsType(source, R.id.button_paymentSuccess_retry, "field 'btnRetry'", Button.class);
  }

  @Override
  public void unbind() {
    OrderPaymentSuccessActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.viewError = null;
    target.tvTitle = null;
    target.tvDescription = null;
    target.btnRetry = null;

    super.unbind();
  }
}
