// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.fragment;

import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class BaseFragment_ViewBinding implements Unbinder {
  private BaseFragment target;

  @UiThread
  public BaseFragment_ViewBinding(BaseFragment target, View source) {
    this.target = target;

    target.progressLayer = Utils.findOptionalViewAsType(source, R.id.layout_prgressBg, "field 'progressLayer'", ViewGroup.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    BaseFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.progressLayer = null;
  }
}
