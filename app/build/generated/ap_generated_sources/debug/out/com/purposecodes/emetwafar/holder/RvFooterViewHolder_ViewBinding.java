// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.holder;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RvFooterViewHolder_ViewBinding implements Unbinder {
  private RvFooterViewHolder target;

  @UiThread
  public RvFooterViewHolder_ViewBinding(RvFooterViewHolder target, View source) {
    this.target = target;

    target.loadText = Utils.findRequiredViewAsType(source, R.id.loadText, "field 'loadText'", TextView.class);
    target.loadProgress = Utils.findRequiredViewAsType(source, R.id.loadProgress, "field 'loadProgress'", ProgressBar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RvFooterViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.loadText = null;
    target.loadProgress = null;
  }
}
