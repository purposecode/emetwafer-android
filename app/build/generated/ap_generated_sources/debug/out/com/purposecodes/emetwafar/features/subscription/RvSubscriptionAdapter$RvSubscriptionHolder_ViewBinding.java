// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.subscription;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.cardview.widget.CardView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RvSubscriptionAdapter$RvSubscriptionHolder_ViewBinding implements Unbinder {
  private RvSubscriptionAdapter.RvSubscriptionHolder target;

  @UiThread
  public RvSubscriptionAdapter$RvSubscriptionHolder_ViewBinding(
      RvSubscriptionAdapter.RvSubscriptionHolder target, View source) {
    this.target = target;

    target.cvBg = Utils.findRequiredViewAsType(source, R.id.cardView_subscription, "field 'cvBg'", CardView.class);
    target.tvTitle = Utils.findRequiredViewAsType(source, R.id.textView_subscription_title, "field 'tvTitle'", TextView.class);
    target.tvAmount = Utils.findRequiredViewAsType(source, R.id.textView_subscription_amount, "field 'tvAmount'", TextView.class);
    target.tvDays = Utils.findRequiredViewAsType(source, R.id.textView_subscription_days, "field 'tvDays'", TextView.class);
    target.tvDescription = Utils.findRequiredViewAsType(source, R.id.textView_subscription_desc, "field 'tvDescription'", TextView.class);
    target.btnTry = Utils.findRequiredViewAsType(source, R.id.button_subscription_try, "field 'btnTry'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    RvSubscriptionAdapter.RvSubscriptionHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.cvBg = null;
    target.tvTitle = null;
    target.tvAmount = null;
    target.tvDays = null;
    target.tvDescription = null;
    target.btnTry = null;
  }
}
