// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.address;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity_ViewBinding;
import com.sk.icode.errorview.ErrorView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SavedAddressActivity_ViewBinding extends BaseActivity_ViewBinding {
  private SavedAddressActivity target;

  private View view7f0a00b7;

  @UiThread
  public SavedAddressActivity_ViewBinding(SavedAddressActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SavedAddressActivity_ViewBinding(final SavedAddressActivity target, View source) {
    super(target, source);

    this.target = target;

    View view;
    target.errorView = Utils.findRequiredViewAsType(source, R.id.errorView, "field 'errorView'", ErrorView.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView_layout_list, "field 'recyclerView'", RecyclerView.class);
    target.viewData = Utils.findRequiredViewAsType(source, R.id.view_savedList, "field 'viewData'", ViewGroup.class);
    view = Utils.findRequiredView(source, R.id.button_savedAddress_addAddress, "field 'btnAddAddress' and method 'onStartCreateAddress'");
    target.btnAddAddress = Utils.castView(view, R.id.button_savedAddress_addAddress, "field 'btnAddAddress'", Button.class);
    view7f0a00b7 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onStartCreateAddress(p0);
      }
    });
    target.tvTitle = Utils.findRequiredViewAsType(source, R.id.textView_savedAddress_title, "field 'tvTitle'", TextView.class);
  }

  @Override
  public void unbind() {
    SavedAddressActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.errorView = null;
    target.recyclerView = null;
    target.viewData = null;
    target.btnAddAddress = null;
    target.tvTitle = null;

    view7f0a00b7.setOnClickListener(null);
    view7f0a00b7 = null;

    super.unbind();
  }
}
