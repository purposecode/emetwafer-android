// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.myaccount;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.AppCompatSpinner;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.google.android.material.button.MaterialButton;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity_ViewBinding;
import com.sk.icode.errorview.ErrorView;
import de.hdodenhof.circleimageview.CircleImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CompanyProfileActivity_ViewBinding extends BaseActivity_ViewBinding {
  private CompanyProfileActivity target;

  private View view7f0a01a2;

  private View view7f0a0083;

  private View view7f0a0085;

  @UiThread
  public CompanyProfileActivity_ViewBinding(CompanyProfileActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CompanyProfileActivity_ViewBinding(final CompanyProfileActivity target, View source) {
    super(target, source);

    this.target = target;

    View view;
    target.ivProfile = Utils.findRequiredViewAsType(source, R.id.imageView_companyProfile_profile, "field 'ivProfile'", CircleImageView.class);
    target.ivVerify = Utils.findRequiredViewAsType(source, R.id.imageView_companyProfile_verify, "field 'ivVerify'", ImageView.class);
    target.tvName = Utils.findRequiredViewAsType(source, R.id.textView_companyProfile_name, "field 'tvName'", TextView.class);
    target.tvEmail = Utils.findRequiredViewAsType(source, R.id.textView_companyProfile_email, "field 'tvEmail'", TextView.class);
    target.tvCompanyRegister = Utils.findRequiredViewAsType(source, R.id.textView_companyProfile_companyRegister, "field 'tvCompanyRegister'", TextView.class);
    target.etName = Utils.findRequiredViewAsType(source, R.id.editText_companyProfile_name, "field 'etName'", TextView.class);
    target.tvCompanyAddress = Utils.findRequiredViewAsType(source, R.id.textView_companyProfile_companyAddress, "field 'tvCompanyAddress'", TextView.class);
    target.etAddress1 = Utils.findRequiredViewAsType(source, R.id.editText_companyProfile_address1, "field 'etAddress1'", EditText.class);
    target.etAddress2 = Utils.findRequiredViewAsType(source, R.id.editText_companyProfile_address2, "field 'etAddress2'", EditText.class);
    target.etNewCity = Utils.findRequiredViewAsType(source, R.id.editText_companyProfile_newCity, "field 'etNewCity'", EditText.class);
    target.etPoBox = Utils.findRequiredViewAsType(source, R.id.editText_companyProfile_poBox, "field 'etPoBox'", EditText.class);
    target.etPhone = Utils.findRequiredViewAsType(source, R.id.editText_companyProfile_companyPhone, "field 'etPhone'", EditText.class);
    target.etMobile = Utils.findRequiredViewAsType(source, R.id.editText_companyProfile_mobile, "field 'etMobile'", EditText.class);
    target.etWeb = Utils.findRequiredViewAsType(source, R.id.editText_companyProfile_web, "field 'etWeb'", EditText.class);
    target.spinnerState = Utils.findRequiredViewAsType(source, R.id.spinner_companyProfile_selectState, "field 'spinnerState'", AppCompatSpinner.class);
    target.spinnerCity = Utils.findRequiredViewAsType(source, R.id.spinner_companyProfile_selectCity, "field 'spinnerCity'", AppCompatSpinner.class);
    view = Utils.findRequiredView(source, R.id.imageButton_companyProfile_changeDP, "field 'ibChangeDp' and method 'showBottomDialog'");
    target.ibChangeDp = Utils.castView(view, R.id.imageButton_companyProfile_changeDP, "field 'ibChangeDp'", ImageButton.class);
    view7f0a01a2 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.showBottomDialog();
      }
    });
    view = Utils.findRequiredView(source, R.id.button_companyProfile_editProfile, "field 'btnEditProfile' and method 'onButtonClick'");
    target.btnEditProfile = Utils.castView(view, R.id.button_companyProfile_editProfile, "field 'btnEditProfile'", MaterialButton.class);
    view7f0a0083 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onButtonClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.button_companyProfile_save, "field 'btnSave' and method 'onButtonClick'");
    target.btnSave = Utils.castView(view, R.id.button_companyProfile_save, "field 'btnSave'", Button.class);
    view7f0a0085 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onButtonClick(p0);
      }
    });
    target.viewAddNewCity = Utils.findRequiredViewAsType(source, R.id.view_companyProfile_otherCity, "field 'viewAddNewCity'", ViewGroup.class);
    target.viewableAddressView = Utils.findRequiredViewAsType(source, R.id.view_companyProfile_viewableAddress, "field 'viewableAddressView'", ViewGroup.class);
    target.editableAddressView = Utils.findRequiredViewAsType(source, R.id.view_companyProfile_editableAddress, "field 'editableAddressView'", ViewGroup.class);
    target.viewBottom_ = Utils.findRequiredViewAsType(source, R.id.view_bottom, "field 'viewBottom_'", ViewGroup.class);
    target.dataView = Utils.findRequiredViewAsType(source, R.id.view_companyProfile, "field 'dataView'", ViewGroup.class);
    target.errorView = Utils.findRequiredViewAsType(source, R.id.errorView, "field 'errorView'", ErrorView.class);
  }

  @Override
  public void unbind() {
    CompanyProfileActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivProfile = null;
    target.ivVerify = null;
    target.tvName = null;
    target.tvEmail = null;
    target.tvCompanyRegister = null;
    target.etName = null;
    target.tvCompanyAddress = null;
    target.etAddress1 = null;
    target.etAddress2 = null;
    target.etNewCity = null;
    target.etPoBox = null;
    target.etPhone = null;
    target.etMobile = null;
    target.etWeb = null;
    target.spinnerState = null;
    target.spinnerCity = null;
    target.ibChangeDp = null;
    target.btnEditProfile = null;
    target.btnSave = null;
    target.viewAddNewCity = null;
    target.viewableAddressView = null;
    target.editableAddressView = null;
    target.viewBottom_ = null;
    target.dataView = null;
    target.errorView = null;

    view7f0a01a2.setOnClickListener(null);
    view7f0a01a2 = null;
    view7f0a0083.setOnClickListener(null);
    view7f0a0083 = null;
    view7f0a0085.setOnClickListener(null);
    view7f0a0085 = null;

    super.unbind();
  }
}
