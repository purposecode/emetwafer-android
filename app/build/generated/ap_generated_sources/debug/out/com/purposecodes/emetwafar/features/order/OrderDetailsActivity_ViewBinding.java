// Generated code from Butter Knife. Do not modify!
package com.purposecodes.emetwafar.features.order;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.purposecodes.emetwafar.R;
import com.purposecodes.emetwafar.activity.BaseActivity_ViewBinding;
import com.sk.icode.errorview.ErrorView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OrderDetailsActivity_ViewBinding extends BaseActivity_ViewBinding {
  private OrderDetailsActivity target;

  private View view7f0a00ac;

  @UiThread
  public OrderDetailsActivity_ViewBinding(OrderDetailsActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public OrderDetailsActivity_ViewBinding(final OrderDetailsActivity target, View source) {
    super(target, source);

    this.target = target;

    View view;
    target.tvShippingStatus = Utils.findRequiredViewAsType(source, R.id.textView_orderDetail_shippingStatus, "field 'tvShippingStatus'", TextView.class);
    target.tvOrderNumber = Utils.findRequiredViewAsType(source, R.id.textView_orderDetail_orderNo, "field 'tvOrderNumber'", TextView.class);
    target.tvSubTotal = Utils.findRequiredViewAsType(source, R.id.textView_orderDetail_subTotal, "field 'tvSubTotal'", TextView.class);
    target.tvVatTotal = Utils.findRequiredViewAsType(source, R.id.textView_orderDetail_vatTotal, "field 'tvVatTotal'", TextView.class);
    target.tvDiscount = Utils.findRequiredViewAsType(source, R.id.textView_orderDetail_discount, "field 'tvDiscount'", TextView.class);
    target.tvVerificationAmount = Utils.findRequiredViewAsType(source, R.id.textView_orderDetail_verificationAmount, "field 'tvVerificationAmount'", TextView.class);
    target.tvWarrantyAmount = Utils.findRequiredViewAsType(source, R.id.textView_orderDetail_warrantyAmount, "field 'tvWarrantyAmount'", TextView.class);
    target.tvGrandTotal = Utils.findRequiredViewAsType(source, R.id.textView_orderDetail_grandTotal, "field 'tvGrandTotal'", TextView.class);
    target.tvPaymentType = Utils.findRequiredViewAsType(source, R.id.textView_orderDetail_paymentType, "field 'tvPaymentType'", TextView.class);
    target.tvReferenceNumber = Utils.findRequiredViewAsType(source, R.id.textView_orderDetail_referenceNumber, "field 'tvReferenceNumber'", TextView.class);
    target.btnDownload = Utils.findRequiredViewAsType(source, R.id.button_orderDetail_download, "field 'btnDownload'", Button.class);
    target.btnCancel = Utils.findRequiredViewAsType(source, R.id.button_orderDetail_cancel, "field 'btnCancel'", Button.class);
    target.errorView = Utils.findRequiredViewAsType(source, R.id.errorView, "field 'errorView'", ErrorView.class);
    target.rvProducts = Utils.findRequiredViewAsType(source, R.id.recyclerView_orderDetail_productList, "field 'rvProducts'", RecyclerView.class);
    target.rvTimeLine = Utils.findRequiredViewAsType(source, R.id.recyclerView_orderDetail_timeLine, "field 'rvTimeLine'", RecyclerView.class);
    target.dataView = Utils.findRequiredViewAsType(source, R.id.view_orderDetail, "field 'dataView'", ViewGroup.class);
    target.bottomView = Utils.findRequiredViewAsType(source, R.id.view_orderDetail_bottom, "field 'bottomView'", ViewGroup.class);
    view = Utils.findRequiredView(source, R.id.button_orderDetail_help, "method 'onButtonClick'");
    view7f0a00ac = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onButtonClick(p0);
      }
    });
  }

  @Override
  public void unbind() {
    OrderDetailsActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvShippingStatus = null;
    target.tvOrderNumber = null;
    target.tvSubTotal = null;
    target.tvVatTotal = null;
    target.tvDiscount = null;
    target.tvVerificationAmount = null;
    target.tvWarrantyAmount = null;
    target.tvGrandTotal = null;
    target.tvPaymentType = null;
    target.tvReferenceNumber = null;
    target.btnDownload = null;
    target.btnCancel = null;
    target.errorView = null;
    target.rvProducts = null;
    target.rvTimeLine = null;
    target.dataView = null;
    target.bottomView = null;

    view7f0a00ac.setOnClickListener(null);
    view7f0a00ac = null;

    super.unbind();
  }
}
