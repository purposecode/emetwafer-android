/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.purposecodes.emetwafar;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.purposecodes.emetwafar.debug";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 4;
  public static final String VERSION_NAME = "1.0.2";
}
